<form id="form_reg_purchase" action="<?= url('purchase/create') ?>" method="post" class="row"><div class="col-md-12">
    @csrf
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Nueva compra</b></h4>
                <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                
            </div>
            <div class="modal-body">
                <div class="row justify-content-center">

                    <div class="col-md-4 mb-2">
                        <label><b>Proveedor:</b></label>
                        <select name="provider_id" class="form-control">
                            @foreach ($providers as $provider)
                            <option value="<?= $provider->id ?>"><?= strtoupper($provider->legal_name) ?></option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 mb-2">
                        <label><b>Comprobante / Guia de compra:</b></label>
                        <input type="text" id="reg_nro_guide" name="voucher" class="form-control" autocomplete="off">
                    </div>
                    <div class="col-md-8 mb-2">
                        <label><b>Productos:</b></label>
                        
                        <select class="form-control" id="product_select2" style="width: 100%"></select>

                        <div style="text-align:center;margin-top:10px;"><a href="#" class="btn btn-warning" id="purchase_product_add" data-target="#purchase_product_table" data-offices="<?= $offices->count() ?>"><i class="fa fa-plus-circle"></i> Agregar</a></div>
                    </div>
                    <div class="col-md-12 mb-2 table-responsive" style="margin-top:20px;">
                        <table class="table" style="min-width:730px;" id="purchase_product_table">
                            <thead>
                                <tr>
                                    <th rowspan="2" style="vertical-align:middle;width:300px;">Producto</th>
                                    <th style="padding:3px 12px;background:#ccc;text-align:center;" rowspan="1" colspan="<?= $offices->count() ?>">Cantidades para:</th>
                                    <th rowspan="2" style="vertical-align:middle;width:100px;text-align:center;">Costo Total (S/)</th>
                                    <th rowspan="2" style="vertical-align:middle;width:80px;text-align:center;">Acción</th>
                                </tr>
                                <tr>
                                    <?php foreach ($offices as $office) { ?>
                                    <th style="padding:3px 12px;background:#eee;text-align:center;width:80px;"><?= strtoupper($office->name) ?></th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="col-md-12 mb-2" style="margin-top:20px;text-align:center;">
                        Total: <span id="purchase_total" style="font-size:30px;" class="PEN">0.00</span>
                    </div>
                    <!-- <div class="col-md-12 mb-2" style="margin-top:20px;text-align:center;">
                        <strong>Deberá registrar el gasto generado por esta compra de manera manual</strong>
                    </div> -->
                    <div class="col-md-12 mb-2" style="margin-top:20px;text-align:center;">
                        <button id="btn_reg_purchase" type="submit" class="btn btn-primary"><i class="fa fa-save"></i> GRABAR</button>
                    </div>
                    <div class="col-md-12">
                        <small>* El costo total permitirá obtener el costo unitario para hallar la utilidad</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div></form>

<script type="text/javascript">
    create_product_select2();
</script>