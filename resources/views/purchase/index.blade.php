@extends('layouts.back')

@section('head')
<div class="doc-title mb-4">
    <h2>
        <span>Compras</span>

        <?php if ($can_manage = has_permission('purchase.manage')) { ?>
        <a href="#" class="btn btn-primary link_open_popup" data-url="<?= url('purchase/create') ?>" data-target="#modal_purchase_create">Nuevo</a>
        <?php } ?>
    </h2>
</div>

@endsection

@section('body')

<!-- <select class="form-control" id="product_purchase">
                        </select> -->
                        
<div class="row justify-content-center">
    <?php show_dates_filter('purchase'); ?>
    <div class="table-responsive">
        <table class="report table table-hover" style=";text-transform:uppercase;">
            <thead><tr class="text-center">
                <th class="text-left" style="width: 150px">Comprobante</th>
                <th class="text-left">Proveedor</th>
                <th style="width:150px">Total</th>
                <th style="width:150px">Usuario</th>
                <th style="width:180px">Fecha</th>
                <th style="width:150px">Estado</th>
                <th style="width:100px">Acción</th>
            </tr></thead>
            <tbody>
                <?php foreach ($purchases ?: [] as $purchase) { ?>
                    <tr style="text-align:center;">
                    <td>{{$purchase->voucher}}</td>
                    <td style="text-align:left;">{{strtoupper(@$purchase->provider->legal_name)}}</td>
                    <td>S/ {{$purchase->total}}</td>
                    <td>{{strtoupper(@$purchase->user->fullname)}}</td>
                    <td><?= date('d/m H:i',strtotime($purchase->created_at)) ?></td>
                    <td>
                        <span class="badge badge-pill badge-<?= $purchase->status ? 'success' : 'danger' ?>"><?= $purchase->status ? 'ACTIVO' : 'ANULADO' ?></span>
                    </td>
                    <td>
                        <a href="#" class="link_open_popup" data-url="<?= url('purchase/show?id='.$purchase->id) ?>" data-target="#modal_purchase_show"><i class="fa fa-eye"></i></a>
                        <?php if ($can_manage && $purchase->is_deletable()) { ?>
                        <a href="#" class="link_open_popup" data-url="<?= url('purchase/delete?id='.$purchase->id) ?>" data-target="#modal_purchase_delete"><i class="fa fa-trash"></i></a>
                        <?php } ?>
                    </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<div id="modal_purchase_show" class="modal fade"></div>
<div id="modal_purchase_delete" class="modal fade"></div>
<div id="modal_purchase_create" class="modal fade"></div>
@endsection
