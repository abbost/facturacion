<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><b>Ver Compra</b></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <div class="row justify-content-center">
                <div class="col-md-6 mb-2">
                    <label><b>Proveedor:</b></label>
                    <input type="text" class="form-control" value="<?= @$purchase->provider->legal_name ?>" readonly>
                </div>
                <div class="col-md-6 mb-2">
                    <label><b>Comprobante / Guia de compra:</b></label>
                    <input type="text" class="form-control" value="<?= @$purchase->voucher ?>" readonly>
                </div>
                <div class="col-md-12 mb-2 table-responsive" style="margin-top:20px;">
                    <label><b>Productos:</b></label>
                    <table class="table table-striped m-b-lg-0" style="min-width:730px;">
                        <thead>
                            <tr>
                                <th rowspan="2" style="vertical-align:middle;width:300px;">Producto</th>
                                <th style="padding:3px 12px;background:#ccc;text-align:center;" rowspan="1" colspan="<?= $offices->count() ?>">Cantidades para:</th>
                                <th rowspan="2" style="vertical-align:middle;width:100px;text-align:center;">Costo</th>
                            </tr>
                            <tr>
                                <?php foreach ($offices as $i => $office) { ?>
                                <th style="padding:3px 12px;background:#eee;text-align:center;width:80px;"><?= $office->name ?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($purchase->details ?: [] as $detail) { ?><tr>
                                <td><?= @$detail->product->name ?></td>
                                <?php foreach ($offices as $office) { ?>
                                <td style="text-align:center;"><?= @$detail->input($office->id)->quantity; ?></td>
                                <?php } ?>
                                <td style="text-align:center;"><?= $detail->total ?></td>
                            </tr><?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 mb-2" style="margin-top:20px;text-align:center;">
                    Total: <span style="font-size:30px;">S/ <?= number_format($purchase->total, 2) ?></span>
                </div>
                @if(!$purchase->status)
                <div class="col-md-12 mb-2" style="margin-top:20px;">
                    <strong>MOTIVO DE ANULACIÓN: </strong> <?= $purchase->delete_reason ?>
                </div>
                @endif

                <div class="modal-footer justify-content-lg-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>
</div>
