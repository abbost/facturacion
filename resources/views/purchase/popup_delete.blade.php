<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><b>Anulación de Compra  <span class="text-success"># <?= @$purchase->voucher ?></span></b></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <form action="<?= url('purchase/delete') ?>" method="post">
                @csrf
                <div class="row justify-content-lg-center" style="margin-bottom: 15px">
                    <label>Indique el motivo de la anulacion</label>
                    <textarea class="form-control w-75" name="reason" style="height:150px;max-height:450px;resize:vertical;" required></textarea>
                </div>
                <div class="modal-footer justify-content-md-between">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                    <input class="btn btn-secondary" value="Registrar" type="submit">
                </div>
            </form>
        </div>
    </div>
</div>
