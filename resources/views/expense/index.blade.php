@extends('layouts.back')

@section('head')
<div class="doc-title mb-4">
    <h2>
        <span>Gastos</span>

        <?php if ($can_manage = has_permission('expense.manage')) { ?>
        <a href="#" class="btn btn-primary link_open_popup" data-url="<?= url('expense/create') ?>" data-target="#modal_expense_create">Nuevo</a>
        <?php } ?>
    </h2>
</div>
@endsection

@section('body')
<div class="row justify-content-center">
    <?php show_dates_filter('expense'); ?>
    <div class="table-responsive">
        <table class="report table table-hover" style="min-width:600px;text-transform:uppercase;">
            <thead><tr style="text-align:center;">
                <th style="width:60px">#</th>                
                <th style="text-align:left;">Descripción</th>
                <th style="width:150px">Monto S/</th>
                <th style="width:150px">Usuario</th>
                <th style="width:180px">Fecha</th>
                <th style="width:80px">Acción</th>
            </tr></thead>
            <tbody>
                <?php foreach ($expenses as $expense) { ?><tr style="text-align:center;">
                    <td><?= $expense->id ?></td>                    
                    <td style="text-align:left;"><?= $expense->description ?></td>
                    <td><?= $expense->amount ?></td>
                    <td><?= $expense->user->fullname ?></td>
                    <td><?= $expense->created_at ?></td>
                    <td>
                        <?php if ($can_manage) { ?>
                        <form action="<?= url("expense/delete?id={$expense->id}") ?>" method="post">
                            @csrf
                            <?php if ($expense->is_deletable()) { ?>
                            <a href="#" class="link_delete_row_data"><i class="fa fa-trash"></i></a>
                            <?php } ?>
                        </form>
                        <?php } ?>
                    </td>
                </tr><?php } ?>
            </tbody>
        </table>
    </div>
</div>
<div id="modal_expense_create" class="modal fade"></div>
@endsection
