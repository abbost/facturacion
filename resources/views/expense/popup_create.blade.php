<div class="modal-dialog">
    <div class="modal-content">
        <form action="<?= url('expense/create') ?>" method="post">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title"><b>Nuevo Gasto</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    @if(has_permission('office.multiwork'))
                    <div class="col-md-12 mb-2">
                        <label><b>Sucursal:</b></label>
                        <select name="office_id" class="form-control">
                            <?php foreach (session('business')->offices as $office) { ?>
                            <option value="<?= $office->id ?>"><?= $office->name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    @endif
                    <div class="col-md-12 mb-2">
                        <label><b>Monto (S/):</b></label>
                        <input type="text" name="amount" class="form-control" autocomplete="off" required="">
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>Descripción:</b></label>                        
                        <input type="text" name="description" class="form-control" autocomplete="off" required="">
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>GRABAR</button>
            </div>
        </form>
    </div>
</div>