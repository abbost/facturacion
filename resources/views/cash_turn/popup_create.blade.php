<form action="<?= url('cash_turn/create') ?>" method="post" class="row"><div class="col-md-12">
    @csrf
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Cierre de caja</b></h4>
                <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <label><b>Saldo actual (S/):</b></label>
                        <div class="form-control" style="font-size:26px;font-weight:bold;color:#00c;text-align:center;" readonly><?= number_format($balance, 2); ?></div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>¿Cuánto dinero va a retirar? (S/):</b></label>
                        <input type="text" name="taken" class="form-control" autocomplete="off" style="text-align:center;" data-balance="<?= $balance ?>" oninput="$('#next_initial_amount').html(parseFloat(isNaN(this.value) ? 0 : Math.round((this.getAttribute('data-balance') - this.value) * 100) / 100).toFixed(2));">
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>Saldo inicial para siguiente cierre (S/):</b></label>
                        <div id="next_initial_amount" class="form-control" style="text-align:center;" readonly><?= number_format($balance, 2); ?></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-primary link_close_popup" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-secondary">Registrar</button>
            </div>
        </div>
    </div>
</div></form>