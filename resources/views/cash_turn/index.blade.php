@extends('layouts.back')

@section('head')
<div class="doc-title mb-4">
    <h2>
        <span>Cierres de caja</span>        

        <?php if ($can_manage = has_permission('cash_turn.manage')) { ?>
        <a href="#" class="btn btn-primary link_open_popup" data-url="<?= url('cash_turn/create') ?>" data-target="#modal_cash_turn_create">Nuevo</a>
        <?php } ?>
    </h2>
    <div style="width: 100%;"><small>Se muestran los 10 últimos cierres</small></div>
</div>
@endsection

@section('body')
<div class="row">
    <div class="table-responsive">
    <table class="table table-sm table-hover" style="text-transform:uppercase;">
        <thead><tr style="text-align:center;">
            <th>Cierre</th>
            <th style="color:#0c0;">Saldo inicial</th>
            <th style="color:#00c;">Efec S/</th>
            <th style="color:#00c;">Tarj S/</th>
            <th style="color:#00c;">Tran S/</th>
            <th style="color:#c00;">Gastos S/</th>
            <th style="color:#000;font-weight:bold;">Saldo final S/</th>
            <th style="color:#000;">Retirado</th>
            <th style="color:#000;">Dejado</th>
            <th style="width:120px;">Usuario</th>
        </tr></thead>
        <tbody>
            @foreach ($cash_turns as $i => $cash_turn)
                <tr style="text-align:center;">
                <td><?= $i == $cash_turns->count() - 1 ? "<strong>ARQUEO ACTUAL</strong>" : $cash_turn->created_at ?></td>
                <td style="color:#0c0;" class="PEN"><?= round($cash_turn->amount_initial, 2) ?></td>
                <td style="color:#00c;" class="PEN"><?= round($cash_turn->amount_cash, 2) ?></td>
                <td style="color:#00c;" class="PEN"><?= round($cash_turn->amount_card, 2) ?></td>
                <td style="color:#00c;" class="PEN"><?= round($cash_turn->amount_transfer, 2) ?></td>
                <td style="color:#c00;" class="PEN"><?= round($cash_turn->amount_expense, 2) ?></td>
                <td style="color:#000;font-weight:bold;" class="PEN"><?= round($cash_turn->balance_2, 2) ?></td>
                <td style="color:#333;" class="PEN"><?= round($cash_turn->amount_taken, 2) ?></td>
                <td style="color:#333;" class="PEN"><?= round($cash_turn->amount_left, 2) ?></td>
                <td><?= @$cash_turn->user->fullname ?></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div></div>
<div id="modal_cash_turn_create" class="modal fade"></div>





@endsection
