
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">      
      <div class="modal-body">
        
        <form id="form-guide" action="#">
        @csrf
        <input type="hidden" name="document_id" value="{{$document->id}}">
        <div class="row m-b-lg-10">
            <div class="col-md-12">
                <h3>Guía de Remisión <span style="float: right;">{{$document->serie.'-'.$document->correlative}}</span></h3>
            </div>
        </div>
        

        <div class="row m-b-lg-10">                            
            <div class="col-md-4">
                <label>Fecha de Emisión (*)</label>
                <input type="date" class="form-control" name="date_" value="<?= date('Y-m-d') ?>" required>
            </div>
            <div class="col-md-4">
                <label>Punto de Partida (*)</label>
                <input type="text" name="place_from" class="form-control" required>
            </div>
            <div class="col-md-4">
                <label>Punto de llegada (*)</label>
                <input type="text" name="place_to" class="form-control" required="">
            </div>
        </div>

        <div class="row m-b-lg-10">
            <div class="col-md-6">
                <label>Apellidos y Nombres / Razón Social (*)</label>
                <input type="text" name="custom_name" class="form-control"  value="{{$document->customer_name}}" required="">
            </div>
            <div class="col-md-6">
                <label>Documento / RUC (*)</label>
                <input type="text" name="custom_doc" class="form-control" maxlength="11" value="{{$document->customer_doc}}" required="">
            </div>
        </div>
        
        <div class="row m-b-lg-5 p-t-lg-10 p-b-lg-10 gray-bg">
            <div class="col-md-2">
                <div>CANTIDAD</div>                
            </div>
            <div class="col-md-6">
                <div>DESCRIPCION (Detalle de los bienes)</div>                
            </div>
            <div class="col-md-2">
                <div>PESO</div>                
            </div>
            <div class="col-md-2">
                <div>COSTO</div>                
            </div>
        </div>    

        @foreach($document->details as $detail)
        <div class="row">
            <div class="col-md-2">                
                <input type="text" name="quantity[]" style="width: 100%" value="{{$detail->quantity}}">
            </div>
            <div class="col-md-6">                
                <input type="text" name="description[]" style="width: 100%" value="{{$detail->description}}">
            </div>
            <div class="col-md-2">                
                <input type="text" name="weight[]" style="width: 100%">
            </div>
            <div class="col-md-2">                
                <input type="text" name="cost[]" style="width: 100%">
            </div>
        </div>            
        @endforeach
            

        <div class="row m-t-lg-20">
            <div class="col-md-12">
                <div class="form-group m-b-lg-5">
                    <label class="m-r-lg-10 text-left d-inline-block" style="width: 200px">Placa</label>
                    <select class="form-control d-inline" name="car_id" style="width: 300px" >
                        @foreach($cars as $car)
                        <option value="{{ $car->id }}"> {{ strtoupper($car['plate'].' - '.$car['brand'])}} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group m-b-lg-5">
                    <label class="m-r-lg-10 text-left d-inline-block" style="width: 200px">Certificado de Inscripción</label>
                    <input type="text" name="car_certificate" class="form-control d-inline" style="width: 300px">
                </div>
                <div class="form-group m-b-lg-5">
                    <label class="m-r-lg-10 text-left d-inline-block" style="width: 200px">Licencia del Conductor</label>
                    <select class="form-control d-inline" name="driver_id" style="width: 300px">
                        @foreach($drivers as $driver)
                        <option value="{{ $driver->id }}"> {{ strtoupper($driver['name'].' - '.$driver['license']) }} </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="float: left;">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="form_guide_submit()">Crear Guía</button>
      </div>
    </form>
    </div>
  </div>
</div>