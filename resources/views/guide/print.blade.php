<html>
  <head>
      <style type="text/css">
        @media print{
            .hidden{
                visibility: hidden;
            }            
            input{
                border: none;
            }
            body{                
                padding-left: 3cm;                
            }
        }

        body{
            width: 24cm;    
            font-family: sans-serif;            
        }
        div{
            font-size: 15px
        }

        input{
            vertical-align: top;
        }

        .d-inline-block{
            display: inline-block;
        }
        .text-center{
            text-align: center;
        }
        .text-left{
            text-align: left;
        }
        .gray-bg{
            background: #ccc
        }
        .m-r-lg-10{
            margin-right: 10px;
        }
        .m-b-lg-10{
            margin-bottom: 10px
        }


      </style>
  </head>
<body>
    <div class="header" style="height: 4.8cm">        
    </div>


    <div class="data">            
        
        <div style="margin-bottom: 0.5cm">
            <div class="hidden d-inline-block" style="width: 2cm">Fecha de Emisión</div>
            <div class="d-inline-block">{{$guide->date}}</div>
        </div>                    

        <table style="width: 100%; margin-bottom: 0.5cm">
            <tr>
                <td width="50%">
                    <div class="hidden text-center gray-bg">Punto de Partida</div>
                    <div class="text-center">{{ strtoupper($guide->place_from) }}</div>
                    <div style="width: 100%">&nbsp;</div>
                </td>
                <td>
                    <div class="hidden text-center gray-bg">Punto de Llegada</div>
                    <div class="text-center">{{ strtoupper($guide->place_to) }}</div>
                    <div style="width: 100%">&nbsp;</div>
                </td>
            </tr>
        </table>


        <table style="width: 100%">
            <tr>
                <td width="50%">
                    <div class="hidden text-center gray-bg">Remitente</div>
                    <div>
                        <div class="hidden">Apellidos y Nombres / Razón Social</div>
                        <div class="text-center">{{ strtoupper($guide->custom_name) }}</div>
                    </div>
                    <div>
                        <div class="hidden d-inline-block" style="width: 2cm">RUC N°</div>
                        <div class="d-inline-block">{{$guide->custom_doc}}</div>
                    </div>
                </td>
                <td>
                    <div class="hidden text-center gray-bg">Destinatario</div>
                    <div>
                        <div class="hidden">Apellidos y Nombres / Razón Social</div>
                        <div>&nbsp;</div>
                    </div>
                    <div>
                        <div class="hidden d-inline-block" style="width: 2cm">RUC N°</div>
                        <div class="d-inline-block">&nbsp;</div>
                    </div>
                </td>
            </tr>
        </table>
    
        <div id="details" style="min-height: 4cm; margin-top: 1cm">
        <table style="width: 100%">
            <tr>
                <td><div class="hidden text-center gray-bg">CANTIDAD</div></td>
                <td><div class="hidden text-center gray-bg">DESCRIPCION (Detalle de los bienes)</div></td>
                <td><div class="hidden text-center gray-bg">PESO</div></td>
                <td><div class="hidden text-center gray-bg">COSTO DEL TRASLADO</div></td>
            </tr>
            @foreach($guide->details as $detail)
            <tr class="text-center">
                <td style="width: 2cm">
                    <div>{{$detail->quantity}}</div>
                </td>
                <td>
                    <div>{{ strtoupper($detail->description) }}</div>
                </td>
                <td style="width: 2cm">
                    <div>{{$detail->weight}}</div>
                </td>
                <td style="width: 4cm">
                    <div>{{$detail->cost}}</div>
                </td>
            </tr>
            @endforeach
        </table>
        </div>
        
            
        <table>
            <tr>
                <td><div class="hidden text-center gray-bg" style="font-size: 10px">DATOS DE IDENTI. DE LA UNID. DE TRANSP. Y DEL CONDUCTOR</div></td>
                <td><div class="hidden text-center gray-bg" style="font-size: 10px">DATOS DE LA EMPRESA SUBCONTRATADA</div></td>
                <td></td>
            </tr>
            <tr>
                <td style="width: 10cm">                            
                    <div style="margin-bottom: 10px">
                        <label class="m-r-lg-10 hidden text-left d-inline-block" style="width: 200px">Marca de Vehiculo</label>
                        <span>{{ strtoupper($guide->car_brand) }}</span>
                    </div>
                    <div style="margin-bottom: 10px">
                        <label class="m-r-lg-10 hidden text-left d-inline-block" style="width: 200px">Placa N°</label>
                        <span>{{ strtoupper($guide->car_plate) }}</span>
                    </div>
                    <div style="margin-bottom: 10px">
                        <label class="m-r-lg-10 hidden text-left d-inline-block" style="width: 200px">Certificado de Inscripción</label>
                        <span>{{ strtoupper($guide->car_certificate) }}</span>
                    </div>
                    <div style="margin-bottom: 10px">
                        <label class="m-r-lg-10 hidden text-left d-inline-block" style="width: 200px">Licencia del Conductor</label>
                        <span>{{ strtoupper($guide->driver_license) }}</span>
                    </div>
                </td>
                <td style="width: 8cm"></td>
                <td></td>
            </tr>
        </table>    
      </div>
</body>

</html>