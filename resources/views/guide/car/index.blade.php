@extends('layouts.back')

@section('head')

<div id="title">
    <h2>Vehículos
    <a href="#" class="btn btn-primary link_open_popup" data-url="<?= url('guide/car/create')?>" data-target="#modal_create">Nuevo</a>
    </h2>    
</div>

@if(session('status'))
    @if(session('error')==0)
    <div class="alert alert-dismissible alert-success row m-b-lg-10">
        <button class="close" type="button" data-dismiss="alert">×</button>
        <p><?= session('status') ?></p>
    </div>
    @else
    <div class="alert alert-dismissible alert-danger row m-b-lg-10">
        <button class="close" type="button" data-dismiss="alert">×</button>
        <p>ERROR. <?= session('status') ?></p>        
    </div>
    @endif
@endif
@endsection

@section('body')
<div class="row">
    <div class="col-md-12">
        <div class="white-bg p-b-lg-20 p-t-lg-20 table-responsive">
        <table class="report table table-hover f-16" id="list_documents">
            <thead>
                <tr class="text-left">
                    <th>ID</th>                                    
                    <th>Marca</th>
                    <th>Placa</th>
                    <th width="100">Acción</th>
                </tr>
            </thead>
            <tbody>                
                @foreach ($cars as $car)
                <tr>
                    <td>{{$car->id}}</td>
                    <td>{{ strtoupper($car->brand) }}</td>
                    <td>{{ strtoupper($car->plate) }}</td>
                    <td>
                        <form action="<?= url('guide/car/delete/'.$car->id) ?>" method="post">
                        @csrf
                        
                        <a href="#" class="link_open_popup" data-url="<?= url('guide/car/edit/'.$car->id) ?>" data-target="#modal_edit" title="Editar"><i class="fa fa-pencil"></i></a>                                                
                        <a href="#" class="link_delete_row_data" title="Eliminar"><i class="fa fa-trash"></i></a>
                        
                    </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>        
        </div>
    </div>
</div>


<div id="modal_create" class="modal fade"></div>
<div id="modal_edit" class="modal fade"></div>

@endsection


