<div class="modal-dialog">
    <div class="modal-content">
        <form action="<?= url('guide/car/update') ?>" method="post">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title"><b>Editar Vehículo</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" name="car_id" value="{{$car->id}}">
                    <div class="col-md-12">
                        <label><b>Placa<i class="fa fa-lock" aria-hidden="true" style="padding: 0 0 0 5px; font-size:20px"></i></b></label>
                        <input type="text" name="plate" class="form-control" autocomplete="off" value="{{$car->plate}}">
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <label><b>Marca<i class="fa fa-lock" aria-hidden="true" style="padding: 0 0 0 5px; font-size:20px"></i></b></label>
                        <input type="text" name="brand" class="form-control" value="{{$car->brand}}">
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary  ">Actualizar</button>
            </div>
        </form>
    </div>
</div>