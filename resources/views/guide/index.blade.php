@extends('layouts.back')

@section('head')

<div id="title">
    <h2>Reporte de Guías</h2>    
</div>

<div class="row justify-content-lg-center">
    <div class="col-md-12" style="margin-bottom:20px;text-align:center;">
        <form class="fom-dates" >
            <div class="mr-3">
                Desde: <input type="date" id="search_date_from" name="date_from" class="form-control" style="display:inline-block;width:auto;height:37px;" value="<?= $date_from ?>">
            </div>
            <div class="mr-3">
                Hasta: <input type="date" id="search_date_to" name="date_to" class="form-control" style="display:inline-block;width:auto;" value="<?= $date_to ?>">
            </div>
            <div class="mr-3">
                <button id="btn_search" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
            </div>
        </form>            
    </div>
</div>


@if(session('status'))
    @if(session('error')==0)
    <div class="alert alert-dismissible alert-success row m-b-lg-10">
        <button class="close" type="button" data-dismiss="alert">×</button>
        <p><?= session('status') ?></p>
    </div>
    @else
    <div class="alert alert-dismissible alert-danger row m-b-lg-10">
        <button class="close" type="button" data-dismiss="alert">×</button>
        <p>ERROR. <?= session('status') ?></p>        
    </div>
    @endif
@endif
@endsection

@section('body')
<div class="row">
    <div class="col-md-12">
        <div class="white-bg p-b-lg-20 p-t-lg-20 table-responsive">
        <table class="report table table-hover f-16" id="list_documents">
            <thead>
                <tr class="text-left">
                    <th width="">Id</th>
                    <th>Documeto</th>                
                    <th width="">Fecha</th>
                    <th width="">Desde</th>
                    <th width="">Hasta</th>
                    <th width="">Nombre|Razón Social</th>
                    <th width="">DNI|RUC</th>
                    <th>Marca</th>
                    <th>Placa</th>
                    <th>Certificado</th>
                    <th>Licencia</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>                
                @foreach ($guides as $guide)
                <tr style="text-transform: uppercase;">
                    <td><strong><?= $guide->id ?></strong></td>    
                    <td>{{ $guide->document->serie.'-'.$guide->document->correlative}}</td>
                    <td><?= date('d/m',strtotime($guide->date)) ?></td>
                    <td>{{ $guide->place_from }}</td>
                    <td>{{ $guide->place_to }}</td>
                    <td>{{ $guide->custom_name }}</td>
                    <td>{{ $guide->custom_doc }}</td>
                    <td>{{ $guide->car_brand }}</td>
                    <td>{{ $guide->car_plate }}</td>
                    <td>{{ $guide->car_certificate }}</td>
                    <td>{{ $guide->driver_license }}</td>
                    <td><a href="#" onclick="print_copy_guide( '{{$guide->id}}' )"><i class="fa fa-print"></i></a></td>        
                </tr>
                @endforeach
            </tbody>
        </table>        
        </div>
    </div>
</div>

@endsection
