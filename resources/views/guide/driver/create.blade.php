<div class="modal-dialog">
    <div class="modal-content">
        <form action="<?= url('guide/driver/create') ?>" method="post">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title"><b>Nuevo Conductor</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label><b>Nombre<i class="fa fa-lock" aria-hidden="true" style="padding: 0 0 0 5px; font-size:20px"></i></b></label>
                        <input type="text" name="name" class="form-control" autocomplete="off">
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <label><b>Licencia<i class="fa fa-lock" aria-hidden="true" style="padding: 0 0 0 5px; font-size:20px"></i></b></label>
                        <input type="text" name="license" class="form-control" step="0.1">
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary  ">Agregar</button>
            </div>
        </form>
    </div>
</div>