@extends('layouts.front')

@section('content')

<section>
    <div class="container-login">            
        <div class="title-login m-t-lg-30 m-b-lg-20">INICIAR SESIÓN</div>
        
        @if (session('errors'))        
          <div class="alert alert-dismissible alert-danger" style="font-size: 14px; margin-bottom: 0">
            <button class="close" type="button" data-dismiss="alert">×</button>
            <p>Hubo un error en el usuario y/o clave</p>
          </div>            
        @endif
        
        <form action="<?= url('login') ?>" method="post" >
            @csrf
            <div class="email">
                <span class="fa fa-user" aria-hidden="true" style="font-size:40px; padding: 0 10px 0 0; color: gray"></span><div class="divsito">
                <input type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required="true" placeholder="Tu usuario" autofocus>
                @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert" style="font-size:14px;color:red;">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            </div>
            <div class="password">
                <span class="fa fa-lock" aria-hidden="true" style="font-size:40px; padding: 0 10px 0 0; color: gray"></span><div class="divsito">
                <input type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required="true" placeholder="Tu contraseña">
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            </div>
            <?php if (session('times') >= 3) { ?>
            <div class="container-captcha">
                <div style="text-align:center;margin-bottom:10px;">
                    <img src="{{ url('captcha') }}" alt="" id="captcha" style="margin:auto;">
                </div>
                <div id="captchaWrapper">
                    <input type="text" name="captcha" style="width:calc(100% - 33px);" required>
                    <div>
                        <a href="#" onclick="event.preventDefault();$('#captcha').attr('src','{{ url('captcha') }}'+'?'+Math.round(Math.random()*10000));">&#10227;</a>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="">
                <button class="btn-into" type="submit" style="width: 150px">INGRESAR</button>
            </div>
        </form> 
    </div>
</div>

@endsection


