@extends('layouts.back')

@section('head')
<div class="doc-title mb-4">
    <h2>
        <span>Sucursales</span>

        <?php if ($can_manage = has_permission('office.manage')) { ?>
        <a href="#" class="btn btn-primary link_open_popup" data-url="<?= url('office/create') ?>" data-target="#modal_office_create">Nuevo</a>
        <?php } ?>
    </h2>
</div>
@endsection

@section('body')
<div class="row"><div class="table-responsive">
    <table class="report table table-hover" style="min-width:600px;text-transform:uppercase;">
        <thead><tr>
            <th class="text-center">Código</th>
            <th>Nombre</th>
            <th style="width:300px;">Dirección</th>
            <th style="width:80px">FACTURAS</th>
            <th style="width:80px">BOLETAS</th>
            <th style="width:80px">PROFORMAS</th>
            <th class="text-center">Usuarios</th>
            <th class="text-center" style="width:80px">Acción</th>
        </tr></thead>
        <tbody>
            <?php foreach ($offices as $i => $office) { ?><tr style="text-align:center;">
                <td class="text-center"><?= $office->code ?></td>
                <td style="text-align:left;"><?= strtoupper($office->name) ?></td>
                <td style="text-align:left;"><?= strtolower($office->address) ?></td>
                <td><?= $office->current_serie_factura ?></td>
                <td><?= $office->current_serie_boleta ?></td>
                <td><?= $office->current_serie_proforma ?></td>
                <td><?= $office->users->where('status', 1)->count() ?></td>
                <td>
                    <?php if ($can_manage) { ?>
                    <form action="<?= url("office/delete?id={$office->id}") ?>" method="post">
                        @csrf
                        <a href="#" class="link_open_popup" data-url="<?= url("office/edit?id={$office->id}") ?>" data-target="#modal_office_edit"><i class="fas fa-pencil-alt"></i></a>
                        <?php if ($office->is_deletable()) { ?>
                        <a href="#" class="link_delete_row_data"><i class="fa fa-trash"></i></a>
                        <?php } ?>
                    </form>
                    <?php } ?>
                </td>
            </tr><?php } ?>
        </tbody>
    </table>
</div></div>
<div id="modal_office_create" class="modal fade"></div>
<div id="modal_office_edit" class="modal fade"></div>
@endsection
