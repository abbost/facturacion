<form action="<?= url('office/edit') ?>" method="post" class="row"><div class="col-md-12">
    @csrf
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Editar Sucursal</b></h4>
                <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <label><b>Código</b></label>
                        <input type="text" name="code" class="form-control" required="required" maxlength="4" value="<?= $office->code ?>">
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>Nombre </b></label>
                        <input type="text" name="name" class="form-control" value="<?= $office->name ?>">
                    </div>
                    <div class="col-md-12 mb-2 mt-3">
                        <h5>Series para</h5>
                    </div>
                    <div class="col-md-4 mb-2">
                        <label><b>Factura <i class="fa fa-lock"></i></b></label>
                        <input type="text" name="serie_factura" class="form-control" value="<?= $office->current_serie_factura ?>" maxlength="4">
                    </div>
                    <div class="col-md-4 mb-2">
                        <label><b>Boleta <i class="fa fa-lock"></i></b></label>
                        <input type="text" name="serie_boleta" class="form-control" value="<?= $office->current_serie_boleta ?>" maxlength="4">
                    </div>
                    <div class="col-md-4 mb-2">
                        <label><b>Proforma <i class="fa fa-lock"></i></b></label>
                        <input type="text" name="serie_proforma" class="form-control" value="<?= $office->current_serie_proforma ?>" maxlength="4">
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>Dirección</b></label>
                        <input type="text" name="address" class="form-control" value="<?= $office->address ?>">
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> GRABAR</button>
            </div>
        </div>
    </div>
</div></form>