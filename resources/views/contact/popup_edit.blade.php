<form id="contact_form" action="<?= url('contact/edit') ?>" method="post" class="row"><div class="col-md-12">
    @csrf
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Editar contacto</b></h4>
                <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mb-2" id="sec_business_wrapper">                        
                        <label><b>Nombre:</b></label>
                        <input type="text" id="contact_name" name="name" class="form-control mb-2" value="{{$contact->name}}">

                        <label><b>Dirección:</b></label>
                        <input type="text" id="contact_address" name="address" class="form-control mb-2" value="{{$contact->address}}">
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary link_close_popup" data-dismiss="modal">Cancelar</button>
                <button type="button" id="edit-contact-form" class="btn btn-primary">Actualizar</button>
            </div>
        </div>
    </div>
</div></form>