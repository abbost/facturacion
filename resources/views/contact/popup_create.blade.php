<form id="contact_form" action="<?= url('contact/create') ?>" method="post" class="row"><div class="col-md-12">
    @csrf
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Nuevo contacto</b></h4>
                <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mb-2" id="sec_business_wrapper">
                        <input type="hidden" id="customer_id_modal" name="customer_id">

                        <label><b>Nombre (*):</b></label>
                        <input type="text" id="contact_name" name="name" class="form-control mb-2" required="">

                        <label><b>Dirección:</b></label>
                        <input type="text" id="contact_address" name="address" class="form-control mb-2">                        
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary link_close_popup" data-dismiss="modal">Cancelar</button>
                <button type="button" id="create-contact-form" class="btn btn-primary">Registrar</button>
            </div>
        </div>
    </div>
</div></form>