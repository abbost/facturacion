@extends('layouts.back')

@section('content')

<div class="container bg-light w-100">
    <div class="text-center p-xs-10" style="font-size:50px; padding: 50px 100px">No tienes permiso de realizar dicha actividad</div>
    <div class="text-center"><img src="{{ asset('img/no_permiso.png')}}" alt="" class="img-fluid" ></div>
</div>

@endsection