@foreach ($documents as $document)
<tr>
<?php
    $environment = @[
        'prod' => ['name'=>'REAL', 'color'=>'#000000'],
        'dev' => ['name'=>'PRUEBA', 'color'=>'#000000']
    ][$document->env];

    $status = @[
        0 => ['name'=>'ACEPTADO', 'badge'=>'badge-success'],
        98 => ['name'=>'PENDIENTE', 'badge'=>'badge-info'],
    ][@$document->cdr->code];
?>
    <td>                
        <strong><?= $document->serie.'-'.$document->correlative ?></strong>
    </td>
    <td>
        {{$document->cdr->ticket}}
    </td>
    <td>
        @foreach($document->resume_details as $detail)
            <span>{{ $detail->serie.'-'.$detail->correlative }}</span>,&nbsp;
        @endforeach
    </td>
    <td><?= @['', 'ENVÍO', 'ANUL.'][$document->operation_type] ?></td>
    <td><?= date('d/m/y',strtotime($document->date)) ?></td>
    <td><?= date('d/m/y',strtotime($document->created_at)) ?></td>
    <td>
        <div style="text-align:center;">
            <span class="badge badge-pill <?= $status['badge'] ?>">
                @if( @$document->cdr->code == 98)
                <i class="fa fa-refresh"></i>
                <a href="#" class="link_resend_document" style="color:#fff;">PENDIENTE</a>
                <form action="<?= url('summary/resend') ?>" method="post">
                    @csrf
                    <input type="hidden" name="document_id" value="<?= $document->id ?>">
                </form>
                @else 
                    {{$status['name']}}
                @endif
            </span>
        </div>
    </td>
    <td><div style="text-align:center;font-size:14px;position:relative;">
        <a href="#" style="font-size:12px;" onclick="event.preventDefault();var s=this.nextSibling.style;s.display=s.display=='none'?'block':'none';">OPCIONES <i class="fa fa-chevron-down" style="transform:translateY(-1px);"></i></a><ul style="display:none;" class="options">
            <li>
                <i class="fa fa-file-code-o" style="color:#777;"></i>
                <a href="<?= $url.$document->encrypted_id.'&export_type=xml' ?>" download="<?= $document->customer_doc.'-'.$document->serie.'-'.$document->correlative.'_XML.xml' ?>">Descargar XML</a>
            </li>
            <?php if (@$document->cdr->answer != '') { ?>
            <li>
                <i class="fa fa-file-code-o" style="color:#777;"></i>
                <a href="<?= $url.$document->encrypted_id.'&export_type=cdr' ?>" download="<?= $document->customer_doc.'-'.$document->serie.'-'.$document->correlative.'_CDR.xml' ?>" >Descargar CDR</a>
            </li>
            <?php } ?>
        </ul>
    </div></td>
</tr>
@endforeach
