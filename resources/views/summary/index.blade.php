@extends('layouts.back')

@section('head')

<div id="title">
    <h2>Reporte de Resúmenes diarios</h2>    
</div>

<div class="row justify-content-lg-center">
    <div class="col-md-12" style="margin-bottom:20px;text-align:center;">
        <form action="<?= url('summary') ?>" class="fom-dates" >
            <div class="mr-3">
                Desde: <input type="date" id="search_date_from" name="date_from" class="form-control" style="display:inline-block;width:auto;height:37px;" value="<?= $date_from ?>">
            </div>
            <div class="mr-3">
                Hasta: <input type="date" id="search_date_to" name="date_to" class="form-control" style="display:inline-block;width:auto;" value="<?= $date_to ?>">
            </div>
            <div class="mr-3">
                <button id="btn_search" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
            </div>
        </form>
                
        <!-- <div style="display: inline-block;">            
            <form action="<?= url('summary/create')?>">
            @csrf            
            <button type="submit" id="btn_crear_resumen" class="btn btn-danger"><i class="fa fa-search"></i> Crear</button>
            </form>            
        </div>                     -->
    </div>
</div>


@if(session('status'))
    @if(session('error')==0)
    <div class="alert alert-dismissible alert-success row m-b-lg-10">
        <button class="close" type="button" data-dismiss="alert">×</button>
        <p><?= session('status') ?></p>
    </div>
    @else
    <div class="alert alert-dismissible alert-danger row m-b-lg-10">
        <button class="close" type="button" data-dismiss="alert">×</button>
        <p>ERROR. <?= session('status') ?></p>        
    </div>
    @endif
@endif
@endsection


@section('body')
<div class="row">
    <div class="col-md-12">
        <div class="white-bg p-b-lg-20 p-t-lg-20 table-responsive">
        <table class="report table table-sm table-hover" id="list_documents">
            <thead>
                <tr class="text-left">
                <th width="100">Número</th>
                <th>Ticket</th>
                <th >Boletas</th>
                <th width="100">Modo</th>
                <th width="140">Fecha Resumen</th>
                <th width="140">Fecha Envío</th>
                <th width="80">Estado</th>
                <th width="100"></th>
            </tr></thead>
            <tbody>@include('summary.list')</tbody>
        </table>
        <?php if (false) { ?><div class="col-md-4 offset-md-4 text-center">
            <button class="btn btn-info" id="btn_load_more_documents">Ver anteriores</button>
        </div><?php } ?>
        </div>
    </div>
</div>

@endsection
