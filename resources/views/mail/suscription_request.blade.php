<html>
<body>

<div style="font-family:Roboto,RobotoDraft,Helvetica,Arial,sans-serif">
	<div style="text-align: center;">
		<div style="font-size: 12px">Somos proveedor autorizado por:</div>
		<img src="http://dircetur.junin.gob.pe/img/carousel/logo3.png" width="200" style="margin-top: -20px">
	</div>
	<div style="text-align: center; color: #00A2B8; font-size: 36px; margin-bottom: 10px"><b>FACTURACION ELECTRONICA</b></div>
</div>

<div style="width: 650px; margin: 0 auto; font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif">

	<div style="text-align: center; color: black; font-size: 26px; margin-bottom: 20px; font-style: italic; background: yellow">Ponte al día con SUNAT de manera fácil !</div>

	<div style="text-align: center; font-size: 20px; margin-bottom: 30px">
		El sistema de facturación ideal para tu negocio. Podrás generar tus comprobantes electrónicos fácilmente en cualquier momento y lugar. Aqui encontrarás una empresa sería, peruana y con atención constante.
	</div>

	<div style="text-align: center; margin-bottom: 20px">
		<img src="https://facturacionelectronicapyme.com/img/home/comedor_quma_ripley.jpeg" style="width:500px; " alt="foto_facturador">
	</div>

	<div style="font-size: 24px; text-align: center; margin-bottom: 10px"><b>Las empresas prefieren nuestro sistema porque:</b></div>
	<div style="font-size: 18px; overflow: auto; margin-bottom: 30px">

		<div style="width: 50%;float: left; margin-bottom: 5px">
			<div style="padding: 0 5px; text-align: center;">
				<div style="border: #ccc solid 1px; border-radius: 10px; background: #eee">
					<div style="padding-top: 10px">
						<img src="https://facturacionelectronicapyme.com/img/home/thumbsup_fa.png" style="width: 40px">
					</div>
					<div style="padding: 0; color: #00A2B8"><b>Fácil</b></div>

					<div style="min-height: 90px;">
						Un sistema muy rápido de aprender. Tan fácil que lo podrá usar desde el primer dia.
					</div>
				</div>
			</div>
		</div>
		
		<div style="width: 50%; float: left; margin-bottom: 5px">
			<div style="padding: 0 5px; text-align: center;">
				<div style="border: #ccc solid 1px; border-radius: 5px">
					<div style="padding-top: 10px">
						<img src="https://facturacionelectronicapyme.com/img/home/lock_fa.png" style="width: 40px">
					</div>					
					<div style="color: #00A2B8"><b>Seguro</b></div>
					<div style="min-height: 90px">
						Tenemos los estandares de SUNAT:<br> Formato UBL 2.1, <br>Código QR y de productos <br> Oficina física en Perú.
					</div>
				</div>
			</div>
		</div>		
		<div style="width: 50%;float: left; margin-bottom: 5px">
			<div style="padding: 0 5px; text-align: center; ">
				<div style="border: #ccc solid 1px; border-radius: 5px; background: #eee">
					<div style="padding-top: 10px">
						<img src="https://facturacionelectronicapyme.com/img/home/mobile_fa.png" style="width: 40px">
					</div>
					<div style="color: #00A2B8"><b>Portatil</b></div>
					<div style="min-height: 90px">
						Lleve su sistema a todas partes y no se pierda ninguna venta. Puede incluso usarlo desde el celular.
					</div>
				</div>
			</div>
		</div>
		<div style="width: 50%; float: left; margin-bottom: 5px">
			<div style="padding: 0 5px; text-align: center; ">
				<div style="border: #ccc solid 1px; border-radius: 5px">
					<div style="padding-top: 10px">
						<img src="https://facturacionelectronicapyme.com/img/home/money_fa.png" style="width: 40px">
					</div>
				<div style="color: #00A2B8"><b>Económico</b></div>
				<div style="min-height: 90px">
					Le damos el sistema de facturación <br><b>más</b> el sistema de ventas <br><b>más</b> soporte técnico permanente..
				</div>
				</div>
			</div>
		</div>
		<div style="clear: both; margin-top: 30px"></div>
	</div>


	<div style="font-size: 24px; text-align: center; margin-bottom: 10px"><b>Comparación con el facturador SUNAT</b></div>
	
	<table border="1" cellpadding="5" style="border-collapse: collapse; width: 100%; margin-bottom: 20px">
		<thead>
			<tr>
				<th></th>
				<th>Facturador PYME</th>
				<th>Facturador SUNAT</th>
			</tr>
		</thead>
		<tbody>
			<tr style="text-align: center;">
				<td>Incluye logo en el documento</td>
				<td>Si</td>
				<td>No</td>
			</tr>
			<tr style="text-align: center;">
				<td>Tiempo para emitir comprobante</td>
				<td>1 minuto</td>
				<td>5 minutos</td>
			</tr>
			<tr style="text-align: center;">
				<td>Capacitación del sistema</td>
				<td>Si</td>
				<td>No</td>
			</tr>			
			<tr style="text-align: center;">
				<td>Soporte técnico permanente</td>
				<td>Si</td>
				<td>No</td>
			</tr>	
			<tr style="text-align: center;">
				<td>Impresión formato ticket y A4</td>
				<td>Si</td>
				<td>No</td>
			</tr>	
		</tbody>
	</table>

	<div style="text-align: center;">
		<img src="https://facturacionelectronicapyme.com/img/print_a4.png" style="border:#eee solid 1px; margin: 0 auto; text-align: center;">	
	</div>
	



	<div style="margin-bottom: 30px">
		<div style="margin-bottom:10px; font-size: 28px; background: yellow; text-align: center; margin-bottom: 20px">Felicitaciones! ganaste un demo por 7 días</div>

		<table cellpadding="0" cellspacing="0">
			<tr>
				<td width="150">RUC</td>
				<td>{{$data['ruc']}}</td>
			</tr>
			<tr>	
				<td>Negocio</td>
				<td>{{$data['business_name']}}</td>
			</tr>
			<tr>	
				<td>Solicitante</td>
				<td>{{$data['representative']}}</td>
			</tr>			
			<tr>	
				<td>Teléfono</td>
				<td>{{$data['phone']}}</td>
			</tr>
			<tr>	
				<td>Email</td>
				<td>{{$data['email']}}</td>
			</tr>			
		</table>


		<div style="margin-bottom: 10px">Solo ingresa a la siguiente dirección y conoce las bondades de nuestro facturador. </div>
		<div  style="margin-bottom: 20px">
		<div style="font-size: 20px"><b>Link:</b> <a href="https://facturacionelectronicapyme.com/login" title="login facturacion pyme" target="_blank">https://facturacionelectronicapyme.com</a></div>
		<div style="font-size: 20px"><b>Usuario:</b> demo@gmail.com</div>
		<div style="font-size: 20px"><b>Password:</b> demo</div>
		</div>

		<a href="https://api.whatsapp.com/send?phone=51987527524&text=Hola%20deseo%20informacion%20de%20la%20facturacion%20electronica&source=&data=" target="_blank" style="padding: 10px 5px; background:#4db83c; display: block; width: 350px; border-radius: 10px; text-decoration: none; text-align: center; color: #fff; margin:0 auto; font-size: 25px; -webkit-box-shadow: 3px 3px 17px 0px rgba(0,0,0,0.75);-moz-box-shadow: 3px 3px 17px 0px rgba(0,0,0,0.75);box-shadow: 3px 3px 17px 0px rgba(0,0,0,0.75);">				
				<img src="https://facturacionelectronicapyme.com/img/home/whatsapp_fa.png" target="_blank" width="30"><br>
				<b style="vertical-align: top;">Llámanos o escribenos<br> 987-527-524</b>
		</a>
	</div>	


	<div style="font-size: 20px; text-align: center;"><b>¿No sabe qué ventajas tiene la facturación electrónica?</b></div>
	<ul>
		<li>Ahorre tiempo en la entrega de comprobantes</li>
		<li>Evite la emisión de papel y la tala de árboles</li>
		<li>Construya un historial tributario para solicitar préstamos en bancos</li>
		<li>Y sobretodo evite problemas legales con SUNAT</li>
	</ul>
	<div style="margin-bottom: 30px">
		<a href="https://facturacionelectronicapyme.com" target="_blank" style="padding: 10px 5px; background:#00A2B8; display: block; width: 250px; border-radius: 10px; text-decoration: none; text-align: center; color: #fff; margin:0 auto; font-size: 25px; -webkit-box-shadow: 3px 3px 17px 0px rgba(0,0,0,0.75);-moz-box-shadow: 3px 3px 17px 0px rgba(0,0,0,0.75);box-shadow: 3px 3px 17px 0px rgba(0,0,0,0.75);"><b>Más información</b></a>
	</div>

	<div style="text-align: center; color: black; font-size: 26px; margin-bottom: 10px; font-style: italic;">No esperes al último momento para hacerlo !!!</div>
	

	<div style="text-align: center; margin-bottom: 20px">
		<div>Autorizado por</div>
		<a href="http://www.abbost.com" target="_blank">
			<img src="http://dircetur.junin.gob.pe/img/carousel/logo3.png" alt="logo_abbost" style="width:200px; ">
		</a>
	</div>

	<div style="text-align: center; margin-bottom: 20px">
		<div>Desarrollado por</div>
		<a href="http://www.abbost.com" target="_blank">
			<img src="https://facturacionelectronicapyme.com/img/partners/abbost.png" alt="logo_abbost" style="width:200px; ">
		</a>
	</div>
	

</div>
</body>
</html>