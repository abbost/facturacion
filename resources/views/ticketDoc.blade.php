<html>
<style type="text/css">
    /*@page{        
        margin-top: 0.3em;
        margin-left: 0.6em;              
    }*/
    *{margin:0px;padding:0px;font-family: monospace;}    
    body{
        padding-left: 0.5cm;
        padding-right: 0.5cm
    }
    p{text-align:center;}
    hr{margin:2px auto;border-color:#aaa;}
    /* body{line-height: 1;} */
    /* #details th, #details td{padding:2px;} */
    #details thead{border-bottom:1px solid #ccc;}
    #details tr+tr{border-top:1px solid #ddd;}
</style>
<body>        
    @if($business->image)
    <div style="text-align:center;margin:10px 0px;">
        <img src="<?= asset("/img/".$business->image) ?>" alt="logo" style="width:100px;">
    </div>
    @endif
    <div style="font-size:12px; margin-top:10px">
        <p><?= $business->legal_name ?></p>
        <p>R.U.C.: <?= $business->ruc ?></p>        
        <p style="font-size:10px; "><?= strtoupper($document->office->address) ?></p>
    </div>
    <hr >
    <div>
        <p style="font-size:12px;"><strong><?= $document_name ?></strong></p>
        <p style="font-size:12px;"><?= $document->serie." - ".$document->correlative ?></p>
        <div style="margin-top:5px;font-size:10px;">
            <p>Fecha de emisión: <?= date('d/m/Y H:i', strtotime($document->created_at)) ?></p>
            <p>Señor (es): <?= $document->customer_name ?></p>
            <p><?= @[1=>"DNI", 4=>"CEX", 6=>"RUC", 7=>"PAS"][$document->customer_doc_type] ?> Nº: <?= $document->customer_doc ?></p>
            <p>Direc: <?= $document->customer_address ?></p>
            <?php if ($document->order_reference != "") { ?><p>Orden de compra: <?= $document->order_reference ?></p><?php } ?>
            <?php if ($document->note != "") { ?><p><b>Observaciones:</b></p> <p><?= nl2br($document->note) ?></p><?php } ?>
        </div>
    </div>
    <hr>
    <table id="details" style="font-size:10px; width: 100%;">
        <tr>            
            <th style="">Descripción</th>
            <th style="width: 1.5cm; text-align:right">V.Venta</th>
            @if($paper_size != 'b9')            
            <th style="width: 1.5cm; text-align:right">Precio</th>
            @endif
        </tr>
        <tbody>
            @foreach ($details as $detail)
            <tr>            
                <td style=""><?= intval($detail->quantity) ?> <?= strtoupper($detail->description) ?></td>
                <td style="text-align:right;"><?= $detail->sub_total ?></td>   
                @if($paper_size != 'b9')            
                <td style="text-align:right;"><?= $detail->total ?></td>   
                @endif         
            </tr>
            @endforeach
        </tbody>
    </table>
    <hr>
    <table style="width:100%;margin:auto;font-size:11px;">
        <tr>
            <td><strong>OP. GRAVADA:</strong></td>
            <td style="text-align:right;">S/ <?= number_format($document->sub_total,2) ?></td>
        </tr>
        <tr>
            <td><strong>IGV (18.00%):</strong></td>
            <td style="text-align:right;">S/ <?= number_format($document->tax,2) ?></td>
        </tr>
        <tr>
            <td><strong>TOTAL:</strong></td>
            <td style="text-align:right;">S/ <?= number_format($document->total,2) ?></td>
        </tr>
    </table>
    <hr>
    <table style="width:100%;margin:auto;font-size:11px;">
        <?php if ($cash =  @collect($document->payments)->firstWhere('means', 'cash')->amount) { ?>
        <tr>
            <td><strong>EFECTIVO:</strong></td>
            <td style="text-align:right;">S/ <?= number_format($cash, 2) ?></td>
        </tr>
        <?php } ?>
        <?php if ($card =  @collect($document->payments)->firstWhere('means', 'card')->amount) { ?>
        <tr>
            <td><strong>TARJETA:</strong></td>
            <td style="text-align:right;">S/ <?= number_format($card, 2) ?></td>
        </tr>
        <?php } ?>
        <?php if ($transfer =  @collect($document->payments)->firstWhere('means', 'transfer')->amount) { ?>
        <tr>
            <td><strong>TRANSFERENCIA:</strong></td>
            <td style="text-align:right;">S/ <?= number_format($transfer, 2) ?></td>
        </tr>
        <?php } ?>
    </table>
    <hr>
    <p style="font-size:11px;"><?= strtoupper($total_text) ?> SOLES</p>
    <div style="text-align:center; ">
        <img id="qr_code" src="<?= $url_qr ?>" alt="Código qr del documento" width="150">
    </div>
    <div style="font-size:11px; margin-top: -1cm;" >
        <p>Representación impresa de la</p>
        <p><?= $document_name ?>.</p>
        <p style="margin-top:5px;">Para consultar el documento visita</p>
        <p>www.facturacionelectronicapyme.com</p>
    </div>
</body>
<script type="text/javascript">var w = self;document.getElementById("qr_code").onload = function () {window.print();w.close();}</script>
</html>
