@extends('layouts.back')

@section('head')
<div class="doc-title mb-4">
    <h2>
        <span>Salidas de Productos</span>

        <?php if ($can_manage = has_permission('output.manage')) { ?>
        <a href="#" class="btn btn-primary link_open_popup" data-url="<?= url('output/create') ?>" data-target="#modal_output_create">Nuevo</a>
        <?php } ?>
    </h2>
</div>
<div class="mb-4" style="text-align:center;">
    <?php show_dates_filter('output'); ?>
</div>
@endsection

@section('body')
<div class="row"><div class="table-responsive">
    <table class="report table table-hover" style="min-width:600px;text-transform:uppercase;">
        <thead><tr class="text-center">
            <th style="text-align:left;">Producto</th>
            <th style="width:80px">Cantidad</th>
            <th style="width:150px">Valor S/</th>
            <th style="width:200px">Tipo de salida</th>            
            <th>Comentarios</th>
            <th style="width:180px">Fecha</th>
            <th width="120">Acción</th>
        </tr></thead>
        <tbody>
            <?php foreach ($outputs as $output) { ?><tr style="text-align:center;">
                <td style="text-align:left;"><?= $output->product->name ?></td>
                <td><?= $output->quantity ?></td>
                <td class="PEN"><?= $output->cost ?></td>
                <td>
                    <?php $sale = @$output->sale_detail->sale; ?>
                    {{ !$sale ? 'Registro manual' : ('Venta ' . $sale->serie . '-' . $sale->correlative) }}
                </td>
                <td>{{$output->comments}}</td>
                <td><?= date('d/m H:i',strtotime($output->created_at)) ?></td>
                <td style="text-align:center;">
                    <?php if ($can_manage) { ?>
                    <form action="<?= url("output/delete?id={$output->id}") ?>" method="post">
                        @csrf
                        <?php if ($output->is_deletable()) { ?>
                        <a href="#" class="link_delete_row_data"><i class="fa fa-trash"></i></a>
                        <?php } ?>                        
                        <a href="" class="print" onclick="{{ 'print_popup("'.url("output/print/".$output->id).'")'}}"><i class="fa fa-print"></i></a>
                    </form>
                    
                    <?php } ?>
                </td>
            </tr><?php } ?>
        </tbody>
    </table>
</div></div>
<div id="modal_output_create" class="modal fade"></div>
@endsection
