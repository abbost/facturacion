<form action="<?= url('output/create') ?>" method="post" class="row"><div class="col-md-12">
    @csrf
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Nuevo consumo</b></h4>
                <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <label><b>Producto:</b></label>
                        <select class="form-control" id="product_select2" name="product_id" style="width: 100%"></select>                        
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>Cantidad:</b></label>
                        <input type="text" name="quantity" class="form-control" autocomplete="off" required="true">
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>Comentarios:</b></label>
                        <textarea class="form-control" name="comments" rows="3"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-primary link_close_popup" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-secondary"><i class="fa fa-save"></i> GRABAR</button>
            </div>
        </div>
    </div>
</div></form>

<script type="text/javascript">
    create_product_select2();
</script>