
<style type="text/css">
    @page{width: 14cm !important;margin-top: -2cm !important;margin-left:-20px;margin-right:-20px;margin-bottom:0px;}
    *{margin:0px;padding:0px;}
    html,body{padding:1cm 10px 20px 10px;}
    @media print{body{margin-top:1cm;}}
   p{text-align:center;}
    hr{margin:2px auto;border-color:#aaa;}
    #details th, #details td{padding:2px;}
    #details thead{border-bottom:1px solid #ccc;}
    #details tr+tr{border-top:1px solid #ddd;}
</style>
<body>
    <!-- <div style="margin-bottom:20px;">.</div> -->
    <div style="">&nbsp;</div>
    <div style="font-size:12px; margin-top:30px">
        <p><?= $business->comercial_name ?></p>
        <p>R.U.C.: <?= $business->ruc ?></p>        
    </div>
    <hr style="width:80%;">
    <div>
        <p style="font-size:12px;"><strong>SALIDA DE PRODUCTOS</strong></p>
        <p style="font-size:12px;">#<?= $output->id ?></p>
        <div style="margin-top:5px;font-size:10px;">
            <p>Fecha de emisión: <?= date('d/m/Y H:i', strtotime($output->created_at)) ?></p>
            <p>Comentarios</p>
            <p>{{ $output->comments }}</p>
        </div>
    </div>
    <hr>
    <table id="details" style="font-size:12px;margin:auto;">
        <thead><tr>
            <th style="width: 30px;">Cant.</th>
            <th style="width: 120px;">Descripción</th>                        
        </tr></thead>
        <tbody>            
            <tr>
            <td style="text-align: center;"><?= intval($output->quantity) ?></td>
            <td style="font-family:monospace;font-size:10px;"><?= strtoupper($output->product->name); ?></td>                        
            </tr>            
        </tbody>
    </table>    
</body>
<script type="text/javascript">var w = self;document.getElementById("qr_code").onload = function () {window.print();w.close();}</script>
</html>
