@extends('layouts.back')

@section('head')
<div id="title">
    <h2>Transacciones del Club</h2>
    <h5><?= session('business')->comercial_name ?></h5>
</div>
@endsection

@section('body')
<div class="row">
    <div class="col-md-12 ">
        <div class="row white-bg p-b-lg-20 p-t-lg-20 table-responsive p-l-lg-10 p-r-lg-10">
        <table class="table table-hover" id="list_transaction">
            <thead>
                <tr class="text-center">
                    <th class="text-left">Código Venta</th>
                    <th style="width:100px">Puntos</th>
                    <th style="width:100px">Monto</th>
                    <th style="width:220px">Fecha Creación</th>
                    <th style="width:100px">Estado</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($transactions as $transaction) { ?><tr class="text-center">
                    <td class="text-left">{{@$transaction->sales_code}}</td>
                    <td class="text-right">{{@$transaction->points}}</td>
                    <td class="text-right">{{@$transaction->amount}}</td>
                    <td class="text-right">{{@$transaction->created_at}}</td>
                    <td class="text-right">{{@['ANULADO','ACTIVO'][$transaction->state]}}</td>
                </tr><?php } ?>
            </tbody>
        </table>
    </div></div>
</div>

@endsection
