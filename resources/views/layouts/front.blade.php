<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ isset($meta_content) ? $meta_content : '' }}">
    <title>{{ isset($title) ? $title : '' }}</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" crossorigin="anonymous">
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto&display=swap">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="<?= asset('css/Nunito.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?= asset('css/front.css') ?>">
    <link rel="stylesheet" href="<?= asset('css/color.css?v=2.0') ?>">
    <link rel="stylesheet" href="<?= asset('css/padd-mr.css') ?>">
    <link rel="stylesheet" href="<?= asset('css/login.css') ?>">        

</head>
<body>
<div class="main_fund">
<div class="fund"></div>

<div class=" login">
        @yield('content')
</div>
    
</body>
<script type="text/javascript" src="{{ asset('js/jquery/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap4.min.js') }}"></script>
</html>
