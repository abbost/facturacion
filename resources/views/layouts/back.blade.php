<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>FACTURACIÖN ELECTRÓNICA PYMES</title>
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" crossorigin="anonymous">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}"> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">        
    <link href="{{ asset('css/FAQ.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css?v=1.2') }}" rel="stylesheet">
    <link href="{{ asset('css/padd-mr.css') }}" rel="stylesheet">
    <link href="{{ asset('css/color.css?v=1.1') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css?v=1.5') }}" rel="stylesheet">    
    <link href="{{ asset('libs/flexdatalist/jquery.flexdatalist.min.css') }}" rel="stylesheet" type="text/css">

</head>
    <?php
        $environment = [
            'development'=>['badge-success', 'Prueba'],
            'production'=>['badge-danger', 'Real'],
        ][session('business')->environment];
    ?> 
<body class="app sidebar-mini sidenav-toggled">
    <header class="app-header">
      <a class="app-header__logo" href="#!">Facturador Abbost</a>

      <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <div class="business-container" style="display:block ? has_permission('office.multiwork') : none">
        <div class="d-inline-flex" style="align-items:center">
          <div class="business-title"><?= session('business')->comercial_name ?></div>
          <div class="environment">
            <span class="badge badge-pill {{$environment[0]}}">Ambiente {{$environment[1]}}</span>
          </div>
        </div>
        <div class="" style="align-items:center; float:right; display:inline-flex">
            <div class="business-title none"><i class="fa fa-user-circle pr-2" aria-hidden="true"></i><?= session('user')->fullname?></div>
            @if(!has_permission('office.multiwork'))
            <div class="business-title"><?= session('office')->name ?></div>
            @else
            <form  class="ml-3" style="display:inline;margin-right:20px;">
                <?php if (isset($type)) { ?><input type="hidden" name="type" value="<?= $type ?>"><?php } ?>
                <select name="office_id" class="btn btn-suc text-uppercase" onchange="$.ajaxblock();this.parentNode.submit();">
                    <?php foreach (offices() as $office) { ?>
                    <option value="<?= @$office->id ?>" <?= @$office->id == office_id() ? 'selected' : '' ?>><?= @$office->name ?></option>
                    <?php } ?>
                </select>
            </form>
            @endif
        </div>
      </div> 
    </header>

    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
       <aside class="app-sidebar">
      <?php
        $links = [
                      
            'invoice' =>           ['url' => 'document/create',             'icon' => 'fa fa-shopping-cart',      'name' => 'Ventas'],            
            'document' =>           ['url' => 'document',                   'icon' => 'fa fa-book',             'name' => 'Reporte'],            
            'summary' =>           ['url' => 'summary',                   'icon' => 'fas fa-hashtag',             'name' => 'Resumen diario'],            
            'guide' =>        ['url' => 'guide',                   'icon' => 'fa fa-truck',             'name' => 'Guías Remisión', 'sons' => [
                'reporte'   =>  ['url' => 'guide',           'icon' => 'fas fa-circle',    'name' => 'Reporte'],
                'vehículos' =>  ['url' => 'guide/car',       'icon' => 'fa fa-truck',       'name' => 'Vehiculos'],
                'choferes'  =>  ['url' => 'guide/driver',    'icon' => 'fa fa-user',        'name' => 'Conductores'],
            ]],            

            'customer' =>           ['url' => 'customer',                   'icon' => 'fa fa-users',            'name' => 'Clientes'],            

            'item' =>            ['url' => '#',                          'icon' => 'fa fa-check-square',    'name' => 'Inventarios', 'sons' => [
                'Productos' =>        ['url' => 'item?type=P',           'icon' => 'fas fa-circle',        'name' => 'Productos'],
                'Servicios' =>         ['url' => 'item?type=S',            'icon' => 'fas fa-circle',        'name' => 'Servicios']
                
            ]],

            'cash_turn' =>      ['url' => 'cash_turn',                  'icon' => 'fa fa-calculator',       'name' => 'Cierre de caja'],

            'expense' =>        ['url' => 'expense',                    'icon' => 'fas fa-dollar-sign',           'name' => 'Gastos'],

            'purchase' =>          ['url' => '#',                          'icon' => 'fas fa-money-bill',            'name' => 'Compras', 'sons' => [
                'purchase' =>       ['url' => 'purchase',                   'icon' => 'fa fa-shopping-cart',    'name' => 'Compras'],                
                'output' =>         ['url' => 'output',                     'icon' => 'fas fa-exchange-alt',         'name' => 'Consumos'],
                'provider' =>       ['url' => 'provider',                   'icon' => 'fa fa-truck',            'name' => 'Proveedores'],
            ]],
            'business' =>           ['url' => '#',                          'icon' => 'fa fa-user-circle"',     'name' => 'Administrador', 'sons' => [
                'business' =>       ['url' => 'business',                   'icon' => 'fa fa-users',             'name' => 'Emisor'],
                'indicator' =>      ['url' => 'indicator',                  'icon' => 'fas fa-chart-bar',        'name' => 'Indicadores'],
                #'office' =>         ['url' => 'office',                     'icon' => 'fa fa-building',         'name' => 'Sucursales'],
                'user' =>           ['url' => 'user',                       'icon' => 'fa fa-users',            'name' => 'Usuarios'],
            ]],            
            'FAQ' =>                ['url' => 'question',                   'icon' => 'fa fa-question-circle',  'name' => 'Ayuda' ],
            'logout' =>             ['url' => '#',                          'icon' => 'fas fa-sign-out-alt',         'name' => 'Salir']
        ];

        if (isset($view)) {
          $links[$view]['active'] = true;
        }
        $links['logout']['id'] = 'btn_logout';
        $links['logout']['permission_free'] = true;        
        $links['FAQ']['permission_free'] = true;
        $links['summary']['permission_free'] = true;

        if(session('business')->has_guide){
          $links['guide']['permission_free'] = true;
        }
      ?>

      <ul class="app-menu pt-3">
        @foreach ($links as $id => $link)
        @if (!isset($link['sons']))
          <?php if (@$link['permission_free'] || has_permission("$id.show")) { ?>
            <li>
              <a id="<?= @$link['id'] ?>" class="app-menu__item <?= @$link['active'] ? 'active' : '' ?>" href="<?= url($link['url']) ?>"><i class="app-menu__icon <?= $link['icon'] ?>"></i><span class="app-menu__label"><?= $link['name'] ?></span></a>
            </li>
          <?php } ?>
        @else
          <?php if (@$link['permission_free'] || has_permission("$id.show")) { ?>
            <li class="treeview">
              <a class="app-menu__item <?= @$link['active'] ? 'active' : '' ?>" data-toggle="treeview" href="#">
                <i class="app-menu__icon <?= $link['icon'] ?>"></i><span class="app-menu__label"><?= $link['name'] ?></span><i class="treeview-indicator fa fa-angle-right"></i>
              </a>
              <ul class="treeview-menu">
                @foreach (@$link['sons']?: [] as $son)
                <li>
                  <a class="treeview-item" href="<?= url($son['url']) ?>"><i class="app-menu__icon <?= $son['icon'] ?>"></i><span><?= $son['name'] ?></span></a>
                </li>
                @endforeach
              </ul>
            </li>
          <?php } ?>
        @endif
        @endforeach
      </ul>
      <form id="form_logout" action="<?= url('logout') ?>" method="post" style="display:none;"> @csrf </form>
    </aside>


    <section class="m-t-xs-80">
        <div class="app-content">
          
          @yield('head')

          <div class="row justify-content-center">
            <?php if (session('error')) { ?>
              <div class="col-md-6">
                <div class="alert alert-dismissible alert-danger p-lg-5">
                  <button class="close p-lg-5" type="button" data-dismiss="alert">×</button>
                  <div><?= @session('error') ?></div>
                </div>
              </div
            ><?php } ?>
            <?php if (session('errors')) { ?>
                <div class="col-md-6">
                  <div class="alert alert-dismissible alert-danger p-lg-5">
                    <button class="close p-lg-5" type="button" data-dismiss="alert">×</button>
                    <li><ul><li><?= implode(session('errors')->all(), '</li><li>') ?></li><ul></li>
                  </div>
                </div>
            <?php } ?>
            <?php if (session('status')) { ?>
                <div class="col-md-6">
                  <div class="alert alert-dismissible alert-success">
                  <button class="close p-lg-5" type="button" data-dismiss="alert">×</button>
                  <div><?= session('status') ?>
                  @if(session('redirect') && session('error')==0)
                    <a href="{{session('redirect')}}" target="_blank" class="m-l-lg-5"><i class="fa fa-print"></i> DESCARGUE EL DOCUMENTO</a>
                  @endif
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>

          @yield('body')

        </div>
    </section>

    <!-- <footer><div class="contentWrapper" style="padding:5px;text-align:center;">
        <small>Desarrollado por <a href="http://www.abbost.com">ABBOST</a>. Todos los derechos reservados</small>
    </div></footer> -->

    <div id="cont-ads" class='modal fade container' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true' ></div>
    <div id="modal_customer_edit" class="modal fade"></div>
    <div id="modal_customer_create" class="modal fade"></div>
</body>

<script type="text/javascript" src="{{ asset('js/jquery/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('libs/datatable/jquery.datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('libs/datatable/datatables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('libs/datatable/datatable.jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('libs/flexdatalist/jquery.flexdatalist.min.js') }}"></script>
<script type="text/javascript">base_url = "<?= url('/') ?>/";</script>
<script src="{{asset('js/ajaxview.js')}}"></script>
<script src="{{asset('js/plugins/chart.js')}}"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="{{asset('js/core.js?v=3.9')}}"></script>

@if(session('redirect'))
<input id="print_redirect" type="hidden" redirect="{{session('redirect').'&window=1'}}">
<script type="text/javascript">        
    print_popup($("#print_redirect").attr('redirect'));
</script>
@endif


<script type="text/javascript">var global_tax = <?= @$business->sector == 'edu' ? 0 : 0.18  ?></script>

<script type="text/javascript">
  <?php if (session('ads')):?>
      var $modal = $("#cont-ads");
      $("body").append($modal);
      $modal.load('<?= asset('news/new.htm') ?>','', function(){ $modal.modal(); });   
  <?php endif; ?>
</script>

<!-- <script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>
<script type="text/javascript">
  FreshWidget.init("", {"queryString": "&widgetType=popup&formTitle=Escr%C3%ADbenos+tu+consulta&submitTitle=Enviar&submitThanks=Gracias+por+tu+comentario.+Te+responderemos+por+correo+electr%C3%B3nico+en+breve.&attachFile=no&searchArea=no", "utf8": "✓", "widgetType": "popup", "buttonType": "text", "buttonText": "Soporte", "buttonColor": "black", "buttonBg": "#00d8ff", "alignment": "2", "offset": "235px", "submitThanks": "Gracias por tu comentario. Te responderemos por correo electrónico en breve.", "formHeight": "500px", "url": "https://abbost.freshdesk.com"} );
</script> -->
</html>
