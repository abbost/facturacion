@extends('layouts.back')

@section('head')
<div class="doc-title mb-4">
    <h2>
        <span>Usuarios</span>

        <?php if ($can_manage = has_permission('user.manage')) { ?>
        <a href="#" class="btn btn-primary link_open_popup" data-url="<?= url('user/create') ?>" data-target="#modal_user_create">Nuevo</a>
        <?php } ?>
    </h2>
    <div>{{$users->count()}} de 3 usuarios permitidos</div>
</div>
@endsection

@section('body')
<div class="row">    
    <div class="table-responsive">
    <table class="report table table-hover" style="min-width:600px;">
        <thead><tr>
            <th style="width:300px;">Detalles</th>
            <th>Permisos</th>
            <th style="width:80px;text-align:center;">Acción</th>
        </tr></thead>
        <tbody>
            <?php $can_manage_permissions = has_permission('permission.manage'); ?>
            <?php foreach ($users as $user) { ?><tr>
                <td>                                        
                    <div><strong>Usuario:</strong> <?= $user->email ?></div>
                    <div><strong>Rol:</strong> <?= ucfirst($user->role) ?></div>
                    <div><strong>Documento:</strong> <?= $user->dni ?></div>
                </td>
                <td style="text-transform:uppercase;">
                    <div class="permission_list_item"><?= @$user->permissions->pluck('description')->implode('</div><div class="permission_list_item">') ?></div>
                </td>
                <td style="text-align:center;">
                    <?php if ($can_manage) { ?>
                    <form action="<?= url("user/delete?id={$user->id}") ?>" method="post">
                        @csrf
                        <a href="#" class="link_open_popup" data-url="<?= url("user/edit?id={$user->id}") ?>" data-target="#modal_user_edit"><i class="fas fa-pencil-alt"></i></a>
                        <?php if (session('user')->id != $user->id) { ?>
                        <?php if ($can_manage_permissions) { ?>
                        <a href="#" class="link_open_popup" data-url="<?= url("user/permission?id={$user->id}") ?>" data-target="#modal_user_permission"><i class="fa fa-key"></i></a>
                        <?php } ?>
                        <a href="#" class="link_delete_row_data"><i class="fa fa-trash"></i></a>
                        <?php } ?>
                    </form>
                    <?php } ?>
                </td>
            </tr><?php } ?>
        </tbody>
    </table>
</div></div>
<div id="modal_user_create" class="modal fade"></div>
<div id="modal_user_edit" class="modal fade"></div>
<div id="modal_user_permission" class="modal fade"></div>
@endsection
