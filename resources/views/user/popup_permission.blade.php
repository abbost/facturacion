<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><b>Editar permisos de usuario</b></h4>
            <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <div class="row">

                
                
                <form action="<?= url('user/permission/add') ?>" method="post" class="col-md-12 mb-2">
                    @csrf
                    @foreach($permissions as $permission)
                    <div>
                        <input id="{{ 'p'.$permission->id }}" type="checkbox" value="{{$permission->id}}" name="permissions[]" <?= isset($assigned[$permission->id]) ? 'checked':'' ?>>
                        <label for="{{ 'p'.$permission->id }}">{{$permission->description}}</label>
                    </div>
                    @endforeach
                    <div class="mb-2" style="text-align:center;">
                        <button type="submit" class="btn btn-info" style="cursor:pointer;">AGREGAR</button>
                    </div>                    
                </form>
                

            </div>
        </div>
    </div>
</div>