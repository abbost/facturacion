<form action="<?= url('user/edit') ?>" method="post" class="row"><div class="col-md-12">
    @csrf
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Editar usuario</b></h4>
                <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <label><b>Número de documento:</b></label>
                        <input type="text" name="dni" class="form-control" value="<?= $user->dni ?>" required>
                    </div>
                    <!-- <div class="col-md-12 mb-2">
                        <label><b>Nombre completo:</b></label>
                        <input type="text" name="fullname" class="form-control" value="<?= $user->fullname ?>" required>
                    </div> -->

                    <?php if (session('update_user_id') != session('user')->id) { ?>
                    <div class="col-md-12 mb-2">
                        <label><b>Tipo de usuario:</b></label>
                        <select name="role" class="form-control">
                            <option value="empleado" <?= $user->role == 'empleado' ? 'selected' : '' ?>>Empleado</option>
                            <option value="jefe" <?= $user->role == 'jefe' ? 'selected' : '' ?>>Jefe de tienda</option>
                            <option value="administrador" <?= $user->role == 'administrador' ? 'selected' : '' ?>>Administrador</option>
                        </select>
                    </div>
                    <?php } ?>

                    <div class="col-md-12 mb-2">
                        <label><b>Nombre de usuario:</b></label>
                        <div class="d-flex">
                            <input type="text" name="fullname" class="form-control" autocomplete="off"  required value="<?= $user->fullname ?>">
                            <input type="hidden" name="domain" value="{{$domain}}">
                            <div class="bold p-l-lg-10">{{'@'.$domain}}</div>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>Contraseña:</b></label>
                        <input type="text" name="password" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary link_close_popup" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> GRABAR</button>
            </div>
        </div>
    </div>
</div></form>