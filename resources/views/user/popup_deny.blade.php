<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><b>Nuevo usuario</b></h4>
            <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12 mb-2">
                    Lo sentimos, usted ha superado el límite de usuarios permitidos.
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-lg-between">
            <button type="button" class="btn btn-secondary link_close_popup" data-dismiss="modal">Aceptar</button>                
        </div>
    </div>
</div>
