@extends('layouts.back')

@section('content')

<div id="title">
    <small><?= session('business')->comercial_name ?> - Suc.: <?= session('office')->name ?></small>
    <h2>Consultar Ticket</h2>  
</div>

<div class="container mb-100">
    <div class="row">
        <?php if (isset($result)): ?>
            <div class="col-md-12 m-b-lg-20">
                <div class="card mb-20">
                    <div class="card-header bg-success text-white">Resultado</div>
                    <div class="card-block">
                        <div class="card bg-light text-dark">
                            <div class="card-body">
                                <?php if ($result->isSuccess()): ?>
                                    <strong>Codigo: </strong> <?=$result->getCode()?> <br>
                                    
                                    <?php if (!is_null($result->getCdrResponse())):?>
                                        <strong>Estado Comprobante: </strong> <?=$result->getCdrResponse()->getDescription()?>
                                        <br>
                                        <strong>Observaciones: </strong> <?=implode('<br>', $result->getCdrResponse()->getNotes())?>
                                        <br>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <div class="alert alert-danger">
                                        <?=$result->getError()->getMessage()?>
                                    </div>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="col-md-12">
            <div class="card bg-primary">
                <!-- <div class="card-header text-white">Consulta de CDR Status</div> -->
                <div class="card-block">
                    <div class="card bg-light text-dark">
                        <div class="card-body">
                            <form action="{{url('ticket/consult')}}" method="post">
                            	@csrf
                                <div class="row">
                                    <div class="col-md-4">                                        
                                        <div class="form-group">
                                            <label for="ruc">Ruc:</label>
                                            <select class="form-control" name="ruc" id="ruc">
                                                @foreach($emisores as $emisor)
                                                    @if(@$_POST['ruc']==$emisor->ruc)
                                                        <option value="{{$emisor->ruc}}" selected>{{strtoupper($emisor->comercial_name) }}</option>
                                                    @else
                                                        <option value="{{$emisor->ruc}}">{{ strtoupper($emisor->comercial_name)}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="ticket">Ticket Id:</label>
                                            <input type="text" class="form-control" name="ticket" id="ticket" value="<?=@$_POST['ticket'] ?: '' ?>" >
                                        </div>
                                    </div>               
                                </div>
                                <button class="btn btn-primary">Consultar Estado</button>
                                
                                <!-- <button class="btn btn-primary" name="cdr">Consultar CDR</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

