@extends('layouts.front')

@section('content')
<div id="title">	
    <h2>Error 404</h2>    
</div>
<section class="hero text-center text-light">
    <div class="container-sm">
        <div class="hero-inner">
            <h1 class="hero-title h2-mobile mt-0" style="color:#f8f9fa;">Parece que hubo un error :(</h1>
        </div>
        <div style="text-align:center;">
         	<a href="{{url('/')}}" class="button button-secondary button-shadow">REGRESAR</a>
        </div>
    </div>
</section>

@endsection