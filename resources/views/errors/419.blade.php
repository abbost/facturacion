@extends('layouts.front')

@section('content')
<div id="title">	
    <h2>Error 419</h2>    
</div>
<section class="hero text-center text-light">
    <div class="container-sm">
        <div class="hero-inner">
            <h1 class="hero-title h2-mobile mt-0" style="color:#f8f9fa;">Disculpe la sesión ya terminó</h1>
        </div>
        <div style="text-align:center;">
         	<a href="{{url('login')}}" class="button button-secondary button-shadow">VOLVER AL LOGIN</a>
        </div>
    </div>
</section>

@endsection