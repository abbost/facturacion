@extends('layouts.back')

@section('head')
<div class="doc-title mb-4">
    <h2>
        <span>Datos de emisor</span>
    </h2>
</div>
@endsection

@section('body')
<form action="{{ url('business/edit') }}" method="post" enctype="multipart/form-data">
<div class="row justify-content-center">    
    <div class="col-md-4">
        <label class="text-right">Logo (512px x 512px)</label>
        <div class="botonFotos-plus" style="background-image:url('{{ asset('img/'.$business->image) }}');height:300px; width: 300px">
            <input type="file" name="image" accept="image/*" onchange="read_image_as_bg(this)">                    
            <input type="hidden" name="hidden_image" value="{{ $business->image }}" />
        </div>        
    </div>
    <div class="col-md-7">    
        <div class="row">
        @csrf
        <div class="col-sm-6 mb-2">
            <label>RUC</label>
            <input class="form-control" type="text" style="width: 100%;" value="{{$business->ruc}}" disabled>
        </div>
        <div class="col-sm-6 mb-2">
            <label>SECTOR</label>
            <input class="form-control" type="text" style="width: 100%;" value="{{$business->sector == 'edu'?'EDUCACION':'COMERCIAL'}}" disabled>
        </div>
        <div class="col-sm-12 mb-2">
            <label>Razón Social:</label>
            <input class="form-control" type="text" style="width: 100%; text-transform: uppercase;"value="{{$business->legal_name}}" disabled>
        </div>
        <div class="col-sm-12 mb-2">
            <label>Nombre Comercial:</label>
            <input class="form-control" type="text" name="comercial_name" style="width: 100%; text-transform: uppercase;" value="{{$business->comercial_name}}">
        </div>
        <div class="col-sm-12 mb-2">
            <label>Dirección:</label>
            <input class="form-control" type="text" style="width: 100%;" value="{{$business->address}}" disabled>
        </div>
        <div class="col-sm-4 mb-2">
            <label>Comprobantes contratados:</label>
            <div class="form-control" readonly><?= $business->monthly_bill_limit; ?> por mes</div>
        </div>
        <div class="col-sm-4 mb-2">
            <label>Impresión predeterminada:</label>
            <select name="print_option" class="form-control">
                <option value="ticket" <?= $business->print_option == 'ticket' ? 'selected' : '' ?>>Ticket</option>
                <option value="pdf a4" <?= $business->print_option == 'pdf a4' ? 'selected' : '' ?>>PDF A4</option>
            </select>
        </div>
        <div class="col-sm-4 mb-2">
            <label>Fecha Fin contrato</label>
            <div class="form-control" readonly><?= date('d-m-Y',strtotime($business->due_date)) ?></div>
        </div>
        <div class="col-sm-12 mb-2">
            <input type="checkbox" name="send_documents_sunat" value="ok" id="chk_send_sunat" <?= $business->send_documents_sunat ? 'checked' : '' ?>>
            <label for="chk_send_sunat">Enviar documentos a SUNAT en directo</label>
        </div>
        <?php if (has_permission('business.manage')) { ?>
        <div class="col-sm-12">
            <button class="btn btn-success pull-right">ACTUALIZAR</button>
        </div>
        <?php } ?>
        </div>    
    </div>    
</div>
</form>
@endsection
