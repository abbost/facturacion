@extends('layouts.back')

@section('head')
<div class="doc-title mb-4">
    <h2>Indicadores</h2>
</div>
@endsection

@section('body')
<div class="row justify-content-center">
    <div class="col-md-10 pt-3 white-bg">
    	<h5>Evolución de ventas en los últimos 6 meses</h5>
    	<small>No incluye ventas anuladas</small>
    	<div class="row white-bg p-b-lg-20 p-t-lg-20 m-b-lg-20 mb-5">
    		 <canvas class="embed-responsive-item chart_bar_vertical" style="width: 100%; height: 162px;"
                data-x="<?= implode(array_column($sales,'month'),",") ?>"
                data-y="<?= implode(array_column($sales,'total'),",") ?>">
            </canvas>
    	</div>
		<h5>Cantidad de comprobantes emitidos en los últimos 6 meses</h5>
    	<small>Si incluye comprobantes anulados</small>
    	<div class="row white-bg p-b-lg-20 p-t-lg-20">
    		<canvas class="embed-responsive-item chart_bar_vertical" style="width: 100%; height: 162px;"
                data-x="<?= implode(array_column($invoices,'month'),",") ?>"
                data-y="<?= implode(array_column($invoices,'total'),",") ?>">
            </canvas>
    	</div>
    </div>
</div>
@endsection