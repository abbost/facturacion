<html>
<?php
    $text_prueba = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum. Lorem ipsum reeew';
    $table_content_height = $details->map(function ($detail) use ($text_prueba) {
        $num_lines = 1;
        $description = $detail->description;
        $len = strlen($description);
        for ($i = 30; $i < $len; $i += 30) {
            if ($description[$i] != ' ') {
                while ($description[--$i] != ' ') ;
                $i++;
                $num_lines++;
            }
        }
        return $num_lines + 1;
    })->sum();
?>
<style type="text/css">
    @page{size:340px <?= 13 + $table_content_height * 0.4; ?>cm !important;margin-top: -2cm !important;margin-left:-20px;margin-right:-20px;margin-bottom:0px;}
    *{margin:0px;padding:0px;}
    html,body{padding:1cm 10px 20px 10px;}
    @media print{body{margin-top:1cm;}}
   p{text-align:center;}
    hr{margin:2px auto;border-color:#aaa;}
    #details th, #details td{padding:2px;}
    #details thead{border-bottom:1px solid #ccc;}
    #details tr+tr{border-top:1px solid #ddd;}
</style>
<body>
    <!-- <div style="margin-bottom:20px;">.</div> -->
    <div style="">&nbsp;</div>
    <div style="font-size:12px; margin-top:30px">
        <p><?= $business->legal_name ?></p>
        <p>R.U.C.: <?= $business->ruc ?></p>
  <!--       <div style="text-align:center;margin:15px 0px;"><img src="<?= public_path()."/img/".$business->image?>" alt="logo" style="width:80%;"></div> -->
        <p style="font-size:10px; "><?= strtoupper($document->office->address) ?></p>
    </div>
    <hr style="width:80%;">
    <div>
        <p style="font-size:12px;"><strong><?= $document_name ?></strong></p>
        <p style="font-size:12px;"><?= $document->serie." - ".$document->correlative ?></p>
        <div style="margin-top:5px;font-size:10px;">
            <p>Fecha de emisión: <?= date('d/m/Y H:i', strtotime($document->created_at)) ?></p>
            <p>Señor (es): <?= $document->customer_name ?></p>
            <p><?= @[1=>"DNI", 4=>"CEX", 6=>"RUC", 7=>"PAS"][$document->customer_doc_type] ?> Nº: <?= $document->customer_doc ?></p>
            <p>Direc: <?= $document->customer_address ?></p>
            <?php if ($document->order_reference != "") { ?><p>Orden de compra: <?= $document->order_reference ?></p><?php } ?>
            <?php if ($document->note != "") { ?><p><b>Observaciones:</b></p> <p><?= nl2br($document->note) ?></p><?php } ?>
        </div>
    </div>
    <hr>
    <table id="details" style="font-size:12px;margin:auto;">
        <thead><tr>
            <th style="width: 30px;">Cant.</th>
            <th style="width: 120px;">Descripción</th>
            <th style="width: 80px; text-align:right">Valor Venta</th>
            <th style="width: 50px; text-align:right">Precio</th>
        </tr></thead>
        <tbody><?php foreach ($details as $detail) { ?><tr>
            <td style="text-align: center;"><?= intval($detail->quantity) ?></td>
            <td style="font-family:monospace;font-size:10px;"><?= $detail->description; ?></td>
            <td style="text-align:right; font-size:10px">S/ <?= $detail->sub_total ?></td>
            <td style="text-align:right; font-size:10px">S/ <?= $detail->total ?></td>
        </tr><?php } ?></tbody>
    </table>
    <hr>
    <table style="width:70%;margin:auto;font-size:10px;">
        <tr>
            <td><strong>Gravada:</strong></td>
            <td style="text-align:right;">S/ <?= number_format($document->sub_total,2) ?></td>
        </tr>
        <tr>
            <td><strong>IGV (18.00%):</strong></td>
            <td style="text-align:right;">S/ <?= number_format($document->tax,2) ?></td>
        </tr>
        <tr>
            <td><strong>Total:</strong></td>
            <td style="text-align:right;">S/ <?= number_format($document->total,2) ?></td>
        </tr>
    </table>
    <hr>
    <table style="width:70%;margin:auto;font-size:10px;">
        <?php if ($cash =  @collect($document->payments)->firstWhere('means', 'cash')->amount) { ?>
        <tr>
            <td><strong>EFECTIVO:</strong></td>
            <td style="text-align:right;">S/ <?= number_format($cash, 2) ?></td>
        </tr>
        <?php } ?>
        <?php if ($card =  @collect($document->payments)->firstWhere('means', 'card')->amount) { ?>
        <tr>
            <td><strong>TARJETA:</strong></td>
            <td style="text-align:right;">S/ <?= number_format($card, 2) ?></td>
        </tr>
        <?php } ?>
        <?php if ($transfer =  @collect($document->payments)->firstWhere('means', 'transfer')->amount) { ?>
        <tr>
            <td><strong>TRANSFERENCIA:</strong></td>
            <td style="text-align:right;">S/ <?= number_format($transfer, 2) ?></td>
        </tr>
        <?php } ?>
    </table>
    <hr>
    <p style="font-size:10px;">Importe en letras: <?= $total_text ?> soles</p>
    <div style="text-align:center;"><img id="qr_code" src="<?= $url_qr ?>" alt="Código qr del documento"></div>
    <div style="font-size:10px;">
        <p>Representación impresa de la</p>
        <p><?= $document_name ?>.</p>
        <p style="margin-top:5px;">Para consultar el documento visita</p>
        <p>www.facturacionelectronicapyme.com</p>
    </div>
</body>
<script type="text/javascript">var w = self;document.getElementById("qr_code").onload = function () {window.print();w.close();}</script>
</html>
