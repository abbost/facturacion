<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>FACTURACIÓN ELECTRÓNICA PYMES</title>
    <style type="text/css">
        *{margin:0px;padding:0px;font-family:'Segoe UI Light', 'Calibri', 'Arial';}
        html,body{height:100%;background-color:rgba(255,255,255,0.5);position:relative;}
        div{position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);font-size:18px;}
        div{border:1px solid #333;padding:20px;}
    </style>
</head>
<body>
    <div><?= $id != null ? $message . "<a href='?id=".$id."&export_type=".$export_type."&ran=".rand(100,100000)."'>Click aquí</a>" : 'Recargue la página por favor' ?></div>
</body>
</html>