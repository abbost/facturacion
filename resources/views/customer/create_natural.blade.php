<form id="customer_create_form" action="<?= url('customer/create') ?>" method="post" class="row"><div class="col-md-12">
    @csrf
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Nuevo cliente</b></h4>
                <div><small>* campos obligatorios</small></div>                
                <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" name="type" value="natural">
                    
                    <div class="col-md-12 mb-2" >                        
                        <label><b>Tipo de documento (*):</b></label>
                        <select name="doc_type" id="receptor_doc_type" class="form-control mb-2" onchange="$('#doc').attr('maxlength', $(':selected', $(this)).attr('data-limit'))">
                            <option value="DNI" data-limit="8" selected>DNI</option>                            
                            <option value="CEX" data-limit="20">CARNE DE ENTRANJERIA</option>
                            <option value="PAS" data-limit="20">PASAPORTE</option>
                        </select>
                    </div>
                        
                    <div class="col-md-12 mb-2" >               
                        <label><b>N° de documento (*):</b></label>         
                        <div class="input-group">
                            <input type="text" id="receptor_doc" name="doc" class="form-control" maxlength="8" required>
                            <span class="input-group-btn">
                                <button class="btn btn-defaul white" type="button" onclick="ws_search_person()" style="background: var(--main-color)">
                                    <i class='fa fa-search'></i>RENIEC</button>
                            </span>
                        </div>
                    </div>

                    <div class="col-md-12 mb-2">
                        <label><b>Nombre completo:</b></label>
                        <input type="text" id="receptor_name" name="fullname" class="form-control mb-2" required="">
                    </div>

                    <div class="col-md-12 mb-2">
                        <label><b>Dirección:</b></label>
                        <input type="text" id="receptor_address" name="address" class="form-control">
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>E-mail:</b></label>
                        <input type="text" name="email" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary link_close_popup" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="customer_create_btn" class="btn btn-primary"><i class="fa fa-save"></i>GRABAR</button>
            </div>
        </div>
    </div>
</div></form>