<form action="<?= url('customer/create') ?>" method="post" class="row"><div class="col-md-12">
    @csrf
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Nuevo cliente</b></h4>
                <div><small>* campos obligatorios</small></div>                
                <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" name="type" value="business">
                    <input type="hidden" id="receptor_doc_type" name="doc_type" value="RUC">

                    <div class="col-md-12 mb-2">
                        <label><b>RUC: (*)</b></label>                        
                        <div class="input-group">
                            <input type="text" id="receptor_doc" name="ruc" class="form-control mb-2" maxlength="11">
                            <span class="input-group-btn">
                                <button class="btn btn-defaul white" type="button" onclick="ws_search_person()" style="background: var(--main-color)">
                                    <i class='fa fa-search'></i>SUNAT</button>
                            </span>
                        </div>
                    </div>

                    
                    <div class="col-md-12 mb-2">
                        <label><b>Razón social: (*)</b></label>
                        <input type="text" id="receptor_name" name="legal_name" class="form-control mb-2">
                    </div>

                    <div class="col-md-12 mb-2">
                        <label><b>Dirección: (*)</b></label>
                        <input type="text" id="receptor_address" name="address" class="form-control">
                    </div>
                    
                    <div class="col-md-12 mb-2">
                        <label><b>Representante:</b></label>
                        <input type="text" name="representative" class="form-control">
                    </div>

                    
                    <div class="col-md-12 mb-2">
                        <label><b>E-mail:</b></label>
                        <input type="text" name="email" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary link_close_popup" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>GRABAR</button>
            </div>
        </div>
    </div>
</div></form>