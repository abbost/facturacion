<form action="<?= url('customer/edit') ?>" method="post" class="row"><div class="col-md-12">
    @csrf
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Editar cliente</b></h4>
                <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php if ($type == 'business') { ?>
                    <div class="col-md-12 mb-2" style="display:none;">
                        <label><b>Razón social:</b></label>
                        <input type="text" name="legal_name" value="<?= $customer->legal_name ?>" class="form-control mb-2">

                        <label><b>RUC:</b></label>
                        <input type="text" name="ruc" value="<?= $customer->ruc ?>" class="form-control mb-2" maxlength="20">

                        <label><b>Representante:</b></label>
                        <input type="text" name="representative" value="<?= $customer->representative ?>" class="form-control">
                    </div>
                    <?php } else if ($type == 'person') { ?>
                    <div class="col-md-12 mb-2">
                        <label><b>Nombre completo:</b></label>
                        <input type="text" name="fullname" value="<?= $customer->fullname ?>" class="form-control mb-2">

                        <label><b>Tipo de documento:</b></label>
                        <select name="doc_type" class="form-control mb-2" onchange="$('#document_person').attr('maxlength', $(':selected', $(this)).attr('data-limit'))">
                            <option value="DNI" <?= $customer->doc_type == 1 ? 'selected' : '' ?> data-limit="8">DNI</option>
                            <option value="RUC" <?= $customer->doc_type == 6 ? 'selected' : '' ?> data-limit="11">RUC</option>
                            <option value="CEX" <?= $customer->doc_type == 4 ? 'selected' : '' ?> data-limit="20">Carné de extranjería</option>
                            <option value="PAS" <?= $customer->doc_type == 7 ? 'selected' : '' ?> data-limit="20">Pasaporte</option>
                        </select>

                        <label><b>Documento:</b></label>
                        <input type="text" id="document_person" name="doc" value="<?= $customer->doc ?>" class="form-control" maxlength="<?= @[1 => 8, 6 => 11, 4 => 20, 7 => 20][$customer->doc_type] ?>">
                    </div>
                    <?php } ?>
                    <div class="col-md-12 mb-2">
                        <label><b>Dirección:</b></label>
                        <input type="text" name="address" value="<?= $customer->address ?>" class="form-control">
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>E-mail:</b></label>
                        <input type="text" name="email" value="<?= $customer->email ?>" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary link_close_popup" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>GRABAR</button>
            </div>
        </div>
    </div>
</div></form>