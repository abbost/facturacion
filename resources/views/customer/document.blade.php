@extends('layouts.back')

@section('head')
<div class="doc-title mb-4">
    <h2>
        <span>Reporte de Ventas</span>
    </h2>
    <button type="button" class="btn" onclick="window.history.back()">Regresar</button>
    <div style="width: 100%;">
        <div class="f-18">Cliente: {{$customer->name}}</div>
        <div class="f-18">Documento: {{$customer->doc}}</div>
    </div>
</div>
@endsection

@section('body')
<div class="row justify-content-lg-center">
    <div>
        <h3>Total: <span class="PEN">{{number_format($total,2)}}</span></h3>
    </div>
    <div class="col-md-12">
        <div class="row white-bg table-responsive" >
            <table class="table report table-sm table-hover f-12" id="list_documents" style="text-transform:uppercase; max-width: none; width: max-content !important; margin: 0 auto;">
                <thead><tr  class="text-center">
                    <th style="">Serie</th>                    
                    <th width="50">#</th>                    
                    <th >Tipo</th>    
                    <th width="200">Cliente</th>
                    <th width="300">Descripción</th>     
                    <th width=150">Notas</th>           
                    <th >PAGO</th>                   
                    <th >TOTAL</th>
                    <th width="100">Fecha</th>
                    <th >Estado</th>                                  
                    <th ></th>
                </tr></thead>
                <tbody>
                    <?php                    
                    foreach ($documents as $document) {                        
                        $encrypted_url = $url . $document->encrypted_id;
                        $status = [
                            ['name'=>'RECHAZADO',   'badge'=>'badge-danger'],
                            ['name'=>'ACEPTADO',    'badge'=>'badge-success'],
                            ['name'=>'ANULADO',     'badge'=>'badge-secondary'],
                            ['name'=>'PENDIENTE',   'badge'=>'badge-info'],
                            ['name'=>'ACEPTADO', 'badge'=>'badge-success']   // anulado con nota de credito
                        ];                    
                    ?>
                    <tr class="<?= $document->status == 2 ? 'gray-bg':'' ?>">
                        <td><strong style="color:#000;"><?= $document->serie ?></strong></td>
                        <td class="text-center"><strong style="color:#000;"><?= $document->correlative ?></strong></td>                        
                        <td><?=  ['00'=>'PROF','01'=>'FACT', '03'=>'BOL', '07'=>'NOTA'][$document->doc_type] ?></td>   
                        <td>
                            <span><?= $document->refered ? '['.$document->refered->serie.'-'.$document->refered->correlative.']' : '' ?> </span>
                            <span><?= strtoupper($document->customer_name) ?></span>
                            @if($document->credit_note)
                            <div>Modificado por {{$document->credit_note->serie.$document->credit_note->correlative}}</div>
                            @endif
                        </td>                                             
                        <td>
                            @foreach($document->details as $detail)
                                <div>{{ $detail->description }} | <span class="PEN">{{$detail->total}}</span></div>
                            @endforeach
                        </td>
                        <td>{{$document->note}}</td>
                        <td class="text-center">
                            <?= [''=>'','CASH'=>'CONTADO','CARD'=>'TARJETA','TRANSFER'=>'TRANSFER','CREDIT'=>'CREDITO'][strtoupper(@$document->payment->means)] ?>
                        </td>    
                        <td class="text-right PEN"><?= number_format($document->total,2) ?></td>
                        <td class="text-right"><?= date('d/m h:i.a',strtotime($document->date)) ?></td>
                        <td>
                            <div style="text-align:center;">
                            <span class="badge badge-pill {{ $status[$document->status]['badge'] }} ">
                                @if ($document->status == 3)
                                <i class="fa fa-refresh"></i>
                                <a href="#" class="link_resend_document white">PENDIENTE</a>
                                <form action="<?= url('document/resend') ?>" method="post">
                                    @csrf
                                    <input type="hidden" name="document_id" value="<?= $document->id ?>">
                                </form>
                                @else 
                                    {{ $status[$document->status]['name'] }}
                                @endif
                            </span>
                            </div>
                        </td>
                        
                        <td>
                        <div style="text-align:center;font-size:13px;position:relative;" class="dropdown">
                            <a href="#" style="font-size:15px;" class="dropdown-link"><i class="fa fa-bars"></i></a>
                            <ul style="display:none;" class="options dropdown-container">    
                                <li>
                                    <i class="fa fa-file-pdf-o" style="color:#f00;"></i>
                                    <a href="<?= $encrypted_url.'&export_type=pdf' ?>" target="_blank">Imprimir A4</a>
                                </li>
                                <li>
                                    <i class="fa fa-file-pdf-o" style="color:#00f;"></i>
                                    <a href="<?= $encrypted_url.'&export_type=ticket' ?>" class="link_print_ticket" target="_blank">Imprimir Ticket</a>
                                </li>
                                
                                <li>
                                    <i class="fa fa-file-code-o" style="color:#777;"></i>
                                    <a href="<?= $encrypted_url.'&export_type=xml' ?>" download="<?= $document->customer_doc.'-'.$document->serie.'-'.$document->correlative.'_XML.xml' ?>">Descargar XML</a>
                                </li>
                                
                                <li>
                                    <i class="fa fa-at" style="color:#00f;"></i>
                                    <a href="#" class="link_show_send_mail" data-url="<?= $encrypted_url.'&export_type=email' ?>">Enviar mail</a>
                                </li>
                                @if ( $document->status == 1 )
                                    <?php if ( (strtotime(date('Y-m-d')) - strtotime(@$document->date ?: '')) / 86400 <= 7 ) { ?>                
                                    <li>
                                        <i class="fa fa-times" style="color:#f00;"></i>
                                        <a href="#" class="link_cancel_document">Anular</a>
                                        <form action="<?= url('document/cancel') ?>" method="post">
                                            @csrf                                            
                                            <input type="hidden" name="refered_document_id" value="{{ $document->id }}">
                                        </form>
                                    </li>
                                    <?php } ?>
                                    <?php if ($document->doc_type != '00') { ?>
                                        <li>
                                            <i class="fa fa-times" style="color:#f00;"></i>
                                            <a href="#" class="link_credit_document">NOTA D CREDITO</a>
                                            <form action="<?= url('document/nota') ?>" method="get">
                                                @csrf
                                                <input type="hidden" name="refered_document_id" value="{{ $document->id }}">
                                                <input type="hidden" name="ref_doc_type" value="<?= $document->doc_type ?>">
                                                <input type="hidden" name="ref_serie" value="<?= $document->serie ?>">
                                                <input type="hidden" name="ref_correlative" value="<?= $document->correlative ?>">
                                            </form> 
                                        </li>
                                    <?php } ?>                
                                @endif
                                                                                                        
                                @if($document->status == 3)                                
                                <li>
                                    <i class="fa fa-pencil" style="color:#f00;"></i>
                                    <a href="<?=  url('document/'.$document->id.'/edit') ?>">Editar</a>
                                </li>
                                @endif
                    
                    
                                @if( business()->has_guide)
                                <li>
                                    <i class="fa fa-file-code-o" style="color:#777;"></i>
                                    <a href="#" class="create-guide" data-document-id="{{$document->id}}">CREAR GUIA</a>
                                </li>
                                @endif
                            </ul>
                        </div></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="guide-modal" tabindex="-1" role="dialog" aria-labelledby="guia-modal-label" aria-hidden="true"></div>
@include('document.popup_send_email')
@include('document.popup_ticket')
@endsection