@extends('layouts.back')

@section('head')
<div class="doc-title mb-4">
    <h2>
        <span>Clientes</span>                
        <a href="#" class="btn btn-primary link_open_popup" data-url="<?= url('customer/create?type='.$type) ?>" data-target="#modal_customer_create">Nuevo</a>                
    </h2>

    <div>
        <form action="<?= url('customer') ?>">
          <select class="btn filter" name="type" onchange="this.parentNode.submit();">
            <option value="natural" <?= $type == 'natural' ? 'selected' : '' ?>>PERSONAS</option>
            <option value="juridico" <?= $type == 'juridico' ? 'selected' : '' ?>>EMPRESAS</option>
          </select>
        </form>
    </div>
</div>
@endsection

@section('body')
<div class="row" style="text-transform:uppercase;"><div class="table-responsive">
    <table class="report table table-sm table-hover">
      <thead><tr style="text-align:center;">
        
        <th style="text-align:left;">Nombre completo</th>
        <th style="width:120px;">Tipo</th>
        <th style="width:200px;">Documento</th>                
        <th style="text-align:left;">Dirección</th>
        <th style="text-align:left;width:260px;">E-mail</th>
        <th style="width:80px;">Acción</th>
      </tr></thead>
      <tbody>
        <?php
            $valor=[
                1 => "DNI",
                4 => "CEX",
                6 => 'RUC',
                7 => "PAS"
            ];
        ?>
        @foreach ($customers as $customer)
          <tr style="text-align:center;">
          <?php if ($type == 'natural') { ?>
            <td style="text-align:left;"><a href="{{url('customer/'.$customer->customer_id.'/documents')}}"><?= strtoupper($customer->fullname) ?></a></td>            
            <td class="text-center"> {{@$valor[$customer->doc_type]}} </td>
            <td class="text-center"><?= $customer->doc ?></td>
          <?php } else { ?>
            <td style="text-align:left;"><a href="{{url('customer/'.$customer->customer_id.'/documents')}}"><?= strtoupper($customer->legal_name) ?></a></td>
            <td class="text-center"> RUC </td>
            <td class="text-center"><?= $customer->ruc ?></td>
          <?php } ?>
          <td style="text-align:left;"><?= strtolower($customer->address) ?></td>
          <td style="text-align:left;text-overflow:ellipsis;white-space:nowrap;overflow:hidden;"><?= strtolower($customer->email) ?></td>
          <td>
            
            <form action="<?= url("customer/delete?id={$customer->customer_id}&type={$type}") ?>" method="post">
              @csrf
              <a href="#" class="link_open_popup" data-url="<?= url("customer/edit?id={$customer->customer_id}") ?>" data-target="#modal_customer_edit"><i class="fas fa-pencil-alt"></i></a>
              <a href="#" class="link_delete_row_data"><i class="fa fa-trash"></i></a>              
            </form>
            
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
</div></div>

@endsection
