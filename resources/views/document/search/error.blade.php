@extends('layouts.front')

@section('content')
<section class="hero text-light text-center">
    <div class="container-sm">
        <div class="hero-inner">
            <h1 class="hero-title h2-mobile mt-0" style="color:#f8f9fa;">Sin coincidencias</h1>
            <p class="hero-paragraph is-revealing">
                El documento que solicita no ha sido encontrado. Por favor revise sus datos de entrada e intente de nuevo.
            </p>
            <a href="{{ url('document/search')}}" class="button button-secondary">Regresar</a>
        </div>
    </div>
</section>
@endsection
