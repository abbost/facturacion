@extends('layouts.front')

@section('content')
<section class="hero text-center">
    <div class="container-sm">
        <div class="hero-inner">
            <h1 class="hero-title h2-mobile mt-0" style="color:#f8f9fa;">Búsqueda de documentos</h1>
            <form class="custom white-bg" action="<?= url('document/search') ?>" method="post" style="color:#343a40;font-weight:bold; -webkit-box-shadow: 2px 2px 12px 0px rgba(0,0,0,0.75);-moz-box-shadow: 2px 2px 12px 0px rgba(0,0,0,0.75);box-shadow: 2px 2px 12px 0px rgba(0,0,0,0.75);">
                @csrf
                <div>
                    <label>Tipo de documento</label>
                    <select class="form-control form-fix" name="doc_type" style="width: 100%">
                        <option value="F">Factura</option>
                        <option value="B">Boleta</option>
                        <option value="NC">Nota de Credito</option>
                    </select>
                </div>
                <div>
                    <label>RUC Emisor:</label>
                    <input type="number" class="form-control form-fix" name="emisor_doc" style="width: 100%" maxlength="11">
                </div>
                <div>
                    <label>Nro. Serie:</label>
                    <input type="text" class="form-control form-fix" name="serie" style="width: 100%;text-transform: uppercase;" maxlength="4">
                </div>
                <div>
                    <label>Correlativo:</label>
                    <input type="number" class="form-control form-fix" name="correlative" style="width: 100%;">
                </div>
                <div>
                    <label>Fecha:</label>
                    <input type="date" class="form-control form-fix" name="date" style="width: 100%">
                </div>
                <div>
                    <label>Monto total:</label>
                    <input type="text" class="form-control form-fix" name="total" style="width: 100%">
                </div>
                <div style="text-align:center;">
                    <button type="submit" class="button button-secondary button-shadow">BUSCAR DOCUMENTO</button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
