@extends('layouts.front')

@section('content')
<section class="hero text-light text-center">
    <div class="container-sm">
        <div class="hero-inner">
            <h1 class="hero-title h2-mobile mt-0">Resultado de la búsqueda</h1>
            <p class="hero-cta is-revealing">
                <a href="{{ $url.'xml' }}" target="_blank" class="button button-secondary button-shadow" download>DESCARGAR XML</a>
                <a href="{{ $url.'cdr' }}" target="_blank" class="button button-secondary button-shadow" download style="margin-left:20px;">DESCARGAR CDR</a>
            </p>
            <iframe style="width:100%;max-width:800px;height:500px;margin-bottom:40px;border:none;" src="{{ $url.'pdf' }}"></iframe>
            <?php if(false) { ?><div style="margin-bottom:40px;">
                <img style="margin:auto;" src="<?= $url.'qr' ?>" alt="Código qr del documento">
            </div><?php } ?>
        </div>
    </div>
</section>
@endsection
