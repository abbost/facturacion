@extends('layouts.back')
@section('body')
<div class="doc-body" >
    <form id="form_reg_document"  class="container-doc" action="<?= url('document/'.$document->id.'/edit') ?>" method="post" autocomplete="off">
        @csrf
        <div class="doc-one">
            <div class="mb-5">
                <input type="hidden" name="document_id" value="<?= $document->id ?>">                
                <h3 class="tile-title mb-3 f-xs-18">EDITAR VENTA<i class="fa fa-shopping-cart pl-3" aria-hidden="true"></i></h3>
                
                <div class="toggle">
                    <label>                            
                    <input type="checkbox" onchange="change_search_mode(this)" <?= $search_mode == 'code'? 'checked':''?> >
                    <span class="button-indecator d-inline-block"></span>
                    <!-- <i class="fas fa-barcode f-20"></i> -->COD BARRAS
                    </label>
                </div>           

                <div>
                    <span class="fa fa-search form-control-feedback text-center" style="position: absolute;width: 2.375rem; height: 2.375rem; line-height: 2.375rem;"></span>
                    <input id="product_name" class="form-control" style="padding-left: 2.375rem;;" autofocus> 
                </div>
                

                <div class="row mb-5" id="add-button-wrapper" style="margin-top: 20px">

                    <div class="col-md-4" style="text-align:center;">                        
                        <label style="display:block;text-align:left;" class="mb-0"><b>Cantidad</b></label>
                        <div class="input-group">
                        <span class="input-group-btn" style="margin-left:auto;">
                            <button class="btn btn-secondary btn-quantity" type="button" onclick="document.getElementById('item_quantity').value--;if(document.getElementById('item_quantity').value<=0)document.getElementById('item_quantity').value=1;" >-</button>
                        </span>
                        <input type="number" class="form-control" id="item_quantity" min="1" max="5" value="1" style="width:calc(100% - 66px);text-align:center;">
                        
                        <span class="input-group-btn" style="margin-right:auto;" min="1">
                            <button class="btn btn-secondary btn-quantity" data-max=5 type="button" onclick="document.getElementById('item_quantity').value++;">+</button>
                        </span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label style="display:block;text-align:left;" class="mb-0"><b>Precio (S/.)</b></label>
                        <input type="number" class="form-control" id="item_unit_price" value="0.00" step="0.01" min="1" style="text-align:center;" oninput="validity.valid||(value='');">
                    </div>
                    <div class="col-md-4 text-right">
                        <label style="display:block;text-align:center;" class="mb-0">&nbsp;</label>
                        <button type="button" id="btn_add_item" class="btn btn-primary">AGREGAR AL DETALLE<i class="fa fa-shopping-cart" style="padding: 0 0 4px 5px"></i></button>
                        
                    </div>
                </div>

                <!--Table-->
                <div id="cart_detail_wrapper" class="col-md-12" style="position:static;">
                    <div class="row white-bg p-b-lg-20">
                        <div class="table-responsive">
                            <table id="list_details" class="table table-sm table-striped m-b-lg-0" >
                                <thead>
                                    <tr class="text-center">
                                        <th style="width:10px;">Código</th>
                                        <th style="width:50px;">Cantidad</th>
                                        <th class="col text-center">Descripción</th>
                                        <th style="width:100px;">Precio</th>
                                        <th style="width:0px;" class="p-lg-0"></th> <!-- no borrar, tare el hidden el impuesto-->
                                        <th style="width:0px;" class="p-lg-0"></th> <!-- no borrar, trae el hidden del subtotal-->
                                        <th style="width:100px;">Total</th>
                                        <th style="width:80px;"></th>
                                    </tr>
                                </thead>
                                <tbody style="color:black">
                                    @foreach($document->details as $i => $detail)
                                        <tr class="text-center" data-index={{$i}}>
                                            <td>                                        
                                                <input type='hidden' class='detail_id' name='products[<?= $i ?>][item_id]' value='<?= $detail->external_item_id ?>'>
                                                <input type='hidden' class='detail_onu_product_code' name='products[<?= $i ?>][onu_product_code]' value='<?= $detail->onu_product_code ?>'>
                                                <?= $detail->onu_product_code ?>
                                            </td>
                                            <td>
                                                <?= $detail->quantity ?>
                                                <input type='hidden' class='detail_quantity' name='products[<?= $i ?>][quantity]' value='<?= $detail->quantity ?>'>
                                            </td>
                                            <td style="text-align:left;">
                                                <?= $detail->description ?>
                                                <input type='hidden' class='detail_description' name='products[<?= $i ?>][description]' value='<?= $detail->description ?>'>
                                            </td>
                                            <td>
                                                <?= $detail->unit_price ?>
                                                <input type='hidden' class='detail_price' name='products[<?= $i ?>][price]' value='<?= $detail->unit_price ?>'>
                                            </td>
                                            <td>
                                                <input type='hidden' class='detail_sub_total' name='products[<?= $i ?>][sub_total]' value='<?= $detail->sub_total ?>'>
                                            </td>
                                            <td>
                                                <input type='hidden' class='detail_tax' name='products[<?= $i ?>][tax]' value='<?= $detail->tax ?>'>
                                            </td>
                                            <td>
                                                <?= $detail->total ?>
                                                <input type='hidden' class='detail_total' name='products[<?= $i ?>][total]' value='<?= $detail->total ?>'>
                                            </td>                            
                                            <td><a href='#' class='link_delete_row' style='color:#f00;'><i class='fa fa-trash'></i></a></td>
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--Total-->
                <div class="row text-right mt-4">
                    <div class="col-md-12 ">
                        <div class="d-inline">Sub Total</div><span class="ml-3">S/.</span>
                        <input class="text-right border border-white" style="width:80px" id="details_sub_total" type="text" name="sub_total" placeholder="0.00"  readonly value="{{$document->sub_total}}">
                    </div>
                    <div class="col-md-12 ">
                        <div class="d-inline">Impuesto</div><span class="ml-3">S/.</span>
                        <input class="text-right border border-white" style="width:80px" id="details_tax" type="text" name="tax" placeholder="0.00" readonly value="{{$document->tax}}">
                    </div>
                    <div class="col-md-12 " style="font-size: 20px">
                        <div class="d-inline">Total</div><span class="ml-3">S/.</span>
                        <input class="text-right border border-white" style="width:80px" id="details_total" type="text" name="total" placeholder="0.00" readonly value="{{$document->total}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="toggle">
                          <label>                            
                            <input type="checkbox" checked onchange="$('#note').toggle()"><span class="button-indecator d-inline-block"></span>
                            <span>Agregar comentarios</span>
                          </label>
                        </div>                        
                        <textarea class="form-control" id="note" name="note" style="width: 100%" rows="5" >{{ $document->note }}</textarea>
                    </div>
                </div>
            </div>
        </div>        

        
        
        <div class="doc-two p-lg-10" style="background: #eee; overflow-y:scroll; overflow-x: hidden; height: calc(100vh - 90px); border-radius: 10px;">                        
                     
            <div class="m-b-lg-10">
                @if($document->doc_type == '03')
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" checked name="document_type" id="inlineRadio1" value="03" >
                    <label class="form-check-label f-16" for="inlineRadio1">Boleta</label>
                </div>
                @endif
                @if($document->doc_type == '01')
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" checked name="document_type" id="inlineRadio2" value="01" >
                    <label class="form-check-label f-16" for="inlineRadio2">Factura</label>
                </div>
                @endif
                @if($document->doc_type == '00')
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" checked name="document_type" id="inlineRadio3" value="00" >
                    <label class="form-check-label f-16" for="inlineRadio3">Proforma</label>
                </div>
                @endif
                <input id="doc_label" type="text" value="{{$document->serie.'-'.$document->correlative}}" class="m-l-lg-10 text-center" disabled style="width: 120px;">
            </div>
            
            <div class="tab-content p-t-lg-10" style="">
                <input type="hidden" name="serie" value="<?= $document->serie ?>">
                <input type="hidden" name="correlative" value="{{ $document->correlative }}">                
                        
                <div class="row">
                    <div class="col-md-4" >Fecha</div>
                    <div class="col-md-8">
                        <input class="form-control-sm" type="date" name="date" value="<?= date('Y-m-d') ?>" required>
                    </div>                        
                </div>

                <div class="row">                    
                    <div class="col-md-4" >
                        @if($document->doc_type == '01')
                        <select class="form-control-sm" id="receptor_doc_type" name="receptor_doc_type" style="width: 100%">                            
                            <option value="RUC" data-limit="11">RUC</option>                            
                        </select>                     
                        @endif                    
                        @if($document->doc_type == '03')
                        <select class="form-control-sm" id="receptor_doc_type" name="receptor_doc_type" style="width: 100%">
                            <option value="DNI" data-limit="8" selected>DNI</option>                            >
                            <option value="CEX" data-limit="20">CARNÉ DE EXTRANJERIA</option>
                            <option value="PAS" data-limit="20">PASAPORTE</option>
                        </select>                     
                        @endif
                    </div>
                    <div class="col-md-8" >
                        <!-- <select id="receptor_doc" name="receptor_doc" class="form-control"  style="width: 100%"></select> -->
                        <div class="input-group">
                            <input type='text' id="receptor_doc" class='flexdatalist form-control' value="{{$document->customer_doc}}" placeholder='Número de documento' data-search-in="doc" data-min-length='1' name='receptor_doc'>
                            <span class="input-group-btn" title="Buscar en SUNAT">
                                <button class="btn btn-defaul white" type="button" onclick="ws_search_person()" style="">
                                    <i class='fa fa-search black'></i>
                                    <img src="{{asset('img/sunat_logo.png')}}" width="20">
                                </button>
                            </span>
                        </div>                        
                    </div>
                </div>
                    
                <div class="row">
                    <div class="col-md-4"><label>Nombre</label></div>
                    <div class="col-md-8">
                        <input id="receptor_name" class="flexdatalist form-control-sm" type="text" name="receptor_name" value="{{$document->customer_name}}" data-search-in="fullname" required>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4" style="">Dirección</div>
                    <div class="col-md-8">
                        <input id="receptor_address" class="form-control-sm" type="text" name="receptor_address" value="{{$document->customer_address}}" required>
                    </div>                    
                </div>   
                
                <?php if(@$business->has_contact){?>
                    <div class="row">
                        <div class="col-md-4">Contacto <a href="#" id="edit-contact-link" style="display: none;"><i class="fas fa-pencil-alt"></i></a>
                        </div>
                        <div class="col-md-8 ">                        
                            <select id="contact_customer" name="contact_id" class="form-control" style="width: 100%">
                                <option value="9">{{$document->contact_name.' - '.$document->contact_address}}</option>            
                            </select>
                            <input type="hidden" id="contact_name" name="contact_name" value="{{ $document->contact_name }}">
                            <input type="hidden" id="contact_address" name="contact_address" value="{{ $document->contact_address }}">
                            <input type="hidden" id="customer_id_parent" value="{{$document->customer_id}}">
                        </div>                    
                    </div>   
                    <div class="row">
                        <div class="col-md-4">Guía N° </div>
                        <div class="col-md-8 ">                        
                            <input type="text" class="form-control" name="guide" value="{{$document->guide}}">
                        </div>
                    </div>       
                <?php } ?>
                
                <div id="mas_opciones">

                <div class="row">
                    <div class="col-md-4">                        
                        <div class="toggle toggle-md">
                          <label>                            
                            <input type="checkbox" onchange="$('#orden_de_compra').toggle()"><span class="button-indecator d-inline-block"></span>
                            <span>Orden compra</span>
                          </label>
                        </div>    
                    </div>
                    <div class="col-md-8" >
                        <div id="orden_de_compra" style="display: none;">
                            <input class="form-control-sm" type="text" name="order_reference" value="{{ $document->order_reference }}">
                        </div>
                    </div>                    
                </div>
        
                <!-- se oculta porque no se va enviar el correo -->
                <!-- <div class="row">
                    <div class="col-md-4">
                        <div class="toggle">
                        <label>                            
                            <input type="checkbox" onchange="$('#mail_panel').toggle()"><span class="button-indecator d-inline-block"></span>
                            <span>Enviar email </span>
                        </label>
                        </div>    
                    </div>
                    <div class="col-md-8">
                        <div id="mail_panel" style="display:none;">
                            <input class="form-control-sm" type="text" name="receptor_email" value="">
                        </div>
                    </div>
                </div> -->
                            
                </div> <!-- mas opciones -->



                <!--MEDIO DE PAGO-->                
                <hr>

                <div class="row">
                    <div class="col-md-4">Efectivo</div>
                    <div class="col-md-8">
                        <select class="form-control" name="payment_means">
                            <option value="cash" <?= $document->payment->means == 'cash' ? 'selected':''?> >EFECTIVO</option>
                            <option value="card" <?= $document->payment->means == 'card' ? 'selected':''?>>TARJETA</option>
                            <option value="transfer" <?= $document->payment->means == 'transfer' ? 'selected':''?>>TRANSFERENCIA</option>
                            <option value="credit" <?= $document->payment->means == 'credit' ? 'selected':''?>>CREDITO</option>
                        </select>
                    </div>
                </div>                

                @if(@$hasContact)

                <div class="row">
                    <div class="col-md-4">Recibido <small class="purple">(opcional)</small></div>
                    <div class="col-md-8 ">
                        <input type="number" name="" id="payment" class="form-control-sm text-center" onkeyup="$('#turned').val( $('#payment').val() - $('#details_total').val() )" >                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">Vuelto <small class="purple">(opcional)</small></div>
                    <div class="col-md-8">
                        <input type="number" name="" id="turned" class="form-control-sm text-center" readonly style="border: none;">  
                    </div>
                </div>
                
                @endif


                <div class="row" style="margin-top: 10px">
                <!--
                    <div class="col-md-12">                        
                        <div class="toggle">
                            <label>                            
                                <input type="checkbox" checked=""  name="print_ticket"><span class="button-indecator d-inline-block"></span>
                                <span>Imprimir <?= business()->print_option ?> </span>
                            </label>
                        </div>
                    </div>
                -->
                    <div class="col-md-12 text-center">
                        <button id="btn_reg_document" type="button" class="btn btn-secondary f-20">
                            REGISTRAR
                        </button>
                    </div>
                </div>                                
            </div> <!-- tab content --> 
        </div><!-- col 2 -->
        
    </form>
</div>
<div id="modal_contact" class="modal fade"></div>
<div id="iframe_ticket_wrapper"></div>
@endsection

