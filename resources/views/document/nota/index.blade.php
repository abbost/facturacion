@extends('layouts.back')

@section('head')
<div class="doc-title mb-4">
    <h2>
        <span>Notas de crédito</span>
    </h2>
</div>
@endsection

@section('body')
<div class="row justify-content-lg-center">
    <div class="col-md-12" style="margin-bottom:20px;text-align:center;">
        <form class="fom-dates" action="<?= url()->full() ?>" method="get">
        <div class="m-left-10">Desde:<input type="date" name="date_from" class="btn filter mb-2 ml-2" value="<?= dates('from') ?>"></div>
        <div class="m-left-10">Hasta:<input type="date" name="date_to" class="btn filter mb-2 ml-2" value="<?= dates('to') ?>"></div>
        <div class="m-left-10">Tipo:<select class="btn filter mb-2 ml-2" name="type">
            <option value="P" <?= $type == 'P' ? 'selected' : '' ?>>PROFORMAS</option>
            <option value="F" <?= $type == 'F' ? 'selected' : '' ?>>FACTURAS</option>
            <option value="B" <?= $type == 'B' ? 'selected' : '' ?>>BOLETAS</option>
            <option value="NC" <?= $type == 'NC' ? 'selected' : '' ?>>NOTAS DE CRÉDITO</option>
            <option value="*" <?= $type == '*' ? 'selected' : '' ?>>TODOS</option>
        </select></div>
        <div class="m-left-10"><button type="submit" class="btn filter btn-primary"><i class="mb-1 fa fa-search"></i> Buscar</button></div>
        </form>
    </div>

    <div class="col-md-12">
        <div class="row white-bg table-responsive" >
            <table class="table table-sm table-hover f-12" id="list_documents" style="text-transform:uppercase; max-width: none; width: max-content !important; margin: 0 auto;">
                <thead><tr  class="text-center">
                    <th style="">Serie</th>                    
                    <th width="50">#</th>                    
                    <th>Tipo</th>                    
                    <th width="200">Cliente</th>
                    <th >Documento</th>    
                    <th width="300">Descripción</th>                                          
                    <th >TOTAL</th>
                    <th width="100">Fecha</th>
                    <th >Estado</th>                                
                    <th ></th>
                </tr></thead>
                <tbody>
                    <?php
                    $can_manage = has_permission('document.manage');                    
                    foreach ($documents as $document) {                        
                        $encrypted_url = $url . $document->encrypted_id;
                        $status = [
                            ['name'=>'RECHAZADO',   'badge'=>'badge-danger'],
                            ['name'=>'ACEPTADO',    'badge'=>'badge-success'],
                            ['name'=>'ANULADO',     'badge'=>'badge-secondary'],
                            ['name'=>'PENDIENTE',   'badge'=>'badge-info'],
                            ['name'=>'ANULADO', 'badge'=>'badge-secondary']   // anulado con nota de credito
                        ];                    
                    ?>
                    <tr class="<?= $document->status == 2 || $document->status == 4 ? 'gray-bg':'' ?>">
                        <td><strong style="color:#000;"><?= $document->serie ?></strong></td>
                        <td class="text-center"><strong style="color:#000;"><?= $document->correlative ?></strong></td>                        
                        <td><?= $type == '*' ? @['00'=>'PROF','01'=>'FACT', '03'=>'BOL', '07'=>'NOTA'][$document->doc_type] : '' ?></td>                        
                        <td><?= $document->refered ? '['.$document->refered->serie.'-'.$document->refered->correlative.']' : '' ?> <?= strtoupper($document->customer_name) ?></td>
                        <td class="text-center">
                            <?= $document->customer_doc ?>
                        </td>        
                        <td>
                            @foreach($document->details as $detail)
                                <div>{{ $detail->description }} | <span class="PEN">{{$detail->total}}</span></div>
                            @endforeach
                        </td>
                        
                        <td class="text-right PEN"><?= number_format($document->total,2) ?></td>
                        <td class="text-right"><?= date('d/m h:i.a',strtotime($document->date)) ?></td>
                        <td>
                            <div style="text-align:center;">
                            <span class="badge badge-pill {{ $status[$document->status]['badge'] }} ">
                                @if ($document->status == 3)
                                <i class="fa fa-refresh"></i>
                                <a href="#" class="link_resend_document white">PENDIENTE</a>
                                <form action="<?= url('document/resend') ?>" method="post">
                                    @csrf
                                    <input type="hidden" name="document_id" value="<?= $document->id ?>">
                                </form>
                                @else 
                                    {{ $status[$document->status]['name'] }}
                                @endif
                            </span>
                            </div>
                        </td>
                        
                        <td>
                        <div style="text-align:center;font-size:13px;position:relative;" class="dropdown">
                            <a href="#" style="font-size:15px;" class="dropdown-link"><i class="fa fa-bars"></i></a>
                            <ul style="display:none;" class="options dropdown-container">    
                                <li>
                                    <i class="fa fa-file-pdf-o" style="color:#f00;"></i>
                                    <a href="<?= $encrypted_url.'&export_type=pdf' ?>" target="_blank">Imprimir A4</a>
                                </li>
                                <li>
                                    <i class="fa fa-file-pdf-o" style="color:#00f;"></i>
                                    <a href="<?= $encrypted_url.'&export_type=ticket' ?>" class="link_print_ticket" target="_blank">Imprimir Ticket</a>
                                </li>
                                
                                <li>
                                    <i class="fa fa-file-code-o" style="color:#777;"></i>
                                    <a href="<?= $encrypted_url.'&export_type=xml' ?>" download="<?= $document->customer_doc.'-'.$document->serie.'-'.$document->correlative.'_XML.xml' ?>">Descargar XML</a>
                                </li>                                                                
                                <li>
                                    <i class="fa fa-at" style="color:#00f;"></i>
                                    <a href="#" class="link_show_send_mail" data-url="<?= $encrypted_url.'&export_type=email' ?>">Enviar mail</a>
                                </li>
                            </ul>
                        </div></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="guide-modal" tabindex="-1" role="dialog" aria-labelledby="guia-modal-label" aria-hidden="true"></div>
@include('document.popup_send_email')
@include('document.popup_ticket')
@endsection