@extends('layouts.back')

@section('head')
<div class="doc-title">
    <div class="pl-3">
        <h2>Nueva Nota de Crédito <span style="font-size:24px;">{{ $serie }} - {{ $correlative }}</span></h2>
    </div>
</div>
@endsection

@section('body')
<div class="doc-body" style="padding: 0 18px">
    <form id="form_reg_document" action="<?= url('document/nota') ?>" method="post" class="row">
    @csrf
    <div class="doc-one">
        <div class="container mb-5">
            <input type="hidden" name="doc_type" value="NC">
            <input type="hidden" name="refered_document_id" value="{{ @$refered->id }}">
            <input type="hidden" name="doc_reference_type" value="{{ @$refered->doc_type }}">
            <input  type="hidden" name="doc_ref_serie" value="<?= @$refered->serie ?>">
            <input  type="hidden" name="doc_ref_correlat" value="<?= @$refered->correlative ?>">
            <input class="form-control" type="hidden" name="correlative" required readonly value="{{$correlative}}">
            <input class="form-control" type="hidden" name="serie" style="width:100%;text-transform:uppercase;" value="<?= $serie ?>" maxlength="5" required>
            
            <h5 class="tile-title pt-4 mb-3">PRODUCTOS<i class="fa fa-home pl-3" aria-hidden="true"></i></h5>

            <div id="cart_detail_wrapper" class="col-md-12" style="position:static;">        
                <div class="row white-bg p-b-lg-20">
                    <div class="table-responsive">
                        <table id="list_details" class="table table-striped m-b-lg-0">
                            <thead>
                                <tr class="text-center">
                                    <th style="width:10px;">Código</th>
                                    <th style="width:50px;">Cantidad</th>
                                    <th class="col text-center">Descripción</th>
                                    <th style="width:80px;">Precio Uni.</th>
                                    <th style="width:80px;">Total Bruto</th>
                                    <th style="width:80px;">Impuesto</th>
                                    <th style="width:80px;">Total</th>
                                    <th style="width:80px;"></th>
                                </tr>
                            </thead>
                            <tbody style="color:black">
                                @if( isset($refered->details))
                                @foreach($refered->details as $i => $detail)
                                <tr class="text-center">
                                    <td>
                                        <input type='hidden' class='detail_id' name='products[<?= $i ?>][id]' value='0'>
                                        <?= $detail->external_item_id ?>
                                        <input type='hidden' class='detail_onu_product_code' name='products[<?= $i ?>][onu_product_code]' value='<?= $detail->onu_product_code ?>'>
                                    </td>
                                    <td>
                                        <?= $detail->quantity ?>
                                        <input type='hidden' class='detail_quantity' name='products[<?= $i ?>][quantity]' value='<?= $detail->quantity ?>'>
                                    </td>
                                    <td style="text-align:left;">
                                        <?= $detail->description ?>
                                        <input type='hidden' class='detail_description' name='products[<?= $i ?>][description]' value='<?= $detail->description ?>'>
                                    </td>
                                    <td>
                                        <?= $detail->unit_price ?>
                                        <input type='hidden' class='detail_price' name='products[<?= $i ?>][price]' value='<?= $detail->unit_price ?>'>
                                    </td>
                                    <td>
                                        <?= $detail->sub_total ?>
                                        <input type='hidden' class='detail_sub_total' name='products[<?= $i ?>][sub_total]' value='<?= $detail->sub_total ?>'>
                                    </td>
                                    <td>
                                        <?= $detail->tax ?>
                                        <input type='hidden' class='detail_tax' name='products[<?= $i ?>][tax]' value='<?= $detail->tax ?>'>
                                    </td>
                                    <td>
                                        <?= $detail->total ?>
                                        <input type='hidden' class='detail_total' name='products[<?= $i ?>][total]' value='<?= $detail->total ?>'>
                                    </td>                            
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="row white-bg p-b-lg-20 p-t-lg-20">
                <div class="col-md-4 text-center">
                    <label style="display:block;">Total Bruto</label>
                    <input class="darkInput text-center" id="details_sub_total" type="text" name="sub_total"  value="<?= @$refered->sub_total ?>" readonly>
                </div>
                <div class="col-md-4 text-center">
                    <label style="display:block;">Total Impuesto (18%)</label>
                    <input class="darkInput text-center" id="details_tax" type="text" name="tax" value="<?= @$refered->tax ?>" readonly>
                </div>
                <div class="col-md-4 text-center">
                    <label style="display:block;">Total a Pagar</label>
                    <input class="darkInput text-center" id="details_total" type="text" name="total" value="<?= @$refered->total ?>" readonly>
                </div>
            </div>
        </div>
        <!--termina el container-->
    </div>
    <!--empieza el doc-two-->

    <div class="doc-two">
        <div class="container">
            <!-- DATOS DEL CLIENTE-->
            <h5 class="tile-title pt-4">DATOS DEL CLIENTE<i class="fa fa-home pl-3" aria-hidden="true"></i></h5> 
            <div class="row">
                <div class="col-md-4 text-left">Fecha</div>
                <div class="col-md-8 mb-3">
                    <input class="form-control date" type="date" name="date" value="<?= date('Y-m-d') ?>" required readonly>
                </div>

                <div id="customer_wrapper" style="padding-left:15px">
                    <div class="row">

                        <div class="col-md-4 mb-3 text-left" style="line-height:37px;">Documento</div>
                        <div class="col-md-8" style="display:inline-flex;"> 
                            <input type="hidden" name="receptor_id" value="{{@$refered->customer_id}}">
                            <select id="receptor_doc_type" class="form-control" style="width:80px;margin-right:5px;" name="receptor_doc_type" readonly>
                                <option value="{{@$refered->customer_doc_name}}" data-limit=" <?= @$refered->customer_doc_name == 'RUC' ? 11 : 8 ?>">{{@$refered->customer_doc_name}}</option>
                            </select>
                            <div class="input-group" style="width:calc(100% - 70px);">
                                <input class="form-control" type="text" id="receptor_doc" name="receptor_doc" required readonly value="<?= @$refered->customer_doc ?>">
                                <span class="input-group-btn mb-3">
                                    <button id="customer_data" class="btn btn-primary" type="button"><i class="fa fa-search" style="padding:0 0 3px 0"></i></button>
                                </span>
                            </div>
                        </div>    




                        <div class="col-md-4 text-left">{{@$refered->doc_type == "03" ? 'Nombres y Apellidos' : 'Razón Social'}}</div>
                        <div class="col-md-8">
                            <input class="form-control mb-3" type="text" name="receptor_name" required value="<?=@$refered->customer_name ?>" readonly>
                        </div>
                        <div class="col-md-4 text-left">Dirección</div>
                        <div class="col-md-8">
                            <input class="form-control mb-3" type="text" name="receptor_address" required value="<?=@$refered->customer_address ?>" readonly>
                        </div>
                        </div>
                    </div>
                </div>
                <!--MOTIVO DE ANULACION-->
                <h5 class="tile-title mt-3">MOTIVO DE ANULACION<i class="fa fa-home pl-3" aria-hidden="true"></i></h5>
                <div class="col-md-12 text-center mb-2 mt-2">
                    <input type="checkbox" id="print_ticket" name="print_ticket" value="1" checked="checked">
                    <label for="print_ticket">Imprimir <?= $business->print_option ?></label>
                </div> 
                <div class="col-md-12 mb-2">
                    <select class="form-control" style="width: 100%;" name="doc_response_code" required>
                        <option value="01" selected="selected">Anulación de la operacion</option>
                        <option value="02">Anulación por error en el RUC</option>
                        <option value="03">Corrección en la descripción</option>
                        <option value="04">Descuento por Item</option>
                        <option value="05">Devolución total</option>
                        <option value="07">Devolución parcial</option>
                        <option value="08">Bonificación</option>
                        <option value="09">Disminución en el valor</option>
                    </select>
                </div>
                <div class="col-md-12 mt-2">
                    <label>Comentarios</label>
                    <input class="form-control" type="text" style="width:100%" name="subject" required value="ERROR">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-3 mb-1">
                <button id="btn_reg_document" type="button" class="btn btn-secondary pull-right f-20">
                    REGISTRAR NOTA DE CREDITO
                </button>
            </div> 
        </div>
    </div>
    </form>
</div>
<div id="iframe_ticket_wrapper"></div>
@endsection
