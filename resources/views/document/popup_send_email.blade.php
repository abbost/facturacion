<div id="modal_send_email" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form_send_email" action="" method="get">
                <div class="modal-header">
                    <h4 class="modal-title">Enviar email de documento</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Email de destinatario:</label>
                            <input type="text" name="email" class="form-control">
                        </div>                        
                    </div>
                </div>
                <div class="modal-footer justify-content-lg-between">
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>