<div class="modal-dialog">
    <div class="modal-content">
        <form action="<?= url('provider/create') ?>" method="post">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title"><b>Nuevo Proveedor</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" id="receptor_doc_type" value="RUC">
                    <div class="col-md-12 mb-2">
                        <label><b>RUC<i class="fa fa-lock" aria-hidden="true" style="padding: 0 0 0 5px; font-size:20px"></i></b></label>
                        <div class="input-group">
                            <input type="text" id="receptor_doc" name="ruc" class="form-control mb-2" maxlength="11">
                            <span class="input-group-btn">
                                <button class="btn btn-defaul white" type="button" onclick="ws_search_person()" style="background: var(--main-color)">
                                    <i class='fa fa-search'></i>SUNAT</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>Razón Social<i class="fa fa-lock" aria-hidden="true" style="padding: 0 0 0 5px; font-size:20px"></i></b></label>
                        <input type="text" id="receptor_name" name="legal_name" class="form-control" autocomplete="off">
                    </div>
                    
                    <div class="col-md-12 mb-2">
                        <label><b>Teléfono<i class="fa fa-lock" aria-hidden="true" style="padding: 0 0 0 5px; font-size:20px"></i></b></label>
                        <input type="text" name="phone" class="form-control" autocomplete="off">
                    </div>

                    <div class="col-md-12 mb-2">
                        <label><b>Dirección<i class="fa fa-lock" aria-hidden="true" style="padding: 0 0 0 5px; font-size:20px"></i></b></label>
                        <input type="text" name="address" class="form-control" autocomplete="off">
                    </div>

                    <div class="col-md-12 mb-2">
                        <label><b>E-mail<i class="fa fa-lock" aria-hidden="true" style="padding: 0 0 0 5px; font-size:20px"></i></b></label>
                        <input type="email" name="email" class="form-control" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
</div>