@extends('layouts.back')

@section('head')
<div class="doc-title mb-4">
    <h2>
        <span>Proveedores</span>

        <?php if ($can_manage = has_permission('provider.manage')) { ?>
        <a href="#" class="btn btn-primary link_open_popup" data-url="<?= url('provider/create') ?>" data-target="#modal_provider_create">Nuevo</a>
        <?php } ?>
    </h2>
</div>
@endsection

@section('body')
<div class="row"><div class="table-responsive">
    <table class="report table table-hover" style="text-transform:uppercase;">
        <thead><tr class="text-center">
            <th style="width: 120px;text-align:center;">Ruc</th>
            <th class="text-left">Razón social</th>
            <th style="width:150px">Teléfono</th>
            <th style="width:200px">E-mail</th>
            <th style="width:100px">Acción</th>
        </tr></thead>
        <tbody>
            <?php foreach ($providers as $provider) { ?><tr style="text-align:center;">
                <td><?= $provider->ruc ?></td>
                <td style="text-align:left;"><?= $provider->legal_name ?></td>
                <td><?= $provider->phone ?></td>
                <td><?= $provider->email ?></td>
                <td>
                    <?php if ($can_manage) { ?>
                    <form action="<?= url("provider/delete?id={$provider->id}") ?>" method="post">
                        @csrf
                        <a href="#" class="link_open_popup" data-url="<?= url("provider/edit?id={$provider->id}") ?>" data-target="#modal_provider_edit"><i class="fa fa-pencil-alt"></i></a>
                        <?php if ($provider->is_deletable()) { ?>
                        <a href="#" class="link_delete_row_data"><i class="fa fa-trash"></i></a>
                        <?php } ?>
                    </form>
                    <?php } ?>
                </td>
            </tr><?php } ?>
        </tbody>
    </table>
</div></div>
<div id="modal_provider_edit" class="modal fade"></div>
<div id="modal_provider_create" class="modal fade"></div>
@endsection
