<div class="modal-dialog">
    <div class="modal-content">
        <form action="<?= url('provider/edit') ?>" method="post">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title"><b>Editar Proveedor</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <label><b>Razón Social<i class="fa fa-lock" aria-hidden="true" style="padding: 0 0 0 5px; font-size:20px"></i></b></label>
                        <input type="text" name="legal_name" class="form-control" value="<?= $provider->legal_name ?>" autocomplete="off">
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>RUC<i class="fa fa-lock" aria-hidden="true" style="padding: 0 0 0 5px; font-size:20px"></i></b></label>
                        <input type="text" name="ruc" class="form-control" value="<?= $provider->ruc ?>" autocomplete="off">
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>Teléfono<i class="fa fa-lock" aria-hidden="true" style="padding: 0 0 0 5px; font-size:20px"></i></b></label>
                        <input type="text" name="phone" class="form-control" value="<?= $provider->phone ?>" autocomplete="off">
                    </div>
                    <div class="col-md-12 mb-2">
                        <label><b>E-mail<i class="fa fa-lock" aria-hidden="true" style="padding: 0 0 0 5px; font-size:20px"></i></b></label>
                        <input type="text" name="email" class="form-control" value="<?= $provider->email ?>" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
</div>