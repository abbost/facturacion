@extends('layouts.back')

@section('body')

<h2>Preguntas Frecuentes</h2><br>

<h4 class="question-title mt-1">¿Cuál es la normativa legal que respalda el FACTURADOR ABBOST?</h4>
<p>Usted está afiliado a la facturación electrónica en la modalidad <b>SISTEMA DE EMISION ELECTRONICA DESDE LOS SISTEMAS DEL CONTRIBUYENTE</b> la cual le permite emitir 
sus comprobantes electrónicos firmando <b>con su propio certificado digital que lleva su RUC y RAZON SOCIAL</b> </p>
<p>Esta es la diferencia con un PROVEEDOR DE SERVICIOS ELECTRONICOS (PSE), donde sus documentos saldrían firmados con el certificado del PSE.</p>
<p>Para mayor información <a href="https://orientacion.sunat.gob.pe/index.php/empresas-menu/comprobantes-de-pago-empresas/comprobantes-de-pago-electronicos-empresas/see-desde-los-sistemas-del-contribuyente" target="_blank">puede dar clic aqui</a> </p>


<h4 class="question-title mt-1">¿Quién es responsable de que mis documentos lleguen a SUNAT?</h4>
<p>El FACTURADOR ABBOST se encarga de enviar los documentos pendientes del día a las 03:00 horas los 365 días del año. Sin embargo, en ocasiones
    los servidores de SUNAT no responden a tiempo por lo que el sistema intentará enviarlos en los días posteriores.  </p>
<p>Ahora bien, <b>los negocios son responasbles directos</b> de asegurar el envío de sus documentos. Ya que en cualquier momento pueden enviar de forma manual sus documentos. Tan
    solo deben darle clic al estado <span class="badge badge-info">PENDIENTE</span> de cada documento.
</p>
<br>

<h4 class="question-title mt-1">¿Cómo enviar el documento por correo electrónico?</h4>
<p>Puede hacerlo de 2 maneras</p>
<ul>
    <li>Al momento de hacer la venta. Recuerde que esto hará que se demore 30 segundos más la confirmación de la venta </li>
    <li>En el reporte de ventas, dando clic al menú opciones <i class="fa fa-bars"></i> de cada documento encontrará la opción <b>ENVIAR MAIL</b></li>
</ul>
<br>

<h4 class="question-title mt-1">¿Cuando colocar DNI a la boleta?</h4>
<ul>
    <li>Cuando el cliente lo solicite</li>
    <li>Cuando la venta supere los S/ 700 soles</li>
</ul>
<br>


<h4 class="question-title mt-1">¿Cómo anular un documento?</h4>
<p>Para anular un documento primero debe haber sido <span class="badge badge-success">ACEPTADO</span> por SUNAT. Salvo que se trate de una PROFORMA.</p>
<p>Para anular un documento existen 2 formas de anular, con comunicación de baja y con nota de crédito:</p>
<ul>
    <li>Para la Comunicación de Baja solo se tienen 5 días calendario. Se consigue dando clic a la opción ANULAR</li>
    <li>Para la Nota de Crédito, se tiene hasta el 31 de diciembre del año en que fue emitido. Se consigue dando clic a la opción EMITIR NOTA DE CREDITO</li>
</ul>

<p>Para anular un documento diríjase al menu-><strong>Reporte</strong> y haga <strong>click</strong> izquierdo en <i class="fa fa-bars"></i> y seleccione cualquiera de las dos opciones.</p>
<img src="{{ asset('img/FAQ/anular_documentos.png')}}" alt="" class="img-fluid w-100" style="margin-bottom:20px">

<br>

<h4 class="question-title mt-1">¿Es posible seguir emitiendo facturas físicas si ya soy emisor electrónico?</h4>
<p>Solo comprobantes de contingencia. Estos podrán ser usados cuando no se pueda acceder al sistema informático. Cuando se restablezca el servicio los comprobantes físicos deberán ser ingresados
    al sistema informático.
</p>
<p>Para mas informacion haga click
<a href="http://orientacion.sunat.gob.pe/index.php/empresas-menu/comprobantes-de-pago-empresas/comprobantes-de-pago-electronicos-empresas/see-sol/comprobantes-que-se-pueden-emitir-desde-see-sol/factura-electronica-see-sol-portal/3747-preguntas-frecuentes-factura-electronica-see-sol-portal" target="_blank" rel="noopener noreferrer">aqui</a></p>
<br>

<h4 class="question-title mt-1">¿Cómo puedo visulizar todas las facturas y boletas emitidas?</h4>
<p>Dirijase al menu->Reporte</p>
<br>

<h4 class="question-title mt-1">¿Cuantos productos y clientes puedo agregar o ingresar en el sistema?</h4>
<p>Puede agregar todos los que usted desee siempre y cuando no se repitan</p>
<br>

<h4 class="question-title mt-1">¿Puedo imprimir el historial mensual de mis ventas?</h4>
<p>En <strong>Reporte</strong> puede imprimir en un excel con todos sus comprobantes, pero antes debe seleccionar las fechas que desea que imprima el sistema</p><br>
<img src="{{ asset('img/FAQ/imprimir_reporte.JPG')}}"  class="img-fluid w-100" alt="imprimir reporte de comprobantes"><br><br>
<br>

@endsection
