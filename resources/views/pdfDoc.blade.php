<html>
<style type="text/css">
    body{font-family:arial; font-size: 14px}
    .conteiner{width:100%;margin:auto;margin-top:15px;margin-bottom:20px;}
    #ident{padding:10px;text-align:center;border:2px solid black;font-size:15px;}                        
    thead th, tfoot td{border-top:1px solid #000;border-bottom:1px solid #000;text-align:center;}
    thead th, tfoot td{padding:5px;border-spacing:0;border-collapse:collapse;}
</style>
<body>
    <div class="conteiner">        
        <table cellpadding="0" cellspacing="0" style="width: 100%; border: none; margin-bottom: 20px" >
            <tr>
                <td style="width: 60%;" >
                    <div style="text-align: center">
                        <img src="<?= $url_logo ?>" style="max-height: 160px; max-width:300px">
                    </div>
                    <div style="text-align: center">
                        <div style="font-size: 20px">{{strtoupper($business->legal_name)}} </div>
                        <div style="font-size: 14px">{{strtoupper($document->office->address)}} </div>
                    </div>
                </td>
                <td style="text-align: center;">
                    <div style="border: #000 solid 1px; border-radius: 10px; font-size: 19px">
                        <div style="margin-top:10px;margin-bottom: 10px">RUC: {{$business->ruc}} </div>
                        <div style="margin-bottom: 10px">{{$document_name}} </div>
                        <div style="margin-bottom: 10px">{{$document->serie."-".$document->correlative}}</div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="height: 20px"></td>
            </tr>
            <tr>
                <td>
                    <!-- <div style="font-weight: bold">CLIENTE</div> -->
                    <div><b>{{@[1=>"DNI", 4=>"CEX", 6=>"RUC", 7=>"PAS"][$document->customer_doc_type]}} :</b>{{$document->customer_doc}} </div>
                    <div><b>Señor(es) :</b>{{strtoupper($document->customer_name)}} </div>
                    <div><b>Dirección :</b>{{strtoupper($document->customer_address)}} </div>
                </td>
                <td style="vertical-align: top">
                    <div><span style="font-weight: bold">Fecha Emisión: </span> {{date('d/m/Y H:i a',strtotime($document->date))}}</div>
                    <div><span style="font-weight: bold">Fecha Vencimiento: </span></div>
                    <div><span style="font-weight: bold">Moneda: </span> {{$document->currency == 'PEN'?'SOLES':'DOLARES'}}</div>                    
                </td>
            </tr>

            <?php if ($document->doc_type == "01" || $document->doc_type == "03") { ?>
                <tr><td colspan="2" style="padding-top: 10px"></td></tr>
                <?php if($document->order_reference <> "") { ?>
                    <tr>
                        <td colspan="2"><b>Orden de compra:</b> <?= $document->order_reference ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
            
            @if(isset($document->guide))
                <tr>
                    <td colspan="2"><b>Guía N°:</b> <?= @$document->guide?></td>
                </tr>
            @endif


            @if(isset($document->refered))
                <tr>
                    <td colspan="2">
                        <div style="font-weight: bold">DOCUMENTO MODIFICADO</div>
                        <div>
                            <span>NUMERO:</span>
                            {{$document->refered->serie."-".$document->refered->correlative}}
                        </div>
                        <div>
                            <span>MOTIVO:</span>
                            {{$document->motive}}
                        </div>
                    </td>
                </tr>
            @endif

            @if(isset($document->contact_name))
                <tr><td colspan="2"><hr></td></tr>
                <tr>
                    <td colspan="2">                        
                        <div>
                            <b>Consignado a:</b>
                            {{$document->contact_name}}
                        </div>
                        <div>
                            <b>Dirección: </b>
                            {{$document->contact_address}}
                        </div>
                    </td>
                </tr>
            @endif

        </table>
        <table cellpadding="5" cellspacing="0" border="0" style="width: 100%; border: none">
            <thead><tr>
                <th>COD</th>
                <th>CANT</th>
                <th>DESCRIPCIÓN</th>
                <th>VALOR VENTA</th>
                <th>PRECIO</th>
            </tr></thead>
            <tbody>
                @foreach($details as $detail)
                <tr style="text-align: center;">
                    <td>-</td>
                    <td>{{$detail->quantity}}</td>
                    <td style="text-align: left;">{{$detail->description}}</td>
                    <td style="text-align: right;">S/ {{number_format($detail->sub_total,2)}}</td>
                    <td style="text-align: right;">S/ {{number_format($detail->total,2)}}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr style="font-weight: bold">
                    <td colspan="3"></td>
                    <td style="text-align: right;">
                        <div>Sub Total</div>
                        <div>IGV 18%</div>
                        <div>Total</div>
                    </td>
                    <td style="text-align: right;">
                        <div>S/ {{ number_format($document->sub_total,2) }}</div>
                        <div>S/ {{ number_format($document->tax,2) }}</div>
                        <div>S/ {{ number_format($document->total,2) }}</div>
                    </td>
                </tr>
            </tfoot>
        </table>
        <div style="text-align: left; border-bottom: #000 solid 1px; padding:5px">
            SON: {{ strtoupper($total_text)}} SOLES
        </div>

        <?php if($document->note <> "") { ?>            
            <div  style="border: #000 solid 1px; margin-top: 10px; padding: 10px">
                <b>Observaciones:</b> 
                <div><?= nl2br($document->note) ?></div>
            </div>            
        <?php } ?>

        <div style="text-align: center;padding: 10px; font-size: 12px">
            Representación impresa de la {{$document_name}}. Para consultar el documento visita www.facturacionelectronicapyme.com
        </div>
        <div style="text-align:center;">
            <img id="qr_code" src="<?= $url_qr ?>" alt="Código qr del documento" style="width: 180px">
        </div>
    </div>
</body>
<script type="text/javascript">
var w = self;
document.getElementById("qr_code").onload = function () {
    window.print();
    w.close();
}</script>
</html>
