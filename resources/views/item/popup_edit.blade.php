<div class="modal-dialog">
    <div class="modal-content">
        <form id="form_upd_item" action="<?= url('item/edit') ?>" method="post">
            @csrf
            <input type="hidden" name="item_id" value="{{$item->id}}">
            <input type="hidden" id="my_onu_product_code" value="{{$item->onu_product_code}}">
            <div class="modal-header">
                <h4 class="modal-title">Editar Item</h4>                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label><b>Código de Barras</b></label>
                        <input type="text" name="code" class="form-control" autocomplete="off" value="{{ $item->code }}">
                    </div>
                    <div class="col-md-12">
                        <label><b>Nombre (*)</b></label>
                        <input type="text" name="name" class="form-control" autocomplete="off" value="{{ $item->name }}">
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <label for="rad_price"><b>Precio de venta (*)</b></label>
                        <input type="number" name="cipher" class="form-control" value="{{$item->office_item->unit_price}}" step="0.1">
                    </div>
                    <div class="col-md-12 m-b-lg-10" style="margin-top:10px;">
                        <label><b>Categoría SUNAT (opcional):</b></label>
                        <div class="search">
                            <span class="fa fa-search"></span>
                            <input type="text" class="form-control" id="onu_product_search" placeholder="Filtrar categoría aquí (mínimo 5 caracteres)" autocomplete="off">
                        </div>
                        <select class="form-control" name="onu_product_code" id="sel_onu_product" style="margin-top:5px;"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> GRABAR</button>
            </div>
        </form>
    </div>
</div>