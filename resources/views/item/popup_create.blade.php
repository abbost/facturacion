<div class="modal-dialog">
    <div class="modal-content">
        <form id="form_reg_item" action="<?= url('item/create') ?>" method="post">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title"><b>Nuevo <?= $type == 'P' ? 'Producto' : 'Servicio' ?></b></h4>
                <div><small>* Campos obligatorios</small></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 m-b-lg-10">
                        <label><b>Código de barras </b></label>
                        <input type="text" name="code" class="form-control" autocomplete="off" maxlength="15">
                    </div>
                    <div class="col-md-12 m-b-lg-10">
                        <label><b>Nombre (*) </b></label>
                        <input type="text" name="name" class="form-control" autocomplete="off" required>
                    </div>
                    <div class="col-md-12 m-b-lg-10">
                        <label><b>Precio de venta (*)</b></label>
                        <input type="number" name="cipher" class="form-control" step="0.1" required>
                    </div>
                    
                    <input type="hidden" name="type" value="<?= $type == 'P' ? 'product' : 'service' ?>">
                    
                    <div class="col-md-12" style="margin-top:10px;">
                        <label><b>Categoría SUNAT (opcional):</b></label>
                        <div class="search">
                            <span class="fa fa-search"></span>
                            <input type="text" class="form-control" id="onu_product_search" placeholder="Filtrar categoría aquí (mínimo 5 caracteres)" autocomplete="off" data-target="#sel_onu_product_1">
                        </div>
                        <select class="form-control" name="onu_product_code" id="sel_onu_product_1" style="margin-top:5px;"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-lg-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary  "><i class="fa fa-save"></i> GRABAR</button>
            </div>
        </form>
    </div>
</div>