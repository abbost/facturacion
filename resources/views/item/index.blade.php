@extends('layouts.back')

@section('head')
<div class="doc-title mb-4">
    <h2>
        <span><?= @$type == 'P' ? 'Productos' : 'Servicios' ?></span>

        <?php if($can_manage = has_permission('item.manage')) { ?>
        <a href="#" class="btn btn-primary link_open_popup" data-url="<?= url('item/create?type='.$type) ?>" data-target="#modal_item_create">Nuevo</a>
        <a href="#" class="btn btn-primary link_open_popup" data-url="<?= url('item/upload?type=' . $type) ?>" data-target="#modal_item_upload">Carga masiva</a>
        <?php } ?>

    </h2>
</div>
@endsection

@section('body')
<div class="row"><div class="table-responsive">
    <table class="table report table-sm table-hover" style="text-transform:uppercase; ">
        <thead><tr class="text-center">
            <th style="width:120px;">Id</th>
            <th style="width:200px;">Cód Barras</th>
            <th style="width:300px;">Nombre</th>
            <th style="width:130px" class="text-right">Precio S/</th>
            @if($type == 'P')
            <th style="width:100px;background-color:#eee;text-align:center;">Stock</th>
            @endif
            <th style="width:80px">Acción</th>
        </tr></thead>
        <tbody>
            <?php $offices_size = offices()->count(); ?>
            <?php foreach ($items as $item) {
                $origin = $item->_origin; $office_item = $item->office_item; ?>
                <tr style="text-align:center;background-color:<?= $office_item->status == 1 ? '#fff' : '#ccc;color:#f00;'; ?>">
                <td><?= $item->id ?></td>
                <td>{{$item->code}}</td>
                <td style="text-align:left;"><?= $item->name ?></td>
                <td class="text-right"><?= number_format($office_item->unit_price,2) ?></td>
                @if($type == 'P')
                <td style="background-color:#eee;text-align:center;">
                    <a href="#" class="link_open_popup" data-url="<?= url('item/inputs?id='.$item->id) ?>" data-target="#modal_product_stock" title="Ver STOCK">
                    <?= round(@$origin->stock(office_id())->quantity ?: 0, 2) ?>           
                    </a>             
                </td>
                @endif
                <td>
                    <form action="<?= url("item/delete?item_id={$item->id}") ?>" method="post">
                        @csrf

                        @if ($can_manage)
                        <a href="#" class="link_open_popup" data-url="<?= url('item/edit?id='.$item->id) ?>" data-target="#modal_item_edit" title="Editar"><i class="fas fa-pencil-alt"></i></a>                        
                        <a href="<?= url('item/availability/change?id='.$item->id) ?>" title="Deshabilitar"><i class="fa fa-lock"></i></a>
                        <a href="#" class="link_delete_row_data" title="Eliminar"><i class="fa fa-trash"></i></a>
                        @endif
                    </form>
                </td>
                </tr
            ><?php } ?>
        </tbody>
    </table>
</div></div>
<div id="modal_item_edit" class="modal fade"></div>
<div id="modal_product_stock" class="modal fade"></div>
<div id="modal_item_create" class="modal fade"></div>
<div id="modal_item_upload" class="modal fade"></div>
@endsection
