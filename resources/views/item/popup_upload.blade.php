<form action="<?= url('item/' . $type . '/import') ?>" method="post" enctype="multipart/form-data" class="row"><div class="col-md-12">
    @csrf
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Carga masiva de <?= $title ?></b></h4>
                <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <a href="<?= url('item/' . $type . '/export') ?>" class="btn btn-primary w-100 " style="padding:10px; font-size:16px; margin-bottom:20px">1.- Descargar Plantilla Excel <?= $title ?></a>
                    </div>
                    <div class="col-md-12" style="display: none">
                        <iframe src="<?= $video_url ?>" frameborder="0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" style="width:100%;height:200px;margin-bottom:20px;" allowfullscreen></iframe>
                    </div>  
                    <div class="col-md-12 mb-2">
                        <buttom class="btn btn-primary w-100" style="padding:10px; font-size:16px; margin-bottom:20px">2.- LLenar Plantilla</buttom>
                    </div>
                    <div class="col-md-12 mb-2">
                        <input type="file" name="file" class="form-control-file" id="file" lang="es">
                    </div>
                    <div class="col-md-12 mb-2">
                        <button type="butoon" class="btn btn-primary w-100" style="padding:10px; font-size:16px; margin-bottom:20px">3.- Subir Plantilla</button>
                    </div>
                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div></form>