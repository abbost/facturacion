<div class="row"><div class="col-md-12">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Stock del producto <?= $name ?> (método PEPS)</b></h4>
                <button type="button" class="close link_close_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover form_detail_dump">
                        <thead><tr>
                            <th>Sucursal</th>
                            <th>Fec. ingreso</th>
                            <th>Cant. inicial</th>
                            <th>Cant. rest.</th>
                            <th>C. unit.</th>
                            <th>C. total.</th>
                            <th>Último consumo</th>
                        </tr></thead>
                        <tbody><?php foreach ($inputs as $input) { ?><tr>
                            <td><?= $input->office->name ?></td>
                            <td><?= $input->created_at ?></td>
                            <td><?= $input->quantity ?></td>
                            <td><?= $input->balance ?></td>
                            <td>S/ <?= $input->unit_cost ?></td>
                            <td>S/ <?= round($input->balance * $input->unit_cost, 2) ?></td>
                            <td><?= $input->updated_at ?></td>
                        </tr><?php } ?></tbody>     
                    </table>
                </div>
            </div>
        </div>
    </div>
</div></div>