@extends('layouts.back')

@section('content')

@if(has_permission('sending.list'))
<div class="d-inline-flex mb-4">
    
    <h2 class="mr-3">Envíos</h2>
    <small style="padding-top:10px"><?= session('business')->comercial_name ?> - Suc.: <?= session('office')->name ?></small>
    <nav></nav>
</div>
<div class="row justify-content-lg-center">
    <div class="col-md-12 ">

        <?php if (session('status')) { ?>
        <div class="alert alert-dismissible alert-success row m-b-lg-10">
            <button class="close" type="button" data-dismiss="alert">×</button>
            <p><?= session('status') ?></p>
        </div>
        <?php } ?>

        <div class="row white-bg table-responsive">
            <table class="report table table-hover" style="min-width:600px;text-transform:uppercase;">
            <thead><tr class="text-center">
                <th class="text-left">Sucursal</th>
                <th style="width:150px">Precio envío</th>
                <th style="width:150px">Valor contenido</th>
                <th style="width:180px">Fecha</th>
                <th style="width:100px">Acción</th>
            </tr></thead>
            <tbody>
                <?php foreach ($sendings ?: [] as $sending) { ?><tr>
                    <td class="text-left">{{$sending->office->name}}</td>
                    <td class="text-center">S/ {{$sending->ship_price}}</td>
                    <td class="text-center">S/ {{@$sending->content_cost}}</td>
                    <td class="text-center">{{$sending->date}}</td>
                    <td style="text-align:center;">
                        <a href="#" data-url="<?= url('purchase/show?id='.$sending->purchase_id) ?>" data-modal="#modal_purchase_show" class="row_data_edit" title="Ver" ><i class="fa fa-eye"></i></a>
                    </td>
                </tr><?php } ?>
            </tbody>
        </table>
    </div></div>
</div>

<div id="modal_purchase_show" class="modal fade" style="color: black; text-shadow: none;" tabindex="-1" role="dialog"></div>
@endif

@endsection
