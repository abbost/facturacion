<div id="modal_promotion_create" class="modal fade" style="color: black; text-shadow: none;" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form_reg_promotion" action="<?= url('promotion/create') ?>" method="post">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title"><b>Nueva Promoción</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label><b>Ítem:</b></label>
                            <select name="item_id" class="form-control"><?php foreach ($items as $item) { ?>
                                <option value="<?= $item->id ?>"><?= $item->description ?></option>
                            <?php } ?></select>
                        </div>
                        <div class="col-md-12" style="margin-top:10px; margin-bottom: 10px">
                            <label>Puntos necesarios</label>
                            <input type="number" name="points" class="form-control">
                        </div>
                        <div class="col-md-12" style="margin-top:10px; margin-bottom: 10px">
                            <label>Porcentaje de descuento</label>
                            <input type="number" name="discount" class="form-control">
                        </div>
                        <div class="col-md-12" style="margin-top:10px; margin-bottom: 10px">
                            <label>Precio Fijo de Promoción</label>
                            <input type="number" name="fixed_price" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-lg-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Agregar</button>
                </div>
            </form>
        </div>
    </div>
</div>
