@extends('layouts.back')

@section('content')
<div id="title">
    <h2>Promociones - Canjear</h2>
    <nav>
        <label id="info_customer" class="blue" style="margin-right:10px;"></label>
        <input type="text" id="dni_customer" placeholder="Ingrese DNI" class="form-control" style="display:inline-block;width:auto;">
        <button id="btn_search_promotions" class="btn-primary form-control" style="display:inline-block;width:auto;margin-left:10px;">Buscar Promociones</button>
    </nav>
</div>
<div class="row">
    <div class="col-md-12 "><div class="row white-bg p-b-lg-20 p-t-lg-20 p-l-lg-10 p-r-lg-10 table-responsive">
        <div class="col-md-4 offset-md-4 text-center">
            <input type="checkbox" id="print_ticket">
            <label for="print_ticket">Imprimir boleta generada?</label>
        </div>
        <table class="table table-hover" id="list_promotions">
            <thead>
                <tr class="text-center">
                    <th class="text-left">Ítem</th>
                    <th style="width:130px">P. unitario S/</th>
                    <th style="width:80px">Puntos</th>
                    <th style="width:130px">Descuento %</th>
                    <th style="width:130px">Precio Fijo S/</th>
                    <th style="width:120px">Acción</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div></div>
</div>

@csrf

@endsection
