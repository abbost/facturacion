<?php foreach ($promotions as $promotion) { ?><tr class="text-center">
    <td class="text-left">{{@$promotion['item']['description']}}</td>
    <td class="text-right">{{@$promotion['item']['unit_price']}}</td>
    <td class="text-right">{{@$promotion['points']}}</td>
    <td class="text-right">{{@$promotion['discount']}}</td>
    <td class="text-right">{{@$promotion['fixed_price']}}</td>
    <td>
        <?php if (@$in_exchange) { ?>
        <a href="#" class="promotion_use btn btn-success" data-id="<?= $promotion['id'] ?>">CANJEAR</a>
        <?php } else { ?>
        <form action="<?= url('promotion/delete?promotion_id='.@$promotion['id']) ?>" method="post">
            @csrf
            <a href="#" class="row_data_delete"><i class="fa fa-trash"></i></a>
        </form>
        <?php } ?>
    </td>
</tr><?php } ?>