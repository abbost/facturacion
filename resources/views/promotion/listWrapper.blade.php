@extends('layouts.back')

@section('content')
<div id="title">
    <h2>Promociones</h2>
    <h5><?= session('business')->comercial_name ?></h5>
    <nav>
        <a href="{{ url('promotion/exchange') }}" class="btn btn-success">Canjear Promoción</a>
        <span data-toggle="modal" data-target="#modal_promotion_create" class="btn btn-primary">+Nueva Promoción</span>
    </nav>
</div>
<div class="row">
    <div class="col-md-12 ">
        <div class="row white-bg p-b-lg-20 p-t-lg-20 table-responsive p-l-lg-10 p-r-lg-10">
        <table class="table table-hover" id="list_promotions">
            <thead>
                <tr class="text-center">
                    <th class="text-left">Ítem</th>
                    <th style="width:130px">P. unitario S/</th>
                    <th style="width:80px">Puntos</th>
                    <th style="width:130px">Descuento %</th>
                    <th style="width:130px">Precio Fijo S/</th>
                    <th style="width:80px">Acción</th>
                </tr>
            </thead>
            <tbody>
                @include('promotion.list')
            </tbody>
        </table>
    </div></div>
</div>

@include('promotion.popup_create')

@endsection
