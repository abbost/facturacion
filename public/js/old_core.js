
onu_products = undefined;

function toggler(id){
    event.preventDefault();
    $("#"+id).toggle('slow');
}

$(".chart_bar_vertical").each(function () {
    var chart = new Chart($(this).get(0).getContext("2d")).Bar({
        labels: $(this).attr("data-x").split(","),
        datasets: [{
            /*label: "My Second dataset",*/
            fillColor: "rgba(11,62,113,0.2)",
            strokeColor: "rgba(11,62,113,1)",
            pointColor: "rgba(11,62,113,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(11,62,113,1)",
            data: $(this).attr("data-y").split(",")
        }]
    });
});

global_datatable = $('table.report').DataTable({
    iDisplayLength: 50,
    dom: "<'row'<'col-sm-6 'B><'col-sm-6 text-right'f>>tp",
    ordering: false,
    autoWidth: false,
    bPaginate: true,
    columns: $('table.report').find("th").get().map(function (elem, i) {return {data: "c_"+i};}),
    oLanguage: {
        sInfoEmpty: "No hay registros",
        sSearch: "",
        sLengthMenu: "Mostrando _MENU_ entradas",
        sInfo: "Mostrando _START_ hasta _END_ de _TOTAL_ entradas"
    },
    // buttons: [
    //     'excel'
    // ]
    buttons: [
    {
        extend: 'excel',
        text: 'Excel',
        //className: 'btn btn-default',
        exportOptions: {
            columns: 'th:not(:last-child)'
        }
    }]    
});

$("#receptor_doc").on("keyup", function (e) {
    if(e.keyCode == 13)
        $("#customer_data").trigger("click");
});

$("body").ready(function () {
    $(this).on("keyup", "#item_search", function (e) {
        e.preventDefault();
        var key = (this.value||"").toLowerCase();
        var filtered_items = items.map(function (item, index) {
            return [item, index];
        }).filter(function (pair) {
            item_name_ = pair[0].description ? pair[0].description.toLowerCase() : '';
            return item_name_.indexOf(key) !== -1;
        });
        if (filtered_items.length > 0) {
            $("#sel_item").attr("empty", 0).html(
                filtered_items.map(function (pair) {
                    return "<option value='"+pair[1]+"' data-onu-code="+pair[0].onu_product_code+">"+pair[0].description+"</option>";
                })
            ).trigger("change");
            $("#btn_reg_item").css({"display":"none"});
            $("#onu_product_wrapper").css({"display":"none"});
        } else {
            $("#sel_item").attr("empty", 1).html("<option value=-1>No hay coincidencias</option>");
            $("#item_unit_price").val(0);
            $("#btn_reg_item").css({"display":"block"});
            $("#onu_product_wrapper").css({"display":"block"});
            $("#onu_product_search").val("").trigger("input");
        }
        if(e.keyCode == 13) {
            $("#btn_add_item").trigger("click");
        }
    });
    $(this).on("keydown", "#item_search", function (e) {
        if (e.keyCode == 9) {
            e.preventDefault();
            $("#item_quantity").trigger("focus");
        }
    });
    $(this).on("keyup", "#item_quantity,#item_unit_price", function (e) {
        e.preventDefault();
        if (e.keyCode == 13) {
            $("#btn_add_item").trigger("click");
        }
    });
    /*Items y promociones*/
    $(this).on("click", ".item_edit", function (e) {
        url = $(this).data('url');
        $("#modal_container").load(url, function(){
            code = $("#my_onu_product_code").val();
            if(code && onu_products != undefined){
                category = onu_products.filter(function(product){return product.code == code;})[0];
                $("#sel_onu_product").append("<option value="+category.code+">"+category.name+"</option>");
            }
            $("#modal_item_edit").modal();
        })
    });
    $(this).on("click", ".item_delete, .promotion_delete", function (e) {
        e.preventDefault();
        var r = confirm("¿Confirma que desea eliminar el registro?");
        if (r == true) {
            window.location = $(this).attr('href');
        }
    });
    $(this).on("click", "#btn_search_promotions", function (e) {
        e.preventDefault();
        var dni = ($("#dni_customer").val() || "").trim();
        if (!dni) {
            $("#info_customer").html("");
            alert("Ingresa un dni válido");return;
        }
        $.ajaxblock();
        $.ajax({
            url: base_url+"promotion/available/get?dni="+dni,
            dataType: "json",
            success: function (response) {
                $("#list_promotions tbody").html(response.content);
                $("#info_customer").html(response.customer && response.points ?
                    response.customer+" - "+response.points+" puntos(s)" : ""
                );
                $.ajaxunblock();
            }
        });
    });
    $(this).on("click", ".promotion_use", function (e) {
        e.preventDefault();
        var promotion_id = $(this).attr("data-id");
        if (!confirm("Generar boleta para "+$("#info_customer").html().split(" - ")[0]+"?")) {
            return;
        }
        $.ajaxblock();
        $.ajax({
            url: base_url+"promotion/use",
            dataType:"json",
            type:"post",
            data:{
                promotion_id: promotion_id,
                _token:$("input[name=_token]").val()
            },
            success: function (response) {
                alert(response.message);
                if(response.error == 0){
                    if ($("#print_ticket").is(":checked")) {
                        $.ajax({
                            url:response.redirect,
                            dataType:"json",
                            success:function (response_) {
                                window.open("about:blank","width=10, height=10","_blank").document.write(response_.content);
                                location.reload();
                            }
                        });
                    } else {
                        location.reload();
                    }
                } else {
                    $.ajaxunblock();
                }
            }
        });
    });

    /*Otro*/
    $(this).on("click", "#btn_logout", function (e) {
        e.preventDefault();
        $("#form_logout").trigger("submit");
    });
    $(this).on("click", "#btn_reg_document", function (e) {
        e.preventDefault();
        if(!$("#list_details tbody tr").length){
            alert("Debe ingresar por lo menos un producto");
            $("#item_search").focus();
            return;
        }
        $.ajaxblock();
        $.ajax({
            url: $("#form_reg_document").attr('action'),
            type: 'post',
            data: $("#form_reg_document").serialize(),
            dataType: 'json',
            success: function(response){
                alert(response.message);location.reload();
                console.log(response);                            
                if(response.error == 0){                    
                    if ($("#print_ticket").is(":checked")) {                        
                        $.ajax({
                            url:response.redirect,
                            dataType:"json",
                            success:function (response_) {
                                console.log(response_);
                                var window2 = window.open("about:blank","width=10, height=10","_blank");
                                if (window2) {
                                    window2.document.write(response_.content);
                                } else {
                                    alert("No se pudo imprimir el pdf. Debe permitir las ventanas emergentes en su navegador.");
                                }
                                location.reload();
                            }
                        });
                    } else {
                        location.reload();
                    }
                } else {
                    $.ajaxunblock();
                }
            }
        });
        // para que no se creen tantos iframes
        if($("#document_iframe").length) {
            $("#document_iframe").remove();
        }
    });

    // $(this).on("click", "#next_correlative", function (e) {        
    //     e.preventDefault();
    //     var serie = $("input[name=serie]").val();
    //     if(serie.length == 0){
    //         alert("Debes ingresar una serie válida");
    //         return;
    //     }
    //     $.ajaxblock();
    //     $.ajax({
    //         url: base_url+"document/next_correlative?serie="+serie,
    //         type: "get",
    //         dataType:"json",
    //         success: function(response){                
    //             $.ajaxunblock();
    //             $("input[name=correlative]").val(response.correlative);
    //             $("#item_search").trigger("keyup");
    //         }
    //     });
    // });
    //$("#next_correlative").trigger("click");

    $(this).on("change", "#chk_anonimous_customer", function (e) {
        e.preventDefault();
        if (this.checked) {
            $("#customer_wrapper").hide();
            $("[name=receptor_doc]").val("---");
            $("[name=receptor_name]").val("---");
            $("[name=receptor_address]").val("---");
        } else {
            $("[name=receptor_doc]").val("");
            $("[name=receptor_name]").val("");
            $("[name=receptor_address]").val("");
            $("#customer_wrapper").show();
        }
        $("[name=receptor_doc_type]").val("DNI");
        $("[name=receptor_email]").val("");
        $("[name=receptor_email_content]").val("");
    });

    if($("#chk_anonimous_customer").length){            
        $("#chk_anonimous_customer").prop("checked", true).trigger("change");
    }


    $(this).on("click", "#customer_data", function (e) {
        e.preventDefault();
        var doc_type = $("select[name=receptor_doc_type]").val();
        var doc = $("input[name=receptor_doc]").val();
        $.ajaxblock();
        $.ajax({
            url: base_url+"document/customer/search?doc="+doc+"&doc_type="+doc_type,
            type: 'get',
            dataType: 'json',
            success: function(response){
                $.ajaxunblock();
                if (response.error == 1 || !response.customer) {
                    alert("Error de SUNAT. El documento es inválido");
                    return;
                }
                var customer = response.customer;
                $("input[name=receptor_doc]").val(customer.doc);
                $("input[name=receptor_name]").val(customer.name.toUpperCase());
                $("input[name=receptor_address]").val((customer.address || "").toUpperCase());
                $("input[name=receptor_email]").val((customer.email || "").toUpperCase());
            }
        });
    });

    $(this).on("change", "#sel_item", function (e) {
        var item = items[this.value];
        $("#item_unit_price").val(item ? item.unit_price : 0);
        // var onu_product = onu_products.filter(function (onu_product) {
        //     return onu_product.code == item.onu_product_code;
        // })[0];
        // alert(onu_products); return;
        // $("#onu_product_search").val(onu_product.name).trigger("input");
        // $("#sel_onu_product").val(onu_product.code);
    });
    $("#sel_item").trigger("change");

    // ADD PRODUCT

    function sum (jquery_array) {
        return jquery_array.map(function () {return parseFloat(this.value);}).get().reduce(function (x, s) {return x + s;}, 0);
    }
    function show_total () {
        $("#details_sub_total").val(sum($("input.detail_sub_total")).toFixed(2));
        $("#details_tax").val(sum($("input.detail_tax")).toFixed(2));
        $("#details_total").val(sum($("input.detail_total")).toFixed(2));
    }
    function detail_column (index, name, value) {
        return value+"<input type='hidden' class='detail_"+name+"' name='products["+index+"]["+name+"]' value='"+value+"'>";
    }
    function add_detail (description, quantity, unit_price, onu_product_code, club, tax) {
        club = club === undefined ? false : club;
        tax = tax === undefined ? true : tax;
        var i = $("#list_details tbody tr").length + 1;
        $("#list_details tbody").append(
            "<tr class='text-center'>"+
                "<td>"+"<input type='hidden' class='detail_id' name='products["+i+"][id]' value='0'>"+
                    (!club?"":"<input type='hidden' name='club_cupon' value='"+club.cupon+"'><input type='hidden' name='club_document' value='"+club.document+"'><input type='hidden' name='club_discount' value='"+club.discount+"'><input type='hidden' name='club_amount' value='"+club.amount+"'>")+
                    detail_column(i, "onu_product_code", onu_product_code)+"</td>"+
                "<td>"+detail_column(i, "quantity", (quantity || 0))+"</td>"+
                "<td style='text-align:left;'>"+detail_column(i, "description", description)+"</td>"+
                "<td>"+detail_column(i, "price", parseFloat(unit_price || 0).toFixed(2))+"</td>"+
                "<td>"+detail_column(i, "sub_total", parseFloat(unit_price * quantity / (tax ? 1.18 : 1) || 0).toFixed(2))+"</td>"+
                "<td>"+detail_column(i, "tax", tax ? parseFloat((unit_price * 0.18 / 1.18) * quantity || 0).toFixed(2) : 0)+"</td>"+
                "<td>"+detail_column(i, "total", parseFloat(unit_price * quantity || 0).toFixed(2))+"</td>"+
                (!club?"<td><a href='#' class='link_delete_row' style='color:#f00;'><i class='fa fa-trash'></i></a></td>":"<td></td>")+
            "</tr>"
        );
        show_total();
    }
    $(this).on("click", "#btn_load_details", function (e) {
        e.preventDefault();
        if (!$("[name=doc_ref_serie]").val() || !$("[name=doc_ref_correlat]").val() || !$("[name=doc_reference_type]:checked").val()) {
            alert("Debes proporcionar el tipo, la serie y correlativo del documento referente primero");
            return;
        }
        $.ajaxblock();
        $.ajax({
            url: base_url+"document/details/load",
            data: {
                "doc_ref_serie": $("[name=doc_ref_serie]").val(),
                "doc_ref_correlat": $("[name=doc_ref_correlat]").val(),
                'doc_type': $("[name=doc_reference_type]:checked").val()
            },
            dataType: "json",
            success: function (response) {
                $.ajaxunblock();
                if (!response || response.status == 0) {
                    alert("El documento referente no existe");
                    return;
                }
                if (response.details && response.details.length > 0) {
                    $("[name=receptor_doc_type]").val(response.document.customer_doc_type);
                    $("[name=receptor_doc]").val(response.document.customer_doc);
                    $("[name=receptor_name]").val(response.document.customer_name);
                    $("[name=receptor_address]").val(response.document.customer_address);
                    $("[name=receptor_email]").val(response.document.customer_email);
                    $("[name=receptor_email_content]").val(response.document.customer_email_content);
                    $("#list_details tbody").empty();
                    response.details.forEach(function (detail, i) {
                        $("#list_details tbody").append(
                            "<tr class='text-center'>"+
                                "<td>"+"<input type='hidden' class='detail_id' name='products["+i+"][id]' value='0'>"+
                                    detail_column(i, "onu_product_code", detail.onu_product_code)+"</td>"+
                                "<td>"+detail_column(i, "quantity", detail.quantity)+"</td>"+
                                "<td style='text-align:left;'>"+detail_column(i, "description", detail.description)+"</td>"+
                                "<td>"+detail_column(i, "price", detail.unit_price)+"</td>"+
                                "<td>"+detail_column(i, "sub_total", detail.sub_total)+"</td>"+
                                "<td>"+detail_column(i, "tax", detail.tax)+"</td>"+
                                "<td>"+detail_column(i, "total", detail.total)+"</td>"+
                                "<td></td>"+
                            "</tr>"
                        );
                    });
                    show_total();
                }
            }
        });
    });

    $(this).on("click", "#btn_add_item", function (e) {
        e.preventDefault();

        if (!$("#item_unit_price").val() || $("#item_unit_price").val() == 0) {
            alert("Ingresa un precio válido");
            $("#item_unit_price").trigger("focus");
            return;
        }

        if (!$("#item_quantity").val() || $("#item_quantity").val() == 0) {
            alert("Ingresa una cantidad válida");
            $("#item_quantity").trigger("focus");
            return;
        }
        // no hay coincidencias
        var item_name = "";
        var onu_product_code = "";
        var id = ($("#sel_item :selected").val() || "").trim();
        if(id && id != -1){
            item_name = $("#sel_item :selected").html();
            onu_product_code = ($("#sel_item :selected").data('onu-code') || 0);
        }else{
            /*if (!$("#sel_onu_product").val()) {
                alert("Debes definir la categoría a la que pertnece el producto");
                $("#onu_product_search").trigger("focus");
                return;
            }*/
            item_name = ($("#item_search").val() || "").trim()
            onu_product_code = ($("#sel_onu_product").val() || 0);
            if (!item_name) {
                alert("Debes ingresar un nombre de producto válido");
                return;
            }
        }
        add_detail(
            item_name,
            $("#item_quantity").val(),
            $("#item_unit_price").val(),
            onu_product_code
        );

        $("#item_search").val("").focus();
    });

    $(this).on("click", "#btn_reg_item", function (e) {
        e.preventDefault();
        // if (!$("#sel_onu_product").val()) {
        //     alert("Debes definir la categoría a la que pertnece el producto");
        //     $("#onu_product_search").trigger("focus");
        //     return;
        // }
        $.ajaxblock();
        $.ajax({
            url: base_url+"document/item/create",
            type: 'post',
            data: {
                "_token": $("input[name=_token]").val(),
                "description": $("#item_search").val(),
                "type_cipher": "price",
                "cipher": $("#item_unit_price").val(),
                "onu_product_code": $("#sel_onu_product").val()
            },
            dataType: 'json',
            success: function (response) {
                $.ajaxunblock();
                if (!response.item) {
                    alert("No se ha podido registrar el item");
                    return;
                }
                items.push(response.item);
                $("#item_search").trigger("keyup");
                add_detail(
                    $("#item_search").val(),
                    $("#item_quantity").val(),
                    $("#item_unit_price").val(),
                    $("#sel_onu_product").val()
                );
            }
        });
    });
    $(this).on("click", ".link_delete_row", function (e) {
        e.preventDefault();
        $(this).parents('tr').remove();
        show_total();
    });
    // LISTADO DE DOCUMENTOS
    $(this).on("click", "#btn_search", function (e) {
        e.preventDefault();
        $.ajaxblock();
        $.ajax({
            url: $(this).attr("data-url")+"?date_from="+$("#search_date_from").val()+"&date_to="+$("#search_date_to").val()+"&type="+$("#search_doc_type").val(),
            type: 'get',
            dataType: "json",
            success: function (response) {
                if (response.error) {
                    alert(response.message);
                } else {
                    var view = response.view,
                        rows = view.substr(4).substring(0,view.length - 9).split("</tr><tr>");
                    if (rows[0] == "") {rows = [];}
                    global_datatable.clear().draw();
                    {
                        var totals_row = {};
                        for (var i = 0; i < 8; i++) {totals_row['c_' + i] = '';}
                        totals_row['c_0'] = "<strong>TOTAL</strong>";
                        totals_row['c_3'] = "<div style='text-align:right;'><strong>" + response.total + "</strong></div>";
                        global_datatable.row.add(totals_row).draw();
                    }
                    rows.forEach(function (line) {                        
                        if (location.hostname === "localhost")
                            var columns_ = line.substr(10).substring(0,line.length - 17).split("</td>\r\n    <td>");
                        else
                            var columns_ = line.substr(10).substring(0,line.length - 16).split("</td>\n    <td>");
                        var columns = {};
                        for (var i in columns_) {
                            columns["c_" + i] = columns_[i].trim();
                        }
                        global_datatable.row.add(columns).draw();
                    });
                }

                $.ajaxunblock();
            }
        });
    });
    $("#btn_search").trigger("click");

    // $(this).on("click", ".link_print_ticket", function (e) {        
    //     e.preventDefault();
    //     var anchor = $(this);
    //     alert( anchor.attr("data-url") );
    //     $.ajax({
    //         url: anchor.attr("data-url"),
    //         dataType: "json",
    //         success: function (response) {
    //             var w_ = window.open("about:blank", "", "_blank");
    //             w_.document.write(response.content);
    //         }
    //     });
    // });

    $(this).on("click", ".link_show_send_mail", function (e) {
        e.preventDefault();
        var modal = $("#modal_send_email");
        modal.find("form").attr("action", $(this).attr("data-url"));
        modal.modal();
    });
    $(this).on("submit", "#form_send_email", function (e) {
        e.preventDefault();
        $.ajaxblock();
        $.ajax({
            url:$(this).attr("action"),
            data:$(this).serialize(),
            dataType:'json',
            success: function (response) {
                $.ajaxunblock();
                if (response.error == 0) {
                    alert(response.message);
                    window.location.reload();
                }
            }
        });
    });
    $(this).on("click", ".link_resend_document", function (e) {

        e.preventDefault();
        var form = $(this).next('form');
        var span = this.parentNode;
    
        $.ajaxblock();
        $.ajax({
            url:$(form).attr("action"),
            data:$(form).serialize(),
            dataType:'json',
            type:'post',
            success: function (response) {                
                console.log(response);
                $.ajaxunblock();
                alert(response.message);
                if (response.error == 0 && response.message == "Comprobante generado correctamente") {
                    $(span).removeClass("badge-info").addClass("badge-success");
                    span.innerHTML = "ACEPTADO";
                }
            }
        });
    });
    $(this).on("click", ".link_cancel_document", function (e) {
        e.preventDefault();
        var r = confirm("¿Confirma que desea eliminar el registro?");
        if (r == true) {
            var form = $(this).next("form");
            $.ajaxblock();
            $.ajax({
                url:$(form).attr("action"),
                data:$(form).serialize(),
                dataType:'json',
                type:'post',
                success: function (response) {
                    $.ajaxunblock();
                    alert(response.message);
                    if (response.error == 0) {
                        window.location.href = base_url+"document/list";
                    }
                }
            });
        }
    });
    $(this).on("click", ".link_credit_document", function (e) {
        e.preventDefault();
        var form = this.nextSibling;
        form.submit();
    });

    // REGISTRO DE ITEMS
    function search_onu_product (key) {
        $("#sel_onu_product").html(key.length < 4 ? "" :
            onu_products.filter(function (onu_product) {
                return onu_product.name.toLowerCase().indexOf(key) !== -1;
            }).map(function (onu_product) {
                return "<option value='"+onu_product.code+"'>"+onu_product.name+"</option>";
            })
        ).trigger("change");
    }
    $(this).on("input", "#onu_product_search", function (e) {
        e.preventDefault();
        var key = (this.value || "").toLowerCase();
        if (onu_products === undefined && key.length >= 4) {
            $.ajaxblock();
            $.ajax({
                url:base_url+"json/onu_products.json",
                dataType: 'json',
                success: function (response) {
                    $.ajaxunblock();
                    onu_products = response;
                    search_onu_product(key);
                }
            });
        } else {
            search_onu_product(key);
        }
    });

    //$("#onu_product_search").trigger("input");

    $(this).on("change", "#sel_onu_product", function (e) {
        e.preventDefault();
        var onu_product_code = this.value;
        if (!onu_product_code) {
            $("#onu_product_class").html("");
            $("#onu_product_family").html("");
            $("#onu_product_segment").html("");
        } else {
            var onu_product_class = onu_products.filter(function (onu_product) {
                return onu_product.code == onu_product_code.substr(0,6) + "00";
            })[0];
        }
    });

    $(this).on("change", "input[name=paid_by]", function (e) {
        e.preventDefault();
        $("#card_data_wrapper input").val("");
        //$("#card_data_wrapper").css({"display": this.value == "card" ? "block" : "none"});
        if(this.value == "card"){
            $("#card_data_wrapper").css("display","block");
            $("#voucher").focus();
        }else{
            $("#card_data_wrapper").css("display","none");
        }
    });

    $(this).on("change", "#use_affiliate_code", function (e) {
        e.preventDefault();
        if (this.checked) {
            $("#affiliate_code_wrapper").css({"display":"block"});
            $("#affiliate_code").focus();
        } else {
            $("#affiliate_code_wrapper").css({"display":"none"});
            $("#btn_unable_affiliate_code").trigger("click");
        }
    });
    $(this).on("click", "#btn_validate_affiliate_code", function (e) {
        e.preventDefault();
        var code = $("#affiliate_code").val(),
            customer_document = $("#receptor_doc").val();
        if(!$("#list_details tbody tr").length){
            alert("Debe ingresar por lo menos un producto");
            return;
        }
        $.ajaxblock();
        $.ajax({
            url: base_url+"promotion/code/validate",
            data: {
                code: code,
                customer_document: customer_document
            },
            dataType: "json",
            success: function (response) {
                alert(response.message);
                $("#discount_applied_label").html(response.discount + "%");
                $("#discount_applied_code").html(code);
                $("#discount_applied_document").html(customer_document);
                $("#discount_applied_wrapper").css({"display":response.error === 0 ? "block" : "none"});
                var total=sum($("input.detail_total")).toFixed(2);

                if(!response.error){
                    add_detail(
                                "Cupón Referido: Xafiro Club",
                                1,
                                total * (response.discount/100) * -1,
                                0,
                                {
                                    cupon:code,
                                    document:customer_document,
                                    discount:response.discount,
                                    amount: total * (response.discount/100)
                                }
                            );
                }
                $("#item_search").val("").focus();
                $.ajaxunblock();
            }
        });
    });
    $(this).on("click", "#btn_unable_affiliate_code", function (e) {
        e.preventDefault();
        $.ajaxblock();
        $.ajax({
            url: base_url+"promotion/code/unable",
            dataType: "json",
            success: function (response) {
                alert(response.message);
                $("#discount_applied_label").html("0%");
                $("#discount_applied_code").html("");
                $("#discount_applied_document").html("");
                $("#discount_applied_wrapper").css({"display":"none"});

                $("input[name='club_cupon']").parents('tr').remove();
                show_total();

                $.ajaxunblock();
            }
        });
    });

    $(this).on("change", "#chk_global_discount", function (e) {
        $("#list_details tbody").html("");
        $("#" + (this.checked ? "no_global_discount_wrapper" : "global_discount_wrapper")).hide();
        $("#" + (this.checked ? "global_discount_wrapper" : "no_global_discount_wrapper")).show();
    });

    $(this).on("click", "#btn_add_discount", function (e) {
        e.preventDefault();
        if (!$("#txt_discount").val() || $("#txt_discount").val() == 0) {
            alert("Debes agregar un monto distinto de cero");
            return;
        }
        add_detail("DESCUENTO GLOBAL", 1, $("#txt_discount").val(), 0, true);
    });
});
