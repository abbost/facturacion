
onu_products = undefined;

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function toggler(id){
    event.preventDefault();
    $("#"+id).toggle('slow');
}

function print_popup( redirect ){    
    if (redirect!="") {
        $.ajaxblock();
        $.ajax({
            url: redirect,
            dataType:"json",
            success:function (response_) {
                $.ajaxunblock();
                var window2 = window.open("about:blank","width=10, height=10","_blank");
                if (window2) {
                    window2.document.write(response_.content);
                } else {
                    alert("No se pudo imprimir el pdf. Debe permitir las ventanas emergentes en su navegador.");
                }
            },
        });
    }
}


$(".chart_bar_vertical").each(function () {
    var chart = new Chart($(this).get(0).getContext("2d")).Bar({
        labels: $(this).attr("data-x").split(","),
        datasets: [{
            /*label: "My Second dataset",*/
            fillColor: "rgba(11,62,113,0.2)",
            strokeColor: "rgba(11,62,113,1)",
            pointColor: "rgba(11,62,113,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(11,62,113,1)",
            data: $(this).attr("data-y").split(",")
        }]
    });
});

global_datatable = $('table.report').DataTable({
    iDisplayLength: 50,
    dom: "<'row'<'col-sm-6 'B><'col-sm-6 text-right'f>>tp",
    ordering: false,
    autoWidth: false,
    bPaginate: true,
    columns: $('table.report').find("th").get().map(function (elem, i) {return {data: "c_"+i};}),
    oLanguage: {
        sInfoEmpty: "No hay registros",
        sSearch: "<i class='fa fa-search' aria-hidden='true'></i>" ,
        sLengthMenu: "Mostrando _MENU_ entradas",
        sInfo: "Mostrando _START_ hasta _END_ de _TOTAL_ entradas"
    },
    buttons: [{
        extend: 'excel',
        text: 'Exportar Excel',
        //className: 'btn btn-default',
        exportOptions: {
            columns: 'th:not(:last-child)'
        }
    }]
});



if($( "#product_name" ).length)
$( "#product_name" ).autocomplete({
    source: function( request, response ) {
        $.ajax({
            type: "get",
            url: base_url + "item/get_json",
            dataType: "json",
            data: {
                term: request.term                
            },
            success: function( data ) {
                response( data ); 
            }
        });
    },	
    minLength: 3,
    focus: function() {
        return false;
    },
    select: function( event, ui ) {		        
        var data = ui.item;		
        $(this).val(data.name);
        $(this).data('item-id',data.id);
        $("#item_unit_price").val(data.unit_price);
        $("#btn_add_item").trigger("click");
        //$("#item_quantity").focus();  // esto hacer que al poner focus se active el metodo keyup de item_quantity
        // deberia haber control de stock             
        return false;
    }
})
.data("ui-autocomplete")._renderItem = function (ul, item) {   
            
    if(item.type == 'product' && item.stock == 0){
        return $("<li class='ui-state-disabled gray' style='padding:3px 20px'>"+item.label.toUpperCase()+"</li>").appendTo(ul);        
    }else{
        return $("<li class='ui-menu-item'><div tabindex='-1' class='ui-menu-item-wrapper'>"+item.label.toUpperCase()+"</div></li>").appendTo(ul);
    }
};	



$("body").ready(function () {

    // $(this).on("keyup", "#product_name", function (e) {
    //     e.preventDefault();            
    //     if(e.keyCode == 13) {                
    //         $("#item_unit_price").focus();
    //     }
    // });
    
    
    $(this).on("keyup", "#item_quantity, #item_unit_price", function (e) {
        e.preventDefault();
        if (e.keyCode == 13) {
            $("#btn_add_item").trigger("click");
        }
    });
    
    
    $(this).on("click",".dropdown-link",function(e) {
        e.preventDefault();
        is_visible = $(this).next('.dropdown-container').is(":visible");                         
        $('.dropdown-container').hide();
        if(is_visible)
            $(this).next('.dropdown-container').hide();
        else
            $(this).next('.dropdown-container').show();
    });


    var screen_width = Math.max(
        document.body.scrollWidth,
        document.documentElement.scrollWidth,
        document.body.offsetWidth,
        document.documentElement.offsetWidth,
        document.documentElement.clientWidth
    );

    if(screen.width <= 768)
        $('.sidebar-mini').removeClass('sidenav-toggled');

    $(this).on("click", ".link_open_popup", function () {
        var modal = $($(this).attr("data-target"));
        var func = $(this).attr("data-function");
        modal.load($(this).attr("data-url"), function () {
            modal.modal("show");
        });
    });
    $(this).on("click", ".link_close_popup", function () {
        $(this).parents(".modal").html("");
    });

    $(this).on("click", ".link_delete_row_data", function (e) {
        e.preventDefault();
        if (confirm("¿Estas seguro/a de que deseas eliminar este registro? \n Se borrará de todas las sucursales")) {
            $(this).parents("form").trigger("submit");
        }
    });
    $(this).on("click", "#btn_logout", function (e) {
        e.preventDefault();
        $("#form_logout").trigger("submit");
    });

    
    /*
     *  PURCHASE
     */

    function update_purchase_total() {
        $("#purchase_total").html(
            (Math.round($("#purchase_product_table input[name=purchase_details\\[total\\]\\[\\]]").get().map(function (x) { return parseFloat(x.value || 0); }).reduce(function (x, a) {
                return x + a;
            }, 0) * 100) / 100).toFixed(2)
        );
    }

    $(this).on("click", "#purchase_product_add", function (e) {
        product_id = $("#product_select2").select2('data')[0]['id'];
        product_name = $("#product_select2").select2('data')[0]['text'];

         var offices = []; 
         offices.length = parseInt($(this).attr("data-offices")); offices.fill(1);

        $($(this).attr("data-target") + " tbody").prepend("<tr>"+
            "<td>"+product_name+"<input type='hidden' name='purchase_details[product_id][]' value='" + product_id + "'></td>"+
            offices.map(function (x) {return "<td><input name='purchase_details[quantity][]' style='width:100%;text-align:center;'></td>";}).join("")+
            "<td><input name='purchase_details[total][]' style='width:100%;text-align:center;'></td>"+
            "<td style='text-align:center;'><a href='#' onclick='this.parentNode.parentNode.remove();update_purchase_total();'><i style='color:#f00;' class='fa fa-trash'></a></td>" +
        "</tr>");
        update_purchase_total();
    });


    $(this).on("input", "#purchase_product_table input[name='purchase_details\\[total\\]\\[\\]']", update_purchase_total);

    $(this).on("click","#btn_reg_purchase", function(e) {
        e.preventDefault();
        if ($("#reg_nro_guide").val().length <1) {
            alert('Porfavor, registre el nro de guia');
            $("#reg_nro_guide").focus();
            return true
        }
        
        if (!$("#purchase_product_table tbody tr").length) {
            alert("Debe ingresar por lo menos un producto");
            $("#purchase_product_search").focus();
            return;
        }

        $("#form_reg_purchase").submit();
        return;
        
    })

        

    
    /*Items y promociones*/
    $(this).on("click", ".item_edit", function (e) {
        $("#modal_item_edit").load($(this).data('url'), function(){
            var code = $("#my_onu_product_code").val();
            if(code && onu_products != undefined){
                category = onu_products.filter(function(product){return product.code == code;})[0];
                $("#sel_onu_product").append("<option value="+category.code+">"+category.name+"</option>");
            }
            $(this).modal();
        })
    });

    $(this).on("click", ".row_data_delete", function (e) {
        e.preventDefault();
        if (confirm("¿Confirma que desea eliminar el registro?")) {
            this.parentNode.submit();
        }
    });

    $(this).on("click", ".row_data_edit", function (e) {
        $($(this).attr("data-modal")).load($(this).data('url'), function () {
            $(this).modal();
        });
    });

    

     /* Record the sale */

    $(this).on("click", "#btn_reg_document", function (e) {
        e.preventDefault();
        
        if(!$("#list_details tbody tr").length){
            alert("Debe ingresar por lo menos un producto");
            $("#sel_item").focus();
            return;
        }

        if( $("#receptor_doc_type").val().toLowerCase() == "ruc") {
            if( ($("#receptor_doc").val() || true).length != 11 ){  
                alert("El RUC está mal ingresado. Debe tener 11 dígitos");
                $("#receptor_doc").focus();
                return
            }
            if ($("#receptor_name").val() == ""){
                alert("Debe ingresar la razón social");
                $("#receptor_name").focus();
                return;
            }
            if ($("#receptor_address").val() == ""){
                alert("Debe ingresar la dirección");
                $("#receptor_address").focus();
                return;
            }
        }


        if( $("#receptor_doc_type").val().toLowerCase() == "dni" && $("#receptor_doc").val() ){
            // En ocasiones receptor_doc tiene null. En esos casos se toma el valor true (1)
            if( $("#receptor_doc").val().length != 8 ){ 
                alert("El DNI está mal ingresado. Debe tener 8 dígitos");
                $("#receptor_doc").focus();
                return;
            }
            if ($("#receptor_name").val() == ""){
                alert("Debe ingresar los nombres y apellidos");
                $("#receptor_name").focus();
                return;
            }
        }

        // var total = Math.round(parseFloat($("#details_total").val()) * 100) / 100;
        // if (total > 700 && $("#chk_anonimous_customer:checked").val() == '1') {
        //     alert("Pasados los 700 soles es obligatorio ingresar el DNI del receptor");            
        //     return;
        // }

        $.ajaxblock();        

        // $.ajax({
        //     url: $("#form_reg_document").attr('action'),
        //     method: 'post',
        //     data: $("#form_reg_document").serialize(),
        //     success: function(response){
        //         $.ajaxunblock();  
        //         alert(response.message)
        //     }
        // })
        
        $("#form_reg_document").submit();
        return;
    });

    

    $(this).on("change", "#chk_anonimous_customer", function (e) {
        e.preventDefault();
        if (this.checked) {
            //$("#customer_wrapper").hide();
            $("#mail_panel").hide();
            $("[name=receptor_doc]").val("---");
            $("[name=receptor_name]").val("---");
            $("[name=receptor_address]").val("---");
        } else {
            $("[name=receptor_doc]").val("");
            $("[name=receptor_name]").val("");
            $("[name=receptor_address]").val("");
            //$("#customer_wrapper").show();
            $("#mail_panel").hide();
        }
        $("[name=receptor_doc_type]").val("DNI");
        $("[name=receptor_email]").val("");
        $("[name=receptor_email_content]").val("");

    });


    function openCreateContact() {    
        //alert("casa de la funcion");
        $('#contact_customer').select2('close');   
        url = base_url + "/contact/create";
        var modal = $("#modal_contact");
        modal.load( url, function () {
            modal.modal("show").focus();            
            modal.find("#customer_id_modal").val( $("#customer_id_parent").val() );
        });                        
    };
    

    $(this).on("click", "#create-contact-form", function (e) {
        if(!$("#contact_form #contact_name").val()){
            alert("Debes ingresar un nombre"); 
            $("#contact_form #contact_name").focus();
            return;
        }
        if (!$("#contact_form #contact_address").val()){
            alert("Debes ingresar una dirección"); 
            $("#contact_form #contact_address").focus();
            return;
        }
        $.ajax({
            url:base_url + 'contact/create',
            data: $("#contact_form").serialize(),
            dataType: 'json',
            method:'post',            
            success:function (data) {                
                $(".modal").modal('hide');                
                var $newOption = $("<option selected='selected'></option>").val(data.id).text(data.text);
                $("#contact_customer").append($newOption); // if we used append, there will be unneeded values                
                $("#contact_customer").select2();

                $("input[name=contact_name]").val(data.name.toUpperCase());
                $("input[name=contact_address]").val((data.address || "").toUpperCase());                    
            }
        });
    });




    $(this).on("click", "#edit-contact-link", function (e) {
        $('#contact_customer').select2('close');   
        contact_id = $("#contact_customer").select2('val');
        if(!contact_id) {
            console.log("No hay contacto");            
            return;
        }
        url = base_url + "/contact/"+contact_id+"/edit";
        var modal = $("#modal_contact");
        modal.load( url, function () {
            modal.modal("show");                        
        });                        
    });
    $(this).on("click", "#edit-contact-form", function (e) {
        if(!$("#contact_form #contact_name").val()){
            alert("Debes ingresar un nombre"); 
            $("#contact_form #contact_name").focus();
            return;
        }
        if (!$("#contact_form #contact_address").val()){
            alert("Debes ingresar una dirección"); 
            $("#contact_form #contact_address").focus();
            return;
        }
        id = $("#contact_customer").select2('val');
        $.ajax({
            url:base_url + 'contact/'+ id +'/edit',
            data: $("#contact_form").serialize(),
            dataType: 'json',
            method:'post',            
            success:function (data) {                
                $(".modal").modal('hide');                
                
                // actualizamos el texto de un option
                $("#contact_customer option[value="+id+"]").text(data.text);
                $("#contact_customer").select2();
                

                $("input[name=contact_name]").val(data.name.toUpperCase());
                $("input[name=contact_address]").val((data.address || "").toUpperCase());               
            }
        });
    });

 

    if( $('#contact_customer').length ){
    $('#contact_customer').select2({
        placeholder: 'Elegir',     
        minimumInputLength: 0,
        ajax: {
            url: base_url+"contact/search",
            delay: 250,
            dataType: 'json',
            data: function (params) {
                return {
                    customer_id : $("#customer_id_parent").val()
                }
                // parameters will be ?doc=[value]&doc_type=[value]
            },
            processResults: function (data) {  
                console.log( $("#customer_id_parent").val() );     
                console.log(data);     
                return {
                    results: data
                };
            },
        cache: true        
        },
        language: {
            noResults: function() {
                return 'No encontrado';
            }
        },
        escapeMarkup: function(markup) {
            return markup;
        },      
        templateResult: function (res) {
            console.log(res);
            if (typeof res.id !== "undefined") {
                var text = res.text;
                // La operacion tendra otro tipo de vista
                if (res.id == 'C') {
                    text = '<a href="#" class="bold black" ><i class="fa fa-plus" ></i> <b>' + res.text + '</b></a>';                     
                }
                return text;
            } else {
                return res.text;
            }
        },
        templateSelection: function(res){
            //openCreateContact(); return;                    
            if (res.id !== "" ) {            
                if (res.id == "C") {                    
                    openCreateContact();
                    $("#contact_customer").select2('data',[]); //refreshSelect2
                    $("#contact_customer").html('').change();
                } else {                                    
                    $("#edit-contact-link").css("display","inline-block");
                    $("input[name=contact_name]").val( (res.name || "").toUpperCase());
                    $("input[name=contact_address]").val((res.address || "").toUpperCase());                    
                    return res.doc || res.text;
                }                                        
            } else {
                return 'Elegir'; // Reemplaza al placeholder
            }            
        }        
    });
    //alert("casa");
    }   
    

    // ADD PRODUCT


    function sum (jquery_array) {
        return jquery_array.map(function () {return parseFloat(this.value);}).get().reduce(function (x, s) {return x + s;}, 0);
    }
    function show_total () {
        var total = sum($("input.detail_total")).toFixed(2);
        var tax = (global_tax === undefined ? 0.18 : global_tax) ;
        //$("#details_sub_total").val(sum($("input.detail_sub_total")).toFixed(2));
        //$("#details_tax").val(sum($("input.detail_tax")).toFixed(2));
        $("#details_sub_total").val( (total / (1+tax) || 0 ).toFixed(2) );
        $("#details_tax").val( (total / (1+tax) * tax || 0 ).toFixed(2) );
        $("#details_total").val(total);
    }
    function detail_column (index, name, value) {
        return value+"<input type='hidden' class='detail_"+name+"' name='products["+index+"]["+name+"]' value='"+value+"'>";
    }
    function detail_column2 (index, name, value) {
        return "<input type='hidden' class='detail_"+name+"' name='products["+index+"]["+name+"]' value='"+value+"'>";
    }
    function add_detail (description, quantity, unit_price, onu_product_code, club, tax, item_id) {                
        // deberia haber control de stock antes de agregar el producto
        tax = (global_tax === undefined ? 0.18 : global_tax) ;
        item_id = item_id === undefined ? 0 : item_id;
        var trs = $("#list_details tbody tr").get();
        var i = 1 + (trs.length && trs.map(function (x) {return parseInt(x.getAttribute("data-index"));}).reduce(function (x, a) {return x > a ? x : a;}, 0));
        $("#list_details tbody").append(
            "<tr class='text-center' data-index='" + i + "'>"+
                "<td>"+
                    "<input type='hidden' class='detail_id' name='products["+i+"][id]' value='0'>"+
                    "<input type='hidden' class='detail_item_id' name='products["+i+"][item_id]' value='" + item_id + "'>"+ item_id +
                    // detail_column(i, "onu_product_code", onu_product_code)+
                "</td>"+
                "<td>" + detail_column(i, "quantity", (quantity || 0)) + "</td>" +
                "<td style='text-align:left;'>" + detail_column(i, "description", description) + "</td>" +
                "<td class='text-right'>" + detail_column(i, "price", parseFloat(unit_price || 0).toFixed(2)) + "</td>" +
                "<td class='p-lg-0'>" + detail_column2(i, "sub_total", parseFloat(unit_price * quantity / (1+tax) || 0).toFixed(2)) + "</td>" +
                "<td class='p-lg-0'>" + detail_column2(i, "tax", tax ? parseFloat((unit_price * tax / (1+tax)) * quantity || 0).toFixed(2) : 0) + "</td>" +
                "<td class='text-right'>" + detail_column(i, "total", parseFloat(unit_price * quantity || 0).toFixed(2)) + "</td>" +
                "<td><a href='#' class='link_delete_row' style='color:#f00;'><i class='fa fa-trash'></i></a></td>" +
            "</tr>"
        );
        show_total();
        $("#product_name").val('').focus();
        $("#product_name").data('item-id',0);
        $("#item_quantity").val(1);
        $("#item_unit_price").val(0.00);
    }


    /* This is for credit note */
    $(this).on("click", "#btn_load_details", function (e) {
        e.preventDefault();
        if (!$("[name=doc_ref_serie]").val() || !$("[name=doc_ref_correlat]").val() || !$("[name=doc_reference_type]:checked").val()) {
            alert("Debes proporcionar el tipo, la serie y correlativo del documento referente primero");
            return;
        }
        $.ajaxblock();
        $.ajax({
            url: base_url+"document/details/load",
            data: {
                "doc_ref_serie": $("[name=doc_ref_serie]").val(),
                "doc_ref_correlat": $("[name=doc_ref_correlat]").val(),
                'doc_type': $("[name=doc_reference_type]:checked").val()
            },
            dataType: "json",
            success: function (response) {
                $.ajaxunblock();
                if (!response || response.status == 0) {
                    alert("El documento referente no existe");
                    return;
                }
                if (response.details && response.details.length > 0) {
                    $("[name=receptor_doc_type]").val(response.document.customer_doc_type);
                    $("[name=receptor_doc]").val(response.document.customer_doc);
                    $("[name=receptor_name]").val(response.document.customer_name);
                    $("[name=receptor_address]").val(response.document.customer_address);
                    $("[name=receptor_email]").val(response.document.customer_email);
                    $("[name=receptor_email_content]").val(response.document.customer_email_content);
                    $("#list_details tbody").empty();
                    response.details.forEach(function (detail, i) {
                        $("#list_details tbody").append(
                            "<tr class='text-center'>"+
                                "<td>"+"<input type='hidden' class='detail_id' name='products["+i+"][id]' value='0'>"+
                                    detail_column(i, "onu_product_code", detail.onu_product_code)+"</td>"+
                                "<td>"+detail_column(i, "quantity", detail.quantity)+"</td>"+
                                "<td style='text-align:left;'>"+detail_column(i, "description", detail.description)+"</td>"+
                                "<td>"+detail_column(i, "price", detail.unit_price)+"</td>"+
                                "<td>"+detail_column(i, "sub_total", detail.sub_total)+"</td>"+
                                "<td>"+detail_column(i, "tax", detail.tax)+"</td>"+
                                "<td>"+detail_column(i, "total", detail.total)+"</td>"+
                                "<td></td>"+
                            "</tr>"
                        );
                    });
                    show_total();
                }
            }
        });
    });

    /* Add to sale detail on create document. Reduce stock from product */

    $(this).on("click", "#btn_add_item", function (e) {
        e.preventDefault();

        if (!$("#item_unit_price").val() || $("#item_unit_price").val() == 0.00) {
            alert("Ingresa un precio válido");
            $("#item_unit_price").focus();
            return;
        }

        if (!$("#item_quantity").val() || $("#item_quantity").val() == 0) {
            alert("Ingresa una cantidad válida");
            $("#item_quantity").focus();
            return;
        }
        var quantity = Math.round(parseFloat($("#item_quantity").val() || 0) * 100) / 100;
        // no hay coincidencias
        var item_name = "";
        var onu_product_code = "";
        var id = $("#product_name").data("item-id");
        
        item_name = ($("#product_name").val() || "").trim()            
        if (!item_name) {
            alert("Debes ingresar un nombre de producto válido");
            return;
        }
        
        add_detail(
            item_name,
            $("#item_quantity").val(),
            $("#item_unit_price").val(),
            onu_product_code,
            undefined,
            undefined,
            id
        );

    });




    /* Remove an item from sale detail in create document */
    $(this).on("click", ".link_delete_row", function (e) {        
        e.preventDefault();
        var parent = $(this).parents('tr');
        var item_id = $(".detail_item_id", parent).val();
        var quantity = Math.round(parseFloat($(".detail_quantity", parent).val() || 0) * 100) / 100;
        parent.remove();
        show_total();
    });

    $(this).on("click", ".link_show_send_mail", function (e) {
        e.preventDefault();
        var modal = $("#modal_send_email");
        modal.find("form").attr("action", $(this).attr("data-url"));
        modal.modal();
    });

    $(this).on("submit", "#form_send_email", function (e) {
        e.preventDefault();
        $.ajaxblock();
        $.ajax({
            url:$(this).attr("action"),
            data:$(this).serialize(),
            dataType:'json',
            success: function (response) {
                $.ajaxunblock();
                if (response.error == 0) {
                    alert(response.message);
                    window.location.reload();
                }
            }
        });
    });
    $(this).on("click", ".link_resend_document", function (e) {

        e.preventDefault();
        var form = $(this).next('form');
        var span = this.parentNode;

        $.ajaxblock();
        $.ajax({
            url:$(form).attr("action"),
            data:$(form).serialize(),
            dataType:'json',
            type:'post',
            success: function (response) {
                $.ajaxunblock();
                alert(response.message);
                if (response.error == 0) {
                    $(span).removeClass("badge-info").addClass("badge-success");
                    span.innerHTML = "ACEPTADO";
                }
            }
        });
    });
    $(this).on("click", ".link_cancel_document", function (e) {
        e.preventDefault();
        var r = confirm("¿Confirma que desea eliminar el registro?");
        if (r == true) {
            var form = $(this).next("form");
            $(form).submit();
            // $.ajaxblock();
            // $.ajax({
            //     url:$(form).attr("action"),
            //     data:$(form).serialize(),
            //     dataType:'json',
            //     type:'post',
            //     success: function (response) {
            //         $.ajaxunblock();
            //         alert(response.message);
            //         if (response.error == 0) {
            //             window.location.href = base_url+"document/list";
            //         }
            //     }
            // });
        }
    });
    $(this).on("click", ".link_credit_document", function (e) {
        e.preventDefault();
        var form = $(this).next("form");
        form.submit();
    });

    // REGISTRO DE ITEMS
    function search_onu_product (key, target) {
        target = target || "#sel_onu_product";
        $(target).html(key.length < 4 ? "" :
            onu_products.filter(function (onu_product) {
                return onu_product.name.toLowerCase().indexOf(key) !== -1;
            }).map(function (onu_product) {
                return "<option value='"+onu_product.code+"'>"+onu_product.name+"</option>";
            })
        ).trigger("change");
    }
    $(this).on("input", "#onu_product_search", function (e) {
        e.preventDefault();
        var key = (this.value || "").toLowerCase();
        var target = $(this).attr("data-target");
        if (onu_products === undefined && key.length >= 4) {
            $.ajaxblock();
            $.ajax({
                url:base_url+"json/onu_products.json",
                dataType: 'json',
                success: function (response) {
                    $.ajaxunblock();
                    onu_products = response;
                    search_onu_product(key, target);
                }
            });
        } else {
            search_onu_product(key);
        }
    });

    //$("#onu_product_search").trigger("input");

    $(this).on("change", "#sel_onu_product, #sel_onu_product_1", function (e) {
        e.preventDefault();
        var onu_product_code = this.value;
        if (!onu_product_code) {
            $("#onu_product_class").html("");
            $("#onu_product_family").html("");
            $("#onu_product_segment").html("");
        } else {
            var onu_product_class = onu_products.filter(function (onu_product) {
                return onu_product.code == onu_product_code.substr(0,6) + "00";
            })[0];
        }
    });

    $(this).on("change", "input[name=paid_by]", function (e) {
        e.preventDefault();
        $("#card_data_wrapper input").val("");
        //$("#card_data_wrapper").css({"display": this.value == "card" ? "block" : "none"});
        if(this.value == "card"){
            $("#card_data_wrapper").css("display","block");
            $("#voucher").focus();
        }else{
            $("#card_data_wrapper").css("display","none");
        }
    });

    $(this).on("change", "#chk_global_discount", function (e) {
        $("#list_details tbody").html("");
        $("#" + (this.checked ? "no_global_discount_wrapper" : "global_discount_wrapper")).hide();
        $("#" + (this.checked ? "global_discount_wrapper" : "no_global_discount_wrapper")).show();
    });

    $(this).on("click", "#btn_add_discount", function (e) {
        e.preventDefault();
        if (!$("#txt_discount").val() || $("#txt_discount").val() == 0) {
            alert("Debes agregar un monto distinto de cero");
            return;
        }
        add_detail("DESCUENTO GLOBAL", 1, $("#txt_discount").val(), 0, true);
    });
});



$(".create-guide").on("click", function(){    
    $modal = $("#guide-modal");
    $document_id = $(this).data('document-id');
    $modal.load(base_url + "guide/create/"+$document_id,function(){
        $modal.modal('show');
    })
})

function form_guide_submit(){
    //alert( $("#form-guide").serialize() ); return;
    $.ajaxblock();

    // $("#form-guide").validator(){

    // }

    $.ajax({
        url : base_url + 'guide/create',
        method: 'post',
        data: $("#form-guide").serialize(),
        dataType: 'json',
        headers: {
            'X-CSRF-Token': document.getElementsByName("csrfToken").value 
        },
        success: function(data){
            $.ajaxunblock();
            if(data.error == 0){
                preparePrintPopUp(data.html_ticket);                    
                $(".modal").modal('hide');
            }else{
                alert(data.message);
            }
        }

    })
}


function print_copy_guide($id){
    //alert($id);
    
    $.ajaxblock();

    $.ajax({
        url : base_url + 'guide/copy/'+$id,
        method: 'get',        
        dataType: 'json',
        headers: {
            'X-CSRF-Token': document.getElementsByName("csrfToken").value 
        },
        success: function(data){
            $.ajaxunblock();
            if(data.error == 0){
                preparePrintPopUp(data.html_ticket);                                    
            }else{
                alert(data.message);
            }
        }

    })
}



function preparePrintPopUp(html){
    //var my_window = window.open("", "_blank", "toolbar=no, scrollbars=no, resizable=no, top=100, left=300, width=300, height=500");
    var my_window = window.open("", "_blank", "toolbar=no, scrollbars=no, resizable=no, top=2, left=-20, width=10, height=5");
    if (!my_window)
        return
    if (!my_window.opener) { my_window.opener = self; }
    my_window.document.open();
    my_window.document.write(html);
    my_window.document.close();
   my_window.addEventListener("load", function() {
       my_window.print()
       setTimeout(function(){my_window.close();}, 1);
   }, false);
}


function read_image_as_bg (file) {
    img = $(file).parent();
    if (file.files && file.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            img.css({"background-image":"url("+e.target.result+")"});
            img.find("span").remove();            
        };
        reader.readAsDataURL(file.files[0]);     
    }
}

// function ws_search_person(){
//     type = $("#doc_type").val();
//     doc = $("#doc").val();
//     if(doc == ''){
//         alert("Debes ingresar un documento válido");
//         return;
//     }

//     if(doc.length<8){
//         alert("El documento debe tener 8 caracteres como mínimo");
//         return;
//     }

//     $.ajaxblock();
//     $.ajax({
//         url: base_url + 'ws_search_person/'+type+'/'+doc,
//         method: 'get',
//         dataType: 'json',
//         success: function(data){
//             $.ajaxunblock();
//             if(data.error == 0){
//                 if(type == 'RUC'){
//                     $("[name=ruc]").val(data.customer.doc);
//                     $("[name=legal_name]").val(data.customer.name);
//                     $("[name=address]").val(data.customer.address);
//                 }else{
//                     $("[name=doc]").val(data.customer.doc);
//                     $("[name=fullname]").val(data.customer.name);
//                 }
//             }else{
//                 alert("El documento no se encuentra en SUNAT");

//             }
//         }    
//     })
// }


function ws_search_person(){
    type = $("#receptor_doc_type").val();
    doc = $("#receptor_doc").val();
    if(doc == ''){
        alert("Debes ingresar un documento válido");
        return;
    }

    if(doc.length<8){
        alert("El documento debe tener 8 caracteres como mínimo");
        return;
    }

    $.ajaxblock();
    $.ajax({
        url: base_url + 'ws_search_person/'+type+'/'+doc,
        method: 'get',
        dataType: 'json',
        success: function(data){
            $.ajaxunblock();
            if(data.error == 0){                
                $("#receptor_doc").val(data.customer.doc);                    
                $("#receptor_address").val(data.customer.address);                
                $("#receptor_name").val(data.customer.name);                
            }else{
                alert("El documento no se encuentra en SUNAT. Deberá registrarlo manualmente");

            }
        }    
    })
}


function create_product_select2(){    
    $('#product_select2').select2({
        //dropdownParent: $("#modal_purchase_create .modal-content"),
        placeholder: 'Elegir',     
        minimumInputLength: 0,
        ajax: {
            url: base_url+"product/search",
            delay: 250,
            dataType: 'json',
            data: function (params) {
                return {
                    term : params.term
                }
                // parameters will be ?doc=[value]&doc_type=[value]
            },
            processResults: function (data) {       
                console.log(data);     
                return {
                    results: data
                };
            },
        cache: true        
        },
        language: {
            noResults: function() {
                return 'No encontrado';
            }
        },
        escapeMarkup: function(markup) {
            return markup;
        },      
        templateResult: function (res) {
            if (typeof res.id !== "undefined") {
                var text = res.text;
                // La operacion tendra otro tipo de vista
                if (res.id == 'C') {
                    text = '<a href="#" class="bold black" ><i class="fa fa-plus" ></i> <b>' + res.text + '</b></a>';                     
                }
                return text;
            } else {
                return res.text;
            }
        },
        templateSelection: function(res){
            //openCreateContact(); return;                    
            if (res.id !== "" ) {            
                                    
                // $("#edit-contact-link").css("display","inline-block");
                // $("input[name=contact_name]").val( (res.name || "").toUpperCase());
                // $("input[name=contact_address]").val((res.address || "").toUpperCase());                                    
                return res.doc || res.text;

            } else {
                return 'Elegir'; // Reemplaza al placeholder
            }            
        }        
    });
};


function change_search_mode(obj){
    
    if ( $(obj).is(':checked') )
        search_mode = 'code';
    else
        search_mode = 'name'

    $.ajax({
        url: base_url + 'item/change_search_mode',
        method: 'post',
        data:{
            'search_mode' : search_mode
        },
        success: function(response){
            console.log(response);
        }
    })
}


// eL flexdatalist descarga todo el lote de registros y los filtra en el local
$('.flexdatalist').flexdatalist({
    minLength: 1,
    visibleProperties: ['name','doc'],
    searchIn: $(this).data('search-in'),
    searchDelay: 300,
    searchContain: true,
    noResultsText: 'No hay resultados para "{keyword}"',    
    url: base_url + 'document/customer/search',
    // params:{
    //     doc_type: $("#receptor_doc_type option:selected").val()
    // }
}).on("select:flexdatalist", function(event, data) {
    $("#receptor_doc").val(data.doc);
    $("#receptor_name").val(data.name);
    $("#receptor_address").val(data.address);
    $("#receptor_email").data('email',data.email);
    $("#customer_id_parent").val(data.customer_id);
});


// if($( "#receptor_doc" ).length)
// $( "#receptor_doc" ).autocomplete({
//     source: function( request, response ) {
//         $.ajax({
//             type: "get",
//             url: base_url + 'document/customer/search',
//             dataType: "json",
//             data: {
//                 term: request.term,
//                 doc_type: $("#receptor_doc_type").val()
//             },
//             success: function( data ) {
//                 response( data ); 
//             }
//         });
//     },	
//     minLength: 3,
//     focus: function() {
//         return false;
//     },
//     select: function( event, ui ) {		        
//         var data = ui.item;		
//         $(this).val(data.doc);
//         $("#receptor_name").val(data.name);
//         $("#receptor_address").val(data.address)
//         $("#customer_id_parent").val(data.customer_id);          
//         return false;
//     }
// }).data("ui-autocomplete")._renderItem = function (ul, item) {         
//     return $("<li class='ui-menu-item'><div tabindex='-1' class='ui-menu-item-wrapper'>"+(item.name || '' ).toUpperCase()+"</div></li>").appendTo(ul);    
// };	


function click_on_tab(obj,doc_type){

    $('#receptor_doc_type').empty();

    if(doc_type == '01'){                
        $('#receptor_doc_type').append($("<option value='RUC' selected>RUC</option>"));      
        $("#receptor_name").parents(".row").eq(0).find("label").text("Razón Social");
    }else{
        $('#receptor_doc_type').append($("<option value='DNI' selected>DNI</option>"));
        $('#receptor_doc_type').append($("<option value='PAS'>PASAPORTE</option>"));
        $('#receptor_doc_type').append($("<option value='CEX'>CARNE EXTRANJERIA</option>"));
        $("#receptor_name").parents(".row").eq(0).find("label").text("Nombres");
    }

    if(doc_type != '00'){        
        $('#mas_opciones').show(); 
    }else
        $('#mas_opciones').hide(); 
    
    $('[name=serie]').val($(obj).data('serie'));
    $('[name=correlative]').val($(obj).data('correlative'));  
    $("#doc_label").val( $(obj).data('serie') + "-" + $(obj).data('correlative'))
}