<?= '<?xml version="1.0" encoding="utf-8"?>' ?>
<SummaryDocuments
        xmlns="urn:sunat:names:specification:ubl:peru:schema:xsd:SummaryDocuments-1"
        xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"
        xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"
        xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
        xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"
        xmlns:sac="urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <ext:UBLExtensions>
        <ext:UBLExtension>
            <ext:ExtensionContent/>
        </ext:UBLExtension>
    </ext:UBLExtensions>
    <?php $emp = $doc->company ?>
    <cbc:UBLVersionID>2.0</cbc:UBLVersionID>
    <cbc:CustomizationID>1.1</cbc:CustomizationID>
    <cbc:ID>{{ $doc->getXmlId() }}</cbc:ID>
    <cbc:ReferenceDate>{{ $doc->fecGeneracion ? $doc->fecGeneracion->format('Y-m-d') :date('Y-m-d') }}</cbc:ReferenceDate>
    <cbc:IssueDate>{{ $doc->fecResumen? $doc->fecResumen->format('Y-m-d') : date('Y-m-d') }}</cbc:IssueDate>
    
    <cac:Signature>
        <cbc:ID>{{ $emp->ruc }}</cbc:ID>
        <cac:SignatoryParty>
            <cac:PartyIdentification>
                <cbc:ID>{{ $emp->ruc }}</cbc:ID>
            </cac:PartyIdentification>
            <cac:PartyName>
                <cbc:Name><![CDATA[{{ $emp->nombreComercial }}]]></cbc:Name>
            </cac:PartyName>
        </cac:SignatoryParty>
        <cac:DigitalSignatureAttachment>
            <cac:ExternalReference>
                <cbc:URI>#SIGN-GREEN</cbc:URI>
            </cac:ExternalReference>
        </cac:DigitalSignatureAttachment>
    </cac:Signature>
    <cac:AccountingSupplierParty>
        <cbc:CustomerAssignedAccountID>{{ $emp->ruc }}</cbc:CustomerAssignedAccountID>
        <cbc:AdditionalAccountID>6</cbc:AdditionalAccountID>
        <cac:Party>
            <cac:PartyLegalEntity>
                <cbc:RegistrationName><![CDATA[{{ $emp->razonSocial }}]]></cbc:RegistrationName>
            </cac:PartyLegalEntity>
        </cac:Party>
    </cac:AccountingSupplierParty>
    @foreach($doc->details as $i => $det)
    <sac:SummaryDocumentsLine>
        <cbc:LineID>{{ $i+1 }}</cbc:LineID>
        <cbc:DocumentTypeCode>{{ $det->tipoDoc }}</cbc:DocumentTypeCode>
        <cbc:ID>{{ $det->serieNro }}</cbc:ID>
        <cac:AccountingCustomerParty>
            <cbc:CustomerAssignedAccountID>{{ $det->clienteNro }}</cbc:CustomerAssignedAccountID>
            <cbc:AdditionalAccountID>{{ $det->clienteTipo }}</cbc:AdditionalAccountID>
        </cac:AccountingCustomerParty>
        @if($det->docReferencia)
        <cac:BillingReference>
            <cac:InvoiceDocumentReference>
                <cbc:ID>{{ $det->docReferencia->nroDoc }}</cbc:ID>
                <cbc:DocumentTypeCode>{{ $det->docReferencia->tipoDoc }}</cbc:DocumentTypeCode>
            </cac:InvoiceDocumentReference>
        </cac:BillingReference>
        @endif
        @if($det->percepcion)
        <?php $perc = $det->percepcion ?>
        <sac:SUNATPerceptionSummaryDocumentReference>
            <sac:SUNATPerceptionSystemCode>{{ $perc->codReg }}</sac:SUNATPerceptionSystemCode>
            <sac:SUNATPerceptionPercent>{{ round($perc->tasa,2) }}</sac:SUNATPerceptionPercent>
            <cbc:TotalInvoiceAmount currencyID="PEN">{{ round($perc->mto,2) }}</cbc:TotalInvoiceAmount>
            <sac:SUNATTotalCashed currencyID="PEN">{{ round($perc->mtoTotal,2) }}</sac:SUNATTotalCashed>
            <cbc:TaxableAmount currencyID="PEN">{{ round($perc->mtoBase,2) }}</cbc:TaxableAmount>
        </sac:SUNATPerceptionSummaryDocumentReference>
        @endif
        <cac:Status>
            <cbc:ConditionCode>{{ $det->estado }}</cbc:ConditionCode>
        </cac:Status>
        <sac:TotalAmount currencyID="{{ $doc->moneda }}">{{ round($det->total,2) }}</sac:TotalAmount>
        @if($det->mtoOperGravadas)
        <sac:BillingPayment>
            <cbc:PaidAmount currencyID="{{ $doc->moneda }}">{{ round($det->mtoOperGravadas,2) }}</cbc:PaidAmount>
            <cbc:InstructionID>01</cbc:InstructionID>
        </sac:BillingPayment>
        @endif
        @if($det->mtoOperExoneradas)
        <sac:BillingPayment>
            <cbc:PaidAmount currencyID="{{ $doc->moneda }}">{{ round($det->mtoOperExoneradas,2) }}</cbc:PaidAmount>
            <cbc:InstructionID>02</cbc:InstructionID>
        </sac:BillingPayment>
        @endif
        @if($det->mtoOperInafectas)
        <sac:BillingPayment>
            <cbc:PaidAmount currencyID="{{ $doc->moneda }}">{{ round($det->mtoOperInafectas,2) }}</cbc:PaidAmount>
            <cbc:InstructionID>03</cbc:InstructionID>
        </sac:BillingPayment>
        @endif
        @if($det->mtoOperExportacion)
        <sac:BillingPayment>
            <cbc:PaidAmount currencyID="{{ $doc->moneda }}">{{ round($det->mtoOperExportacion,2) }}</cbc:PaidAmount>
            <cbc:InstructionID>04</cbc:InstructionID>
        </sac:BillingPayment>
        @endif
        @if($det->mtoOperGratuitas)
        <sac:BillingPayment>
            <cbc:PaidAmount currencyID="{{ $doc->moneda }}">{{ round($det->mtoOperGratuitas,2) }}</cbc:PaidAmount>
            <cbc:InstructionID>05</cbc:InstructionID>
        </sac:BillingPayment>
        @endif
        @if($det->mtoOtrosCargos)
        <cac:AllowanceCharge>
            <cbc:ChargeIndicator>true</cbc:ChargeIndicator>
            <cbc:Amount currencyID="{{ $doc->moneda }}">{{ round($det->mtoOtrosCargos,2) }}</cbc:Amount>
        </cac:AllowanceCharge>
        @endif
        @if($det->mtoIvap)
        <?php $ivap = round($det->mtoIvap,2) ?>
        <cac:TaxTotal>
            <cbc:TaxAmount currencyID="{{ $doc->moneda }}">{{ $ivap }}</cbc:TaxAmount>
            <cac:TaxSubtotal>
                <cbc:TaxAmount currencyID="{{ $doc->moneda }}">{{ $ivap }}</cbc:TaxAmount>
                <cac:TaxCategory>
                    <cac:TaxScheme>
                        <cbc:ID>1016</cbc:ID>
                        <cbc:Name>IVAP</cbc:Name>
                        <cbc:TaxTypeCode>VAT</cbc:TaxTypeCode>
                    </cac:TaxScheme>
                </cac:TaxCategory>
            </cac:TaxSubtotal>
        </cac:TaxTotal>
        @else
        <?php $igv = round($det->mtoIGV) ?>
        <cac:TaxTotal>
            <cbc:TaxAmount currencyID="{{ $doc->moneda }}">{{ $igv }}</cbc:TaxAmount>
            <cac:TaxSubtotal>
                <cbc:TaxAmount currencyID="{{ $doc->moneda }}">{{ $igv }}</cbc:TaxAmount>
                <cac:TaxCategory>
                    <cac:TaxScheme>
                        <cbc:ID>1000</cbc:ID>
                        <cbc:Name>IGV</cbc:Name>
                        <cbc:TaxTypeCode>VAT</cbc:TaxTypeCode>
                    </cac:TaxScheme>
                </cac:TaxCategory>
            </cac:TaxSubtotal>
        </cac:TaxTotal>
        @endif
        @if($det->mtoISC)
        <?php $isc = round($det->mtoISC)?>
        <cac:TaxTotal>
            <cbc:TaxAmount currencyID="{{ $doc->moneda }}">{{ $isc }}</cbc:TaxAmount>
            <cac:TaxSubtotal>
                <cbc:TaxAmount currencyID="{{ $doc->moneda }}">{{$isc }}</cbc:TaxAmount>
                <cac:TaxCategory>
                    <cac:TaxScheme>
                        <cbc:ID>2000</cbc:ID>
                        <cbc:Name>ISC</cbc:Name>
                        <cbc:TaxTypeCode>EXC</cbc:TaxTypeCode>
                    </cac:TaxScheme>
                </cac:TaxCategory>
            </cac:TaxSubtotal>
        </cac:TaxTotal>
        @endif
        @if($det->mtoOtrosTributos)
        <?php $oth = round($det->mtoOtrosTributos,2)?>
        <cac:TaxTotal>
            <cbc:TaxAmount currencyID="{{ $doc->moneda }}">{{ $oth }}</cbc:TaxAmount>
            <cac:TaxSubtotal>
                <cbc:TaxAmount currencyID="{{ $doc->moneda }}">{{ $oth }}</cbc:TaxAmount>
                <cac:TaxCategory>
                    <cac:TaxScheme>
                        <cbc:ID>9999</cbc:ID>
                        <cbc:Name>OTROS</cbc:Name>
                        <cbc:TaxTypeCode>OTH</cbc:TaxTypeCode>
                    </cac:TaxScheme>
                </cac:TaxCategory>
            </cac:TaxSubtotal>
        </cac:TaxTotal>
        @endif
    </sac:SummaryDocumentsLine>
    @endforeach
</SummaryDocuments>