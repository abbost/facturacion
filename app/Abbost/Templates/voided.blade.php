<?= '<?xml version="1.0" encoding="utf-8"?>' ?>
<VoidedDocuments xmlns="urn:sunat:names:specification:ubl:peru:schema:xsd:VoidedDocuments-1"
                 xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"
                 xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"
                 xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"
                 xmlns:sac="urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1"
                 xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <ext:UBLExtensions>
        <ext:UBLExtension>
            <ext:ExtensionContent/>
        </ext:UBLExtension>
    </ext:UBLExtensions>
    <cbc:UBLVersionID>2.0</cbc:UBLVersionID>
    <cbc:CustomizationID>1.0</cbc:CustomizationID>
    <cbc:ID>{{ $doc->getXmlId() }}</cbc:ID>
    <cbc:ReferenceDate>{{ $doc->fecGeneracion? $doc->fecGeneracion->format('Y-m-d') : date('Y-m-d')}}</cbc:ReferenceDate>
    <cbc:IssueDate>{{ $doc->fecComunicacion? $doc->fecComunicacion->format('Y-m-d') : date('Y-m-d')}}</cbc:IssueDate>
    <?php $emp = $doc->company ?>
    <cac:Signature>
        <cbc:ID>{{ $emp->ruc }}</cbc:ID>
        <cbc:Note>ABBOST Builder</cbc:Note>
        <cac:SignatoryParty>
            <cac:PartyIdentification>
                <cbc:ID>{{ $emp->ruc }}</cbc:ID>
            </cac:PartyIdentification>
            <cac:PartyName>
                <cbc:Name><![CDATA[{{ $emp->nombreComercial }}]]></cbc:Name>
            </cac:PartyName>
        </cac:SignatoryParty>
        <cac:DigitalSignatureAttachment>
            <cac:ExternalReference>
                <cbc:URI>#SIGN-GREEN</cbc:URI>
            </cac:ExternalReference>
        </cac:DigitalSignatureAttachment>
    </cac:Signature>
    <cac:AccountingSupplierParty>
        <cbc:CustomerAssignedAccountID>{{ $emp->ruc }}</cbc:CustomerAssignedAccountID>
        <cbc:AdditionalAccountID>6</cbc:AdditionalAccountID>
        <cac:Party>
            <cac:PartyLegalEntity>
                <cbc:RegistrationName><![CDATA[{{ $emp->razonSocial }}]]></cbc:RegistrationName>
            </cac:PartyLegalEntity>
        </cac:Party>
    </cac:AccountingSupplierParty>
    @foreach($doc->details as $i => $det)
    <sac:VoidedDocumentsLine>
        <cbc:LineID>{{ $i+1 }}</cbc:LineID>
        <cbc:DocumentTypeCode>{{ $det->tipoDoc }}</cbc:DocumentTypeCode>
        <sac:DocumentSerialID>{{ $det->serie }}</sac:DocumentSerialID>
        <sac:DocumentNumberID>{{ $det->correlativo }}</sac:DocumentNumberID>
        <sac:VoidReasonDescription><![CDATA[{{ $det->desMotivoBaja }}]]></sac:VoidReasonDescription>
    </sac:VoidedDocumentsLine>
    @endforeach
</VoidedDocuments>