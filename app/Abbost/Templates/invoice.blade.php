<?php
    // function format($number,$flag = false){
    //     return number_format($number,2,".","");        
    // }
?>
<?= '<?xml version="1.0" encoding="utf-8"?>' ?>
<Invoice xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2"
         xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"
         xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"
         xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
         xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2">
    <ext:UBLExtensions>
        <ext:UBLExtension>
            <ext:ExtensionContent/>
        </ext:UBLExtension>
    </ext:UBLExtensions>
    <?php $emp = $doc->company ?>
    <cbc:UBLVersionID>2.1</cbc:UBLVersionID>
    <cbc:CustomizationID>2.0</cbc:CustomizationID>
    <cbc:ID>{{ $doc->serie }}-{{ $doc->correlativo }}</cbc:ID>
    <cbc:IssueDate>{{ date('Y-m-d',strtotime($doc->fechaEmision))?:date('Y-m-d') }}</cbc:IssueDate>
    <cbc:IssueTime>{{ date('H:i:s',strtotime($doc->fechaEmision))?:date('H:i:s') }}</cbc:IssueTime>
    @if($doc->fecVencimiento)
    <cbc:DueDate>{{ $doc->fecVencimiento?:date('Y-m-d') }}</cbc:DueDate>
    @endif
    <cbc:InvoiceTypeCode listID="{{ $doc->tipoOperacion }}">{{ $doc->tipoDoc }}</cbc:InvoiceTypeCode>
    @foreach($doc->legends as $leg)
    <cbc:Note languageLocaleID="{{ $leg->code }}"><![CDATA[{{ $leg->value }}]]></cbc:Note>
    @endforeach
    <cbc:DocumentCurrencyCode>{{ $doc->tipoMoneda }}</cbc:DocumentCurrencyCode>
    @if ($doc->compra)
    <cac:OrderReference>
        <cbc:ID>{{ $doc->compra }}</cbc:ID>
    </cac:OrderReference>
    @endif
    @if ($doc->guias)
    @foreach($doc->guias as $guia)
    <cac:DespatchDocumentReference>
        <cbc:ID>{{ $guia['nroDoc'] }}</cbc:ID>
        <cbc:DocumentTypeCode listAgencyName="PE:SUNAT" listName="Tipo de Documento" listURI="urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01">{{ $guia['tipoDoc'] }}</cbc:DocumentTypeCode>
    </cac:DespatchDocumentReference>
    @endforeach
    @endif
    
    @if ($doc->relDocs)
    @foreach($doc->relDocs as $rel)
    <cac:AdditionalDocumentReference>
        <cbc:ID>{{ $rel->nroDoc }}</cbc:ID>
        <cbc:DocumentTypeCode>{{ $rel->tipoDoc }}</cbc:DocumentTypeCode>
    </cac:AdditionalDocumentReference>
    @endforeach
    @endif

    @if($doc->anticipos)
    @foreach( $doc->anticipos as $i => $ant)
    <cac:AdditionalDocumentReference>
        <cbc:ID>{{ $ant->nroDocRel }}</cbc:ID>
        <cbc:DocumentTypeCode>{{ $ant->tipoDocRel }}</cbc:DocumentTypeCode>
        <cbc:DocumentStatusCode>{{ $i+1 }}</cbc:DocumentStatusCode>
        <cac:IssuerParty>
            <cac:PartyIdentification>
                <cbc:ID schemeID="6">{{ $emp->ruc }}</cbc:ID>
            </cac:PartyIdentification>
        </cac:IssuerParty>
    </cac:AdditionalDocumentReference>
    @endforeach
    @endif
    <cac:Signature>
        <cbc:ID>{{ $emp->ruc }}</cbc:ID>
        <cbc:Note>ELABORADO POR FACTURACIONELECTRONICAPYME</cbc:Note>
        <cac:SignatoryParty>
            <cac:PartyIdentification>
                <cbc:ID>{{ $emp->ruc }}</cbc:ID>
            </cac:PartyIdentification>
            <cac:PartyName>
                <cbc:Name><![CDATA[{{ $emp->nombreComercial }}]]></cbc:Name>
            </cac:PartyName>
        </cac:SignatoryParty>
        <cac:DigitalSignatureAttachment>
            <cac:ExternalReference>
                <cbc:URI>#SIGN-ABBOST</cbc:URI>
            </cac:ExternalReference>
        </cac:DigitalSignatureAttachment>
    </cac:Signature>
    <cac:AccountingSupplierParty>
        <cac:Party>
            <cac:PartyIdentification>
                <cbc:ID schemeID="6">{{ $emp->ruc }}</cbc:ID>
            </cac:PartyIdentification>
            <cac:PartyName>
                <cbc:Name><![CDATA[{{ $emp->nombreComercial }}]]></cbc:Name>
            </cac:PartyName>
            <cac:PartyLegalEntity>
                <cbc:RegistrationName><![CDATA[{{ $emp->razonSocial }}]]></cbc:RegistrationName>
                <?php $addr = $emp->address ?>
                <cac:RegistrationAddress>
                    @if($addr->ubigueo)
                    <cbc:ID>{{ $addr->ubigueo }}</cbc:ID>
                    @endif
                    @if($addr->codLocal)
                    <cbc:AddressTypeCode>{{ $addr->codLocal }}</cbc:AddressTypeCode>
                    @endif
                    @if($addr->urbanizacion)
                    <cbc:CitySubdivisionName>{{ $addr->urbanizacion }}</cbc:CitySubdivisionName>
                    @endif
                    @if($addr->provincia)
                    <cbc:CityName>{{ $addr->provincia }}</cbc:CityName>
                    @endif
                    @if($addr->departamento)
                    <cbc:CountrySubentity>{{ $addr->departamento }}</cbc:CountrySubentity>
                    @endif
                    @if($addr->distrito)
                    <cbc:District>{{ $addr->distrito }}</cbc:District>
                    @endif                    
                    <cac:AddressLine>
                        <cbc:Line><![CDATA[{{ $addr->direccion }}]]></cbc:Line>
                    </cac:AddressLine>
                    <cac:Country>
                        <cbc:IdentificationCode>{{ $addr->codigoPais }}</cbc:IdentificationCode>
                    </cac:Country>
                </cac:RegistrationAddress>
            </cac:PartyLegalEntity>
            @if($emp->email or $emp->telephone)
            <cac:Contact>
                @if($emp->telephone)
                <cbc:Telephone>{{ $emp->telephone }}</cbc:Telephone>
                @endif
                @if($emp->email)
                <cbc:ElectronicMail>{{ $emp->email }}</cbc:ElectronicMail>
                @endif
            </cac:Contact>
            @endif
        </cac:Party>
    </cac:AccountingSupplierParty>
    <?php $client = $doc->client?>
    <cac:AccountingCustomerParty>
        <cac:Party>
            <cac:PartyIdentification>
                <cbc:ID schemeID="{{ $client->tipoDoc }}">{{ $client->numDoc }}</cbc:ID>
            </cac:PartyIdentification>
            <cac:PartyLegalEntity>
                <cbc:RegistrationName><![CDATA[{{ $client->rznSocial }}]]></cbc:RegistrationName>
                @if($client->address)
                <?php $addr = $client->address ?>
                <cac:RegistrationAddress>
                    @if(@$addr->ubigueo)
                    <cbc:ID>{{ $addr->ubigueo }}</cbc:ID>
                    @endif
                    <cac:AddressLine>
                        <cbc:Line><![CDATA[{{ $addr->direccion}}]]></cbc:Line>
                    </cac:AddressLine>
                    <cac:Country>
                        <cbc:IdentificationCode>{{ $addr->codigoPais }}</cbc:IdentificationCode>
                    </cac:Country>
                </cac:RegistrationAddress>
                @endif
            </cac:PartyLegalEntity>
            @if($client->email or $client->telephone)
            <cac:Contact>
                @if($client->telephone)
                <cbc:Telephone>{{ $client->telephone }}</cbc:Telephone>
                @endif
                @if($client->email)
                <cbc:ElectronicMail>{{ $client->email }}</cbc:ElectronicMail>
                @endif
            </cac:Contact>
            @endif
        </cac:Party>
    </cac:AccountingCustomerParty>
    <?php $seller = $doc->seller?>
    @if($seller)
    <cac:SellerSupplierParty>
        <cac:Party>
            <cac:PartyIdentification>
                <cbc:ID schemeID="{{ $seller->tipoDoc }}">{{ $seller->numDoc }}</cbc:ID>
            </cac:PartyIdentification>
            <cac:PartyLegalEntity>
                <cbc:RegistrationName><![CDATA[{{ $seller->rznSocial }}]]></cbc:RegistrationName>
                @if($seller->address)
                {{$addr = $seller->address}}
                <cac:RegistrationAddress>
                    @if($addr->ubigueo)
                    <cbc:ID>{{ $addr->ubigueo }}</cbc:ID>
                    @endif
                    <cac:AddressLine>
                        <cbc:Line><![CDATA[{{ $addr->direccion }}]]></cbc:Line>
                    </cac:AddressLine>
                    <cac:Country>
                        <cbc:IdentificationCode>{{ $addr->codigoPais }}</cbc:IdentificationCode>
                    </cac:Country>
                </cac:RegistrationAddress>
                @endif
            </cac:PartyLegalEntity>
            @if($seller->email or $seller->telephone)
            <cac:Contact>
                @if($seller->telephone)
                <cbc:Telephone>{{ $seller->telephone }}</cbc:Telephone>
                @endif
                @if($seller->email)
                <cbc:ElectronicMail>{{ $seller->email }}</cbc:ElectronicMail>
                @endif
            </cac:Contact>
            @endif
        </cac:Party>
    </cac:SellerSupplierParty>
    @endif
    @if($doc->detraccion)
    {{$detr = $doc->detraccion}}
    <cac:PaymentMeans>
        <cbc:PaymentMeansCode>{{ $detr->codMedioPago }}</cbc:PaymentMeansCode>
        <cac:PayeeFinancialAccount>
            <cbc:ID>{{ $detr->ctaBanco }}</cbc:ID>
        </cac:PayeeFinancialAccount>
    </cac:PaymentMeans>
    <cac:PaymentTerms>
        <cbc:PaymentMeansID>{{ $detr->codBienDetraccion }}</cbc:PaymentMeansID>
        <cbc:PaymentPercent>{{ $detr->percent }}</cbc:PaymentPercent>
        <cbc:Amount currencyID="PEN">{{ format($detr->mount) }}</cbc:Amount>
    </cac:PaymentTerms>
    @endif
    @if($doc->perception)
    <cac:PaymentTerms>
        <cbc:ID>Percepcion</cbc:ID>
        <cbc:Amount currencyID="PEN">{{ format($doc->perception.mtoTotal) }}</cbc:Amount>
    </cac:PaymentTerms>
    @endif
    @if($doc->anticipos)
    @foreach($doc->anticipos as $i => $ant)
    <cac:PrepaidPayment>
        <cbc:ID>{{ $i + 1 }}</cbc:ID>
        <cbc:PaidAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($ant->total) }}</cbc:PaidAmount>
    </cac:PrepaidPayment>
    @endforeach
    @endif
    @if($doc->cargos)
    @foreach($doc->cargos as $cargo)
    <cac:AllowanceCharge>
        <cbc:ChargeIndicator>true</cbc:ChargeIndicator>
        <cbc:AllowanceChargeReasonCode>{{ $cargo->codTipo }}</cbc:AllowanceChargeReasonCode>
        <cbc:MultiplierFactorNumeric>{{ $cargo->factor }}</cbc:MultiplierFactorNumeric>
        <cbc:Amount currencyID="{{ $doc->tipoMoneda }}">{{ format($cargo->monto) }}</cbc:Amount>
        <cbc:BaseAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($cargo->montoBase) }}</cbc:BaseAmount>
    </cac:AllowanceCharge>
    @endforeach
    @endif
    @if($doc->descuentos)
    @foreach($doc->descuentos as $desc)
    <cac:AllowanceCharge>
        <cbc:ChargeIndicator>false</cbc:ChargeIndicator>
        <cbc:AllowanceChargeReasonCode>{{ $desc->codTipo }}</cbc:AllowanceChargeReasonCode>
        <cbc:MultiplierFactorNumeric>{{ $desc->factor }}</cbc:MultiplierFactorNumeric>
        <cbc:Amount currencyID="{{ $doc->tipoMoneda }}">{{ format($desc->monto) }}</cbc:Amount>
        <cbc:BaseAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($desc->montoBase) }}</cbc:BaseAmount>
    </cac:AllowanceCharge>
    @endforeach
    @endif
    @if($doc->perception)
    {{$perc = $doc->perception}}
    <cac:AllowanceCharge>
        <cbc:ChargeIndicator>true</cbc:ChargeIndicator>
        <cbc:AllowanceChargeReasonCode>{{ $perc->codReg }}</cbc:AllowanceChargeReasonCode>
        <cbc:MultiplierFactorNumeric>{{ $perc->porcentaje }}</cbc:MultiplierFactorNumeric>
        <cbc:Amount currencyID="PEN">{{ format($perc->mto) }}</cbc:Amount>
        <cbc:BaseAmount currencyID="PEN">{{ format($perc->mtoBase) }}</cbc:BaseAmount>
    </cac:AllowanceCharge>
    @endif
    <cac:TaxTotal>
        <cbc:TaxAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->totalImpuestos) }}</cbc:TaxAmount>
        @if($doc->mtoISC)
        <cac:TaxSubtotal>
            <cbc:TaxableAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->mtoBaseIsc)}}</cbc:TaxableAmount>
            <cbc:TaxAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->mtoISC) }}</cbc:TaxAmount>
            <cac:TaxCategory>
                <cac:TaxScheme>
                    <cbc:ID>2000</cbc:ID>
                    <cbc:Name>ISC</cbc:Name>
                    <cbc:TaxTypeCode>EXC</cbc:TaxTypeCode>
                </cac:TaxScheme>
            </cac:TaxCategory>
        </cac:TaxSubtotal>
        @endif
        @if($doc->mtoOperGravadas)
        <cac:TaxSubtotal>
            <cbc:TaxableAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->mtoOperGravadas) }}</cbc:TaxableAmount>
            <cbc:TaxAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->mtoIGV) }}</cbc:TaxAmount>
            <cac:TaxCategory>
                <cac:TaxScheme>
                    <cbc:ID>1000</cbc:ID>
                    <cbc:Name>IGV</cbc:Name>
                    <cbc:TaxTypeCode>VAT</cbc:TaxTypeCode>
                </cac:TaxScheme>
            </cac:TaxCategory>
        </cac:TaxSubtotal>
        @endif
        @if($doc->mtoOperInafectas)
            <cac:TaxSubtotal>
                <cbc:TaxableAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->mtoOperInafectas) }}</cbc:TaxableAmount>
                <cbc:TaxAmount currencyID="{{ $doc->tipoMoneda }}">0</cbc:TaxAmount>
                <cac:TaxCategory>
                    <cac:TaxScheme>
                        <cbc:ID>9998</cbc:ID>
                        <cbc:Name>INA</cbc:Name>
                        <cbc:TaxTypeCode>FRE</cbc:TaxTypeCode>
                    </cac:TaxScheme>
                </cac:TaxCategory>
            </cac:TaxSubtotal>
        @endif
        @if($doc->mtoOperExoneradas)
            <cac:TaxSubtotal>
                <cbc:TaxableAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->mtoOperExoneradas) }}</cbc:TaxableAmount>
                <cbc:TaxAmount currencyID="{{ $doc->tipoMoneda }}">0</cbc:TaxAmount>
                <cac:TaxCategory>
                    <cac:TaxScheme>
                        <cbc:ID>9997</cbc:ID>
                        <cbc:Name>EXO</cbc:Name>
                        <cbc:TaxTypeCode>VAT</cbc:TaxTypeCode>
                    </cac:TaxScheme>
                </cac:TaxCategory>
            </cac:TaxSubtotal>
        @endif
        @if($doc->mtoOperGratuitas)
            <cac:TaxSubtotal>
                <cbc:TaxableAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->mtoOperGratuitas) }}</cbc:TaxableAmount>
                <cbc:TaxAmount currencyID="{{ $doc->tipoMoneda }}">0</cbc:TaxAmount>
                <cac:TaxCategory>
                    <cac:TaxScheme>
                        <cbc:ID>9996</cbc:ID>
                        <cbc:Name>GRA</cbc:Name>
                        <cbc:TaxTypeCode>FRE</cbc:TaxTypeCode>
                    </cac:TaxScheme>
                </cac:TaxCategory>
            </cac:TaxSubtotal>
        @endif
        @if($doc->mtoOperExportacion)
            <cac:TaxSubtotal>
                <cbc:TaxableAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->mtoOperExportacion) }}</cbc:TaxableAmount>
                <cbc:TaxAmount currencyID="{{ $doc->tipoMoneda }}">0</cbc:TaxAmount>
                <cac:TaxCategory>
                    <cac:TaxScheme>
                        <cbc:ID>9995</cbc:ID>
                        <cbc:Name>EXP</cbc:Name>
                        <cbc:TaxTypeCode>FRE</cbc:TaxTypeCode>
                    </cac:TaxScheme>
                </cac:TaxCategory>
            </cac:TaxSubtotal>
        @endif
        @if($doc->mtoOtrosTributos)
        <cac:TaxSubtotal>
            <cbc:TaxableAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->mtoBaseOth)}}</cbc:TaxableAmount>
            <cbc:TaxAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->mtoOtrosTributos) }}</cbc:TaxAmount>
            <cac:TaxCategory>
                <cac:TaxScheme>
                    <cbc:ID>9999</cbc:ID>
                    <cbc:Name>OTROS</cbc:Name>
                    <cbc:TaxTypeCode>OTH</cbc:TaxTypeCode>
                </cac:TaxScheme>
            </cac:TaxCategory>
        </cac:TaxSubtotal>
        @endif
    </cac:TaxTotal>
    <cac:LegalMonetaryTotal>
        <cbc:LineExtensionAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->valorVenta) }}</cbc:LineExtensionAmount>
        <cbc:TaxInclusiveAmount currencyID="PEN">{{ format($doc->mtoImpVenta) }}</cbc:TaxInclusiveAmount>
        @if($doc->mtoDescuentos)
        <cbc:AllowanceTotalAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->mtoDescuentos) }}</cbc:AllowanceTotalAmount>
        @endif
        @if($doc->sumOtrosCargos)
        <cbc:ChargeTotalAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->sumOtrosCargos) }}</cbc:ChargeTotalAmount>
        @endif
        @if($doc->totalAnticipos)
        <cbc:PrepaidAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->totalAnticipos) }}</cbc:PrepaidAmount>
        @endif
        <cbc:PayableAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($doc->mtoImpVenta) }}</cbc:PayableAmount>
    </cac:LegalMonetaryTotal>    
    @foreach($doc->details as $i => $detail)    
    <cac:InvoiceLine>
        <cbc:ID>{{ $i+1 }}</cbc:ID>
        <cbc:InvoicedQuantity unitCode="{{ $detail->unidad }}">{{ $detail->cantidad }}</cbc:InvoicedQuantity>
        <cbc:LineExtensionAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($detail->mtoValorVenta) }}</cbc:LineExtensionAmount>
        <cac:PricingReference>
            @if($detail->mtoValorGratuito)
            <cac:AlternativeConditionPrice>
                <cbc:PriceAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($detail->mtoValorGratuito) }}</cbc:PriceAmount>
                <cbc:PriceTypeCode>02</cbc:PriceTypeCode>
            </cac:AlternativeConditionPrice>
            @else
            <cac:AlternativeConditionPrice>
                <cbc:PriceAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($detail->mtoPrecioUnitario) }}</cbc:PriceAmount>
                <cbc:PriceTypeCode>01</cbc:PriceTypeCode>
            </cac:AlternativeConditionPrice>
            @endif
        </cac:PricingReference>
        @if($detail->cargos)
        @foreach($detail->cargos as $cargo)
        <cac:AllowanceCharge>
            <cbc:ChargeIndicator>true</cbc:ChargeIndicator>
            <cbc:AllowanceChargeReasonCode>{{ $cargo->codTipo }}</cbc:AllowanceChargeReasonCode>
            <cbc:MultiplierFactorNumeric>{{ $cargo->factor }}</cbc:MultiplierFactorNumeric>
            <cbc:Amount currencyID="{{ $doc->tipoMoneda }}">{{ $cargo->monto }}</cbc:Amount>
            <cbc:BaseAmount currencyID="{{ $doc->tipoMoneda }}">{{ $cargo->montoBase }}</cbc:BaseAmount>
        </cac:AllowanceCharge>
        @endforeach
        @endif
        @if($detail->descuentos)
        @foreach($detail->descuentos as $desc)
        <cac:AllowanceCharge>
            <cbc:ChargeIndicator>false</cbc:ChargeIndicator>
            <cbc:AllowanceChargeReasonCode>{{ $desc->codTipo }}</cbc:AllowanceChargeReasonCode>
            <cbc:MultiplierFactorNumeric>{{ $desc->factor }}</cbc:MultiplierFactorNumeric>
            <cbc:Amount currencyID="{{ $doc->tipoMoneda }}">{{ $desc->monto }}</cbc:Amount>
            <cbc:BaseAmount currencyID="{{ $doc->tipoMoneda }}">{{ $desc->montoBase }}</cbc:BaseAmount>
        </cac:AllowanceCharge>
        @endforeach
        @endif
        <cac:TaxTotal>
            <cbc:TaxAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($detail->totalImpuestos) }}</cbc:TaxAmount>
            @if($detail->isc)
            <cac:TaxSubtotal>
                <cbc:TaxableAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($detail->mtoBaseIsc) }}</cbc:TaxableAmount>
                <cbc:TaxAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($detail->isc) }}</cbc:TaxAmount>
                <cac:TaxCategory>
                    <cbc:Percent>{{ $detail->porcentajeIsc }}</cbc:Percent>
                    <cbc:TierRange>{{ $detail->tipSisIsc }}</cbc:TierRange>
                    <cac:TaxScheme>
                        <cbc:ID>2000</cbc:ID>
                        <cbc:Name>ISC</cbc:Name>
                        <cbc:TaxTypeCode>EXC</cbc:TaxTypeCode>
                    </cac:TaxScheme>
                </cac:TaxCategory>
            </cac:TaxSubtotal>
            @endif
            <cac:TaxSubtotal>
                <cbc:TaxableAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($detail->mtoBaseIgv) }}</cbc:TaxableAmount>
                <cbc:TaxAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($detail->igv) }}</cbc:TaxAmount>
                <cac:TaxCategory>
                    <cbc:Percent>{{ $detail->porcentajeIgv }}</cbc:Percent>
                    <cbc:TaxExemptionReasonCode>{{ $detail->tipAfeIgv }}</cbc:TaxExemptionReasonCode>
                    <cac:TaxScheme>
                        <cbc:ID>{{ $doc->tax['id'] }}</cbc:ID>
                        <cbc:Name>{{ $doc->tax['name'] }}</cbc:Name>
                        <cbc:TaxTypeCode>{{ $doc->tax['code'] }}</cbc:TaxTypeCode>
                    </cac:TaxScheme>
                </cac:TaxCategory>
            </cac:TaxSubtotal>
            @if($detail->otroTributo)
                <cac:TaxSubtotal>
                    <cbc:TaxableAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($detail->mtoBaseOth) }}</cbc:TaxableAmount>
                    <cbc:TaxAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($detail->otroTributo) }}</cbc:TaxAmount>
                    <cac:TaxCategory>
                        <cbc:Percent>{{ $detail->porcentajeOth }}</cbc:Percent>
                        <cac:TaxScheme>
                            <cbc:ID>9999</cbc:ID>
                            <cbc:Name>OTROS</cbc:Name>
                            <cbc:TaxTypeCode>OTH</cbc:TaxTypeCode>
                        </cac:TaxScheme>
                    </cac:TaxCategory>
                </cac:TaxSubtotal>
            @endif
        </cac:TaxTotal>
        <cac:Item>
            <cbc:Description><![CDATA[{{ $detail->descripcion }}]]></cbc:Description>
            @if($detail->codProducto)
            <cac:SellersItemIdentification>
                <cbc:ID>{{ $detail->codProducto }}</cbc:ID>
            </cac:SellersItemIdentification>
            @endif
            @if($detail->codProdSunat)
            <cac:CommodityClassification>
                <cbc:ItemClassificationCode>{{ $detail->codProdSunat }}</cbc:ItemClassificationCode>
            </cac:CommodityClassification>
            @endif
            @if($detail->codProdGS1)
            <cac:StandardItemIdentification>
                <cbc:ID>{{ $detail->codProdGS1 }}</cbc:ID>
            </cac:StandardItemIdentification>
            @endif
            @if($detail->atributos)
                @foreach($detail->atributos as $atr)
                    <cac:AdditionalItemProperty >
                        <cbc:Name>{{ $atr->name }}</cbc:Name>
                        <cbc:NameCode>{{ $atr->code }}</cbc:NameCode>
                        @if($atr->value)
                        <cbc:Value>{{ $atr->value }}</cbc:Value>
                        @endif
                        @if($atr->fecInicio || $atr->fecFin || $atr->duracion)
                            <cac:UsabilityPeriod>
                                @if($atr->fecInicio)
                                <cbc:StartDate>{{ date('Y-m-d',strtotime($atr->fecInicio)) }}</cbc:StartDate>
                                @endif
                                @if($atr->fecFin)
                                <cbc:EndDate>{{ date('Y-m-d', strtotime($atr->fecFin)) }}</cbc:EndDate>
                                @endif
                                @if($atr->duracion)
                                <cbc:DurationMeasure unitCode="DAY">{{ $atr->duracion }}</cbc:DurationMeasure>
                                @endif
                            </cac:UsabilityPeriod>
                        @endif
                    </cac:AdditionalItemProperty>
                @endforeach
            @endif
        </cac:Item>
        <cac:Price>
            <cbc:PriceAmount currencyID="{{ $doc->tipoMoneda }}">{{ format($detail->mtoValorUnitario) }}</cbc:PriceAmount>
        </cac:Price>
    </cac:InvoiceLine>
    @endforeach
</Invoice>
