<?php

namespace App\Abbost\WS;

use SoapHeader;
use SoapVar;

class SoapClient
{
   private $client;

    /**
     * SoapClient constructor.
     *
     * @param string $wsdl       Url of WSDL
     * @param array  $parameters Soap's parameters
     */
    public function __construct($wsdl = '', $parameters = [])
    {
        if (empty($wsdl)) {
            $wsdl = WsdlProvider::getBillPath();
        }
        #$this->client = new \SoapClient($wsdl, $parameters);
        $this->client = new \SoapClient($wsdl);
        #var_dump($parameters);
    }

    /**
     * @param $user
     * @param $password
     */
    public function setCredentials($user, $password)
    {
        $this->client->__setSoapHeaders($this->getHeader($user, $password));
    }

    /**
     * Set Url of Service.
     *
     * @param string $url
     */
    public function setService($url)
    {
        #$url = "http://google.com";
        $this->client->__setLocation($url);
    }

    /**
     * @param $function
     * @param $arguments
     *
     * @return mixed
     */
    public function call($function, $arguments)
    {
        return $this->client->__soapCall($function, $arguments);
    }    


    public function getHeader($username, $password){
        $WSS_NAMESPACE = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
    
        $security = new SoapVar(
            [new SoapVar(
                [
                    new SoapVar($username, XSD_STRING, null, null, 'Username', $WSS_NAMESPACE),
                    new SoapVar($password, XSD_STRING, null, null, 'Password', $WSS_NAMESPACE),
                ],
                SOAP_ENC_OBJECT,
                null,
                null,
                'UsernameToken',
                $WSS_NAMESPACE
            )],
            SOAP_ENC_OBJECT
        );
        return new SoapHeader($WSS_NAMESPACE, 'Security', $security, false);    
    }
}
