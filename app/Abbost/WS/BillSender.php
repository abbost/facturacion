<?php

namespace App\Abbost\WS;

use App\Abbost\Model\Response\BillResult;

class BillSender extends BaseSunat
{
	    /**
     * @param string $filename
     * @param string $content
     *
     * @return BillResult
     */
    public function send ($filename, $content) {

        $client = $this->getClient();
        $result = new BillResult();

        try {
            $zipContent = $this->compress($filename.'.xml', $content);
            $params = [
                'fileName' => $filename.'.zip',
                'contentFile' => $zipContent,
            ];
            $response = $client->call('sendBill', ['parameters' => $params]);
            $cdrZip = $response->applicationResponse;
            $cdr_response = $this->extractResponse($cdrZip);
            $result
                ->setCdrResponse($cdr_response['response']) // ws/src/ws/reader/DomCdrReader
                ->setCdrZip($cdrZip)
                ->setSuccess(true)
                ->setXmlContent($cdr_response['content']);
        } catch (\SoapFault $e) {
            $result->setError($this->getErrorFromFault($e));
        }

        return $result;
    }

   /* new */
    public function extract($zipContent){
        return $this->extractResponse($zipContent);
    }
}