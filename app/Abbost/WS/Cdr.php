<?php

namespace App\Abbost\WS;

use App\Abbost\WS\ConsultCdrService;
use App\Abbost\WS\SoapClient;


class Cdr
{

	private $errorMsg = null;
	private $filename = null;
    //

	function validateFields(array $items)
	{
	    #global $errorMsg;
	    $validateFiels = ['rucSol', 'userSol', 'passSol', 'ruc', 'tipo', 'serie', 'numero'];
	    foreach ($items as $key => $value) {
	        if (in_array($key, $validateFiels) && empty($value)) {
	            #$errorMsg = 'El campo '.$key.', es requerido';
	            return false;
	        }
	    }

	    return true;
	}

	function getCdrStatusService($user, $password)
	{		
	    $ws = new SoapClient(SunatEndpoints::FE_CONSULTA_CDR.'?wsdl');
	    $ws->setCredentials($user, $password);

	    $service = new ConsultCdrService();
	    $service->setClient($ws);

	    return $service;
	}

	function savedFile($filename, $content)
	{
		$pathZip = public_path('/xml/'.$filename);
	    #$pathZip = __DIR__.'/../../files/'.$filename;
	    file_put_contents($pathZip, $content);
	}

	public function process($fields)
	{		
	    global $filename;

	    if (!isset($fields['rucSol'])) {
	        return null;
	    }

	    # Esto da un error en API/CreateDocument
	    // if (!$this->validateFields($fields)) {
	    //     return null;
	    // }

	    $service = $this->getCdrStatusService($fields['rucSol'].$fields['userSol'], $fields['passSol']);

	    $arguments = [
	        $fields['ruc'],
	        $fields['tipo'],
	        $fields['serie'],
	        intval($fields['numero'])
	    ];
	    if (isset($fields['cdr'])) {
	        $result = $service->getStatusCdr(...$arguments);
	        if ($result->getCdrZip()) {
	            $filename = 'R-'.implode('-', $arguments).'.zip';
	            $this->savedFile($filename, $result->getCdrZip());
	        }

	        return $result;
	    }

	    return $service->getStatus(...$arguments);
	}

	function consult(Request $request){
		global $filename;
		// $data['rucSol'] = '20409355090';
		// $data['userSol'] = 'ABBOST19';
		// $data['passSol'] = 'Hotelbliam19';
		// $data['ruc'] = '20409355090';
		// $data['tipo'] = "01";
		// $data['serie'] = "F001";
		// $data['numero'] = 10;		

		$data['rucSol'] = $request->ruc_sol;
		$data['userSol'] = $request->user_sol;
		$data['passSol'] = $request->pass_sol;
		$data['ruc'] = $request->ruc;
		$data['tipo'] = $request->tipo;
		$data['serie'] = $request->serie;
		$data['numero'] =$request->correlativo;
		if(isset($request->cdr))
			$data['cdr'] = 1;

		$result = $this->process($data);
		return view('cdr/index',['result'=>$result,'filename'=>$filename]);

	}

}
