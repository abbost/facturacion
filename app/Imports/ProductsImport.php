<?php

namespace App\Imports;

use App\Models\Item;
use App\Models\OfficeItem;
use App\Models\Product;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;


class ProductsImport implements ToModel,WithStartRow
{
    public function model(array $row){
        $item = new Item;
        $item->business_id = session('business')->id;
        $item->code = $row[0];
        $item->name = $row[1];
        $item->unit_value = round($row[2] / 1.18, 2);
        $item->unit_price = round($row[2], 2);
        $item->origin =  2;
        $item->save();

        $origin = new Product;
        $origin->name = $item->name;
        $origin->business_id = session('business')->id;
        $origin->item_id = $item->id;
        $origin->save();

        $item->origin_id = $origin->id;
        $item->save();

        $offices = business('office')->get();
        foreach ($offices as $office) {
            $office_item = new OfficeItem;
            $office_item->business_id = business_id();
            $office_item->item_id = $item->id;
            $office_item->office_id = $office->id;
            $office_item->origin = $item->origin;
            $office_item->origin_id = $item->origin_id;
            $office_item->unit_value = $item->unit_value;
            $office_item->unit_price = $item->unit_price;
            $office_item->save();
        }
    }

     public function startRow(): int {
        return 2;
    }
   
}
