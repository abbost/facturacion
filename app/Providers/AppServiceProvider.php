<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

use App\Models\Business;
use App\Models\User;
use App\Models\Office;

class AppServiceProvider extends ServiceProvider{

    public function boot(){
        if (env('APP_ENV') === 'production') {
            $this->app['url']->forceScheme('https');
        }
    }

    public function register(){
    }
}
