<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Business;
use App\Models\Office;
use App\Models\OfficeItem;
use App\Models\CashTurn;

use Illuminate\Http\Request;

class OfficeController extends Controller{

    public function index (Request $request) {
        return view('office.index', [
            'offices' => business('office')->where('status', 1)->get(),
            'view' => 'business'
        ]);
    }

    public function create (Request $request) {
        return view('office.popup_create');
    }

    public function store (Request $request) {
        $office = $this->save($request, (new Office));
        $cash_turn = new CashTurn;
        $cash_turn->business_id = business_id();
        $cash_turn->user_id = session('user')->id;
        $cash_turn->office_id = $office->id;
        $cash_turn->save();

        foreach (business('item')->get() as $item) {
            $office_item = new OfficeItem;
            $office_item->business_id = business_id();
            $office_item->item_id = $item->id;
            $office_item->office_id = $office->id;
            $office_item->origin = $item->origin;
            $office_item->origin_id = $item->origin_id;
            $office_item->unit_value = $item->unit_value;
            $office_item->unit_price = $item->unit_price;
            $office_item->status = $item->status;
            $office_item->save();
        }

        session(['business' => Business::find(business_id())]);
        return redirect('office')
            ->with('status', 'Sucursal registrada correctamente');
    }

    public function edit (Request $request) {
        $office = business('office', $request->id);
        session(['update_office_id' => $office->id]);
        return view('office.popup_edit', [
            'office' => $office,
        ]);
    }

    public function update (Request $request) {
        $office = $this->save($request, Office::findOrFail(session('update_office_id')));
        return redirect('office')
            ->with('status', 'Sucursal actualizada correctamente');
    }

    public function save (Request $request, $office) {
        $office->business_id = business_id();
        $office->code = $request->code;
        $office->name = $request->name;

        $office->current_serie_factura = $request->serie_factura;
        $office->current_serie_boleta = $request->serie_boleta;
        $office->current_serie_proforma = $request->serie_proforma;
        $office->current_serie_nota_credito = $request->serie_nota_credito;

        $office->address = $request->address;
        $office->save();
        return $office;
    }

    public function delete (Request $request) {
        $office = business('office', $request->id);
        if (!$office->is_deletable()) {
            return redirect('office')
                ->with('error','No se puede eliminar la sucursal si tiene usuarios aún activos');
        }
        $office->cash_turns()->delete();

        office('office_item', null, $office->id)->delete();

        $office->delete();
        return redirect('office')
            ->with('status','Sucursal eliminada correctamente');
    }

}