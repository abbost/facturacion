<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use App\Models\Document;
use App\Models\DocumentDetail;
use PDF;

class SummaryController extends Controller{
	
	/* SIN SEPARACIÓN DE SUCURSAL AÚN */
    public function index (Request $request) {
        $business = business();
        $request->date_from = @$request->date_from ?: date('Y-m-d');
        $request->date_to = @$request->date_to ?: date('Y-m-d');
        if ((strtotime($request->date_to) - strtotime($request->date_from)) / 60 / 60 / 24 > 31) {
            return redirect('summary')
                ->with('error', "No se pueden hacer consultas con intervalos mayores a 31 días");
        }
        $data = [
            'business_id' => $business->id,
            'doc_type' => 'RD',
            'date_from' => $request->date_from,
            'date_to' => $request->date_to,
        ];

        $response = (object)app('App\Http\Controllers\API\DocumentController')->getRds(new Request($data));

        $documents = $response->documents;
        $documents->each(function ($document) {            
            $document->encrypted_id = id_mask($document->id, 'encrypt');
        });

        return view('summary.index', [
            'business' => business(),            
            'documents'=> @$documents ?: [],
            #'url'=>$this->apiUrl.'document/search?id=',
            'url' => url('api/document/search?id='),            
            'date_from' => $request->date_from,
            'date_to' => $request->date_to,
            'view' => 'summary'
        ]);
    }


    public function create(){
    	$business = business();
		$response = app('App\Http\Controllers\API\DocumentController')->resendBatch($business->id);    			
		if(!$response )
			$message = "NO hay boletas para enviar";
		else
			$message = "Se ha enviado el resumen diario";

		return redirect('summary')
               ->with('error', $message);                    
    }

    /** 
       No se reenvia nada el api, sino se consulta si el estado del tickey paso de proceso (98) a aceptado (0)
    **/
    public function resend(Request $data){        
        $data['emisor_doc'] = business()->ruc;
        $data['emisor_sector'] = business()->sector;
        $response = (object)app('App\Http\Controllers\API\DocumentController')->rdResend($data);
        return [
            'error'=>@$response->error,
            'message'=>@$response->message
        ];
    }

}

?>