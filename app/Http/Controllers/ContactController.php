<?php

namespace App\Http\Controllers;

use App\Models\Contact;

use Illuminate\Http\Request;

class ContactController extends Controller{

    public function create (Request $request) {
        return view('contact.popup_create');
    }

    public function store (Request $request) {
        $contact = new Contact;        
        $contact->customer_id = $request->customer_id;
        $contact->name = $request->name;                
        $contact->address = $request->address;
        $contact->save();
        return json_encode([
            'id' => $contact->id,
            'text'=>$contact->name.' - '.$contact->address,
            'name' => $contact->name,
            'address' => $contact->address 
        ]) ;
    }


    public function edit($id) {
        $contact = Contact::find($id);       
        return view('contact.popup_edit', [
            'contact' => $contact            
        ]);
    }

    public function update ($id, Request $request) {
        $contact = Contact::find($id);  
        $contact->name = $request->name;        
        $contact->address = $request->address;
        $contact->save();
        return json_encode([
            'id' => $contact->id,
            'text'=>$contact->name.' - '.$contact->address,
            'name' => $contact->name,
            'address' => $contact->address
        ]) ;
    }

    public function search(Request $request){
        # Esto se pone para que ignore los registros con customer_id null
        if(!$request->customer_id)
            return json_encode([['id'=>'C','doc'=>'', 'text'=>'Crear contacto']]);

        $contacts = Contact::where('customer_id',$request->customer_id)->get();
        
        foreach($contacts as $contact){
            $result[] = [
                'id'=>$contact->id,
                'text'=>$contact->name.' - '.$contact->address,
                'name'=>$contact->name,                
                'address'=>$contact->address
            ];
        }
        
        $result[] = ['id'=>'C','doc'=>'', 'text'=>'Crear contacto']; #Esto es para que en el select2 aparezca el link Crear Cliente
        
        return json_encode($result);
    }

}