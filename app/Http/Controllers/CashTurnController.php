<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CashTurn;
use App\Models\Document;
use App\Models\Expense;
use App\Models\Office;
use App\Models\Payment;

class CashTurnController extends Controller {

    public function index (Request $request) {
        $cash_turns = office('cash_turn')->where('status',1)->get()->splice(1);
        $cash_turns->push($this->get_current());
        $cash_turns = $cash_turns->reverse();
        session(['current_cash_turn_balance' => $cash_turns->first()->balance_2]);
        return view('cash_turn.index', [
            'cash_turns' => $cash_turns,
            'view' => 'cash_turn'
        ]);
    }
    
    public function create (Request $request) {
        return view('cash_turn.popup_create', [
            'balance' => session('current_cash_turn_balance')
        ]);
    }

    public function store (Request $request) {
        $current_cash_turn = $this->get_current();
        $balance = $current_cash_turn->balance_2;

        if (!is_numeric($request->taken)) {
            return redirect('cash_turn')
                ->with('error', 'Debe ingresar un monto a retirar válido');
        }

        if ($balance < 0) {
            return redirect('cash_turn')
                ->with('error', 'No se puede sacar dinero si el saldo es negativo');
        }

        if (($taken = round($request->taken, 2)) > $balance) {
            return redirect('cash_turn')
                ->with('error', 'Debe ingresar un monto a retirar válido');
        }

        $current_cash_turn->business_id = business_id();
        $current_cash_turn->office_id = office_id();
        $current_cash_turn->user_id = session('user')->id;
        $current_cash_turn->amount_taken = $taken;
        $current_cash_turn->amount_left = round($balance - $taken, 2);
        $current_cash_turn->save();

        return redirect('cash_turn')
            ->with('status', 'Cierre de caja registrado exitosamente');
    }

    private function get_current () {
        $last_cash_turn = office('cash_turn')->orderBy('id', 'desc')->first();
        $last_date = $last_cash_turn? $last_cash_turn->created_at : '';
        $last_amount = $last_cash_turn? $last_cash_turn->amount_left : 0;
        
        $payments = Payment::join('document','document.id','payment.document_id')
            ->where('document.status','<>',0)
            ->where('document.status','<>',2)  // 2: estado de documentos anulados
            ->where('payment.business_id',business_id())    
            ->where('payment.office_id',office_id())                        
            ->where('payment.env', ['development' => 'dev', 'production' => 'prod'][@business()->environment])
            ->where('payment.status', 1)
            ->where('payment.created_at', '>', $last_date)
            ->select('payment.*')
            ->get();


        $cash_list = $payments->where('means', 'cash');
        $card_list = $payments->where('means', 'card');
        $transfer_list = $payments->where('means', 'transfer');
        $expense_list = office('expense')
            ->where('status', 1)
            ->where('created_at', '>', $last_date)
            ->get();

        $current_cash_turn = new CashTurn;
        $current_cash_turn->amount_initial = $last_amount;

        $current_cash_turn->amount_cash = $cash_list->sum('amount');
        $current_cash_turn->amount_card = $card_list->sum('amount');
        $current_cash_turn->amount_transfer = $transfer_list->sum('amount');
        $current_cash_turn->amount_expense = $expense_list->sum('amount');
        
        $current_cash_turn->quantity_cash = $cash_list->count();
        $current_cash_turn->quantity_card = $card_list->count();
        $current_cash_turn->quantity_transfer = $transfer_list->count();
        $current_cash_turn->quantity_expense = $expense_list->count();
    
        $current_cash_turn->balance_2 = round(
            $current_cash_turn->amount_initial +
            $current_cash_turn->amount_cash +
            $current_cash_turn->amount_card +
            $current_cash_turn->amount_transfer -
            $current_cash_turn->amount_expense
        , 2);
        $current_cash_turn->amount_taken = 0;
        $current_cash_turn->amount_left = 0;

        return $current_cash_turn;
    }

}
