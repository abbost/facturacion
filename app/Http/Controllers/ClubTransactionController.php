<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClubTransactionController extends Controller{
	public function index(){
		$data=['business_id'=>session('business')->id];
		$transactions=[];
		$club_response = @json_decode($this->sendToClub($data,'business/transaction/get','post'));
		if(@$club_response->status == 'ok'){
			$transactions = @$club_response->transactions;
		}
		return view('transaction.index',
			['transactions'=>$transactions,
			'view'=>'club']);
	}
}