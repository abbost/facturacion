<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use App\Models\Item;
use App\Models\Document;
use App\Models\DocumentDetail;
use App\Models\Office;
use App\Models\Sale;
use App\Models\SaleDetail;
use App\Models\Payment;
use App\Models\CBusiness;
use App\Models\CPerson;
use App\Models\Contact;
use PDF;

class DocumentController extends Controller{

    /* PROBABLEMENTE LA FUNCIÓN MÁS CONCURRENTE */
    public function index(Request $request){
        if ((strtotime($request->date_to) - strtotime($request->date_from)) / 60 / 60 / 24 > 31) {
            return redirect('document')
                ->with('error', "No se pueden hacer consultas con intervalos mayores a 31 días");
        }
        
        set_current_dates($request);
        $types = [
            'P' => ['P', '00'],
            'F' => ['F', '01'],
            'B' => ['B', '03'],
            'NC' => ['NC', '07'],
            '*' => ['*'],
        ];
        
        $query = filtered('document', false, 'date')
            #->with('details')
            #->with('refered')
            #->with('cdr')
            ->with('payment')            
            ->where('env', business()->environment == 'production' ? 'prod' : 'dev');                

        /* Si no se elige todos (*) se muestra las facturas por default */
        $type = @$types[$request->type] ?: ['*'];        
        if ($type[0] == '*') {            
            $query = $query->whereIn('doc_type', ['00','01', '03','07']);
        }else{
            $query = $query->where('doc_type', $type[1]);            
        }

        $documents = $query
            ->orderBy('created_at', 'desc')
            ->get();

        
        // las notas de crédito tienen una vista especial
        if($type[0] == 'NC')        
            return view('document.nota.index', [                
                'documents' => $documents,
                'url' => url('api/document/search?id='),
                'type' => $type[0],
                'view' => 'document'
            ]);

        // los otros documentos tienen la vista general
        $total = 0;            
        foreach($documents as $document){
            // las notas de crédito se restan
            if($document->doc_type == '07')
                $total -= $document->total;
            else
                $total += ($document->status != 0 && $document->status != 2 ? $document->total : 0 );
            
            $document->has_resume_parent = $document->has_resume_parents();
            $document->encrypted_id = id_mask($document->id, 'encrypt');            
        }
        return view('document.index', [
            'total' => $total,
            'documents' => $documents,
            'url' => url('api/document/search?id='),
            'type' => $type[0],
            'view' => 'document'
        ]);
    }



    public function create(Request $request){
        $serie['boleta'] = office()->current_serie_boleta;
        $correlative['boleta'] = app('App\Http\Controllers\API\DocumentController')
            ->getNextCorrelative(business_id(), '03', $serie['boleta']);

        $serie['proforma'] = office()->current_serie_proforma;
        $correlative['proforma'] = app('App\Http\Controllers\API\DocumentController')
                ->getNextCorrelative(business_id(), '00', $serie['proforma']);

        $serie['factura'] = office()->current_serie_factura;
        $correlative['factura'] = app('App\Http\Controllers\API\DocumentController')
                    ->getNextCorrelative(business_id(), '01', $serie['factura']);        

        // Cargar stock en Items
        $items = items()->map(function ($item) {
            $item->type = $item->origin == 2 ? 'product' : 'service';
            $item->stock = $item->origin == 2 ? (@$item->_origin->stock(office_id())->quantity ?: 0) : 0;
            $item->unit_price = $item->office_item->unit_price;
            return $item;
        });

        return view('document.create', [
            'serie' => $serie,
            'correlative' => $correlative,
            'business'=> business(),
            'search_mode' => @session()->get('search_mode')?:'name',
            'items' => $items,
            'view' => 'invoice',
            'hasContact' => business()->has_contact
        ]);
    }

    public function store(Request $data){

        $data['emisor_doc'] = business()->ruc;
        $data['office_id'] = office_id();
        $data['emisor_sector'] = business()->sector;

        // se crea el document        
        if($data['document_type']=='00')
        $response = (object)app('App\Http\Controllers\API\DocumentController')->proforma_create($data);
        else if($data['document_type']=='01')
        $response = (object)app('App\Http\Controllers\API\DocumentController')->factura_create($data);
        else if($data['document_type']=='03')
        $response = (object)app('App\Http\Controllers\API\DocumentController')->boleta_create($data);
        else
            return redirect('document/boleta')->with('error','No existe ese tipo de coumento');

        if (@$response->error === 0) {
            $this->process_document($response->redirect, $data);
        }

        return redirect('document/create')
            ->with(@$response->error == 1 ? 'error' : 'status', $response->message)
            ->with('redirect', $data['print_ticket']? $response->redirect : '')
            ->withInput($response->error == 0 ? [] : $data->all()); // si la operacion es exitosa no necesita recordarse los inputs
    }


    
    public function edit($id){
        $document = Document::find($id);        
        $business = business();
        if($document->business_id <> business_id() )
            abort(403, 'Unauthorized action.');

        $items = items()->map(function ($item) {
            $item->type = $item->origin == 2 ? 'product' : 'service';
            $item->stock = $item->origin == 2 ? (@$item->_origin->stock(office_id())->quantity ?: 0) : 0;
            $item->unit_price = $item->office_item->unit_price;
            return $item;
        });
        
        return view('document.edit', [            
            'business'=> $business,
            'document' => $document,       
            'search_mode' => @session()->get('search_mode')?:'name',     
            'hasContact' => $business->has_contact,
            'items' => $items
        ]);
    }


    public function update(Request $data){
        
        $this->document_update($data);        
        $response = (object)['error' => 0, 'message' => 'EL documento '.$data->serie.'se actualizó correctamente'];

        return redirect('document')
            ->with(@$response->error == 1 ? 'error' : 'status', $response->message)            
            ->withInput($response->error == 0 ? [] : $data->all()); // si la operacion es exitosa no necesita recordarse los inputs        
    }




    public function nota_create(Request $request){
        #$serie = office()->current_serie_nota_credito;
        $serie = $request->ref_doc_type == "01" ? "FC01" : "BC01";
        $correlative = app('App\Http\Controllers\API\DocumentController')
                    ->getNextCorrelative(business_id(), '07', $serie);           
        $refered = office('document', $request->refered_document_id);           
        
        if($refered->business_id <> business_id() )
            abort(403, 'Unauthorized action.');
        
        $refered->customer_doc_name = [1 => "DNI", 4 => "CEX", 6 => "RUC", 7 => "PAS"][$refered->customer_doc_type];

        return view('document.nota.create', [
            'ref_serie'=>@$request->ref_serie,
            'ref_correlative'=>@$request->ref_correlative,
            'ref_doc_type'=>@$request->ref_doc_type,            
            'serie' => $serie,
            'correlative' => $correlative,
            'business'=> business(),
            'refered'=>$refered,            
            'view' => 'invoice'
        ]);
    }

    public function nota_store(Request $data){
        $data['emisor_doc'] = business()->ruc;
        $data['office_id'] = office_id();
        $data['emisor_sector'] = business()->sector;

        $response = (object)app('App\Http\Controllers\API\DocumentController')->nota_create($data);
        
        
        return redirect('document')
            ->with(@$response->error == 1 ? 'error' : 'status', $response->message)
            ->with('redirect', $data['print_ticket']? $response->redirect : '')
            ->withInput( $response->error == 0 ? [] : $data->all()); // si la operacion es exitosa no necesita recordarse los inputs
    }


    
    public function resend(Request $data){
        $data['emisor_doc'] = business()->ruc;
        $data['emisor_sector'] = business()->sector;
        $response = (object)app('App\Http\Controllers\API\DocumentController')->resend($data);
        return [
            'error' => @$response->error,
            'message' => @$response->message
        ];
    }



    public function cancel(Request $data){
        $data->validate([
            'refered_doc_type' => 'in:00,01,03'
        ]);
        $data['emisor_doc'] = business()->ruc;
                
        /**
         * Evitando que un usuario elimine documentos de otros usuarios 
        **/
        $refered_document = office('document', $data->refered_document_id);   
        if($refered_document->business_id <> business()->id)
            abort(403, 'Unauthorized action.');

        switch ($data->refered_doc_type) {
            case '00':                                
                $refered_document->status = 2;
                $refered_document->save();                
                $response = (object)[
                    'error' => 0,
                    'message' => 'Proforma anulada correctamente'
                ];
                break;
            case '01':
                $response = (object)app('App\Http\Controllers\API\DocumentController')->comunicacion_baja_create($data);
                break;
            case '03':
                $response = (object)app('App\Http\Controllers\API\DocumentController')->resumen_create($data);
                break;
        }
        return redirect('document')
            ->with(@$response->error ? 'error' : 'status', @$response->message);
    }



    /* Cargar los detalles para la nota de crédito */

    public function loadDetails(Request $request){
        $data = [
            'business_id' => business_id(),
            'doc_type' => $request->doc_type,
            'doc_ref_serie' => $request->doc_ref_serie,
            'doc_ref_correlat' => $request->doc_ref_correlat
        ];
        $response = (object)app('App\Http\Controllers\API\DocumentController')->loadDetails(new Request($data));

        $response->document->customer_doc_type = @[1 => "DNI", 4 => "CEX", 6 => "RUC", 7 => "PAS"][$response->document->customer_doc_type];
        return [
            'status'=>@$response->status ?: 0,
            'document'=>@$response->document,
            'details'=>@$response->details ?: []
        ];
    }




    private function process_document ($redirect, $request) {

        // OBTENER DOCUMENTO
        $id_masked_start = strpos($redirect, "api/document/search?id=") + 23;
        $id_masked_length = strpos($redirect, "&export_type=") - $id_masked_start;
        $document_id_mask = substr($redirect, $id_masked_start, $id_masked_length);
        if (!$document_id_mask) {
            return;
        }
        $document = office('document', id_mask($document_id_mask, 'decrypt'));
        if (!$document) {
            return;
        }

        // ACOMODAR CLIENTE
        $customer_origin = null;
        if($document->customer_doc == '00000000')
            # Buscamos globalmente            
            $customer_origin = \App\Models\CPerson::where('doc', '00000000')->first();        
        else
            if ($document->customer_doc_type == 6)
                $customer_origin = business('cbusiness')->where('status',1)->where('ruc', $document->customer_doc)->first();
            else
                $customer_origin = business('cperson')->where('status',1)->where('doc', $document->customer_doc)->first();

        if (!$customer_origin) {
            $customer = new \App\Models\Customer;            
            $customer->business_id = $document->business_id;
            $customer->save();

            // si se crea un cliente nuevo y un contacto al mismo tiempo
            if($request->contact_id){
                $contact = \App\Models\Contact::find($request->contact_id);
                $contact->customer_id = $customer->id;
                $contact->save();
            }

            if ($document->customer_doc_type == 6) {
                $customer_origin = new \App\Models\CBusiness;
                $customer_origin->legal_name = $document->customer_name;
                $customer_origin->ruc = $document->customer_doc;
                $origin = 1;

            } else {
                $customer_origin = new \App\Models\CPerson;
                $customer_origin->fullname = $document->customer_name;
                $customer_origin->doc_type = $document->customer_doc_type;
                $customer_origin->doc = $document->customer_doc;
                $origin = 2;
            }

            $customer_origin->address = $document->customer_address;
            $customer_origin->email = $document->customer_email;
            $customer_origin->customer_id = $customer->id;
            $customer_origin->business_id = $document->business_id;
            $customer_origin->save();

            $customer->origin = $origin;
            $customer->origin_id = $customer_origin->id;
            $customer->save();
        }
 
        $document->customer_id = $customer_origin->customer_id;
        $document->user_id = session('user')->id;        
        $document->save();
        


        // REDUCIR STOCK
        foreach ($document->details as $document_detail) {
            $item = business('item')->where('id', $document_detail->external_item_id)->first();
            if (@$item->origin == 2) {
                $item->_origin
                    ->stock($document->office_id)
                    ->reduce_stock($document_detail->quantity, $document_detail->id);
            }
        }

        
        $payment = new Payment;
        $payment->business_id = $document->business_id;
        $payment->office_id = $document->office_id;        
        $payment->document_id = $document->id;
        $payment->env = $document->env;
        $payment->amount = $document->total;
        $payment->means = @$request['payment_means'];
        $payment->save();              
    }



    public function document_update(Request $request){
        
        $document = Document::find($request->document_id);

         $customer_origin = null;
        if($request->receptor_doc == '00000000')
            # Buscamos globalmente            
            $customer_origin = \App\Models\CPerson::where('doc', '00000000')->first();        
        else
            if ( strtoupper($request->receptor_doc_type) == 'RUC')
                $customer_origin = business('cbusiness')->where('status',1)->where('ruc', $request->receptor_doc)->first();
            else
                $customer_origin = business('cperson')->where('status',1)->where('doc', $request->receptor_doc)->first();


        if (!$customer_origin) {
            $customer = new \App\Models\Customer;            
            $customer->business_id = business()->id;  //con request->business_id no funciona
            $customer->save();

            //si se crea un cliente nuevo y un contacto al mismo tiempo
            if($request->contact_id){
                $contact = \App\Models\Contact::find($request->contact_id);
                $contact->customer_id = $customer->id;
                $contact->save();
            }

            if ( strtoupper($request->receptor_doc_type) == 'RUC') {
                $customer_origin = new \App\Models\CBusiness;
                $customer_origin->legal_name = $request->receptor_name;
                $customer_origin->ruc = $request->receptor_doc;
                $origin = 1;

            } else {
                $customer_origin = new \App\Models\CPerson;
                $customer_origin->fullname = $request->receptor_name;
                $customer_origin->doc_type = $request->receptor_doc_type;
                $customer_origin->doc = $request->receptor_doc;
                $origin = 2;
            }

            $customer_origin->address = $request->receptor_address;
            #$customer_origin->email = $request->receptor_email;
            $customer_origin->customer_id = $customer->id;
            $customer_origin->business_id = business()->id;
            $customer_origin->save();

            $customer->origin = $origin;
            $customer->origin_id = $customer_origin->id;
            $customer->save();
        }

        if($request->contact_id){
            $contact = Contact::find($request->contact_id);
            $document->contact_name = $contact->name;
            $document->contact_address = $contact->address;
            $document->contact_id = @$request->contact_id;       
        }

        $document->customer_doc = $request->receptor_doc;
        $document->customer_name = $request->receptor_name;        
        $document->customer_address = $request->receptor_address;
        $document->date = $request->date;        
        $document->sub_total = $request->sub_total;                
        $document->tax = $request->tax;                
        $document->total = $request->total;             
        $document->note = $request->note;
        $document->order_reference = $request->order_reference;   
        $document->guide = $request->guide;        
        $document->save();

        // borramos los detalles
        DocumentDetail::where('document_id',$document->id)->delete();

        // agregamos los nuevos detalles
        foreach ($request->products as $item) {
            $document_detail = new DocumentDetail();
            $document_detail->document_id = $document->id;
            $document_detail->description  = $item['description'];                    
            $document_detail->quantity = $item['quantity'];
            $document_detail->unit_price = $item['price'];
            $document_detail->sub_total = $item['sub_total'];
            $document_detail->tax = $item['tax'];
            $document_detail->total = $item['total'];
            $document_detail->save();

            #Actualizamos el stock
            // $item = business('item')->where('id', $sale_detail['item_id'])->first();
            // if (@$item->origin == 2) {
            //     $item->_origin
            //         ->stock($document->office_id)
            //         ->reduce_stock($sale_detail['quantity'], $sale_detail->id);
            // }
        }

        $payment = Payment::where('document_id',$document->id)->first();
        $payment->means = @$request['payment_means'];
        $payment->amount = @$request->total;
        $payment->save();

    }

}
