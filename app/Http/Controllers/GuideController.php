<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Car;
use App\Models\Driver;
use App\Models\Guide;
use App\Models\GuideDetail;
use App\Models\Document;
use DB;

class GuideController extends Controller{

	public function index(Request $request){
		#aqui aparecera la lista de guia, solo tendra la opcion de imprimir
		$request->date_from = @$request->date_from ?: date('Y-m-d');
        $request->date_to = @$request->date_to ?: date('Y-m-d');
		$guides = Guide::all();
		
		return view('guide.index',[
			'date_from' => $request->date_from,
            'date_to' => $request->date_to,
            'guides' => $guides,
            'view' => 'guide'            
		]);
	}

	public function create($document_id){
		
		$document = office('document',$document_id);
			
		return view('guide.create',[
			'document' => $document,
			'cars' => Car::all(),
            'drivers' => Driver::all()
		]);
	}

	public function store(Request $request){
		$data = $request->all();	
		$car = Car::find($request->car_id);
		$drive = Driver::find($request->driver_id);


		$guide = new Guide;
		$guide->document_id = $data['document_id'];
		$guide->date = $data['date_'];
		$guide->place_from = $data['place_from'];
		$guide->custom_name = $data['custom_name'];
		$guide->custom_doc = $data['custom_doc'];
		$guide->place_to = $data['place_to'];
		$guide->car_brand =  $car->brand;
		$guide->car_plate =  $car->plate;
		$guide->car_certificate =  $data['car_certificate'];
		$guide->driver_license =  $drive->license;
		$guide->save();

		for($i=0 ; $i< sizeof($data['quantity']) ; $i++){
			$guide_detail = new GuideDetail();
			$guide_detail->guide_id = $guide->id;
			$guide_detail->quantity = $data['quantity'][$i];
			$guide_detail->description = $data['description'][$i];
			$guide_detail->weight = $data['weight'][$i];
			$guide_detail->cost = $data['cost'][$i];
			$guide_detail->save();
		}

		$html = view('guide.print',['guide'=>$guide])->render();
		$error = 0;
		return json_encode([
			'message' => 'Guia creada correctamente',
			'error' => $error,
			'html_ticket' => $html
		]);
	}

	public function copy($id){
		$guide = Guide::find($id);
		$html = view('guide.print',['guide'=>$guide])->render();
		$error = 0;
		return json_encode([
			'message' => '´Copia generada correctamente',
			'error' => $error,
			'html_ticket' => $html
		]);
	}

	public function test($id){
		$guide = Guide::find($id);
		return view('guide.print',['guide'=>$guide]);
	}

}

?>