<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Expense;
use App\Models\Office;

class ExpenseController extends Controller {

    public function index (Request $request) {
        set_current_dates($request);
        return view('expense.index', [
            'expenses' => filtered('expense')->where('status', 1)->orderBy('id', 'desc')->get(),
            'view' => 'expense'
        ]);
    }

    public function create (Request $request) {
        return view('expense.popup_create');
    }

    public function store(Request $request){
        $expense = new Expense;
        $expense->business_id = business_id();
        $expense->user_id = session('user')->id;
        $expense->office_id = office_id();
        $expense->amount = round(floatval($request->amount), 2);
        $expense->description = $request->description;
        $expense->save();
        return redirect('expense')
            ->with('status', 'Gasto registrado correctamente');
    }

    public function delete(Request $request){
        $expense = office('expense', $request->id);
        if (!$expense->is_deletable()) {
            return redirect('expense')
                ->with('error', 'Este gasto no pudo ser eliminado debido a que pertenece a un cierre de caja pasado');
        }
        $expense->status = 0;
        $expense->save();
        return redirect('expense')
            ->with('status', 'Gasto eliminado correctamente');
    }

}
