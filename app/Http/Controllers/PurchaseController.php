<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Purchase;
use App\Models\PurchaseDetail;
use App\Models\Provider;
use App\Models\Product;
use App\Models\Stock;
use App\Models\Office;
use App\Models\Input;

class PurchaseController extends Controller {

    public function index (Request $request) {
        set_current_dates($request);
        return view('purchase.index', [
            'purchases' => business('purchase')
                ->where('created_at', '>=', dates('from'))
                ->where('created_at', '<', date('Y-m-d', strtotime(dates('to') . ' +1 day')))
                ->orderBy('id', 'desc')
                ->get(),
            'view' => 'purchase'
        ]);
    }

    public function show (Request $request) {
        $purchase = business('purchase', $request->id);
        return view('purchase.popup_show',[
            'purchase' => $purchase,
            'offices' => $purchase->offices(),
        ]);
    }

    public function create(Request $request){
        return view('purchase.popup_create', [
            'providers' => business('provider')->get(),
            #'products' => business('product')->where('status',1)->get(),
            'offices' => offices()
        ]);
    }

    public function store (Request $request) {

        $provider = business('provider', $request->provider_id);

        $details = $request->purchase_details;
        if (!sizeof($details['total'] ?: []) || !sizeof($details['product_id'] ?: []) || !sizeof($details['quantity'] ?: [])) {
            return redirect('purchase')
                ->with('error','Faltan datos para completar la compra');
        }

        $purchase_total = 0;
        foreach ($details['total'] as $value) {
            if (!is_numeric($value ?: 0)) {
                return redirect('purchase')
                    ->with('error','Faltan datos para completar la compra');
            }
            $purchase_total += $value;
        }

        foreach ($details['quantity'] as $value) {
            if (!is_numeric($value ?: 0)) {
                return redirect('purchase')
                    ->with('error','Faltan datos para completar la compra');
            }
        }

        foreach ($details['product_id'] as $i => $value) {
            $details['product_id'][$i] = business('product', $value);
        }

        $offices = offices();

        $purchase = new Purchase;
        $purchase->business_id = business_id();
        $purchase->provider_id = $provider->id;
        $purchase->user_id = session('user')->id;
        $purchase->voucher = $request->voucher;
        $purchase->total = $purchase_total;
        $purchase->save();

        foreach ($details['product_id'] as $i => $product) {
            {
                $purchase_detail_quantity = 0;
                for ($j = $i * $offices->count(), $k = 0; $k < $offices->count(); $k++, $j++) {
                    $purchase_detail_quantity += round(@$details['quantity'][$j]);
                }
                if (!$purchase_detail_quantity) {
                    continue;
                }
            }
            $purchase_detail = new PurchaseDetail;
            $purchase_detail->purchase_id = $purchase->id;
            $purchase_detail->product_id = $product->id;
            $purchase_detail->quantity = $purchase_detail_quantity;
            $purchase_detail->total = round(floatval($details['total'][$i]), 2);
            $purchase_detail->save();

            $purchase_total += $purchase_detail->total;
            $input_unit_cost = round(floatval($purchase_detail->total / $purchase_detail->quantity), 2);

            for ($j = $i * $offices->count(), $k = 0; $k < $offices->count(); $k++, $j++) {
                $quantity = round(@$details['quantity'][$j]);
                $office_id = $offices->get($k)->id;
                if (!$quantity) {
                    continue;
                }
                $stock = $product->stock($office_id);
                if (!$stock) {
                    $stock = new Stock;
                    $stock->business_id = business_id();
                    $stock->office_id = $office_id;
                    $stock->product_id = $product->id;
                    $stock->quantity = 0;
                    $stock->save();
                }
                $input = new Input;
                $input->business_id = business_id();
                $input->office_id = $office_id;
                $input->product_id = $product->id;
                $input->stock_id = $stock->id;
                $input->purchase_detail_id = $purchase_detail->id;
                $input->quantity = $quantity;
                $input->balance = $quantity;
                $input->unit_cost = $input_unit_cost;
                $input->save();

                $stock->quantity += $quantity;
                $stock->save();
            }
        }

        return redirect('purchase')
            ->with('status', 'Compra registrada correctamente');

    }

    public function cancel (Request $request) {
        $purchase = business('purchase', $request->id);
        session(['delete_purchase_id' => $purchase->id]);
        return view('purchase.popup_delete', [
            'purchase' => $purchase,
        ]);
    }

    public function delete (Request $request) {
        $purchase = business('purchase', session('delete_purchase_id'));
        if (!$request->reason) {
            return redirect('purchase')
                ->with('error', 'Se debe especificar una razón de anulación');
        } else if (!$purchase->is_deletable()) {
            return redirect('purchase')
                ->with('error', 'Esta compra no pudo ser eliminada debido a que sus items ya tienen consumos registrados');
        }
        $purchase->delete_reason = $request->reason;
        $purchase->status = 0;
        $purchase->save();
        $purchase->safe_delete();
        return redirect('purchase')
            ->with('status', 'Compra anulada correctamente');
    }

}
