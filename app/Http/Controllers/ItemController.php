<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;

use App\Imports\ProductsImport;
use App\Imports\ServicesImport;
use App\Models\Item;
use App\Models\Service;
use App\Models\Product;
use App\Models\Input;
use App\Models\Office;
use App\Models\OfficeItem;

class ItemController extends Controller{

    // Inventario de productos
    public function index (Request $request) {
        return view('item.index', [
            'items' => business('item')->where('status',1)->where('origin', $request->type == 'P' ? 2 : 1)->orderBy('name', 'asc')->get(),
            'offices' => business('office')->get(),
            'type' => $request->type == 'P' ? 'P' : 'S',
            'view' => 'item'
        ]);
    }

    public function change_search_mode(Request $request){        
        session()->put('search_mode',$request->search_mode);        
    }

    public function get_json(Request $request){
        $term = $request->term;        
        
        $target = @session()->get('search_mode') ?: 'name';        
        $items = business('item')->where('status',1)->where($target,'like',"%$term%")->get();
        $items = $items->map(function ($item) {
            $item->type = $item->origin == 2 ? 'product' : 'service';
            $item->stock = $item->origin == 2 ? (@$item->_origin->stock(office_id())->quantity ?: 0) : 9999;
            $item->unit_price = $item->office_item->unit_price;
            $item->label = $item->name.($item->origin == 2 ? '___STOCK : '.$item->stock : '').'__PRECIO : S/.'.$item->unit_price;
            return $item;
        });

        return response()->json($items);
    }

    public function show_inputs (Request $request) {
        $item = business('item')
            ->where('id', $request->id)
            ->where('origin', 2)
            ->firstOrFail();
        $inputs = office('input')
            ->where('product_id', $item->origin_id)
            ->where('status', 1)
            ->where('balance', '>', 0);
        return view('item.popup_inputs', [
            'inputs' => $inputs->orderBy('id', 'asc')->get(),
            'name' => $item->name
        ]);
    }

    public function create (Request $request) {
        return view('item.popup_create', [
            'type' => $request->type
        ]);
    }

    public function store (Request $request) {
        $item = $this->save($request, (new Item));
        $item->origin = $request->type == 'product' ? 2 : 1;

        $origin = $item->origin == 2 ? (new Product) : (new Service);
        $origin->name = $item->name;
        $origin->business_id = business_id();
        $origin->item_id = $item->id;
        $origin->save();

        $item->origin_id = $origin->id;
        $item->save();

        $offices = business('office')->get();
        foreach ($offices as $office) {
            $office_item = new OfficeItem;
            $office_item->business_id = business_id();
            $office_item->item_id = $item->id;
            $office_item->office_id = $office->id;
            $office_item->origin = $item->origin;
            $office_item->origin_id = $item->origin_id;
            $office_item->unit_value = $item->unit_value;
            $office_item->unit_price = $item->unit_price;
            $office_item->save();
        }

        if (@$request->from_document_create === 'true') {
            return $item;
        }
        return redirect('item?type=' . ($item->origin == 2 ? 'P' : 'S'))
            ->with('status','Ítem registrado correctamente');
    }

    public function edit(Request $request){
        $item = business('item', $request->id);
        session(['update_item_id' => $item->id]);
        return view('item/popup_edit', [
            'item' => $item
        ]);
    }

    public function update(Request $request){
        $item = $this->save($request, business('item', session('update_item_id')));
        $origin = $item->_origin;
        $origin->name = $item->name;
        $origin->save();

        $office_item = $item->office_item;
        $office_item->unit_price = $item->unit_price;
        $office_item->unit_value = $item->unit_value;
        $office_item->save();

        return redirect('item?type=' . ($item->origin == 2 ? 'P' : 'S'))
            ->with('status','Ítem actualizado correctamente');
    }

    public function save(Request $request, $item){
        $item->business_id = business_id();
        $item->code = $request->code;
        $item->name = $request->name;
        $item->unit_value = round($request->cipher / 1.18, 2);
        $item->unit_price = round($request->cipher, 2);
        if ($request->onu_product_code) {
            $item->onu_product_code = $request->onu_product_code;
        }
        $item->save();
        return $item;
    }

    // public function delete (Request $request) {
    //     $item = business('item', $request->item_id);
    //     if (!$item->is_deletable()) {
    //         return redirect('item?type=' . ($item->origin == 2 ? 'P' : 'S'))
    //             ->with('error', 'Este ítem no pudo ser eliminado debido a que ya tiene stock registrado en el sistema o ventas vinculadas');
    //     }
    //     if ($item->origin == 2) {
    //         $product = $item->_origin;
    //         $product->stocks->each(function ($stock) {
    //             $stock->delete();
    //         });
    //     }
    //     $item->_origin->delete();
    //     $item->delete();
    //     return redirect('item?type=' . ($item->origin == 2 ? 'P' : 'S'))
    //         ->with('status', 'Ítem eliminado correctamente');
    // }


    public function delete (Request $request) {
        $item = business('item', $request->item_id);
        $item->status = 0;
        $item->save();

        $item->_origin->status = 0;
        $item->_origin->save();


        $item->office_item->status = 0;
        $item->office_item->save();

        return redirect('item?type=' . ($item->origin == 2 ? 'P' : 'S'))
            ->with('status', 'Ítem eliminado correctamente');
    }



    public function change_availability (Request $request) {
        $item = business('item', $request->id);
        $office_item = $item->office_item;
        $office_item->status ^= 1;
        $office_item->save();
        return redirect('item?type=' . ($item->origin == 2 ? 'P' : 'S'))
            ->with('status', 'Ítem ' . ($office_item->status ? 'activado' : 'desactivado') . ' correctamente');
    }

    // IMPORT EXCEL

    public function show_upload (Request $request) {
        return view('item.popup_upload', [
            'type' => $request->type == 'P' ? 'product' : 'service',
            'title' => $request->type == 'P' ? 'Producto' : 'Servicio',
            'video_url' => 'https://www.youtube.com/embed/45cYwDMibGo',
        ]);
    }

    public function product_export_excel(){
        $file_path=public_path('/download_excel/products_import.xlsx');
        return Response::download($file_path);
    }
    
    public function product_import_excel(Request $request){
        $validator = Validator::make($request->all(),
            ['file' => 'mimes:xlsx,xls']
        );

        if ($validator->fails()) {
            return redirect('item/?type=P')->with('error','Debe cargar el excel de Productos');
        }
        $file = $request->file('file');
        if($file){
            Excel::import(new ProductsImport, $file); 
            return redirect('item?type=P')->with('status','los ítems productos ha sido registrado correctamente');
        }else {
            return redirect('item?type=P')->with('error','Debe cargar el excel de Productos');
        }
    }

    public function service_export_excel(){
        $file_path=public_path('/download_excel/services_import.xlsx');
        return Response::download($file_path);
    }

    public function service_import_excel(Request $request){
        $validator = Validator::make($request->all(), 
            [   'file' => 'mimes:xlsx,xls'  ]
        );

        if ($validator->fails()) {
            return redirect('item/?type=S')->with('error','<strong style="color:#F00">Debe cargar el excel de servicios</strong>');
        }

        $file = $request->file('file');
        if($file){
            Excel::import(new ServicesImport, $file); 
            return redirect('item?type=S')->with('status','los ítems servicios ha sido registrado correctamente');
        } else {
            return redirect('item?type=S')->with('error','<strong style="color:#F00">Debe cargar el excel de Servicios</strong>');
        }
        
    }



     public function search(Request $request){
        $term = strtolower($request->term);
        $result = [];

        $products = business('product')
        ->where('status',1)
        ->where(function ($q) use ($term) { # esto se usa para encerrar en parentesis ( cond1 OR cond2)
            $q->where('name','like','%'.$term.'%');
        })
        ->get();


        foreach($products as $product) {                          
            $result[] = [
                'id' => $product->id,
                'text' => $product->name, 
                'name'=> $product->name            
            ];
        }
            

        return \Response::json($result);
    }


}
