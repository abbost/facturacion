<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Document as Document;

class CustomerController extends Controller{
    public function searchCustomer(Request $request){
        $document = Document::where('business_id', $request->business_id)
            ->where('customer_doc_type', @["DNI" => 1, "CEX" => 4, "RUC" => 6, "PAS" => 7][$request->customer_doc_type])
            ->where('customer_doc','like', '%'.$request->customer_doc.'%')
            ->whereNotNull('customer_name')
            ->orderBy('id', 'desc')->first();
        return [
            'doc'=>@$document->customer_doc,
            'name'=>@$document->customer_name,
            'address'=>@$document->customer_address,
            'email'=>@$document->customer_email
        ];
    }
}
