<?php

namespace App\Http\Controllers\API\DocumentTraits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\Document as Document;
use App\Models\DocumentDetail as DocumentDetail;
use App\Models\Business as Business;
use App\Models\Office as Office;
use App\Models\Item as Item;
use App\Models\SunatCDR as SunatCDR;
use App\Models\ResumeDetail as ResumeDetail;
// use App\Models\Cancellation as Cancellation;

use App\Abbost\Model\Sale\Invoice;
use App\Abbost\Model\Sale\Note;
use App\Abbost\Model\Sale\SaleDetail;
use App\Abbost\Model\Sale\Legend;
use App\Abbost\Model\Voided\Voided;
use App\Abbost\Model\Voided\VoidedDetail;
// use App\Abbost\Model\Sale\Document;
use App\Abbost\Model\Sale\Tax;
use App\Abbost\Model\Summary\Summary;
use App\Abbost\Model\Summary\SummaryDetail;
use App\Abbost\Model\Summary\SummaryPerception;
use App\Abbost\Model\Client\Client;
use App\Abbost\Model\Company\Company;
use App\Abbost\Model\Company\Address;
use App\Abbost\WS\SoapClient;
use App\Abbost\WS\BillSender;
use App\Abbost\WS\SummarySender;
use App\Abbost\WS\ExtService;
use App\Abbost\WS\SunatEndpoints;
use App\Abbost\WS\ConsultCdrService;

// use DOMDocument;
// use ZipArchive;
// use stdClass;

#use SoapClient;
use DB;
use PDF;
use QrCode;
// use SimpleXMLElement;
use Exception;

trait DocumentCreate {

    /*
        ESTADOS DE UN DOCUMENTO:
            0   ->  RECHAZADO
            1   ->  ACEPTADO
            2   ->  ANULADO
            3   ->  PENDIENTE
            4   ->  CON N. CRÉDITO
            5   ->  ELIMINADO
    */

    // CUANDO EL CLIENTE SOLO QUIERE CREAR TCKETS Y NO GUARDARLOS COMO BOLETA O FACTURA (CONTROL INTERNO)
    public function proforma_create(Request $data){

        // VERIFICAMOS EMISOR
        $business = Business::where('ruc', $data->emisor_doc)->first();

        if($stopper = $this->checkPolitics($business)){
            return $stopper;
        }

        // OBTENIENDO LOCAL O SUCURSAL
        $office = \App\Models\Office::where('business_id', $business->id);
        if ($data['office_id']) {
            $office = $office->where('id', $data['office_id']);
        }
        $office = $office->first();

        // VERIFICAMOS SI EL CORRELATIVO ESTÁ DUPLICADO
        if($this->isRepeatedDocument($business->id, $data->serie, $data->correlative)) {
            return ['error'=>1,'message'=>'Ya existe un documento con esa serie y correlativo'];
        }

        // LLENAMOS DATA
        $data['receptor_doc_type'] = ["DNI" => 1, "CEX" => 4, "RUC" => 6, "PAS" => 7][strtoupper($data->receptor_doc_type)];        
        $data['date'] = $data->date." ".date("H:i:s");  #Pueden llenar una documento con fecha pasada
        $data['currency'] = $data->currency ?: 'PEN';
        $data['doc_oper_type'] = $data->doc_oper_type ?: '00';
        $data['office_id'] = $office->id;

        // GUARDAMOS EN BD
        $document = $this->saveDocument($data, "00", $business);

        // COMPROBAMOS RESULTADOS
        $message = "El documento ".$document->serie."-".$document->correlative." ha sido aceptada";
        $redirect = url(
            'api/document/search?id='.$this->id_mask($document->id, 'encrypt').'&export_type='.
            ($business->print_option == 'ticket' ? 'ticket' : 'pdf')
        );
        $document->status = 1;
        $document->save();

        return [
            'error' => 0,
            'message' => $message,
            'redirect' => $redirect
        ];
    }




    // CÓDIGO PARA EL REGISTRO DE BOLETAS, FACTURAS Y NOTAS DE CRÉDITO LUEGO DE SU ENVÍO EN LAS FUNCIONES RESPECTIVAS
    public function processResult($result, $data, $business, $docType, $serie, $correlative){

        // ANALIZAMOS RESPUESTAS 
        if($result->getCdrResponse()){
            $code = intval( $result->getCdrResponse()->getCode() );
            $message = $result->getCdrResponse()->getDescription();
            $notes =  implode(',', $result->getCdrResponse()->getNotes() );
        }else{
            $code = intval( $result->getError()->getCode() );
            $message = $result->getError()->getMessage();
            $notes = '';
        }

        $xml = '';
        $documentId = 0;
        $redirect = '';

        // REGISTRO DEL DOCUMENTO
        if($code >= 100 && $code < 2000){
            // EXCEPCIONES - documento reenviable
            $error = 1;
        }elseif($code >= 2000 && $code < 4000){
            // ERRORES - documento rechazado
            $error = 1;
            if(isset($data['document_id'])) {
                $document = Document::find($data['document_id']);
            }else{
                $document = $this->saveDocument($data, $docType ,$business);
            }
            $document->status = 0;
            $document->save();

            // CON SALE
            //$document->safe_delete();

            $documentId = $document->id;
        } elseif($code == 0 || $code > 4000) {
            // OBSERVACIONES - documento aceptado
            $error = 0;
            if(isset($data['document_id'])){
                $document = Document::find($data['document_id']);
            }else{
                $document = $this->saveDocument($data, $docType ,$business);
            }
            $document->status = 1;
            $document->save();

            // ANULAR EL DOCUMENTO REFERIDO
            if ($document->doc_type == '07') {
                $refered = $document->refered;
                $refered->status = 4;
                $refered->save();

                // CON SALE
                //$refered->safe_delete();

            }

            // POBLAMOS LOS DATOS A REGISTRARSE EN EL CDR Y A RETORNAR
            $xml = base64_encode($result->getXmlContent());
            $documentId = $document->id;
            $redirect = url(
                'api/document/search?id='.$this->id_mask($document->id, 'encrypt').'&export_type='.
                ($business->print_option == 'ticket' ? 'ticket' : 'pdf')
            );

            // Esta función se traslado a save document para que se envia cuando sunat esté offline            
            // if(@$data['receptor_email']){
            //     $message.= "<br>".$this->sendMailToCustomer($document, $business, [
            //         'receptor_email' => $data['receptor_email'],
            //         'receptor_email_content' => $data['receptor_email_content']
            //     ]);
            // };
        }

        // REGISTRAMOS EL CDR
        $sunat_cdr = new SunatCDR;
        $sunat_cdr->business_id = $business->id;
        $sunat_cdr->document_id = $documentId;
        $sunat_cdr->serie = $serie;
        $sunat_cdr->correlative = $correlative;
        $sunat_cdr->code = $code;
        $sunat_cdr->answer = $xml;
        $sunat_cdr->notes = $notes;
        $sunat_cdr->message = $message;
        $sunat_cdr->env = ['development'=>'dev','production'=>'prod'][$business->environment];
        $sunat_cdr->save();

        return ['error'=>$error, 'message'=>$message, 'redirect' => $redirect];   

    }





    // CREACIÓN Y ENVÍO DE FACTURAS
    public function factura_create(Request $data){

        // VERIFICAMOS EMISOR
        $business = Business::where('ruc', $data->emisor_doc)->first();
        if($stopper = $this->checkPolitics($business)){
            return $stopper;
        }

        // OBTENIENDO LOCAL O SUCURSAL
        $office = \App\Models\Office::where('business_id', $business->id);
        if ($data['office_id']) {
            $office = $office->where('id', $data['office_id']);
        }
        $office = $office->first();

        // VERIFICAMOS TICKET (DE XAFIRO) EN CASO LO HAYA
        if($data->ticket_id && $this->isRepeatedTicket($business->id, $data->ticket_id)) {
            return ['error'=>1,'message'=>'Ya existe un documento con ese ticket'];
        }

        // VERIFICAMOS SI EL CORRELATIVO ESTÁ DUPLICADO
        if($this->isRepeatedDocument($business->id, $data->serie, $data->correlative)) {
            return ['error'=>1,'message'=>'Ya existe un documento con esa serie y correlativo'];
        }

        // VERIFICAMOS RUC DEL RECEPTOR
        if (!$data->receptor_doc || strlen($data->receptor_doc) != 11) {
            return ['error'=>1,'message'=>'El RUC está mal ingresado','redirect'=>''];
        }

        // LLENAMOS DATA
        $data['receptor_doc_type'] = 6; # RUC
        $data['date'] = $data->date." ".date("H:i:s");  #Pueden llenar una documento con fecha pasada        
        $data['currency'] = $data->currency?:'PEN';
        $data['doc_oper_type'] = $data->doc_oper_type?:'01';
        $data['office_id'] = $office->id;

        // VERIFICAMOS SI SE PUEDE ENVIAR A SUNAT O NO (por configuración de usuario)

        if($stopper = $this->checkActiveSunat($business)){
            $document = $this->saveDocument($data,"01",$business);
            $url = url(
                'api/document/search?id='.$this->id_mask($document->id, 'encrypt').'&export_type='.
                ($business->print_option == 'ticket' ? 'ticket' : 'pdf')
            );
            return ['error'=>0,'message'=>"Documento generado correctamente",'redirect'=>$url];
        }

        // PREPARAMOS DATOS DE RECEPTOR
        {
            $address = new Address();
            $address->setUrbanizacion('NONE') # Ubigeo = 15101
                ->setDireccion($data->receptor_address)                
                ->setCodigoPais('PE');

            $client = new Client();
            $client->setTipoDoc('6') # Siempre sera RUC : 6
                ->setNumDoc($data->receptor_doc)
                ->setRznSocial($data->receptor_name)
                ->setAddress($address);
        }

        // PREPARAMOS DATOS DE EMISOR
        {
            $address = new Address(); # Ubigeo = 15101
            $address->setDireccion($office->address)
                ->setCodLocal($office->code)
                ->setCodigoPais('PE');

            $company = new Company();
            $company->setRuc($business->ruc)
                ->setRazonSocial($business->legal_name)
                ->setNombreComercial($business->comercial_name)
                ->setAddress($address);
        }

        // PREPARAMOS DATOS DE FACTURA
        $invoice = new Invoice();
        $invoice
            ->setUblVersion('2.1')
            #->setFecVencimiento(date('Y-m-d'))
            ->setTipoOperacion('0101')
            ->setTipoDoc('01') #Siempre será Factura : 01
            ->setSerie($data->serie)
            ->setCorrelativo($data->correlative)
            ->setFechaEmision($data['date'])
            ->setTipoMoneda('PEN')
            ->setCompany($company)
            ->setClient($client)
            #->setMtoOperGravadas($data->sub_total)
            ->setMtoIGV($data->tax)
            ->setTotalImpuestos($data->tax)
            ->setValorVenta($data->sub_total)
            ->setMtoImpVenta($data->total);


        if($data->emisor_sector == 'edu'){
            $invoice->setMtoOperInafectas($data->sub_total);
            $invoice->setTax( Tax::getByAfectacion(30) );  #IGV PARA LOS COLEGIOS ES CERO
        }else{
            $invoice->setMtoOperGravadas($data->sub_total);
            $invoice->setTax( Tax::getByAfectacion(10) );  #IGV ES 10
        }


        if($data->order_reference){
            $invoice->setCompra($data->order_reference);
        }

        if($data->guide){            
            $invoice->setGuias([['nroDoc'=>$data->guide, 'tipoDoc' => '09']]);
        }

        if($data->note){
            $invoice->setNota($data->note);
        }

        // PREPARAMOS DATOS DE DETALLES DE FACTURA
        $items = [];
        foreach($data->products as $product):
            $item = new SaleDetail();
            $item->setCodProducto('P001')
                ->setUnidad('NIU')
                ->setDescripcion(@$product['description'])
                ->setCantidad(@$product['quantity'])
                ->setMtoValorUnitario(@$product['sub_total'] / (@$product['quantity'] ?: 1))
                ->setMtoValorVenta(@$product['sub_total'])
                ->setMtoBaseIgv(@$product['sub_total'])                
                ->setIgv(@$product['tax'])
                ->setTotalImpuestos(@$product['tax'])
                ->setMtoPrecioUnitario(@$product['total'] / (@$product['quantity'] ?: 1));                

                if($data->emisor_sector == 'edu'){
                    $item->setPorcentajeIgv(0)
                         ->setTipAfeIgv('30');
                }else{
                    $item->setPorcentajeIgv(18)
                         ->setTipAfeIgv('10');
                }            
            
            $items[] = $item;
        endforeach;
        
        $invoice->setDetails($items);


        $invoice
            ->setLegends([
                (new Legend())
                    ->setCode('1000')
                    ->setValue(num2letras($data->total))
            ]);

        // FIRMAMOS EL DOCUMENTO
        $fileName = $business->ruc.'-'.$invoice->tipoDoc.'-'.$invoice->serie.'-'.$invoice->correlativo;

        $xml = view('xml::invoice',['doc'=>$invoice])->render();
        $path = public_path('/xml/'.$fileName.'.xml');
        file_put_contents($path, $xml);
        $xmlSigned = $this->sign( $business, $path );
        file_put_contents($path, $xmlSigned);

        // CREAMOS EL WSCLIENT
        $wsClient = new SoapClient($this->WSDL);
        $wsClient->setService($this->getSunatService($business));
        $wsClient->setCredentials(strtoupper($business->ruc.$business->sol_user), $business->sol_pass);

        // ENVIAMOS EL DOCUMENTO
        $billSender = new BillSender();
        $billSender->setClient($wsClient);
        $result = $billSender->send($fileName,$xmlSigned); # Es de tipo BillResult

        return $this->processResult($result, $data, $business, $invoice->tipoDoc, $invoice->serie, $invoice->correlativo);

    }




    // CREACIÓN Y ENVÍO DE BOLETAS
    public function boleta_create(Request $data){

        // VERIFICAMOS EMISOR
        $business = Business::where('ruc', $data->emisor_doc)->first();
        if($stopper = $this->checkPolitics($business)){
            return $stopper;
        }

        // OBTENIENDO LOCAL O SUCURSAL
        $office = \App\Models\Office::where('business_id', $business->id);
        if ($data['office_id']) {
            $office = $office->where('id', $data['office_id']);
        }
        $office = $office->first();


        // VERIFICAMOS TICKET (DE XAFIRO) EN CASO LO HAYA
        if($data->ticket_id && $this->isRepeatedTicket($business->id, $data->ticket_id)) {
            return ['error'=>1,'message'=>'Ya existe un documento con ese ticket'];
        }

        // VERIFICAMOS SI EL CORRELATIVO ESTÁ DUPLICADO
        if($this->isRepeatedDocument($business->id, $data->serie, $data->correlative)) {
            return ['error'=>1,'message'=>'Ya existe un documento con esa serie y correlativo'];
        }

        // VERIFICAMOS DNI DEL RECEPTOR
        // EL NOMBRE DEL RECEPTOR DEBE TENER 3 DIGITOS COMO MINIMO ---

        if (!$data->receptor_doc || strlen($data->receptor_doc) != 8 || strlen($data->receptor_name) < 3 )
            if (floatval($data->total) > 700)
                return ['error'=>1,'message'=>"Pasados los 700 soles es obligatorio ingresar un DNI de 8 digitos y un nombre de 3 dígitos o más"];
            else{
                $data['receptor_doc'] = '00000000'; 
                $data['receptor_name'] = '---'; 
            }
                

        // if ($data->receptor_doc == "---" && $data->receptor_name == "---") {
        //     if (floatval($data->total) > 700) {
        //         return ['error'=>1,'message'=>"Pasados los 700 soles es obligatorio ingresar el DNI del receptor"];
        //     }            
        //     $data['receptor_doc'] = '00000000';
        // }

        // LLENAMOS DATA
        $data['receptor_doc_type'] = ["DNI" => 1, "CEX" => 4, "RUC" => 6, "PAS" => 7][strtoupper($data->receptor_doc_type)];

        // VERIFICAMOS SI EL DOCUMENTO ES NULO O VACIO PARA PONER UN DOCUMENTO POR DEFAULT
        // if (array_search($data['receptor_doc'], ['---', '-', null]) !== false) {
        //     $data['receptor_doc'] = '00000000';
        // }

        $data['date'] = $data->date." ".date("H:i:s");  #Pueden llenar una documento con fecha pasada
        $data['currency'] = $data->currency?:'PEN';
        $data['doc_oper_type'] = $data->doc_oper_type?:'01';
        $data['office_id'] = $office->id;

        // VERIFICAMOS SI SE PUEDE ENVIAR A SUNAT O NO (por configuración de usuario)
        if($stopper = $this->checkActiveSunat($business)){
            $document = $this->saveDocument($data,"03",$business);
            $url = url(
                'api/document/search?id='.$this->id_mask($document->id, 'encrypt').'&export_type='.
                ($business->print_option == 'ticket' ? 'ticket' : 'pdf')
            );
            return ['error'=>0,'message'=>"Documento generado correctamente",'redirect'=>$url];
        }

        // PREPARAMOS DATOS DE RECEPTOR
        {
            $address = new Address();
            $address->setUrbanizacion('NONE') # Ubigeo = 15101
                ->setDireccion($data->receptor_address)
                ->setCodigoPais('PE');

            $client = new Client();
            $client->setTipoDoc($data->receptor_doc_type) # Puede ser DNI , PAS, CEX
                ->setNumDoc($data->receptor_doc)
                ->setRznSocial($data->receptor_name)
                ->setAddress($address);
        }

        // PREPARAMOS DATOS DE EMISOR
        {
            $address = new Address(); 
            $address->setDireccion($office->address)
                ->setCodLocal($office->code)
                ->setCodigoPais('PE');

            $company = new Company();
            $company->setRuc($business->ruc)
                ->setRazonSocial($business->legal_name)
                ->setNombreComercial($business->comercial_name)
                ->setAddress($address);
        }

        // PREPARAMOS DATOS DE BOLETA
        $invoice = new Invoice();
        $invoice
            ->setUblVersion('2.1')
            ->setTipoOperacion('0101')
            ->setTipoDoc('03') #Siempre será 03: Boleta
            ->setSerie($data->serie)
            ->setCorrelativo($data->correlative)
            ->setFechaEmision($data['date'])
            ->setTipoMoneda('PEN')
            ->setCompany($company)
            ->setClient($client)
            #->setMtoOperGravadas($data->sub_total)
            ->setMtoIGV($data->tax)
            ->setTotalImpuestos($data->tax)
            ->setValorVenta($data->sub_total)
            ->setMtoImpVenta($data->total);
        

        if($data->emisor_sector == 'edu'){
            $invoice->setMtoOperInafectas($data->sub_total);
            $invoice->setTax( Tax::getByAfectacion(30) );  #IGV PARA LOS COLEGIOS ES CERO
        }else{
            $invoice->setMtoOperGravadas($data->sub_total);
            $invoice->setTax( Tax::getByAfectacion(10) );  #IGV ES 10
        }


       if($data->order_reference){
            $invoice->setCompra($data->order_reference);
        }

        if($data->guide){            
            $invoice->setGuias([['nroDoc'=>$data->guide, 'tipoDoc' => '09']]);
        }

        if($data->note){
            $invoice->setNota($data->note);
        }
        
        // RREPARAMOS DATOS DE DETALLES DE BOLETA
        $items = [];
        foreach($data->products as $product):
            $item = new SaleDetail();
            $item->setCodProducto('P001')
                ->setUnidad('NIU')
                ->setDescripcion($product['description'])
                ->setCantidad($product['quantity'])
                ->setMtoValorUnitario($product['sub_total'] / $product['quantity'])
                ->setMtoValorVenta($product['sub_total'])
                ->setMtoBaseIgv($product['sub_total'])                
                ->setIgv($product['tax'])
                ->setTotalImpuestos($product['tax'])
                ->setMtoPrecioUnitario($product['total'] / $product['quantity']);

                if($data->emisor_sector == 'edu'){
                    $item->setPorcentajeIgv(0)
                         ->setTipAfeIgv('30');
                }else{
                    $item->setPorcentajeIgv(18)
                         ->setTipAfeIgv('10');
                }
                        
            $items[] = $item;
        endforeach;
        $invoice->setDetails($items);


        $invoice
            ->setLegends([
                (new Legend())
                    ->setCode('1000')
                    ->setValue(num2letras($data->total))
            ]);

        // FIRMAMOS EL DOCUMENTO
        $fileName = $business->ruc.'-'.$invoice->tipoDoc.'-'.$invoice->serie.'-'.$invoice->correlativo;

        $xml = view('xml::invoice',['doc'=>$invoice])->render();
        $path = public_path('/xml/'.$fileName.'.xml');
        file_put_contents($path, $xml);
        $xmlSigned = $this->sign( $business, $path );

        // CREAMOS EL WSCLIENT
        $wsClient = new SoapClient( $this->WSDL );
        $wsClient->setService($this->getSunatService($business));
        $wsClient->setCredentials(strtoupper($business->ruc.$business->sol_user), $business->sol_pass);

        // ENVIAMOS EL DOCUMENTO
        $billSender = new BillSender();
        $billSender->setClient($wsClient);
        $result = $billSender->send($fileName,$xmlSigned);

        return $this->processResult($result, $data, $business, $invoice->tipoDoc, $invoice->serie, $invoice->correlativo);

    }




    // CREACIÓN Y ENVÍO DE NOTAS DE CRÉDITO
    public function nota_create(Request $data){

        // VERIFICAMOS EMISOR
        $business = Business::where('ruc', $data->emisor_doc)->first();
        if($stopper = $this->checkPolitics($business)){
            return $stopper;
        }

        // OBTENIENDO LOCAL O SUCURSAL
        $office = \App\Models\Office::where('business_id', $business->id);
        if ($data['office_id']) {
            $office = $office->where('id', $data['office_id']);
        }
        $office = $office->first();

        // LLENAMOS DATA
        $data['receptor_doc_type'] = ["DNI" => 1, "CEX" => 4, "RUC" => 6, "PAS" => 7][strtoupper($data->receptor_doc_type)];
        #$data['time'] = $data->time?: date('H:i:s');
        $data['currency'] = $data->currency?:'PEN';
        $data['doc_oper_type'] = $data->doc_oper_type?:'01';
        $data['office_id'] = $office->id;


        // PREPARAMOS DATOS DE RECEPTOR
        {
            $address = new Address();
            $address->setUrbanizacion('NONE') # Ubigeo = 15101
                ->setDireccion($data->receptor_address)
                ->setCodigoPais('PE');

            $client = new Client();
            $client->setTipoDoc($data->receptor_doc_type)
                ->setNumDoc($data->receptor_doc)
                ->setRznSocial($data->receptor_name)
                ->setAddress($address);
        }

        // PREPARAMOS DATOS DE EMISOR
        {
            $address = new Address(); 
            $address->setDireccion($office->address)
                ->setCodLocal($office->code)
                ->setCodigoPais('PE');

            $company = new Company();
            $company->setRuc($business->ruc)
                ->setRazonSocial($business->legal_name)
                ->setNombreComercial($business->comercial_name)
                ->setAddress($address);
        }

        // PREPARAMOS DATOS DE NOTA DE CRÉDITO
        $note = new Note();
        $note
            ->setUblVersion('2.1')
            ->setTipDocAfectado(@['F' => '01', 'B' => '03', '03' => '03', '01' => '01'][$data->doc_reference_type])
            ->setNumDocfectado($data->doc_ref_serie."-".$data->doc_ref_correlat)
            ->setCodMotivo($data->doc_response_code)
            ->setDesMotivo($data->subject)
            ->setTipoDoc('07')
            ->setSerie($data->serie)
            ->setFechaEmision(new \DateTime())
            ->setCorrelativo($data->correlative)
            ->setTipoMoneda('PEN')
            // ->setGuias([
            //     (new Document())
            //     ->setTipoDoc('09')
            //     ->setNroDoc('001-213')
            // ])
            ->setCompany($company)
            ->setClient($client)
            #->setMtoOperGravadas($data->sub_total)
            ->setMtoIGV($data->tax)
            ->setTotalImpuestos($data->tax)
            ->setMtoImpVenta($data->total);            
        

        if($data->emisor_sector == 'edu'){
            $note->setMtoOperInafectas($data->sub_total);
            $note->setTax( Tax::getByAfectacion(30) );  #IGV PARA LOS COLEGIOS ES CERO
        }else{
            $note->setMtoOperGravadas($data->sub_total);
            $note->setTax( Tax::getByAfectacion(10) );  #IGV ES 10
        }


        // PREPARAMOS DATOS DE DETALLES DE NOTA DE CRÉDITO
        $items = [];
        foreach($data->products as $product):
            $item = new SaleDetail();
            $item->setCodProducto('P001')
                ->setUnidad('NIU')
                ->setDescripcion($product['description'])
                ->setCantidad($product['quantity'])
                ->setMtoValorUnitario($product['sub_total'])
                ->setMtoValorVenta($product['sub_total'])
                ->setMtoBaseIgv($product['sub_total'])
                ->setIgv($product['tax'])
                ->setTotalImpuestos($product['tax'])
                ->setMtoPrecioUnitario($product['total']);

                if($data->emisor_sector == 'edu'){
                    $item->setPorcentajeIgv(0)
                         ->setTipAfeIgv('30');
                }else{
                    $item->setPorcentajeIgv(18)
                         ->setTipAfeIgv('10');
                }
            
            $items[] = $item;
        endforeach;
        
        $note->setDetails($items);


        $note
            ->setLegends([
                (new Legend())
                    ->setCode('1000')
                    ->setValue(num2letras($data->total))
            ]);

        // AJUSTAMOS EL DOCUMENTO SI VIENE DE XAFIRO
        $refered_document = null;
        if(!$data->refered_document_id){
            $env = ['development'=>'dev', 'production' => 'prod'][$business->environment];
            $refered_document = Document::where('serie',$data->doc_ref_serie)
                ->where('correlative', $data->doc_ref_correlat)
                ->where('doc_type', @['F' => '01', 'B' => '03'][$data->doc_reference_type])
                ->where('business_id', $business->id)
                ->where('env', $env)
                ->first();
            if (!$refered_document) {
                return ['error' => 1, 'message' => 'El documento referido no existe', 'redirect' => ''];
            }
            $data->merge(['refered_document_id' => $refered_document->id]);
        }else{
            $refered_document = Document::find($data->refered_document_id);
        }

        // FIRMAMOS EL DOCUMENTO
        $fileName = $business->ruc.'-'.$note->tipoDoc.'-'.$note->serie.'-'.$note->correlativo;

        $xml = view('xml::notacr',['doc'=>$note])->render();
        $path = public_path('/xml/'.$fileName.'.xml');
        file_put_contents($path, $xml);
        $xmlSigned = $this->sign( $business, $path );

        // VERIFICAMOS SI SE PUEDE ENVIAR A SUNAT O NO (por configuración de usuario)
        if($stopper = $this->checkActiveSunat($business)){
            $document = $this->saveDocument($data,"07",$business);
            $url = url('api/document/search?id='.$this->id_mask($document->id, 'encrypt').'&export_type='.
                ($business->print_option == 'ticket' ? 'ticket' : 'pdf')
            );
            return ['error'=>0,'message'=>"Documento generado correctamente",'redirect'=>$url];
        }

        // CREAMOS EL WS CLIENTE
        $wsClient = new SoapClient($this->WSDL);
        $wsClient->setService($this->getSunatService($business));
        $wsClient->setCredentials(strtoupper($business->ruc.$business->sol_user), $business->sol_pass);
        
        // ENVIAMOS EL DOCUMENTO
        $billSender = new BillSender();
        $billSender->setClient($wsClient);
        $result = $billSender->send($fileName,$xmlSigned);

        return $this->processResult($result, $data, $business, $note->tipoDoc, $note->serie, $note->correlativo);

    }




    // CREACIÓN Y ENVÍO DE COMUNICACIONES DE BAJA (ANULACIÓN DE FACTURAS)
    public function comunicacion_baja_create(Request $data){

        #SOLO PARA FACTURAS
        $business = Business::where('ruc', $data->emisor_doc)->first();

        // OBTENIENDO LOCAL O SUCURSAL
        $office = \App\Models\Office::where('business_id', $business->id);
        if ($data['office_id']) {
            $office = $office->where('id', $data['office_id']);
        }
        $office = $office->first();

        $serie = "RA";
        $correlative = $this->getNextCorrelative($business->id,"RA","RA");
        $refered_document = null;
        if(!$data->refered_document_id):
            $env = ['development'=>'dev', 'production' => 'prod'][$business->environment];
            $refered_document = Document::where('serie',$data->doc_ref_serie)
                ->where('correlative', $data->doc_ref_correlat)
                ->where('doc_type', ['F' => '01', 'B' => '03'][$data->doc_reference_type])
                ->where('business_id', $business->id)
                ->where('env', $env)
                ->first();
            if (!$refered_document) {
                return ['error' => 1, 'message' => 'El documento referido no existe', 'redirect' => ''];
            }
            $data->merge(['refered_document_id' => $refered_document->id]);
        else:
            $refered_document = Document::find($data->refered_document_id);
        endif;

        // Business
        {
            $address = new Address(); 
            $address->setDireccion($office->address)
                ->setCodLocal($office->code)
                ->setCodigoPais('PE');

            $company = new Company();
            $company->setRuc($business->ruc)
                ->setRazonSocial($business->legal_name)
                ->setNombreComercial($business->comercial_name)
                ->setAddress($address);
        }

        // Documento
        $detail = new VoidedDetail();
        $detail->setTipoDoc($refered_document->doc_type)
            ->setSerie($refered_document->serie)
            ->setCorrelativo($refered_document->correlative)
            ->setDesMotivoBaja('ERROR EN CÁLCULOS');

        $voided = new Voided();
        $voided->setCorrelativo($correlative)
            ->setFecGeneracion(new \DateTime($refered_document->date))
            ->setFecComunicacion(new \DateTime())
            ->setCompany($company)
            ->setDetails([$detail]);

        /* GUARDAMOS EN LA BD */        
        $data['serie'] = 'RA';
        $data['date'] = date('Y-m-d');
        $data['time'] = date('H:i:s');
        $data['correlative'] = $correlative;        
        $data['office_id'] = $office->id;

        $fileName = $business->ruc."-".$voided->getXmlId();        
        $xml = view('xml::voided',['doc'=>$voided])->render();
        $path = public_path('/xml/'.$fileName.'.xml');
        file_put_contents($path, $xml);
        $xmlSigned = $this->sign( $business, $path );
                
        /* CREAMOS EL WS CLIENTE */
        $wsClient = new SoapClient( $this->WSDL );
        $wsClient->setService($this->getSunatService($business));
        $wsClient->setCredentials(strtoupper($business->ruc.$business->sol_user), $business->sol_pass);

        /* ENVIAMOS EL DOCUMENTO */
        $sumSender = new SummarySender();
        $sumSender->setClient($wsClient);
        $res = $sumSender->send($fileName,$xmlSigned);

        /* COMPROBAMOS RESULTADOS */
        $xml = "";
        $message = "";
        $notes = "";
        $code = 1;
        $ticket = 0;
        $error = 1;

        /* COMPROBAMOS RESULTADOS */
        if ($res->isSuccess()):
            $ticket = $res->getTicket();

            /** CONSULTADMOS EL ESTADO DEL TICKET */
            $ticketClient = $this->getTicketClient($business);
            $verification = $ticketClient->getStatus($ticket);

            #cuando ya esta procesado codigo = 0 o codigo = 99
            if ($verification->isSuccess()) {
                $error = 0;
                $cdr = $verification->getCdrResponse(); #string
                $zip = $verification->getCdrZip();
                $xml = $verification->getXmlContent();
                $message = $cdr->getDescription();
                $code = $cdr->getCode();
                $notes = implode(",", $cdr->getNotes() );    #getNotes es []

                $refered_document->status = 2; #estado de anulado
                $refered_document->save();

                // CON SALE
                //$refered_document->safe_delete();

                $document = $this->saveDocument($data,$serie,$business);
                $document->status = 1;
                $document->save();

            } else{
                #Aqui entran las EXCEPCIONES, LOS ERRORES Y LOS PENDIENTES
                $message = $verification->getError()->getMessage();
                $code = $verification->getError()->getCode();

                if($code == 98){
                    $error = 0;
                    $document = $this->saveDocument($data,$serie,$business);
                    $document->status = 1;
                    $document->save();
                    $refered_document->status = 2; #estado de anulado
                    $refered_document->save();

                    // CON SALE
                    //$refered_document->safe_delete();

                }
                #cuando arroja error [2000 - 4000] se puede enivar con el mismo correlativo
            }
        else:
            #casos:
            #1. Documento que ya se encuentra anulado o rechazado
            #2. Documento fuera de fecha
            $message = $res->getError()->getMessage();
            $code = intval($res->getError()->getCode());
        endif;
        
        // refered_document : es el documento a anular
        // documen: es la comunicación de baja. 

        /* MAS QUE UN CDR ES UN LOG  */
        $sunat_cdr = new SunatCDR;
        $sunat_cdr->business_id = $business->id;
        $sunat_cdr->serie = $serie;
        $sunat_cdr->correlative = $correlative;
        $sunat_cdr->document_id = @$document ? $document->id : 0;
        $sunat_cdr->ticket = $ticket;
        $sunat_cdr->code = intval($code);
        $sunat_cdr->notes = $notes;
        $sunat_cdr->answer = base64_encode($xml);
        $sunat_cdr->message = $message;
        $sunat_cdr->env = ['development'=>'dev','production'=>'prod'][$business->environment];
        $sunat_cdr->save();

        return ['error'=>$error, 'message'=>$message];    
    }






    // CÓDIGO PARA EL REGISTRO DE RESÚMENES DIARIOS LUEGO DE SU ENVÍO EN LAS FUNCIONES RESPECTIVAS
    public function resumen_create_base ($business, $documents, $status) {
        /*
            Solo debería recibir boletas (documentos con doc_type = '03')
            Business y listado de documentos previamente creados
            Con status se define si se está anulando o reenviando los documentos
            Status = 3 cuando se quiere anular la boleta. Status = 1 en resendBatch
        */

        // PREPARAMOS DATOS DE EMISOR
        {
            $address = new Address();
            $address->setDireccion($business->address)
                ->setCodigoPais('PE');
            $company = new Company();
            $company->setRuc($business->ruc)
                ->setRazonSocial($business->legal_name)
                ->setNombreComercial($business->comercial_name)
                ->setAddress($address);
        }

        // PREPARAMOS DATOS DE DETALLE DE RESUMEN
        $details = $documents->map(function ($boleta) use ($status) {
            $detail = new SummaryDetail();
            $detail->setTipoDoc($boleta->doc_type)
                ->setSerieNro($boleta->serie."-".$boleta->correlative)
                ->setEstado($status) #Estado 1 es aceptado; 3 es anulado
                ->setClienteTipo($boleta->customer_doc_type)
                ->setClienteNro($boleta->customer_doc)
                ->setTotal($boleta->total)
                ->setMtoOperGravadas($boleta->sub_total)
                ->setMtoIGV($boleta->tax);
            return $detail;
        })->toArray();

        // PREPARAMOS DATOS DE RESUMEN
        $serie = "RC";
        $correlative = $this->getNextCorrelative($business->id, $serie , $serie);

        $generation_date = new \DateTime();
        // si es anulacion (status = 3) dejo la fecha normal, si es resumen es un dia antes
        #$generation_date = $status == 3 ? $generation_date : $generation_date->sub(new \DateInterval('P1D'));
        // tomamos la fecha de la primera boleta
        $generation_date = $status == 3 ? $generation_date : new \DateTime($documents->first()->date);

        $sum = new Summary();
        $sum->setFecGeneracion($generation_date)
            ->setFecResumen(new \DateTime())
            ->setCorrelativo($correlative)
            ->setCompany($company)
            ->setDetails($details);

        // LLENAMOS DATA
        $data['serie'] = $serie;
        $data['date'] = $generation_date->format('Y-m-d');        
        $data['correlative'] = $correlative;
        $data['refered_document_id'] = null;

        // FIRMAMOS EL DOCUMENTO
        $fileName = $business->ruc."-".$sum->getXmlId();
        $xml = view('xml::summary',['doc'=>$sum])->render();
        $path = public_path('/xml/'.$fileName.'.xml');
        file_put_contents($path, $xml);
        $xmlSigned = $this->sign($business, $path);

        // CREAMOS EL WSCLIENT
        $wsClient = new SoapClient( $this->WSDL );
        $wsClient->setService($this->getSunatService($business));
        $wsClient->setCredentials(strtoupper($business->ruc.$business->sol_user), $business->sol_pass);

        // ENVIAMOS EL DOCUMENTO
        $sumSender = new SummarySender();
        $sumSender->setClient($wsClient);
        $res = $sumSender->send($fileName,$xmlSigned);

        // COMPROBAMOS RESULTADOS
        $xml = "";
        $message = "";
        $code = 1;
        $ticket = 0;
        $error = 1;
        $notes = '';

        $document = null;
        if ($res->isSuccess()):
            $ticket = $res->getTicket();

            // CONSULTADMOS EL ESTADO DEL TICKET
            $ticketClient = $this->getTicketClient($business);
            $verification = $ticketClient->getStatus($ticket);

            // VERIFICAMOS LA RESPUESTA
            if ($verification->isSuccess()) {
                // Código = 0 ó 99
                $error = 0;
                $cdr = $verification->getCdrResponse(); #string
                $zip = $verification->getCdrZip();
                $xml = $verification->getXmlContent();
                $message = $cdr->getDescription();
                $code = $cdr->getCode();
                $notes = implode(",", $cdr->getNotes() );    #getNotes es []

                $document = $this->saveDocument($data, $serie, $business);
                $document->status = 1;
                $document->operation_type = $status == 3 ? 2 : 1;
                $document->save();

                foreach ($documents as $detail) {
                    $detail->status = $status == 3 ? 2 : ($status == 1 ? 1 : $detail->status);
                    $detail->save();

                    // CON SALE
                    // if ($status == 3) {
                    //     $detail->safe_delete();
                    // }

                    $resume_detail = new ResumeDetail;
                    $resume_detail->resume_id = $document->id;
                    $resume_detail->detail_id = $detail->id;
                    $resume_detail->save();
                }

            } else{                
                #Aqui entran las EXCEPCIONES, LOS ERRORES Y LOS PENDIENTES
                $message = $verification->getError()->getMessage();
                $code = $verification->getError()->getCode();

                if($code == 98){
                    $error = 0;

                    $document = $this->saveDocument($data,$serie,$business);
                    $document->status = 1;
                    $document->operation_type = $status == 3 ? 2 : 1;
                    $document->save();

                    foreach ($documents as $detail) {
                        $detail->status = $status == 3 ? 2 : ($status == 1 ? 1 : $detail->status);
                        $detail->save();

                        // CON SALE
                        // if ($status == 3) {
                        //     $detail->safe_delete();
                        // }

                        $resume_detail = new ResumeDetail;
                        $resume_detail->resume_id = $document->id;
                        $resume_detail->detail_id = $detail->id;
                        $resume_detail->save();
                    }
                }

                #cuando arroja error [2000 - 4000] se puede enivar con el mismo correlativo
            }

        else:
            #casos:
            #1. Documento que ya se encuentra anulado o rechazado
            #2. Documento fuera de fecha            
            $message = $res->getError()->getMessage();
            #print $message; die();
            $code = intval($res->getError()->getCode());
        endif;

        // REGISTRAMOS EL CDR
        $sunat_cdr = new SunatCDR;
        $sunat_cdr->business_id = $business->id;
        $sunat_cdr->serie = $serie;
        $sunat_cdr->correlative = $correlative;
        $sunat_cdr->document_id = @$document ? $document->id : 0;
        $sunat_cdr->ticket = $ticket;
        $sunat_cdr->code = intval($code);
        $sunat_cdr->notes = $notes;
        $sunat_cdr->answer = base64_encode($xml);
        $sunat_cdr->message = $message;
        $sunat_cdr->env = ['development'=>'dev','production'=>'prod'][$business->environment];
        $sunat_cdr->save();

        return ['error'=>$error, 'message'=>$message];

    }



    // CREACIÓN DE RESÚMENES DIARIOS
    
    public function resumen_create(Request $data){
        $business = Business::where('ruc', $data->emisor_doc)->first();
        /** anular una boleta desde xafiro **/
        if (!$data->refered_document_id) {
            $env = ['development'=>'dev', 'production' => 'prod'][$business->environment];
            $refered_document = Document::where('serie',$data->doc_ref_serie)
                ->where('correlative', $data->doc_ref_correlat)
                ->where('doc_type', ['F' => '01', 'B' => '03'][$data->doc_reference_type])
                ->where('business_id', $business->id)
                ->where('env', $env)
                ->first();
            if (!$refered_document) {
                return ['error' => 1, 'message' => 'El documento referido no existe', 'redirect' => ''];
            }
        } 
        #Anular la boleta desde el facturador
        else {
            $refered_document = Document::find($data->refered_document_id);
        }

        #Envio al documento para anular
        return $this->resumen_create_base($business, collect([$refered_document]), 3);
    }
    

}
