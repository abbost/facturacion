<?php

namespace App\Http\Controllers\API\DocumentTraits;

use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Log;

use App\Models\Document as Document;
use App\Models\DocumentDetail as DocumentDetail;
use App\Models\Business as Business;
use App\Models\Item as Item;
use App\Models\SunatCDR as SunatCDR;
use App\Models\ResumeDetail as ResumeDetail;

use Exception;

trait DocumentSearch {

    // Listado de documentos (boletas, facturas, notas de crédito y proformas)
    public function get(Request $request){
        $business = Business::find($request->business_id);
        $query = Document::with('refered')
            ->with('cdr')
            ->with('payments')
            ->where('business_id', $request->business_id)
            ->whereIn('doc_type', ['00','01', '03', '07']);
        if ($business->send_documents_sunat) {
            $query = $query->where('status', '<>', 5);
        }
        if ($request->doc_type != '*') {
            $query = $query->where('doc_type', @['F'=>'01', 'B'=>'03', 'NC'=>'07'][$request->doc_type] ?: '01');
        }

        {
            /* QUITAR AL MOMENTO DE AISLAR POR COMPLETO WEB DE API */
            if (@$request->office_id != null) {
                $query = $query->where('office_id', $request->office_id);
            }
        }

        $query = $query->where('env', @$business->environment == 'production' ? 'prod' : 'dev');
        #$request->date_from = @$request->date_from ?: date('Y-m-d');
        $documents = $query
            ->where('date(date)', '>=', @$request->date_from ?: date('Y-m-d'))
            ->where('date(date)', '<=', @$request->date_to ?: date('Y-m-d'))
            ->orderBy('created_at', 'desc')
            ->get();
        $documents->each(function ($document) {
            $document->has_resume_parent = $document->has_resume_parents();
        });
        return [
            'documents' => $documents
        ];
    }

    // Listado de Resumenes diarios
    public function getRds(Request $request){
        $business = Business::find($request->business_id);
        $documents = Document::with('resume_details')
            ->with('cdr')
            ->where('business_id', $request->business_id)
            ->where('doc_type', 'RC')
            ->where('status','<>',0)
            ->where('env', @$business->environment == 'production' ? 'prod' : 'dev')
            ->whereRaw("date(created_at) >= '" . (@$request->date_from ?: date('Y-m-d'))."'")            
            ->whereRaw("date(created_at) <= '" . (@$request->date_to ?: date('Y-m-d'))."'")
            ->orderBy('created_at', 'desc')
            ->get();

        return [
            'documents' => $documents
        ];
    }

    // Detalles para nota de crédito
    public function loadDetails(Request $request){
        $document = Document::where('doc_type', @['F' => '01', 'B' => '03'][$request->doc_type])
            ->where('business_id', $request->business_id)
            ->where('serie', $request->doc_ref_serie)
            ->where('correlative', $request->doc_ref_correlat)
            ->orderBy('id', 'desc')->first();
        return [
            'status' => intval(!empty($document)),
            'document' => $document,
            'details' => @$document->details ?: []
        ];
    }

    // Siguiente correlativo para cualquier tipo de documento
    public function getNextCorrelative($business_id, $doc_type, $serie){
        $business = Business::find($business_id);
        $document = Document::where('business_id', $business->id);
        $document = $document
            ->where('doc_type', $doc_type)
            ->where('serie', $serie)
            ->where('env', @$business->environment == 'production' ? 'prod' : 'dev')
            ->orderBy('id', 'desc')->first();
        return intval(@$document->correlative) + 1;
    }

    // Para saber si un ticket de Xafiro ya tiene un documento linqueado en el facturador
    public function isRepeatedTicket($businessId, $ticketId){
        /* En el entorno desarrollo no me importa validar que exista */
        $document = Document::where('business_id', $businessId)
            ->where('ticket_id', $ticketId)
            ->where('env', 'prod')
            ->first();
        return !empty($document);
    }

    // Para saber si el correlativo se repite
    public function isRepeatedDocument($businessId, $serie, $correlative){
        /* En el entorno desarrollo no me importa validar que exista */
        $document = Document::where('business_id', $businessId)
            ->where('serie', $serie)
            ->where('correlative', $correlative)
            ->where('env', 'prod')
            ->first();
        return !empty($document);
    }

    // Búsqueda de facturas, boletas y notas de crédito
    public function search (Request $request, Mailer $mailer) {
        $custom = 0;
        $cancelled = false; 
        try {
            if (array_search($request->export_type, ['xml', 'xml_cancelled', 'cdr', 'cdr_cancelled', 'pdf', 'ticket', 'email', 'none']) === false) {
                $custom = 1;
                throw new Exception('Tipo de documento a exportar no válido');
            }
            #Este search es de la WEB PUBLICA
            if (@$request->id == null && $request->export_type == 'none') {
                $doc_type_pair = @[
                    "F"=>["01","FACTURA DE VENTA ELECTRONICA"],
                    "B"=>["03","BOLETA DE VENTA ELECTRONICA"],
                    "NC"=>["07","NOTA DE CREDITO ELECTRONICA"],
                    "ND"=>["08","NOTA DE DEBITO ELECTRONICA"]
                ][strtoupper(@$request->doc_type ?: "")] ?: false;
                if (!$doc_type_pair) {
                    $custom = 1;
                    throw new Exception('Tipo de documento no válido');
                }
                $business = Business::where('ruc', $request->emisor_doc)->first();
                if (empty($business)) {
                    $custom = 1;
                    throw new Exception('Los datos del emisor no están registrados en la base de datos');
                }
                $document = Document::where('business_id', $business->id)
                    ->where('serie', $request->serie)
                    ->where('correlative', intval($request->correlative))
                    ->where('date(date)', $request->date)
                    ->where('total', $request->total)
                    ->where('doc_type', $doc_type_pair[0])->first();
                if(!isset($document) || !($products = $document->details())){
                    $custom = 1;
                    throw new Exception('El documento no está registrado en la base de datos');
                }
            } 
            #Esta busqueda viene desde adentro de la web
            else {
                $document = Document::find($this->id_mask($request->id, 'decrypt'));
                if(!isset($document) || ($document->doc_type != 'RC' && !($products = $document->details()))){
                    $custom = 1;
                    throw new Exception('El documento no está registrado en la base de datos');
                }

                if (substr($request->export_type, 4) == 'cancelled') {
                    $document = Document::where('business_id', $document->business_id)
                        ->where('doc_type', @['01'=>'RA', '03'=>'RC'][$document->doc_type])
                        ->where('refered_document_id', $document->id)->orderBy('id', 'desc')->first();
                    if(!isset($document) || !($products = $document->details())){
                        $custom = 1;
                        throw new Exception('El documento de cancelación no está registrado en la base de datos');
                    }
                    $request->export_type = substr($request->export_type, 0, 3);
                    $cancelled = true;
                }
                $business = $document->business;
                $doc_type_pair = @[
                    "01"=>[0,"FACTURA ELECTRONICA"],
                    "03"=>[0,"BOLETA ELECTRONICA"],
                    "07"=>[0,"NOTA DE CREDITO ELECTRONICA"],
                    #"08"=>[0,"NOTA DE DEBITO ELECTRONICA"]
                ][$document->doc_type];
            }

            if($cancelled)
                $file_name = $business->ruc.'-'.$document->doc_type.'-'.date('Ymd',strtotime($document->date)).'-'.$document->correlative;
            else
                $file_name = $business->ruc.'-'.$document->doc_type.'-'.$document->serie.'-'.$document->correlative;
           

            if ($document->doc_type == 'RC') {
                $file_name = $business->ruc . '-RC-' . date('Ymd', strtotime($document->created_at)) . '-' . $document->correlative;
            }
            $encrypted_id = $this->id_mask($document->id, 'encrypt');

            switch (@$request->export_type) {
                case 'xml':
                    return response(file_get_contents(public_path("xml/".$file_name.".xml")))->withHeaders([
                       'Content-Disposition' => 'attachment; filename="'.(
                            ($document->customer_doc ?: $document->doc_type).'-'.
                            ($document->serie ?: date('Ymd', strtotime($document->date))).'-'.
                            $document->correlative
                        ).'_XML.xml"',
                        'Content-Type' => 'application/octet-stream'
                    ]);
                case 'cdr':
                    $cdr = @$document->cdr ?: (@$document->resume_parents->last()->cdr ?: null);
                    return response(base64_decode(@$cdr->answer))->withHeaders([
                       'Content-Disposition' => 'attachment; filename="'.(
                            ($document->customer_doc ?: $document->doc_type).'-'.
                            ($document->serie ?: date('Ymd', strtotime($document->date))).'-'.
                            $document->correlative
                        ).'_CDR.xml"',
                        'Content-Type' => 'application/octet-stream'
                    ]);
                case 'pdf':
                case 'ticket':
                    return $this->makePDF($document, $business, @$request->export_type, @$request->window);
                case 'email':
                    $message = $this->sendMailToCustomer($document, $business, [
                        'receptor_email' => $request->email,
                        'receptor_email_content' => $request->receptor_email_content
                    ]);
                    return [
                        'error'=>0,
                        'message'=>$message
                    ];
                default:
                    return [
                        'error'=>0,
                        'message'=>'El documento si se encuentra registrado en la base de datos',
                        'id'=>$encrypted_id
                    ];
            }
        } catch (\Exception $e) {
            return ($request->export_type == 'none' ? [
                'error'=>1,
                'message'=>$e->getMessage()
            ] : view('searchError', [
                'message'=>$custom == 1 ? "Por favor, intente recargar la página" : $e->getMessage(),
                'id'=>@$encrypted_id,
                'export_type'=>$request->export_type
            ]));
        }
    }

}
