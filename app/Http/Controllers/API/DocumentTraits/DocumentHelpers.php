<?php

namespace App\Http\Controllers\API\DocumentTraits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

use App\Mail\DocumentInfo as DocumentInfo;

use Greenter\XMLSecLibs\Sunat\SignedXml;
// use Greenter\Model\Response\BillResult;

use App\Models\Document as Document;
use App\Models\DocumentDetail as DocumentDetail;
use App\Models\Business as Business;
use App\Models\Item as Item;
use App\Models\Payment as Payment;
use App\Models\SunatCDR as SunatCDR;
use App\Models\ResumeDetail as ResumeDetail;
// use App\Models\Cancellation as Cancellation;

use App\Abbost\Model\Sale\Invoice;
use App\Abbost\Model\Sale\Note;
use App\Abbost\Model\Sale\SaleDetail;
use App\Abbost\Model\Sale\Legend;
use App\Abbost\Model\Voided\Voided;
use App\Abbost\Model\Voided\VoidedDetail;
// use App\Abbost\Model\Sale\Document;
use App\Abbost\Model\Sale\Tax;
use App\Abbost\Model\Summary\Summary;
use App\Abbost\Model\Summary\SummaryDetail;
use App\Abbost\Model\Summary\SummaryPerception;
use App\Abbost\Model\Client\Client;
use App\Abbost\Model\Company\Company;
use App\Abbost\Model\Company\Address;
use App\Abbost\WS\SoapClient;
use App\Abbost\WS\BillSender;
use App\Abbost\WS\SummarySender;
use App\Abbost\WS\ExtService;
use App\Abbost\WS\SunatEndpoints;
use App\Abbost\WS\ConsultCdrService;

// use DOMDocument;
// use ZipArchive;
// use stdClass;

#use SoapClient;
use DB;
use PDF;
use QrCode;
// use SimpleXMLElement;
use Exception;

trait DocumentHelpers {

    /* ////////////////////////////////////////////////////////////////////
        SUNAT HELPERS
    //////////////////////////////////////////////////////////////////// */

    // FIRMA DE DOCUMENTO
    private function sign($business, $document){
        $certPath = storage_path("certificate/".$business->ruc.".pem");
        $signer = new SignedXml();
        $signer->setCertificateFromFile($certPath);
        return $signer->signFromFile($document); #SIGNED XML
    }

    // ELECCIÓN DEL ENTORNO DE TRABAJO CORRESPONDIENTE
    public function getSunatService($business){
        if($business->environment == "development"){
            return SunatEndpoints::FE_BETA;
        }
        return SunatEndpoints::FE_PRODUCCION;
    }

    // VERIFICACIÓN PARA DETERMINAR SI EL EMISOR PUEDE O NO USAR EL SISTEMA
    private function checkPolitics($business){

        // MÁXIMA CANTIDAD DE COMPROBANTES EXCEDIDA
        if (
            $business->environment == 'production' &&
            $business->document_sum_now()->quantity >= $business->monthly_bill_limit
        ) {
            return [
                'error'=>1,
                'message'=>"La cantidad máxima de comprobantes emitibles por mes ya ha sido alcanzada",
                'redirect'=>''
            ];
        }

        // FECHA DE PAGO EXCEDIDA
        if ($business->due_date < date('Y-m-d')) {
            return [
                'error'=>1,
                'message'=>"La fecha del contrato de facturación electrónica ha vencido. Favor de comunicarse con el área de soporte",
                'redirect'=>''
            ];
        }

        return false;
    }

    // VERIFICACIÓN PARA DETERMINAR SI EL EMISOR PUEDE O NO ENVIAR COMPROBANTES A SUNAT
    private function checkActiveSunat($business){
        return $business->send_documents_sunat == 0;
    }

    #CheckStatus from ticket. LLamardo en el envio de Resument
    public function getTicketClient ($business) {
        #Ticket es un numero
        $sender = new ExtService();

        $wsClient = new SoapClient($this->WSDL);
        $wsClient->setService($this->getSunatService($business));
        $wsClient->setCredentials(strtoupper($business->ruc.$business->sol_user), $business->sol_pass);

        $sender->setClient($wsClient);

        #Return StatusResult Class
        #return $sender->getStatus($ticket);
        return $sender;
    }

    // OBTENCIÓN DE CDR PARA REENVIOS
    public function getCDR($business, $serie, $correlative, $doc_type){
        $fields['rucSol'] = $business->ruc;
        $fields['userSol'] = strtoupper($business->sol_user);
        $fields['passSol'] = $business->sol_pass; 
        $fields['ruc'] = $business->ruc;
        $fields['tipo'] = $doc_type;
        $fields['serie'] = $serie;
        $fields['numero'] = $correlative;
        $fields['cdr'] = false; // descarga el archivo

        if (!isset($fields['rucSol'])) {
            return null;
        }
        # Esto da un error en API/CreateDocument
        // if (!$this->validateFields($fields)) {
        //      return null;
        // }

        $wsClient = new SoapClient(SunatEndpoints::FE_CONSULTA_CDR.'?wsdl');
        $wsClient->setCredentials(strtoupper($business->ruc.$business->sol_user), $business->sol_pass);
        $service = new ConsultCdrService();
        $service->setClient($wsClient);

        $arguments = [
            $fields['ruc'],
            $fields['tipo'],
            $fields['serie'],
            intval($fields['numero'])
        ];

        $result = $service->getStatusCdr(...$arguments);

        #Descargar el CDR y lo guardamos
        if ($fields['cdr']) {
            if ($result->getCdrZip()) {
                $filename = 'R-'.implode('-', $arguments).'.zip';
                $this->savedFile($filename, $result->getCdrZip());
            }
        }
        
        return $result;
    }

    /* ////////////////////////////////////////////////////////////////////
        OPCIONALES DE ENVÍO
    //////////////////////////////////////////////////////////////////// */

    // ENVIO MAIL AL RECEPTOR
    private function sendMailToCustomer ($document, $business, $data) {
        try {
            $file_name = $business->ruc."-".$document->doc_type."-".$document->serie."-".$document->correlative;

            $limit = 100;
            $cur_date = date('Y-m-d');            
            if($business->mailing($cur_date)->count() > $limit){
                return "No se pudo enviar. Superó el limite de $limit mails enviados diariamente";
            }


            Mail::to($data['receptor_email'])
                ->send(new DocumentInfo(
                    $this->makePDF($document, $business, 'pdf'),
                    @file_get_contents(public_path("xml/".$file_name.".xml")) ?: '',
                    @base64_decode(@$document->cdr->answer) ?: '', '',
                    #$data['receptor_email_content'],
                    $business->comercial_name
                )
            );

            DB::table('mailing')->insert([
                'document_id' => $document->id,
                'business_id' => $business->id,
                'email' => $data['receptor_email'],
                'created_at' => date('Y-m-d H:i')
            ]);

            return "Mails enviados correctamente";
        } catch (\Exception $e) {
            Log::error($business->ruc."-".$document->customer_name.'- MAIL ERROR'.$e->getMessage());
            return $e->getMessage();
        }
    }

    // CREACIÓN DE PDF DEL DOCUMENTO
    private function makePDF($document, $business, $export_type, $window = false){
        try {
            $doc_type_pair = @[
                "00"=>[0,"PROFORMA DE VENTA"],
                "01"=>[0,"FACTURA DE VENTA ELECTRONICA"],
                "03"=>[0,"BOLETA DE VENTA ELECTRONICA"],
                "07"=>[0,"NOTA DE CREDITO ELECTRONICA"],
                "08"=>[0,"NOTA DE DEBITO ELECTRONICA"]
            ][$document->doc_type];
            $file_name = $business->ruc."-".$document->doc_type."-".$document->serie."-".$document->correlative;
            $path_qr_file = public_path('/img/qr/'.md5($document->id).'.png');

            $qr_file = QrCode::format('png')->size(75)->generate(implode('|', [
                $business->ruc,
                $document->doc_type,
                $document->serie,
                $document->correlative,
                $document->tax,
                $document->total,
                $document->date,0,0
            ]));
            $data = [
                'document' => $document,
                'business' => $business,
                'details' => @$document->details ?: [],
                'document_name' => $doc_type_pair[1],
                'total_text' => num2letras($document->total),
            ];

            if ($window === '1') {
                /**
                 * Esto se llama cuando se imprime directamente al hacer la venta
                 */
                $data['url_logo'] = "data:image/jpeg;base64,".base64_encode(file_get_contents(public_path("/img/".$business->image)));
                $data['url_qr'] = "data:image/png;base64,".base64_encode($qr_file);
                if($export_type == 'pdf')                
                    $paper_size = 'A4';
                else
                    #$paper_size = ($business->id == 31 ?'b9':'b7' ); // el b9 más pequeño es solo para kIMSA MARKET                    
                    $paper_size = $business->ticket_size;

                $data['paper_size'] = $paper_size;

                return ['content' => view($export_type.'Doc', $data)->render()];

            } else {
                $data['url_logo'] = public_path("/img/".$business->image);
                $path_qr_file = public_path('img/qr/'.md5($document->id).'.png');
                #print $path_qr_file; die();
                file_put_contents($path_qr_file, $qr_file);
                $data['url_qr'] = $path_qr_file;                

                if($export_type == 'pdf')                
                    $paper_size = 'A4';
                else
                    #$paper_size = ($business->id == 31 ?'b9':'b7' ); // el b9 más pequeño es solo para kIMSA MARKET                    
                    $paper_size = $business->ticket_size;

                $data['paper_size'] = $paper_size;

                $pdf = PDF::loadView($export_type.'Doc', $data)
                ->setPaper($paper_size, 'portrait')
                ->stream($file_name.'_'.$export_type.'.pdf');
                
                unlink($path_qr_file); #Qr
                return $pdf;
            }
        } catch(\Exception $e) {
            #Log::error($business->ruc."-".$document->customer_name.'-GENERATE PDF ERROR'.$e->getMessage());
            return ['error'=>0, 'message'=> $e->getMessage()];
        }
    }   

    /* ////////////////////////////////////////////////////////////////////
        DOCUMENT HELPERS
    //////////////////////////////////////////////////////////////////// */

    // GUARDADO DE ARCHIVO EN PUBLIC/XML
    private function savedFile ($filename, $content) {
        $pathZip = public_path('/xml/'.$filename);        
        file_put_contents($pathZip, $content);
    }

    // GUARDADO DE DOCUMENTO EN LA BASE DE DATOS
    private function saveDocument ($RQ, $doc_type, $business) {
        $document = new Document;
        $document->business_id = $business->id;
        $document->date = $RQ['date'] ?: date('Y-m-d H:i:s');        
        $document->serie = strtoupper(@$RQ['serie'] ?: '');
        $document->correlative = $RQ['correlative'];
        $document->doc_type = $doc_type;
        $document->env = ['development'=>'dev','production'=>'prod'][$business->environment];
        $document->status = 3;
        $document->office_id = @$RQ['office_id'];

        if ($doc_type =="RA" || $doc_type =="RC") {
            $document->refered_document_id = @$RQ['refered_document_id'];
            $document->save();
            return $document;
        }

        if ($doc_type == "07") {
            $document->refered_document_id = $RQ['refered_document_id'];
            $document->reference_code = $RQ['doc_reference_code'];
            $document->response_code = $RQ['doc_response_code'];
            $document->motive = $RQ['subject'];
        }

        $document->currency = $RQ['currency'];
        $document->operation_type = $RQ['doc_oper_type'];
        if ($RQ['receptor_doc_type'] == 0) {
            $RQ['receptor_doc_type'] = null;
        }
        $document->customer_doc_type = $RQ['receptor_doc_type'];
        // if (array_search($RQ['receptor_doc_type'], [1,4,7]) !== false && array_search(@$RQ['receptor_doc'], ['---', '-', null]) !== false) {
        //     $RQ['receptor_doc'] = '00000000';
        // }
        $document->customer_id = @$RQ['receptor_id'];
        $document->customer_doc = @$RQ['receptor_doc'];
        $document->customer_name = @$RQ['receptor_name'];
        $document->customer_address = @$RQ['receptor_address'];
        $document->customer_email = @$RQ['receptor_email'];
        if ($document->customer_email != '') {
            $document->customer_email_content = @$RQ['receptor_email_content'];
        }

        $document->contact_name = @$RQ['contact_name'];
        $document->contact_address = @$RQ['contact_address'];
        $document->contact_id = @$RQ['contact_id'];
        $document->guide = @$RQ['guide'];

        $document->sub_total = $RQ['sub_total'];
        $document->tax = $RQ['tax'];
        $document->total = $RQ['total'];
        $document->order_reference = @$RQ['order_reference'];
        $document->note = @$RQ['note'];
        $document->ticket_id = @$RQ['ticket_id'];
        $document->save();

        // if($document->refered){            
        //     $document->refered->status = 4;
        //     $document->refered->save();
        // }

        foreach ($RQ['products'] as $item) {
            if (floatval($item['total']) == 0) {
                continue;
            }
            $document_detail = new DocumentDetail;
            $document_detail->document_id = $document->id;
            $document_detail->external_item_id = @$item['item_id'];
            $document_detail->description = $item['description'];
            $document_detail->price_type = '01';
            $document_detail->quantity = $item['quantity'];
            $document_detail->discount = 0;
            $document_detail->unit_price = @$item['price'] ?: ($item['total'] / $item['quantity']);
            $document_detail->sub_total = round($item['sub_total'], 2);
            $document_detail->tax = round($item['tax'], 2);
            $document_detail->total = round($item['total'], 2);
            $document_detail->onu_product_code = @$item['onu_product_code'];
            $document_detail->save();
        }

        // Se coloca aqui para enviar el correo cuando el envio a sunat no está activado
        if ($document->customer_email != '') 
        $this->sendMailToCustomer($document, $business, [
            'receptor_email' => $document->customer_email,
            'receptor_email_content' => ''
        ]);

        return $document;
    }

    // CREACIÓN DEL ID ENCRIPTADO DE DOCUMENTO PARA LAS BUSUQEDAS
    private function id_mask ($string, $mode) {
        # https://bhoover.com/using-php-openssl_encrypt-openssl_decrypt-encrypt-decrypt-data/
        try {
            $key = "JD{,¬M<@K}Ç3M'¨Ka6.^vDL7¬|Plig[K¬6¨-WC9B22mh7+Z¿Li";
            $encryption_key = base64_decode($key);
            switch ($mode) {
                case 'encrypt':
                    $content = "";
                    do {
                        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-ctr'));
                        $encrypted = openssl_encrypt($string, 'aes-256-ctr', $encryption_key, 0, $iv);
                        $content = base64_encode($encrypted . '::' . $iv);
                    } while (strpos($content, '+') !== false);
                    return $content;
                case 'decrypt':
                    list($encrypted_data, $iv) = explode('::', base64_decode($string), 2);
                    return openssl_decrypt($encrypted_data, 'aes-256-ctr', $encryption_key, 0, $iv);
                default:
                    return $string;
            }
        } catch (\Exception $e) {
            return ['error'=>'ID incorrecto'];
        }
    }

}
