<?php

namespace App\Http\Controllers\API\DocumentTraits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\Document as Document;
use App\Models\DocumentDetail as DocumentDetail;
use App\Models\Business as Business;
use App\Models\Office;
use App\Models\Item as Item;
use App\Models\SunatCDR as SunatCDR;
use App\Models\ResumeDetail as ResumeDetail;
// use App\Models\Cancellation as Cancellation;

use App\Abbost\Model\Sale\Invoice;
use App\Abbost\Model\Sale\Note;
use App\Abbost\Model\Sale\SaleDetail;
use App\Abbost\Model\Sale\Legend;
use App\Abbost\Model\Voided\Voided;
use App\Abbost\Model\Voided\VoidedDetail;
// use App\Abbost\Model\Sale\Document;
use App\Abbost\Model\Sale\Tax;
use App\Abbost\Model\Summary\Summary;
use App\Abbost\Model\Summary\SummaryDetail;
use App\Abbost\Model\Summary\SummaryPerception;
use App\Abbost\Model\Client\Client;
use App\Abbost\Model\Company\Company;
use App\Abbost\Model\Company\Address;
use App\Abbost\WS\SoapClient;
use App\Abbost\WS\BillSender;
use App\Abbost\WS\SummarySender;
use App\Abbost\WS\ExtService;
use App\Abbost\WS\SunatEndpoints;
use App\Abbost\WS\ConsultCdrService;

// use DOMDocument;
// use ZipArchive;
// use stdClass;

#use SoapClient;
use DB;
use PDF;
use QrCode;
// use SimpleXMLElement;
use Exception;

trait DocumentResend {

    // BASE PARA EL CÓDIGO DE REENVÍO DE DOCUMENTOS
    public function resend_base ($business, $document) {
        $result = $this->getCDR($business, $document->serie, $document->correlative, $document->doc_type);
        if($result->isSuccess() && $business->environment == 'production'):
            $error = 0;
            $message = $result->getMessage(); 
            /* MAS QUE UN CDR ES UN LOG  */
            $sunat_cdr = new SunatCDR;
            $sunat_cdr->business_id = $business->id;
            $sunat_cdr->document_id = $document ? $document->id : 0;
            $sunat_cdr->code = intval($result->getCode());
            $sunat_cdr->serie = $document->serie;
            $sunat_cdr->correlative = $document->correlative;
            $sunat_cdr->answer = base64_encode($result->getXmlContent());
            #$sunat_cdr->notes = $result->getNotes();
            $sunat_cdr->message = $message;
            $sunat_cdr->env = ['development'=>'dev','production'=>'prod'][$business->environment];
            $sunat_cdr->save();

            /* Actualizamos el documento */
            $document->status = 1;
            $document->save();

            return ['error' => $error, 'message' => $message];
        else:
            if($document->doc_type == "01" || $document->doc_type == "03") {
                return $this->bill_resend($document);
            } elseif($document->doc_type = "07") {
                return $this->note_resend($document);
            }
        endif;
    }

    // REENVÍO DE BOLETAS, FACTURAS Y NOTAS DE CRÉDITO (REDIRECCIONA A LOS MÉTODOS CORRESPONDIENTES)
    public function resend(Request $request){
        $business = Business::where('ruc',$request->emisor_doc)->first();
        $document = Document::where('id',$request->document_id)->where('business_id',$business->id)->first();
        if (empty($document)) {
            return ['error' => 0, 'message' => 'El documento no está registrado en la base de datos'];
        }
        return $this->resend_base($business, $document);
    }

    /* REENVÍO DE RESUMENES DIARIOS
       Más que reenviar consulta el estado del ticket 
    */

    /*
    public function rdResend (Request $request) {
        $business = Business::where('ruc',$request->emisor_doc)->first();
        $document = Document::where('id',$request->document_id)->where('business_id',$business->id)->first();
        if (empty($document)) {
            return ['error' => 0, 'message' => 'El documento no está registrado en la base de datos'];
        }
        $status = 1;
        
        $ticket = $document->cdr->ticket;
        // CONSULTADMOS EL ESTADO DEL TICKET
        $ticketClient = $this->getTicketClient($business);
        // El getsstatus solo funciona en produccion, en desarrollo siempre dira que no existe la respuesta 
        $verification = $ticketClient->getStatus($ticket);

        // VERIFICAMOS LA RESPUESTA
        if ($verification->isSuccess()) {
            // Código = 0 ó 99
            $error = 0;
            $cdr = $verification->getCdrResponse(); #string

            // ACTUALIZAMOS EL CDR DEL DOCUMENTO
            $sunat_cdr = $document->cdr;
            $sunat_cdr->code = intval($cdr->getCode());
            $sunat_cdr->notes = implode(",", $cdr->getNotes());
            $sunat_cdr->answer = base64_encode($verification->getXmlContent());
            $sunat_cdr->message = $cdr->getDescription();
            $sunat_cdr->save();
            return ['error'=>$error, 'message'=>$sunat_cdr->message];
        } else {
            return ['error'=>1, 'message'=>$document->cdr->message];
        }
    }
    */

    
    public function rdResend (Request $request) {
        $business = Business::where('ruc',$request->emisor_doc)->first();
        $document = Document::where('id',$request->document_id)->where('business_id',$business->id)->first();
        if (empty($document)) {
            return ['error' => 0, 'message' => 'El documento no está registrado en la base de datos'];
        }
        $status = $document->operation_type == 2 ? 3: 1; // puede ser 1:aceptado o 2:anulado esto es internero. En sunat 3: anulado
        
        $ticket = $document->cdr->ticket;
        // CONSULTADMOS EL ESTADO DEL TICKET
        $ticketClient = $this->getTicketClient($business);
        // El getsstatus solo funciona en produccion, en desarrollo siempre dira que no existe la respuesta 
        $verification = $ticketClient->getStatus($ticket);

        // VERIFICAMOS LA RESPUESTA
        if ($verification->isSuccess()) {
            // Código = 0 ó 99
            $error = 0;
            $cdr = $verification->getCdrResponse(); #string

            //ACTUALIZAMOS EL ESTADO DE LOS DOCUMENTOS A ACEPTADO
            foreach ($document->resume_details as $_detail) {
                $_detail->status = $status == 3 ? 2 : 1;
                $_detail->save();
            }

            // ACTUALIZAMOS EL CDR DEL DOCUMENTO
            $sunat_cdr = $document->cdr;
            $sunat_cdr->code = intval($cdr->getCode());
            $sunat_cdr->notes = implode(",", $cdr->getNotes());
            $sunat_cdr->answer = base64_encode($verification->getXmlContent());
            $sunat_cdr->message = $cdr->getDescription();
            $sunat_cdr->save();
            return ['error'=>$error, 'message'=>$sunat_cdr->message];
        } else {
            # Reenviamos el documento. Document y document_Details ya existen
            $documents = $document->resume_details;

            {
                $address = new Address();
                $address->setDireccion($business->address)
                    ->setCodigoPais('PE');
                $company = new Company();
                $company->setRuc($business->ruc)
                    ->setRazonSocial($business->legal_name)
                    ->setNombreComercial($business->comercial_name)
                    ->setAddress($address);
            }
    
            // PREPARAMOS DATOS DE DETALLE DE RESUMEN
            $details = $documents->map(function ($boleta) use ($status) {
                $detail = new SummaryDetail();
                $detail->setTipoDoc($boleta->doc_type)
                    ->setSerieNro($boleta->serie."-".$boleta->correlative)
                    ->setEstado($status) #Estado 1 es aceptado; 3 es anulado
                    ->setClienteTipo($boleta->customer_doc_type)
                    ->setClienteNro($boleta->customer_doc)
                    ->setTotal($boleta->total)
                    ->setMtoOperGravadas($boleta->sub_total)
                    ->setMtoIGV($boleta->tax);
                return $detail;
            })->toArray();
    
            // PREPARAMOS DATOS DE RESUMEN
            $serie = $document->serie;
            $correlative = $document->correlative;
    
            $sum = new Summary();
            $sum->setFecGeneracion( new \DateTime($document->date))
                ->setFecResumen(new \DateTime($document->date))
                ->setCorrelativo($correlative)
                ->setCompany($company)
                ->setDetails($details);
    
            // LLENAMOS DATA
            $data['serie'] = $serie;
            $data['date'] = date('Y-m-d H:i:s');        
            $data['correlative'] = $correlative;
            $data['refered_document_id'] = null;
    
            // FIRMAMOS EL DOCUMENTO
            $fileName = $business->ruc."-".$sum->getXmlId();
            $xml = view('xml::summary',['doc'=>$sum])->render();
            $path = public_path('/xml/'.$fileName.'.xml');
            file_put_contents($path, $xml);
            $xmlSigned = $this->sign($business, $path);
    
            // CREAMOS EL WSCLIENT
            $wsClient = new SoapClient( $this->WSDL );
            $wsClient->setService($this->getSunatService($business));
            $wsClient->setCredentials(strtoupper($business->ruc.$business->sol_user), $business->sol_pass);
    
            // ENVIAMOS EL DOCUMENTO
            $sumSender = new SummarySender();
            $sumSender->setClient($wsClient);
            $res = $sumSender->send($fileName,$xmlSigned);
    
            // COMPROBAMOS RESULTADOS
            $xml = "";
            $message = "";
            $code = 1;
            $ticket = 0;
            $error = 1;
            $notes = '';
                
            if ($res->isSuccess()):
                $ticket = $res->getTicket();
    
                // CONSULTADMOS EL ESTADO DEL TICKET
                $ticketClient = $this->getTicketClient($business);
                $verification = $ticketClient->getStatus($ticket);
    
                // VERIFICAMOS LA RESPUESTA
                if ($verification->isSuccess()) {
                    // Código = 0 ó 99
                    $error = 0;
                    $cdr = $verification->getCdrResponse(); #string
                    $zip = $verification->getCdrZip();
                    $xml = $verification->getXmlContent();
                    $message = $cdr->getDescription();
                    $code = $cdr->getCode();
                    $notes = implode(",", $cdr->getNotes() );    #getNotes es []
                        
                    $document->status = 1;
                    #$document->operation_type = $status == 3 ? 2 : 1;
                    $document->save();

                    //ACTUALIZAMOS EL ESTADO DE LOS DOCUMENTOS A ACEPTADO
                    foreach ($document->resume_details as $_detail) {
                        $_detail->status = $status == 3 ? 2 : 1;
                        $_detail->save();
                    }
    
                } else{
                    #Aqui entran las EXCEPCIONES, LOS ERRORES Y LOS PENDIENTES
                    $message = $verification->getError()->getMessage();
                    $code = $verification->getError()->getCode();
    
                    if($code == 98){
                        $error = 0;
                        
                        $document->status = 1;
                        #$document->operation_type = $status == 3 ? 2 : 1;
                        $document->save();
                    }
    
                    #cuando arroja error [2000 - 4000] se puede enivar con el mismo correlativo
                }

            else:
                #casos:
                #1. Documento que ya se encuentra anulado o rechazado
                #2. Documento fuera de fecha
    
                $message = $res->getError()->getMessage();
                $code = intval($res->getError()->getCode());
            endif;
    
            // REGISTRAMOS EL CDR
            $sunat_cdr = new SunatCDR;
            $sunat_cdr->business_id = $business->id;
            $sunat_cdr->serie = $serie;
            $sunat_cdr->correlative = $correlative;
            $sunat_cdr->document_id = @$document ? $document->id : 0;
            $sunat_cdr->ticket = $ticket;
            $sunat_cdr->code = intval($code);
            $sunat_cdr->notes = $notes;
            $sunat_cdr->answer = base64_encode($xml);
            $sunat_cdr->message = $message;
            $sunat_cdr->env = ['development'=>'dev','production'=>'prod'][$business->environment];
            $sunat_cdr->save();
    
            return ['error'=>$error, 'message'=>$message];

        }
    }
    

    // REENVÍO DE BOLETAS Y FACTURAS
    public function bill_resend($document){

        $business = $document->business;
        $office = Office::find($document->office_id);
        
        // PREPARAMOS DATOS DE RECEPTOR
        {
            $address = new Address();
            $address->setUrbanizacion('NONE')
                ->setDireccion($document->customer_address)
                ->setCodigoPais('PE');
            $client = new Client();
            $client->setTipoDoc($document->customer_doc_type) # Puede ser DNI , PAS, CEX
                ->setNumDoc($document->customer_doc)
                ->setRznSocial($document->customer_name)
                ->setAddress($address);
        }

        // PREPARAMOS DATOS DE EMISOR
        {
            $address = new Address();            
            $address->setDireccion($office->address)
                ->setCodLocal($office->code)
                ->setCodigoPais('PE');

            $company = new Company();
            $company->setRuc($business->ruc)
                ->setRazonSocial($business->legal_name)
                ->setNombreComercial($business->comercial_name)
                ->setAddress($address);
        }

        // PREPARAMOS DATOS DE DOCUMENTO
        $invoice = new Invoice();
        $invoice
            ->setUblVersion('2.1')
            ->setTipoOperacion('0101')
            ->setTipoDoc($document->doc_type) #Siempre será 03: Boleta
            ->setSerie($document->serie)
            ->setCorrelativo($document->correlative)
            ->setFechaEmision($document->date)
            ->setTipoMoneda('PEN')
            ->setCompany($company)
            ->setClient($client)
            #->setMtoOperGravadas($document->sub_total)
            #->setMtoOperExoneradas(100)
            ->setMtoIGV($document->tax)
            ->setTotalImpuestos($document->tax)
            ->setValorVenta($document->sub_total)
            ->setMtoImpVenta($document->total);
        

        if($business->sector == 'edu'){
            $invoice->setMtoOperInafectas($document->sub_total);
            $invoice->setTax( Tax::getByAfectacion(30) );  #IGV PARA LOS COLEGIOS ES CERO
        }else{
            $invoice->setMtoOperGravadas($document->sub_total);
            $invoice->setTax( Tax::getByAfectacion(10) );  #IGV ES 10
        }


        if($document->order_reference){
            $invoice->setCompra($document->order_reference);
        }

        if($document->guide){            
            $invoice->setGuias([['nroDoc'=>$document->guide, 'tipoDoc' => '09']]);
        }

        if($document->note){
            $invoice->setNota($document->note);
        }

        // PREPARAMOS DATOS DE DETALLE DE DOCUMENTO
        $items = [];
        foreach($document->details as $detail):
            $item = new SaleDetail();
            $item->setCodProducto('P001')
                ->setUnidad('NIU')
                ->setDescripcion($detail->description)
                ->setCantidad($detail->quantity)
                ->setMtoValorUnitario($detail->sub_total / $detail->quantity)
                ->setMtoValorVenta($detail->sub_total)
                ->setMtoBaseIgv($detail->sub_total)                
                ->setIgv($detail->tax)                
                ->setTotalImpuestos($detail->tax)
                ->setMtoPrecioUnitario($detail->total / $detail->quantity);

                if($business->sector == 'edu'){
                    $item->setPorcentajeIgv(0)
                         ->setTipAfeIgv('30');
                }else{
                    $item->setPorcentajeIgv(18)
                         ->setTipAfeIgv('10');
                }            

            $items[] = $item;
        endforeach;
        $invoice->setDetails($items);

        $invoice
            ->setLegends([
                (new Legend())
                    ->setCode('1000')
                    ->setValue(num2letras($document->total))
            ]);

        // FIRMAMOS EL DOCUMENTO
        $fileName = $business->ruc.'-'.$invoice->tipoDoc.'-'.$invoice->serie.'-'.$invoice->correlativo;

        $xml = view('xml::invoice',['doc'=>$invoice])->render();
        $path = public_path('/xml/'.$fileName.'.xml');
        file_put_contents($path, $xml);
        $xmlSigned = $this->sign( $business, $path );
        file_put_contents($path, $xmlSigned);

        // CREAMOS EL WSCLIENT
        $wsClient = new SoapClient( $this->WSDL );
        $wsClient->setService($this->getSunatService($business));
        $wsClient->setCredentials(strtoupper($business->ruc.$business->sol_user), $business->sol_pass);

        // ENVIAMOS EL DOCUMENTO
        $billSender = new BillSender();
        $billSender->setClient($wsClient);
        $result = $billSender->send($fileName,$xmlSigned);

        $data['document_id'] = $document->id;

        return $this->processResult($result, $data, $business, $invoice->tipoDoc, $invoice->serie, $invoice->correlativo);

    }

    // REENVÍO DE NOTAS DE CRÉDITO
    public function note_resend($document){

        $business = $document->business;
        $office = Office::find($document->office_id);
        $refered = Document::find($document->refered_document_id);

        // Cliente
        {
            $address = new Address();
            // $address->setUbigueo('150101')
            //     ->setDepartamento('LIMA')
            //     ->setProvincia('LIMA')
            //     ->setDistrito('LIMA')
            $address->setUrbanizacion('NONE')
                ->setDireccion($document->customer_address)
                ->setCodigoPais('PE');

            $client = new Client();
            $client->setTipoDoc($document->customer_doc_type) # Puede ser DNI , PAS, CEX
                ->setNumDoc($document->customer_doc)
                ->setRznSocial($document->customer_name)
                ->setAddress($address);
        }


        // Business
        {
            $address = new Address();
            // $address->setUbigueo('150101')
            //     ->setDepartamento('LIMA')
            //     ->setProvincia('LIMA')
            //     ->setDistrito('LIMA')
            //     ->setUrbanizacion('NONE')
            //     ->setDireccion($business->address)
            //     ->setCodigoPais('PE');

            $address->setDireccion($office->address)
                ->setCodLocal($office->code)
                ->setCodigoPais('PE');


            $company = new Company();
            $company->setRuc($business->ruc)
                ->setRazonSocial($business->legal_name)
                ->setNombreComercial($business->comercial_name)
                ->setAddress($address);
        }

        // Documento
        $note = new Note();
        $note
            ->setUblVersion('2.1')
            ->setTipDocAfectado(@['F' => '01', 'B' => '03', '03' => '03', '01' => '01'][$data->doc_reference_type])
            ->setNumDocfectado($refered->serie."-".$refered->correlative)
            ->setTipDocAfectado($refered->doc_type)
            ->setCodMotivo($document->response_code)
            ->setDesMotivo($document->motive)
            ->setTipoDoc('07')
            ->setSerie($document->serie)
            ->setFechaEmision($document->date)
            ->setCorrelativo($document->correlative)
            ->setTipoMoneda('PEN')
            ->setCompany($company)
            ->setClient($client)
            #->setMtoOperGravadas($document->sub_total)
            ->setMtoIGV($document->tax)
            ->setTotalImpuestos($document->tax)
            ->setMtoImpVenta($document->total);            

        
        if($business->sector == 'edu'){
            $note->setMtoOperInafectas($document->sub_total);
            $note->setTax( Tax::getByAfectacion(30) );  #IGV PARA LOS COLEGIOS ES CERO
        }else{
            $note->setMtoOperGravadas($document->sub_total);
            $note->setTax( Tax::getByAfectacion(10) );  #IGV ES 10
        }

        $items = [];
        foreach($document->details as $detail):            
            $item = new SaleDetail();
            $item->setCodProducto('P001')
                ->setUnidad('NIU')
                ->setDescripcion($detail->description)
                ->setCantidad($detail->quantity)
                ->setMtoValorUnitario($detail->unit_price)
                ->setMtoValorVenta($detail->unit_price)
                ->setMtoBaseIgv($detail->sub_total)                
                ->setIgv($detail->tax)                
                ->setTotalImpuestos($detail->tax)
                ->setMtoPrecioUnitario($detail->total);

                if($business->sector == 'edu'){
                    $item->setPorcentajeIgv(0)
                         ->setTipAfeIgv('30');
                }else{
                    $item->setPorcentajeIgv(18)
                         ->setTipAfeIgv('10');
                }

            $items[] = $item;            
        endforeach;

        $note->setDetails($items);


        $note
            ->setLegends([
                (new Legend())
                    ->setCode('1000')
                    ->setValue(num2letras($document->total))
            ]);

        /* FIRMAMOS EL DOCUMENTO */
        $fileName = $business->ruc.'-'.$note->tipoDoc.'-'.$note->serie.'-'.$note->correlativo;

        $xml = view('xml::notacr',['doc'=>$note])->render();
        $path = public_path('/xml/'.$fileName.'.xml');
        file_put_contents($path, $xml);
        $xmlSigned = $this->sign( $business, $path );
        file_put_contents($path, $xmlSigned);

        /* CREAMOS EL WS CLIENTE */
        $wsClient = new SoapClient( $this->WSDL );
        $wsClient->setService($this->getSunatService($business));
        $wsClient->setCredentials(strtoupper($business->ruc.$business->sol_user), $business->sol_pass);

        /* ENVIAMOS EL DOCUMENTO */
        $billSender = new BillSender();
        $billSender->setClient($wsClient);
        $result = $billSender->send($fileName,$xmlSigned);

        $data['document_id'] = $document->id;
        return $this->processResult($result, $data, $business, $note->tipoDoc, $note->serie, $note->correlativo);

    }

    // REENVÍO MASIVO DE BOLETAS, FACTURAS Y NOTAS DE CRÉDITO PENDIENTES (TODOS LOS EMISORES HÁBILES)
    public function resendBatch($business_id = false, $date = false){
    
        if($business_id)
            $businesses = Business::where('due_date', '>=', date('Y-m-d'))            
                    ->where('id', $business_id)
                    ->get();
        else            
            $businesses = Business::where('due_date', '>=', date('Y-m-d'))            
                    ->where('environment', 'production')
                    ->where('id','<>',24)
                    ->get();                    
                        
        #print_r($businesses); die();
        $counter = 0;    


        foreach ($businesses as $business) {
            $env = ['development'=>'dev','production'=>'prod'][$business->environment];            

            if($date)
                $documents = Document::where('env', $env)
                ->where('business_id',$business->id)
                ->where('status', 3)                
                ->whereRaw("date(date) = '$date' ")            
                ->get();
            else
                $documents = Document::where('env', $env)
                ->where('business_id',$business->id)
                ->where('status', 3)                                
                ->get();

                        
            # Envio de boletas
            $boletas = $documents->where('doc_type', '03');            
            #print $boletas->count(); die();

            if ($boletas->count() ) {            
                $this->resumen_create_base($business, $boletas, 1);            
                $counter += $boletas->count();
            }
            
            # Envio de facturas y notas de crédito            
            $facturas = $documents->whereIn('doc_type', ['01', '07']);
            if( $facturas->count()){
                foreach ($facturas as $factura) {
                    $this->resend_base($business, $factura);
                }
                $counter += $facturas->count();
            }        
             
        }
        return $counter;
    }

}
