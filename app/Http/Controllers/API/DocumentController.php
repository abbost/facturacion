<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Http\Controllers\API\DocumentTraits\DocumentSearch;
use App\Http\Controllers\API\DocumentTraits\DocumentCreate;
use App\Http\Controllers\API\DocumentTraits\DocumentResend;
use App\Http\Controllers\API\DocumentTraits\DocumentHelpers;

class DocumentController extends Controller{
    use DocumentSearch, DocumentCreate, DocumentResend, DocumentHelpers;
    private $pagination = 20;
    private $WSDL = "";

    public function __construct(){
        view()->addNamespace('xml', app_path('Abbost/Templates'));
        $this->WSDL = public_path("wsdl/billService.wsdl"); #incluye los wsdl para producción y pruebas
    }

}


/**********************
    VALORES
***********************

"DNI" => 1
"CEX" => 4
"RUC" => 6 
"PAS" => 7

*********************/

/************* COMPOSICION DEL REQUEST ***************

Request $data
$data                       devuelve una lista gigantesca de objetos
$data->request              devuelve el objeto PARAMETER_BAG. NO SIRVE DE NADA
$data->all()                devuelve EL ARREGLO que esta dentro de PARAMTER_BAG
$data['algo'] => 'abc'      actualiza lo que esta dentro de parameters
$data->atributos            esto crea un objeto a la altura de los demas objetos como PARAMTER_BAG.

print $data->attr === $data['attr']

object(Symfony\Component\HttpFoundation\ParameterBag)#64 (1) {
  ["parameters":protected]=>
  array(23) {
    ["_token"]=> string(40) "HKLJaEtzvgi3wASJEeYIirKjeO2f2QaTUivRTlv2"
    ["doc_type"]=> string(1) "B"
    ["serie"]=> string(4) "B001"
    ["correlative"]=> string(2) "57"
    ["date"]=> string(10) "2019-09-12"
    ["order_reference"]=> NULL
    ["note"]=> NULL
    ["anonimous"]=> string(1) "1"
    ["receptor_doc_type"]=> string(3) "DNI"
    ["receptor_doc"]=>string(3) "---"
    ["receptor_name"]=>string(3) "---"
    ["receptor_address"]=>string(3) "---"
    ["receptor_email"]=>NULL
    ["receptor_email_content"]=>NULL
    ["products"]=>
    array(1) {
      [1]=>
      array(8) {
        ["id"]=> string(1) "0"
        ["onu_product_code"]=> string(1) "0"
        ["quantity"]=>string(1) "1"
        ["description"]=>string(36) "soporte tecnico xafiro 5 puntos TASA"
        ["price"]=>string(7) "1250.00"
        ["sub_total"]=>string(7) "1059.32"
        ["tax"]=>string(6) "190.68"
        ["total"]=>string(7) "1250.00"
      }
    }
    ["sub_total"]=> string(7) "1059.32"
    ["tax"]=> string(6) "190.68"
    ["total"]=> string(7) "1250.00"
    ["print_ticket"]=> string(1) "1"
    ["paid_by"]=> string(4) "cash"
    ["card"]=> NULL
    ["voucher"]=>NULL
    ["emisor_doc"]=>string(11) "20549370676"
  }
}

["emisor_doc"]=>string(8) "12345678"

/*********** /