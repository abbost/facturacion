<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Driver;

class DriverController extends Controller{
	public function index(){
		$drivers = Driver::where('status',1)->get();
		return view('guide.driver.index',[
			'drivers'=>$drivers,
			'view' => 'guide'
		]);
	}

	public function create(){		
		return view('guide.driver.create');
	}

	public function store(Request $request){		
		$driver = new Driver();
		$driver->name = $request->name;
		$driver->license = $request->license;
		$driver->save();

		return redirect('guide/driver');
	}

	public function edit($id){		
		$driver = Driver::find($id);		
		return view('guide.driver.edit',['driver'=>$driver]);
	}

	public function update(Request $request){		
		$driver = Driver::find($request->driver_id);
		$driver->name = $request->name;
		$driver->license = $request->license;
		$driver->save();
		return redirect('guide/driver');
	}

	public function delete($id){		
		$driver = Driver::find($id);
		$driver->status = 0;		
		$driver->save();
		return redirect('guide/driver');
	}
}