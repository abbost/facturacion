<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Car;

class CarController extends Controller{
	public function index(){
		$cars = Car::where('status',1)->get();
		return view('guide.car.index',[
			'cars'=>$cars,
			'view' => 'guide'
		]);
	}

	public function create(){		
		return view('guide.car.create');
	}

	public function store(Request $request){		
		$car = new Car();
		$car->brand = $request->brand;
		$car->plate = $request->plate;
		$car->save();

		return redirect('guide/car');
	}

	public function edit($id){		
		$car = Car::find($id);
		return view('guide.car.edit',['car'=>$car]);
	}

	public function update(Request $request){		
		$car = Car::find($request->car_id);
		$car->brand = $request->brand;
		$car->plate = $request->plate;
		$car->save();
		return redirect('guide/car');
	}

	public function delete($id){		
		$car = Car::find($id);
		$car->status = 0;		
		$car->save();
		return redirect('guide/car');
	}
}