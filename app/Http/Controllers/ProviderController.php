<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Provider;

class ProviderController extends Controller {

    public function index (Request $request) {
        return view('provider.index', [
            'providers' => business('provider')->get(),
            'view' => 'purchase'
        ]);
    }

    public function create (Request $request) {
        return view('provider.popup_create');
    }

    public function store (Request $request){
        $provider = $this->save($request, (new Provider));
        return redirect('provider')
            ->with('status', 'Proveedor registrado correctamente');
    }


    public function edit (Request $request) {
        $provider = Provider::where('business_id', session('business')->id)->where('id', $request->id)->firstOrFail();
        session(['update_provider_id' => $provider->id]);
        return view('provider/popup_edit',[
            'provider' => $provider
        ]);
    }

    public function update (Request $request){
        $provider = $this->save($request, Provider::findOrFail(session('update_provider_id')));
        return redirect('provider')
            ->with('status','El proveedor ha sido actualizado correctamente');
    }

    public function save (Request $request, $provider) {
        $provider->business_id = session('business')->id;
        $provider->legal_name = $request->legal_name;
        $provider->ruc = $request->ruc;
        $provider->phone = $request->phone;
        $provider->email = $request->email;
        $provider->save();
        return $provider;
    }

    public function delete(Request $request){
        $provider = business('provider', $request->id);
        if (!$provider->is_deletable()) {
            return redirect('provider')
                ->with('error', 'El proveedor no pudo ser eliminado debido a que ya tiene compras registradas');
        }
        $provider->delete();
        return redirect('provider')
            ->with('status', 'El proveedor ha sido eliminado correctamente');
    }


}
