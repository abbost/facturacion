<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Business;
use App\Models\Item;
use App\Models\Promotion;

class LoginController extends Controller{
    protected $redirectTo = 'document/create';

    public function showLoginForm(){
        session(['business'=>null]);
        session(['user'=>null]);
        session(['office'=>null]);
        session(['permissions'=>null]);
        #session(['times'=>0]);
        return view('auth.login', [
            'view'=>'login'
        ]);
    }

    public function login(Request $request){
        session(['business'=>null]);
        session(['user'=>null]);
        session(['office'=>null]);
        session(['times'=>1+(session('times')?:0)]);
        $user = $this->validateAccess($request);
        if(@$user->error===null){
            session(['user'=>$user]);
            session(['business'=>$user->business]);
            session(['office'=>$user->office]);
            session(['permissions'=>$user->permissions]);            

            if (is_readable(public_path("news/new.htm"))){                
                return redirect($this->redirectTo)->with('ads',true);
            }else
                return redirect($this->redirectTo);

        }
        throw ValidationException::withMessages([
            'email' => [trans($user->error)]
        ]);
    }

    public function validateAccess(Request $request){
        $data = [
            'email'=>$request->email,
            'password'=>$request->password
        ];
        $user = $this->access($request);
        if(!$user||isset($user->error)){
            $user = (object)['error'=>'auth.failed'];
        }
        if(session('captcha') != null && $request->captcha != session('captcha')){
            $user = (object)['error'=>'auth.badcaptcha'];
        }
        return $user;
    }

    public function generate_captcha(){
        $captcha_length = 5;
        $set = implode('',[
            "0123456789",
            "abcdefghijklmnopqrstuvwxyz",
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            #"@#&%/+*_¿?¡!"
        ]);
        $rand_gen="";
        for($i=0;$i<$captcha_length;$i++)
            $rand_gen.=$set[rand(0,strlen($set)-1)];
        # $angle = rand(0,30);
        $lines = rand(1,1);
        $image = imagecreatetruecolor(115, 44);
        $black = imagecolorallocate($image, 0, 0, 0);
        $color = imagecolorallocate($image, 172, 32, 255);
        $white = imagecolorallocate($image, 255, 255, 255);
        imagefilledrectangle($image,0,0,115,44,$white);
        for($i=0;$i<$lines;$i++){
            imageline($image, rand(0,115), 0, rand(0,115), 44, $color);
            imageline($image, 0, rand(0,44), 115, rand(0,44), $color);
        }
        ini_set('display_errors',1);
        error_reporting(E_ALL);
        imagettftext($image, 20, 0, 8, 32, $color, __DIR__."/../../../../public/font/Century_Gothic_Regular.ttf", $rand_gen);
        session(['captcha' => $rand_gen]);
        header('Content-type: image/png');
        imagepng($image,NULL,0);
    }

    public function logout(Request $request){
        $request->session()->invalidate();
        return redirect('login');
    }

    public function access(Request $request){
        $user = User::where('email',$request->email)->first();
        if(!$user||!Hash::check($request['password'],$user['password'])){
            return (object)['error'=>'Login incorrecto'];
        }
        return $user;
    }

}
