<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $apiUrl = "http://localhost/facturacione/user_facturacion_electronica/api/";
    #public $apiUrl = "https://api.facturacionelectronicapyme.com/api/";
    public function sendToApi($data,$url,$mode){
        $data['token'] = "ABC";
        $content = json_encode($data);
        $curl = curl_init($this->apiUrl.$url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-type: application/json",]);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($mode));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        #curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        #var_dump($response);
        if(curl_errno($curl)) return ['codigo'=>1,'error'=>curl_error($curl)];
        curl_close($curl);
        return $response;
    }

    public $clubUrl = "http://localhost/xafiroclub/xafiroclub_admin/api/";

    public function sendToClub($data,$url,$mode){
        $data['token'] = "xa_club_KEY2000";
        $content = json_encode($data);
        $curl = curl_init($this->clubUrl.$url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-type: application/json",]);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($mode));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        #curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        #var_dump($response); die();
        if(curl_errno($curl)) return ['codigo'=>1,'error'=>curl_error($curl)];
        curl_close($curl);
        return $response;
    }
}
