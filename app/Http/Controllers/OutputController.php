<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Output;

class OutputController extends Controller {

    public function index (Request $request) {
        set_current_dates($request);
        return view('output.index', [
            'outputs' => filtered('output')->orderBy('id', 'desc')->get(),
            'view' => 'purchase'
        ]);
    }

    public function create(Request $request){
        return view('output.popup_create', [
            'products' => business('product')->get()->map(function ($product) {
                $product->_stock = @$product->stock(office_id())->quantity ?: 0;
                return $product;
            })
        ]);
    }

    public function store(Request $request) {
        $stock = office('stock')->where('product_id', $request->product_id)->firstOrFail();
        $response = $stock->reduce_stock($request->quantity, null, $request->comments);
        return redirect('output')
            ->with($response['status'] ? 'error' : 'status', $response['message']);
    }

    public function delete(Request $request){
        $output = office('output', $request->id);
        if (!$output->is_deletable()) {
            return redirect('output')
                ->with('error', 'Este consumo no pudo ser eliminado debido a que tiene una venta vinculada');
        }
        $output->safe_delete();
        return redirect('output')
            ->with('status', 'El consumo ha sido eliminado correctamente');
    }

    public function print_($id){
        $business = business();
        $output = Output::find($id);
        $html = view('output.print',compact('business','output'))->render();                
        return json_encode(['content'=>$html]);
    }

}
