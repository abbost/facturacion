<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CPerson;
use App\Models\CBusiness;
use App\Models\Document;

use Illuminate\Http\Request;

class CustomerController extends Controller{

    public function index (Request $request) {
        $type = $request->type == 'juridico' ? 'juridico' : 'natural';
        if($type == 'juridico')
            $customers = business('cbusiness')->where('status',1)->orderBy('legal_name', 'asc')->get();
        else
            $customers = business('cperson')->where('status',1)->orderBy('fullname', 'asc')->get();
        
        return view('customer.index', [
            'customers' => $customers,
            'type' => $type,
            'view' => 'customer'
        ]);
    }

    public function create (Request $request) {
        $type = $request->type == 'juridico' ? 'juridico' : 'natural';
        if($type == 'juridico')
            return view('customer.create_business');
        else
            return view('customer.create_natural');
    }

    public function store (Request $request) {
        $request->validate([
            'type' => 'in:natural,business',
        ]);

        $customer = new Customer;
        $customer->business_id = business_id();
        $customer->origin = $request->type == 'business' ? 1 : 2;

        $customer_origin = null;
        switch ($customer->origin) {
            case 1:
                $request->validate([
                    'legal_name' => 'required',
                    'ruc' => 'required'
                ]);

                if (business('cbusiness')->where('status',1)->where('ruc', $request->ruc)->first()) {
                    return redirect('customer?type=juridico')
                        ->with('error', 'No se pudo registrar. Ya existe un cliente el RUC '.$request->ruc);
                }

                $customer_origin = new CBusiness;
                $customer_origin->legal_name = $request->legal_name;
                $customer_origin->ruc = $request->ruc;
                $customer_origin->representative = $request->representative;

                break;
            case 2:
                $request->validate([
                    'fullname' => 'required',
                    'doc_type' => 'in:DNI,RUC,CEX,PAS',
                    'doc' => 'required',
                ]);
                $doc_type = ["DNI" => 1, "CEX" => 4, 'RUC' => 6, "PAS" => 7][$request->doc_type];

                if (business('cperson')->where('status',1)->where('doc_type', $doc_type)->where('doc', $request->doc)->first()) {
                    return redirect('customer?type=natural')
                        ->with('error', 'No se pudo registrar. Ya existe un cliente con el documento '.$request->doc);
                }

                $customer_origin = new CPerson;
                $customer_origin->fullname = $request->fullname;
                $customer_origin->doc_type = $doc_type;
                $customer_origin->doc = $request->doc;

                break;
        }

        $customer->save();

        $customer_origin->business_id = business_id();
        $customer_origin->address = @$request->address;
        $customer_origin->email = @$request->email;
        $customer_origin->customer_id = $customer->id;
        $customer_origin->save();

        $customer->origin_id = $customer_origin->id;
        $customer->save();
        
        return redirect('customer?type=' . ['', 'juridico', 'natural'][$customer->origin])
                ->with('status', 'Cliente registrado correctamente');
    }

    public function edit (Request $request) {
        $customer = business('customer', $request->id);
        session(['update_customer_id' => $customer->id]);
        return view('customer.popup_edit', [
            'customer' => $customer->_origin,
            'type' => ['', 'business', 'person'][$customer->origin]
        ]);
    }

    public function update (Request $request) {
        $customer = business('customer', session('update_customer_id'));
        $customer_origin = $customer->_origin;
        switch ($customer->origin) {
            case 1:
                $request->validate([
                    'legal_name' => 'required',
                    'ruc' => 'required'
                ]);

                if ($request->doc != $customer_origin->doc && business('cbusiness')->where('ruc', $request->ruc)->first()) {
                    return redirect('customer?type=juridico')
                        ->with('error', 'Ya existe un cliente con igual RUC');
                }

                $customer_origin->legal_name = $request->legal_name;
                $customer_origin->ruc = $request->ruc;
                $customer_origin->representative = $request->representative;

                break;
            case 2:
                $request->validate([
                    'fullname' => 'required',
                    'doc_type' => 'in:DNI,RUC,CEX,PAS',
                    'doc' => 'required',
                ]);
                $doc_type = ["DNI" => 1, "CEX" => 4, 'RUC' => 6, "PAS" => 7][$request->doc_type];

                if ($request->doc != $customer_origin->doc && business('cperson')->where('doc_type', $doc_type)->where('doc', $request->doc)->first()) {
                    return redirect('customer?type=natural')
                        ->with('error', 'Ya existe un cliente con igual ' . $request->doc_type);
                }

                $customer_origin->fullname = $request->fullname;
                $customer_origin->doc_type = $doc_type;
                $customer_origin->doc = $request->doc;

                break;
        }

        $customer_origin->address = @$request->address;
        $customer_origin->email = @$request->email;
        $customer_origin->save();

        return redirect('customer?type=' . ['', 'juridico', 'natural'][$customer->origin])
            ->with('status', 'Cliente actualizado correctamente');
    }

    public function delete (Request $request) {
        $customer = business('customer', $request->id);
        $customer->status = 0;
        $customer->save();

        $customer->_origin->status = 0;
        $customer->_origin->save();

        $name = $customer->_origin->legal_name ?: $customer->_origin->fullname;

        return redirect('customer?type=' . ['', 'juridico', 'natural'][$customer->origin])
            ->with('status', 'Cliente '.$name.' eliminado correctamente');
    }

    // este archivo se baja una sola vez
    public function searchCustomer(Request $request){        
        // hasta que resolvamos el envio de parametros
        #if(strtolower($request->doc_type) == 'ruc')
            $business = CBusiness::select('legal_name as name','ruc as doc','address','email','customer_id')
            ->where('ruc','like','%'.$request->keyword.'%')
            ->where('business_id', business_id())
            ->where('status',1)->get();
        #else
            $person = CPerson::select('fullname as name','doc','address','email','customer_id')
            ->where('doc','like','%'.$request->keyword.'%')
            ->where('business_id', business_id())
            ->where('status',1)->get();
        
        $customers =  $business->toBase()->merge($person->toBase());
        return response()->json($customers);
    }

    public function get_documents($customer_id){        
        $customer = Customer::find($customer_id)->_origin; // devuelve person o business
        $customer->name = $customer->fullname ?: $customer->legal_name;
        $customer->doc = $customer->doc ?: $customer->ruc;
        if($customer->business_id <> business_id())
            abort(403, 'Unauthorized action.');
        
        $url =  url('api/document/search?id=');
        $total = 0;
        $documents = Document::where('customer_id',$customer_id)
        ->where('env', business()->environment == 'production' ? 'prod' : 'dev')
        ->orderBy('created_at','desc')->get();
        
        foreach($documents as $document){
            // las notas de crédito se restan
            if($document->doc_type == '07')
                $total -= $document->total;
            else
                $total += ($document->status != 0 && $document->status != 2 ? $document->total : 0 );
                   
            $document->encrypted_id = id_mask($document->id, 'encrypt');            
        }        
        return view('customer.document',compact('customer','documents','total','url'));
    }

    // public function searchCustomer_old(Request $request){
    //     $doc_type = strtolower($request->doc_type);
    //     $result = [];

    //     if($doc_type == 'ruc'):
    //         $customers = CBusiness::where('business_id', business_id())                
    //             ->where(function ($q) use ($request) { # esto se usa para encerrar en parentesis ( cond1 OR cond2)
    //                 $q->where('ruc','like','%'.$request->doc.'%')
    //                 ->orwhere('legal_name','like', '%'.$request->doc.'%');
    //             })
    //             ->where('status',1)
    //             ->get();
            
    //         foreach($customers as $customer) {                 
    //              $customer->legal_name = strtoupper( $customer->legal_name );                
    //              $result[] = [
    //                 'id' => $customer->ruc,  # El id es muy importante porque es el name que se pasará por el POST
    //                 'doc'=>$customer->ruc, 
    //                 'customer_id' => $customer->customer_id,
    //                 'text' => $customer->ruc.' __ '.$customer->legal_name, 
    //                 'name'=> $customer->legal_name, 
    //                 'address'=> $customer->address
    //             ];
    //         }
    //     else:
    //         $customers = CPerson::where('business_id', business_id())                
    //             ->where(function ($q) use ($request) {
    //                 $q->where('doc','like','%'.$request->doc.'%')
    //                 ->orwhere('fullname','like', '%'.$request->doc.'%');
    //             })
    //             ->where('status',1)
    //             ->get();                
            
    //         foreach($customers as $customer) {                 
    //              $customer->fullname = strtoupper( $customer->fullname );
    //              $result[] = [
    //                 'id' => $customer->doc,  # El id es muy importante porque es el name que se pasará por el POST
    //                 'doc'=>$customer->doc, 
    //                 'customer_id' => $customer->customer_id,
    //                 'text' => $customer->doc.' __ '.$customer->fullname, 
    //                 'name'=> $customer->fullname, 
    //                 'address'=> $customer->address
    //              ];
    //         }
    //     endif;

        //$result[] = ['id'=>'C','doc'=>'']; #Esto es para que en el select2 aparezca el link Crear Cliente

        //return \Response::json($result);
    //}


    // public function searchCustomerSunat(Request $request){

    //     $result = @(new \Sunat\Sunat())->search($request->doc);                
    //     $customer = [];
    //     $error = 1;
    //     if ($result->success == true) {
    //         $error = 0;
    //         if (strtolower($request->doc_type) != 'ruc') {
    //             $result->result->ruc = substr($result->result->ruc ?: '', 2, 8);
    //         }            
    //         $customer = [
    //             'id' => $result->result->ruc,                
    //             'doc' => $result->result->ruc,
    //             'customer_id' => 0,
    //             'text' => $result->result->ruc.' __ '. strtoupper($result->result->razon_social),
    //             'name' => strtoupper($result->result->razon_social),
    //             'address' => $result->result->direccion,
    //         ];
    //     }
    //     return \Response::json([
    //         'error' => $error,
    //         'customer' => $customer
    //     ]);
    // }


    public function searchCustomerSunat(Request $request){

        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6InBlZHJvc2F1bmFAaG90bWFpbC5jb20ifQ.GrXOLI1stfw8UmRQ8025TtF_yk4cbW9WjER2QyRo48E";

        if (strtolower($request->doc_type) == 'ruc')
            $url = "https://dniruc.apisperu.com/api/v1/ruc/".$request->doc."?token=".$token;
        else
            $url = "https://dniruc.apisperu.com/api/v1/dni/".$request->doc."?token=".$token;
        
        #print $url; die();
        $curl = curl_init($url);        
        curl_setopt($curl, CURLOPT_HEADER, false);                
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        #curl_setopt($curl, CURLOPT_POSTFIELDS, "token=".$token);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);           
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // esto para evitar enviarlo con SSL y pueda funcionar de localhost
        $result = json_decode(curl_exec($curl));
                
        if(curl_errno($curl)) 
            return ['codigo'=>1,'error'=>curl_error($curl)];
        curl_close($curl);
            
        // var_dump($result); die();

        $customer = [];
        $error = 1;
        if ($result) {
            $error = 0;
            if (strtolower($request->doc_type) == 'ruc') {
                $customer = [
                    'id' => $result->ruc,                
                    'doc' => $result->ruc,
                    'customer_id' => 0,
                    'text' => $result->ruc.' __ '. strtoupper($result->razonSocial),
                    'name' => strtoupper($result->razonSocial),
                    'address' => $result->direccion,
                ];            
            }else{                            
                $customer = [
                    'id' => $result->dni,                
                    'doc' => $result->dni,
                    'customer_id' => 0,
                    'text' => $result->dni.' __ '. strtoupper($result->apellidoPaterno.' '.$result->apellidoMaterno.' '.$result->nombres),
                    'name' => strtoupper($result->apellidoPaterno.' '.$result->apellidoMaterno.' '.$result->nombres),
                    'address' => '',
                ];
            }   
        }

        return \Response::json([
            'error' => $error,
            'customer' => $customer
        ]);
    }



}