<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Office;
use App\Models\UserPermission;
use App\Models\Permission;

use Illuminate\Http\Request;

class UserController extends Controller{

    public function index (Request $request) {
        return view('user.index', [
            'users' => office('user')->where('status', 1)->get(),
            'view' => 'business'
        ]);
    }



    public function create (Request $request) {
        $count = business('user')->where('status',1)->count();
        $domain = strtolower(str_replace(" ", "", session('business')->comercial_name));
        if($count == 3)
            return view('user.popup_deny');
        else
            return view('user.popup_create',compact('domain'));
    }


    public function store (Request $request) {
        $request->validate([
            'fullname' => 'required',
            #'dni' => 'required',
            'role' => 'required',
            #'email' => 'required',
            'password' => 'required'
        ]);

        $count = business('user')->where('status',1)->count();
        if($count == 3){
            return redirect('user')
                ->with('error', 'Error. Se ha superado el limite de usuarios');
        }

        if ($user = business('user')->where('dni', $request->dni)->first()) {
            return redirect('user')
                ->with('error', 'Error. Un usuario con este dni <b>'.$request->dni.'</b> ya está registrado');
        }

        if ($user = business('user')->where('email', $request->email)->first()) {
            return redirect('user')
                ->with('error', 'Error. Un usuario con este mail <b>'.$request->email.'</b> ya está registrado');
        }

        $user = $this->save($request, (new User), $request->password);

        return redirect('user')
            ->with('status', 'Usuario registrado correctamente');
    }


    public function edit (Request $request) {
        $user = office('user', $request->id);
        $domain = strtolower(str_replace(" ", "", session('business')->comercial_name));
        session(['update_user_id' => $user->id]);

        return view('user.popup_edit', [
            'user' => $user,
            'domain' => $domain
        ]);
    }

    public function update (Request $request) {        

        if ($user = business('user')->where('email', $request->email)->first()) {
            return redirect('user')
                ->with('error', 'Error. Un usuario con este mail <b>'.$request->email.'</b> ya está registrado');
        }        
        $user = office('user', session('update_user_id'));
        $user = $this->save($request, $user, $request->password);
        
        return redirect('user')
            ->with('status', 'Usuario actualizado correctamente');
    }


    public function save (Request $request, $user, $password = null) {

        $user->business_id = business_id();
        $user->office_id = office_id();
        $user->fullname = $request->fullname;
        $user->dni = $request->dni;
        $user->role = @[
            'empleado' => 'empleado',
            'jefe' => 'jefe',
            'administrador' => 'administrador'
        ][$request->role] ?: @$user->role ?: 'empleado';
        #$user->email = $request->email;
        $user->email = $request->fullname.'@'.$request->domain;
        if ($password) {
            $user->password = bcrypt($request->password);
        }
        $user->save();

        UserPermission::where('user_id', $user->id)->delete();
        Permission::where('for_' . $user->role, 1)->get()->each(function ($permission) use ($user) {
            $user->add_permission($permission->id);
        });

        return $user;
    }

    public function delete (Request $request) {
        $user = office('user', $request->id);
        $user->status = 0;
        $user->save();
        return redirect('user')
            ->with('status','Usuario eliminado correctamente');
    }

    public function showPermission (Request $request){
        $user = office('user', $request->id);
        session(['permission_user_id' => $user->id]);
        
        $assigned = [];
        foreach($user->permissions as $perm)
            $assigned[$perm->id] = true;

        return view('user.popup_permission', [
            'user' => $user,
            'permissions' => Permission::all(),
            'assigned' => $assigned,
            'permissions_not_assigned' => $user->missing_permissions()
        ]);
    }

    public function add_permissions (Request $request) {
        $permissions = $request->permissions;
        if (empty($permissions)) {
            return redirect('user')
                ->with('error', 'No se han seleccionado permisos');
        }
     
        $user = office('user', session('permission_user_id'));
        # borrando registros
        UserPermission::where('user_id',$user->id)->delete();

        foreach ($permissions as $permission_id) {
            $user->add_permission($permission_id);
        }

        return redirect('user')
            ->with('status', 'Permiso(s) agregado(s) correctamente');
    }

    public function delete_permission (Request $request) {
        $user = office('user', session('permission_user_id'));
        $user->delete_permission($request->id);
        return redirect('user')
            ->with('status', 'Permiso eliminado correctamente');
    }

    public function notPermission (){
        return view('notPermission');
    }
}