<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Promotion;
use App\Models\Item;

class PromotionController extends Controller{

    public function showList(){
        return view('promotion.listWrapper', [
            'promotions'=>session('promotions'),
            'items'=>session('items'),
            'view'=>'promotion'
        ]);
    }

    public function create(Request $request){
        $promotion = new Promotion;
        $promotion->business_id = session('business')->id;
        $promotion->item_id = $request->item_id;
        $promotion->points = $request->points;
        $promotion->discount = $request->discount?:0;
        $promotion->fixed_price = $request->fixed_price?:0;
        $item = Item::find($request->item_id);
        $data = [
            'businessId'=> session('business')->id,
            'name' => 'Promoción: '.$item->description,
            'points' => $request->points,
            'discount' => $request->discount?:0,
            'fixed_price' => $request->fixed_price?:0
        ];
        $club_response = @json_decode($this->sendToClub($data,'promotion/create','post'));
        if (@$club_response->status == 'ok') {
            $promotion->club_reference_id = @$club_response->promo_id;
        }else{    
            return redirect('promotion/list');
        }
        $promotion->save();
        $newPromo = Promotion::with('item')->where('id', $promotion->id)->first();
        //$promotion = @json_decode($this->sendToAPI($data,'promotion/create','post'));
        if ($newPromo) {
            session(['promotions'=>session('promotions')->merge(collect([$newPromo]))]);
        }
        return redirect('promotion/list');
    }

    public function delete(Request $request){
        $promotion = Promotion::where('business_id', session('business')->id)
            ->where('id', $request->promotion_id)->first();
        if ($promotion) {
            $data=[
                'id'=>$promotion->club_reference_id,
            ];
            $club_response = @json_decode($this->sendToClub($data,'promotion/delete','post'));
            if(@$club_response->status=='ok'){
                $promotion->delete();
            }else{
                return redirect('promotion/list');
            }
        }
        //$response = @json_decode($this->sendToAPI($data,'emisor/promotion/delete','post'));
        $promotions = session('promotions');
        $index = $this->get_promotion_index($request->promotion_id);
        if ($index !== -1) {
            unset($promotions[$index]);
        }
        session(['promotions'=>$promotions]);
        return redirect('promotion/list');
    }

    public function showExchange(){
        return view('promotion.exchange',[
            'view'=>'promotion'
        ]);
    }

    public function getAvailable(Request $request){
        session(['promotion_affiliate'=>null]);
        $data = [
            'business_id'=> session('business')->id,
            'document' => $request->dni
        ];
        $response = @json_decode($this->sendToClub($data,'point/get','post'));
        if (@$response->error) {
            return [
                'customer' => "",
                'points' => "",
                'content' => "<tr><td>".$response->error_msg."</td></tr>"
            ];
        }
        $promotions = array_filter(session('promotions')->toArray(), function ($promotion) use ($response) {
            return $promotion['points'] <= $response->points;
        });
        session(['promotion_affiliate' => (object)[
            'name' => strtoupper(@$response->firstname.' '.$response->lastname),
            'dni' => $request->dni,
        ]]);
        return [
            'customer' => session('promotion_affiliate')->name,
            'points' => @$response->points,
            'content' => (sizeof($promotions) == 0 ?
                "<tr><td>Se necesita más fondos para activar una promoción en este negocio</td></tr>" :
                view('promotion.list', [
                    'promotions' => $promotions,
                    'in_exchange' => true
                ])->render()
            )
        ];
    }

    public function use(Request $request){
        $promotion = @session('promotions')[$this->get_promotion_index($request->promotion_id)];
        if (!$promotion) {
            return [
                'error'=>1,
                'message'=>'El código de promoción no es válido',
                'redirect'=>''
            ];
        }
        $serie = session('business')->current_serie_boleta;
        if(@$promotion->discount != 0){
            $unit_price = $promotion->item->unit_price * (1 - round($promotion->discount/100, 2));
            $unit_value = $promotion->item->unit_value * (1 - round($promotion->discount/100, 2));
        }else if(@$promotion->fixed_price != 0){
            $unit_price = $promotion->fixed_price;
            $unit_value = round($promotion->fixed_price / 1.18, 2);
        }else{
            return [
                'error'=>1,
                'message'=>'No se ha programado correctamente el valor del descuento',
                'redirect'=>'--'
            ];
        }
        $data = [
            'emisor_doc' => session('business')->ruc,
            'date' => date('Y-m-d'),
            'time' => date('H:i:s'),
            'serie' => $serie,
            'correlative' => @app('App\Http\Controllers\DocumentController')->getNextCorrelative(new Request(['serie' => $serie]))['correlative'],
            'doc_type' => 'B',
            'currency' => 'PEN',
            'doc_oper_type' => '01',
            'receptor_doc_type' => 'DNI',
            'receptor_doc' => session('promotion_affiliate')->dni,
            'receptor_name' => session('promotion_affiliate')->name,
            'receptor_address' => '---',
            'sub_total' => $unit_value,
            'tax' => $unit_price - $unit_value,
            'total' => $unit_price,
            'products' => [[
                'id' => $promotion->item->id,
                'description' => $promotion->item->description,
                'quantity' => 1,
                'total' => $unit_price,
                'sub_total' => $unit_value,
                'tax' => $unit_price - $unit_value,
                'onu_product_code' => @$promotion->item->onu_product_code
            ]]
        ];
        //$response = @json_decode($this->sendToAPI($data,'document/create','post'));
        $response = @json_decode(json_encode(app('App\Http\Controllers\API\DocumentController')->create(new Request($data))));
        if (!@$response->error) {
            $data_ = [
                'document' => $data['receptor_doc'],
                'sale_id' => $data['correlative'],
                'business_id' => session('business')->id,
                'points' => $promotion->points,
                'amount' => $promotion->item->unit_price - $unit_price,
                'promo_id'=> $promotion->club_reference_id,
            ];
            $club_response = @json_decode($this->sendToClub($data_,'point/use','post'));
            if (isset($club_response->error)) {
                $response->message = (@$response->message ?: '').'. '.@$club_response->error_msg;
            }
        }
        return [
            'error'=>@$response->error,
            'message'=>@$response->message,
            'redirect'=>@$response->redirect
        ];
    }

    public function validateCode(Request $request){
        session(['xafiro_club_discount' => null]);
        if (!@$request->code) {
            return ['error' => 1, 'discount' => 0, 'message' => 'Se debe introducir un código'];
        }
        if (!@$request->customer_document || $request->customer_document == '---') {
            return ['error' => 1, 'discount' => 0, 'message' => 'Se debe introducir un dni de usuario válido'];
        }

        $creator_id = ""; $first_letter = ""; $business_id = "";
        $i = -1; while (strpos("0123456789", $request->code[++$i]) !== false) {
            $creator_id .= $request->code[$i];
            if($i == strlen($request->code)-1){break;}
        }
        $first_letter = $request->code[$i];
        while (++$i < strlen($request->code)) {
            $business_id .= $request->code[$i];
        }
        if (strtoupper(session('business')->comercial_name[0]) != $first_letter || $business_id != session('business')->id) {
            return ['error' => 1, 'discount' => 0, 'message' => 'Este código no es válido para este negocio'];
        }
        $data = [
            'business_id' => session('business')->id,
            'document' => $request->customer_document,
            'code' => $request->code
        ];
        $club_response = @json_decode($this->sendToClub($data,'coupon/check','post'));
        if (!@$club_response || @$club_response->error) {
            return ['error' => 1, 'discount' => 0, 'message' => @$club_response->message ?: 'Ocurrió un error al intentar validar código.'];
        }
        $data = [
            'business_id' => session('business')->id,
            'customer_doc_type' => 'DNI',
            'customer_doc' => $request->customer_document
        ];
        $response = @json_decode($this->sendToAPI($data,'document/customer/search','get'));
        if (!@$response || @$response->doc == 'null') {
            return ['error' => 1, 'discount' => 0, 'message' => 'Lo sentimos. El usuario no es un cliente nuevo'];
        }
        //return ['error' => 1, 'discount' => 0, 'message' => 'all ok'];
        session(['xafiro_club_discount' => (object)[
            'value' => @$club_response->discount,
            'creator_id' => $creator_id,
            'user_document' => $request->customer_document
        ]]);
        return ['error' => 0, 'discount' => @$club_response->discount, 'message' => 'El código está disponible para su uso'];
    }

    public function unableCode(){
        session(['xafiro_club_discount' => null]);
        return ['message' => 'El código se ha quitado exitosamente.'];
    }

    private function get_promotion_index($id){
        $promotions = session('promotions');
        foreach ($promotions as $k => $promotion) {
            if ($promotion->id == $id) {
                return $k;
            }
        }
        return -1;
    }
}
