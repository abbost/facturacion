<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Document as Document;
use App\Models\Business as Business;
use DB;

class IndicatorController extends Controller{

    public function index(){
    	$data = [
            'business_id' => business_id(),
            'interval' => 6
    	];
    	return view('indicator.index', [
            //'sales' => @json_decode($this->sendToAPI($data,'indicator/sales','get')),
            //'invoices' => @json_decode($this->sendToAPI($data,'indicator/invoices','get')),
            'sales' => $this->getSalesByMonth(new Request($data)),
            'invoices' => $this->getInvoicesByMonth(new Request($data)),
            'view' => 'business'
        ]);
    }

    public function getSalesByMonth(Request $request){
        $emisor = Business::find($request->business_id);
        $interval = $request->interval;
        $id = $emisor->id;
        $dateStart = date('Y-m-d',strtotime(date('Y-m-01')." - $interval MONTH"));
        $data = DB::select("
            SELECT sum(total) as  total, month(d.created_at) as month FROM document d
            inner join business e on d.business_id = e.id and e.id = $id
            where date(d.created_at) >= '$dateStart' and d.status <> 0 and if(e.environment = 'production', d.env = 'prod', d.env = 'dev')
            group by month(d.created_at), year(d.created_at)
        ");
        $month = ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        for($i=0;$i<sizeof($data);$i++){
            $data[$i]->month = $month[$data[$i]->month];
        };
        return $data;
    }

    public function getInvoicesByMonth(Request $request){
        $emisor = Business::find($request->business_id);
        $interval = $request->interval;
        $id = $emisor->id;
        $dateStart = date('Y-m-d',strtotime(date('Y-m-01')." - $interval MONTH"));
        $data = DB::select("
            SELECT count(*) as total, month(d.created_at) as month FROM document d
            inner join business e on d.business_id = e.id and e.id = $id
            where date(d.created_at) >= '$dateStart' and if(e.environment = 'production', d.env = 'prod', d.env = 'dev')
            group by month(d.created_at), year(d.created_at)
        ");
        $month = ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        for($i=0;$i<sizeof($data);$i++){
            $data[$i]->month = $month[$data[$i]->month];
        };
        return $data;
    }
}
