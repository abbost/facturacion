<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Business;
use PDF;

class BusinessController extends Controller{

    public function index () {
        return view('business.index', [
            'business' => business(),
            'view' => 'business'
        ]);
    }

    public function update (Request $request) {

        $business = Business::find(business_id());

        # Se guarda la imagen con el ruc de nombre 
        $image_url = $request->hidden_image;
        if ($request->image) {
            $image_url = $business->ruc.'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('img'), $image_url);
        }
        
        $business->comercial_name = $request->comercial_name;
        $business->print_option = $request->print_option == 'ticket' ? 'ticket' : 'pdf a4';
        $business->send_documents_sunat = intval($request->send_documents_sunat == 'ok');
        $business->image = $image_url;
        $business->save();
        session(['business'=>$business]);
        return redirect('business')
            ->with('status', 'Perfil actualizado correctamente');
    }
}
