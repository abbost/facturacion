<?php

namespace App\Http\Middleware;

use Closure;

class StartSession {
    public function handle($request, Closure $next){
        if (!session('business') || !session('user') || session('user')->status != 1 || !session('office') || session('office')->status != 1) {
            return redirect('login');
        }
        $actions = $request->route()->getAction();
        if (isset($actions['permission']) && !has_permission($actions['permission'])) {
            return redirect('user/notPermission');
        }
        set_current_office_id($request);
        return $next($request);
    }
}
