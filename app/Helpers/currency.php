<?php

/*
 *  ROLE - YA NO SE DEBE USAR
 */

function is_admin_or_boss () {
    return @['ADMINISTRATOR'=>1, 'BOSS'=>1][session('user')->role];
}

function is_admin () {
    return session('user')->role == 'ADMINISTRATOR';
}

/*
 *  PERMISSION
 */

function has_permission($permission) {
    return session('user')->has_permission($permission);
}

/*
 *  ENCRIPTACION
 */

function id_mask ($string, $mode) {
    # https://bhoover.com/using-php-openssl_encrypt-openssl_decrypt-encrypt-decrypt-data/
    try {
        $key = "JD{,¬M<@K}Ç3M'¨Ka6.^vDL7¬|Plig[K¬6¨-WC9B22mh7+Z¿Li";
        $encryption_key = base64_decode($key);
        switch ($mode) {
            case 'encrypt':
                $content = "";
                do {
                    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-ctr'));
                    $encrypted = openssl_encrypt($string, 'aes-256-ctr', $encryption_key, 0, $iv);
                    $content = base64_encode($encrypted . '::' . $iv);
                } while (strpos($content, '+') !== false);
                return $content;
            case 'decrypt':
                list($encrypted_data, $iv) = explode('::', base64_decode($string), 2);
                return openssl_decrypt($encrypted_data, 'aes-256-ctr', $encryption_key, 0, $iv);
            default:
                return $string;
        }
    } catch (\Exception $e) {
        return ['error'=>'ID incorrecto'];
    }
}

/*
 *  DATETIME
 */

function formated_hour ($datetime) {
    if (!$datetime) {return '';}
    return date('g:i a', strtotime($datetime));
}

function formated_date ($datetime) {
    return date('Y-m-d', strtotime($datetime));
}

function formated_month ($datetime) {
    return date('Y-m', strtotime($datetime));
}

/*
 *  REQUEST DATES
 */

function set_current_dates ($request) {
    $date_from = formated_date($request->date_from);
    $date_to = formated_date($request->date_to);
    $date_range = (strtotime($date_to) - strtotime($date_from)) / 86400;
    $today = date('Y-m-d');
    session(['current_dates' => (object)($request->date_from && $request->date_to && $date_range <= 31 && $date_range >= 0 ?
        ['from' => $date_from, 'to' => $date_to] :
        ['from' => $today, 'to' => $today]
    )]);
}

function dates ($type = null) {
    if (!$type) {
        return session('current_dates');
    }
    return @session('current_dates')->{$type};
}

function show_dates_filter ($url) { ?>
<div class="col-md-12" style="margin-bottom:20px;text-align:center;">
  <form class="fom-dates" action="<?= url($url) ?>" method="get">
    <div class="m-left-10"> Desde: <input type="date" name="date_from" class="btn filter mb-2 ml-2" value="<?= dates('from') ?>"></div>
    <div class="m-left-10"> Hasta: <input type="date" name="date_to" class="btn filter mb-2 ml-2" value="<?= dates('to') ?>"></div>
    <div class="m-left-10"> <button type="submit" class="btn filter btn-primary"><i class="fa fa-search"></i> Buscar</button></div>
  </form>
</div>
<?php }

/*
 *  MONTHS
 */

function set_current_months ($request) {
    $month_from = formated_month($request->month_from);
    $month_to = formated_month($request->month_to);
    $month_range = (strtotime($month_to) - strtotime($month_from));
    $today = date('Y-m');
    session(['current_months' => (object)($request->month_from && $request->month_to && $month_range >= 0 ?
        ['from' => $month_from, 'to' => $month_to] :
        ['from' => date('Y-m', strtotime($today . ' -5 month')), 'to' => $today]
    )]);
}

function months ($type = null) {
    if (!$type) {
        return session('current_months');
    }
    return @session('current_months')->{$type};
}

function show_months_filter ($url) { ?>
  <form action="<?= url($url) ?>" style="display:inline-block;">
    Ini: <input type="month" name="month_from" class="btn filter" value="<?= months('from') ?>">
    Fin: <input type="month" name="month_to" class="btn filter" value="<?= months('to') ?>">
    <button type="submit" class="btn filter"><i class="fa fa-search"></i> Buscar</button>
  </form>
<?php }

/*
 *  BUSINESS
 */

function business_id () {
    return session('business')->id;
}

function business ($child = null, $id = null) {
    if (!$child) {
        return session('business');
    }
    $child = @[
        'customer' =>       '\App\Models\Customer',
        'cbusiness' =>      '\App\Models\CBusiness',
        'cperson' =>        '\App\Models\CPerson',
        'item' =>           '\App\Models\Item',
        'office' =>         '\App\Models\Office',
        'product' =>        '\App\Models\Product',
        'provider' =>       '\App\Models\Provider',
        'purchase' =>       '\App\Models\Purchase',
        'service' =>        '\App\Models\Service',
        'user' =>           '\App\Models\User',
    ][$child];
    if ($child) {
        $query = $child::where('business_id', business_id());
        if ($id) {
            $query = $query->where('id', $id)->firstOrFail();
        }
        return $query;
    }
}

/*
 *  OFFICE
 */

function set_current_office_id ($request) {
    session(['current_office' => business('office', !@$request->office_id ?
        (session('current_office')          ?   session('current_office')->id   : session('office')->id) :
        (has_permission('office.multiwork') ?   $request->office_id             : session('office')->id)
    )]);
}

function office_id () {
    return session('current_office')->id;
}

function office ($child = null, $id = null, $office_id = null, $date_range = false, $month = false, $date_column = null) {
    if (!$child) {
        return session('current_office');
    }
    if (!$office_id) {
        $office_id = office_id();
    }
    $child = @[
        'cash_turn' =>      '\App\Models\CashTurn',
        'document' =>       '\App\Models\Document',
        'expense' =>        '\App\Models\Expense',
        'input' =>          '\App\Models\Input',
        'output' =>         '\App\Models\Output',
        'payment' =>        '\App\Models\Payment',
        'sale' =>           '\App\Models\Sale',
        'stock' =>          '\App\Models\Stock',
        'user' =>           '\App\Models\User',
        'office_item' =>    '\App\Models\OfficeItem',
    ][$child];
    
    if ($child) {
        $query = $child::where('office_id', $office_id);
        if ($id) {
            $query = $query->where('id', $id)->firstOrFail();
        }
        if ($date_range) {
            $date_column = $date_column ?: 'created_at';
            if ($month) {
                $sql = 'concat(year(' . $date_column . '), "-", if(month(' . $date_column . ') < 10, concat("0", month(' . $date_column . ')), month(' . $date_column . ')))';
                $query = $query
                    ->whereRaw($sql . ' >= ?', months('from'))
                    ->whereRaw($sql . ' < ?', date('Y-m', strtotime(months('to') . ' +1 month')));
            } else {
                $query = $query
                    ->where($date_column, '>=', dates('from'))
                    ->where($date_column, '<', date('Y-m-d', strtotime(dates('to') . ' +1 day')));
            }
        }
        return $query;
    }
}

function offices () {
    return has_permission('office.multiwork') ? business('office')->get() : collect([session('office')]);
}

function show_office_filter ($url) { if (has_permission('office.multiwork')) { ?>
  <form action="<?= url($url) ?>" style="display:inline-block;">
    <select class="btn filter" name="office_id" onchange="this.parentNode.submit();">
      <?php foreach (offices() as $office) { ?>
      <option value="<?= $office->id ?>" <?= $office->id == office_id() ? 'selected' : '' ?>><?= $office->name ?></option>
      <?php } ?>
    </select>
  </form>
<?php }}

function filtered ($obj, $month = false, $date_column = null) {
    return office($obj, null, null, true, $month, $date_column);
}

/*
 *  ITEMS
 */

function items () {
    return office()->items;
}

function products () {
    return items()->where('origin', 2)->map(function ($item) {
        return $item->_origin;
    });
}

/*
 *  NUMBERS
 */

function formatValue($value){
   return number_format($value,2);
}

function num2letras($num, $fem = false, $dec = true) { 
   $matuni[2]  = "dos"; 
   $matuni[3]  = "tres"; 
   $matuni[4]  = "cuatro"; 
   $matuni[5]  = "cinco"; 
   $matuni[6]  = "seis"; 
   $matuni[7]  = "siete"; 
   $matuni[8]  = "ocho"; 
   $matuni[9]  = "nueve"; 
   $matuni[10] = "diez"; 
   $matuni[11] = "once"; 
   $matuni[12] = "doce"; 
   $matuni[13] = "trece"; 
   $matuni[14] = "catorce"; 
   $matuni[15] = "quince"; 
   $matuni[16] = "dieciseis"; 
   $matuni[17] = "diecisiete"; 
   $matuni[18] = "dieciocho"; 
   $matuni[19] = "diecinueve"; 
   $matuni[20] = "veinte"; 
   $matunisub[2] = "dos"; 
   $matunisub[3] = "tres"; 
   $matunisub[4] = "cuatro"; 
   $matunisub[5] = "quin"; 
   $matunisub[6] = "seis"; 
   $matunisub[7] = "sete"; 
   $matunisub[8] = "ocho"; 
   $matunisub[9] = "nove"; 

   $matdec[2] = "veint"; 
   $matdec[3] = "treinta"; 
   $matdec[4] = "cuarenta"; 
   $matdec[5] = "cincuenta"; 
   $matdec[6] = "sesenta"; 
   $matdec[7] = "setenta"; 
   $matdec[8] = "ochenta"; 
   $matdec[9] = "noventa"; 
   $matsub[3]  = 'mill'; 
   $matsub[5]  = 'bill'; 
   $matsub[7]  = 'mill'; 
   $matsub[9]  = 'trill'; 
   $matsub[11] = 'mill'; 
   $matsub[13] = 'bill'; 
   $matsub[15] = 'mill'; 
   $matmil[4]  = 'millones'; 
   $matmil[6]  = 'billones'; 
   $matmil[7]  = 'de billones'; 
   $matmil[8]  = 'millones de billones'; 
   $matmil[10] = 'trillones'; 
   $matmil[11] = 'de trillones'; 
   $matmil[12] = 'millones de trillones'; 
   $matmil[13] = 'de trillones'; 
   $matmil[14] = 'billones de trillones'; 
   $matmil[15] = 'de billones de trillones'; 
   $matmil[16] = 'millones de billones de trillones'; 
   
   //Zi hack
   $float=explode('.',$num);
   $num=$float[0];

   $num = trim((string)@$num); 
   if ($num[0] == '-') { 
      $neg = 'menos '; 
      $num = substr($num, 1); 
   }else 
      $neg = ''; 

   /** nuevp  **/
   if($num[0] == '0' && strlen($num)==1){
        return 'Cero con '.$float[1].'/100';    
   }
   /** **/

   while ($num[0] == '0') $num = substr($num, 1); 
   if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num; 
   $zeros = true; 
   $punt = false; 
   $ent = ''; 
   $fra = ''; 
   for ($c = 0; $c < strlen($num); $c++) { 
      $n = $num[$c]; 
      if (! (strpos(".,'''", $n) === false)) { 
         if ($punt) break; 
         else{ 
            $punt = true; 
            continue; 
         } 

      }elseif (! (strpos('0123456789', $n) === false)) { 
         if ($punt) { 
            if ($n != '0') $zeros = false; 
            $fra .= $n; 
         }else 

            $ent .= $n; 
      }else 

         break; 

   } 
   $ent = '     ' . $ent; 
   if ($dec and $fra and ! $zeros) { 
      $fin = ' coma'; 
      for ($n = 0; $n < strlen($fra); $n++) { 
         if (($s = $fra[$n]) == '0') 
            $fin .= ' cero'; 
         elseif ($s == '1') 
            $fin .= $fem ? ' una' : ' un'; 
         else 
            $fin .= ' ' . $matuni[$s]; 
      } 
   }else 
      $fin = ''; 
   if ((int)$ent === 0) return 'Cero ' . $fin; 
   $tex = ''; 
   $sub = 0; 
   $mils = 0; 
   $neutro = false; 
   while ( ($num = substr($ent, -3)) != '   ') { 
      $ent = substr($ent, 0, -3); 
      if (++$sub < 3 and $fem) { 
         $matuni[1] = 'una'; 
         $subcent = 'as'; 
      }else{ 
         $matuni[1] = $neutro ? 'un' : 'uno'; 
         $subcent = 'os'; 
      } 
      $t = ''; 
      $n2 = substr($num, 1); 
      if ($n2 == '00') { 
      }elseif ($n2 < 21) 
         $t = ' ' . $matuni[(int)$n2]; 
      elseif ($n2 < 30) { 
         $n3 = $num[2]; 
         if ($n3 != 0) $t = 'i' . $matuni[$n3]; 
         $n2 = $num[1]; 
         $t = ' ' . $matdec[$n2] . $t; 
      }else{ 
         $n3 = $num[2]; 
         if ($n3 != 0) $t = ' y ' . $matuni[$n3]; 
         $n2 = $num[1]; 
         $t = ' ' . $matdec[$n2] . $t; 
      } 
      $n = $num[0]; 
      if ($n == 1) { 
         $t = ' ciento' . $t; 
      }elseif ($n == 5){ 
         $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t; 
      }elseif ($n != 0){ 
         $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t; 
      } 
      if ($sub == 1) { 
      }elseif (! isset($matsub[$sub])) { 
         if ($num == 1) { 
            $t = ' mil'; 
         }elseif ($num > 1){ 
            $t .= ' mil'; 
         } 
      }elseif ($num == 1) { 
         $t .= ' ' . $matsub[$sub] . '?n'; 
      }elseif ($num > 1){ 
         $t .= ' ' . $matsub[$sub] . 'ones'; 
      }   
      if ($num == '000') $mils ++; 
      elseif ($mils != 0) { 
         if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub]; 
         $mils = 0; 
      } 
      $neutro = true; 
      $tex = $t . $tex; 
   } 
   $tex = $neg . substr($tex, 1) . $fin; 
   //Zi hack --> return ucfirst($tex);
   if(!isset($float[1])) $float[1] = '00';
   $end_num=ucfirst($tex).' con '.$float[1].'/100';
   return $end_num; 
}


function format($number,$flag = false){
    return number_format($number,2,".","");
    #return format($number,2);
}

?>