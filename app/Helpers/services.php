<?php


    function search_person($type, $doc){

    	$type = strtolower($type);
    	if($type != 'ruc'  && $type != 'dni')
	    	return \Response::json([
	            'error' => 1	            
	        ]);

        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6InBlZHJvc2F1bmFAaG90bWFpbC5jb20ifQ.GrXOLI1stfw8UmRQ8025TtF_yk4cbW9WjER2QyRo48E";

        if (strtolower($type) == 'ruc')
            $url = "https://dniruc.apisperu.com/api/v1/ruc/".$doc."?token=".$token;
        else
            $url = "https://dniruc.apisperu.com/api/v1/dni/".$doc."?token=".$token;
        
        #print $url; die();
        $curl = curl_init($url);        
        curl_setopt($curl, CURLOPT_HEADER, false);                
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        #curl_setopt($curl, CURLOPT_POSTFIELDS, "token=".$token);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);           
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // esto para evitar enviarlo con SSL y pueda funcionar de localhost
        $result = json_decode(curl_exec($curl));
                
        if(curl_errno($curl)) 
            return ['codigo'=>1,'error'=>curl_error($curl)];
        curl_close($curl);
            
        // var_dump($result); die();

        $customer = [];
        $error = 1;
        if ($result) {
            $error = 0;
            if (strtolower($type) == 'ruc') {
                $customer = [
                    'id' => $result->ruc,                
                    'doc' => $result->ruc,
                    'customer_id' => 0,
                    'text' => $result->ruc.' __ '. strtoupper($result->razonSocial),
                    'name' => strtoupper($result->razonSocial),
                    'address' => $result->direccion,
                ];            
            }else{                            
                $customer = [
                    'id' => $result->dni,                
                    'doc' => $result->dni,
                    'customer_id' => 0,
                    'text' => $result->dni.' __ '. strtoupper($result->apellidoPaterno.' '.$result->apellidoMaterno.' '.$result->nombres),
                    'name' => strtoupper($result->apellidoPaterno.' '.$result->apellidoMaterno.' '.$result->nombres),
                    'address' => '',
                ];
            }   
        }

        return \Response::json([
            'error' => $error,
            'customer' => $customer
        ]);
    }

?>