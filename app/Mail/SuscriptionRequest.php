<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuscriptionRequest extends Mailable{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data){
        $this->data = $data;
    }

    public function build(){
        return $this->view('mail.suscription_request')->subject('Felicitaciones '.strtolower($this->data['representative']).' tienes un demo aprobado');
    }
}
