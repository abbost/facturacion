<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DocumentInfo extends Mailable{
    use Queueable, SerializesModels;
    public $pdf;
    public $xml;
    public $cdr;
    public $text;
    public $name;

    public function __construct($pdf,$xml,$cdr, $text,$name){
        $this->pdf = $pdf;
        $this->xml = $xml;
        $this->cdr = $cdr;
        $this->text = $text;
        $this->name = $name;
    }

    public function build(){
        $this->attachData($this->pdf, 'invoice.pdf',['mime'=>'application/pdf']);
        $this->attachData($this->xml, 'invoice.xml',['mime'=>'application/xml']);
        $this->attachData($this->cdr, 'cdr.xml',['mime'=>'application/xml']);
        $name = strtoupper($this->name);
        #$name = "ABBOST";
        return $this->view('mail.mailPdf')->from('admin@facturacionelectronicapyme.com',$name)->subject('COMPROBANTE ELECTRONICO');;
    }
}
