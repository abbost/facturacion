<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model{
    protected $table = 'user_permission';

    public function users () {
        return $this->hasMany('App\Models\User', 'user_id');
    }

    public function permission () {
        return $this->hasMany('App\Models\Permission', 'permission_id');
    }
}
