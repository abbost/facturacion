<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model{
    protected $table = 'expense';

    public function office () {
        return $this->belongsTo('App\Models\Office', 'office_id');
    }
    public function user () {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function is_deletable () {
        return \App\Models\CashTurn::where('created_at', '>', $this->created_at)->count() == 0;
    }
}
