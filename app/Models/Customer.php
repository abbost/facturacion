<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model{
    protected $table = 'customer';

    public function _origin () {
        return $this->hasOne(['', 'App\Models\CBusiness', 'App\Models\CPerson'][$this->origin], 'customer_id');
    }

    public function documents () {
        return $this->hasMany('App\Models\Document', 'customer_id');
    }

    public function is_deletable () {
        return $this->documents->count() == 0;
    }

    public function contact(){
        return $this->hasMany('App\Models\Contact', 'customer_id');
    }   
}
