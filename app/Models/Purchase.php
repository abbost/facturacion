<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model{
    protected $table = 'purchase';

    public function provider () {
        return $this->belongsTo('App\Models\Provider', 'provider_id');
    }

    public function user () {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function details () {
        return $this->hasMany('App\Models\PurchaseDetail', 'purchase_id');
    }

    public function offices () {
        $office_ids = $this->details->map(function ($detail) {return $detail->inputs->pluck('office_id');})->flatten()->toArray();
        return \App\Models\Office::whereIn('id', $office_ids)->get();
    }

    public function sendings () {
        return $this->hasMany('App\Models\Sending', 'purchase_id');
    }
    
    public function is_deletable () {
        $consumes = 0;
        foreach ($this->details ?: [] as $purchase_detail) {
            foreach ($purchase_detail->inputs ?: [] as $input) {
                $consumes += $input->outputs->count();
            }
        }
        return $this->status == 1 && $consumes == 0;
    }
    public function safe_delete () {
        $this->details->each(function ($detail) {
            $detail->safe_delete();
            $detail->status = 0;
            $detail->save();
        });
    }

}
