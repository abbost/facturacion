<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Output;

class Stock extends Model{
    protected $table = 'stock';

    public function inputs () {
        return $this->hasMany('App\Models\Input', 'stock_id');
    }
    public function reduce_stock ($quantity, $document_detail_id = null, $comments = '') {
        if ($quantity > $this->quantity) {
            return ['status' => 1, 'message' => 'El stock es insuficiente'];
        }
        $inputs = $this->inputs()->where('status', 1)->where('balance', '>', 0)->orderBy('id', 'asc')->get();
        $i = -1;
        $quantity_reduced = $quantity;
        while ($quantity > 0) {
            $input = $inputs->get(++$i);
            if ($input->balance > $quantity) {
                $output_quantity = $quantity;
                $input->balance = round($input->balance - $quantity, 2);
                $quantity = 0;
            } else {
                $output_quantity = $input->balance;
                $quantity = round($quantity - $input->balance, 2);
                $input->balance = 0;
            }
            $input->save();
            $output = new Output;
            $output->business_id = session('business')->id;
            $output->office_id = $input->office_id;
            $output->product_id = $input->product_id;
            $output->stock_id = $input->stock_id;
            $output->input_id = $input->id;
            $output->document_detail_id = $document_detail_id;
            $output->quantity = $output_quantity;
            $output->cost = round($output_quantity * $input->unit_cost, 2);
            $output->comments = $comments;
            $output->save();
        }
        $this->quantity -= $quantity_reduced;
        $this->save();
        return ['status' => 0, 'message' => "Stock reducido con éxito"];
    }
}
