<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfficeItem extends Model{
    protected $table = 'office_item';

    public function item () {
        return $this->belongsTo('App\Models\Item', 'item_id');
    }
}
