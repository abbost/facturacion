<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model{
    protected $table = 'service';

    public function is_deletable () {
        return true;
    }
}
