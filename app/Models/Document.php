<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ResumeDetail;

class Document extends Model{
    protected $table = 'document';

    public function details () {
        return $this->hasMany('App\Models\DocumentDetail','document_id');
    }

    // UN documento puede tener varios CDR's
    public function cdr () {
        return $this->hasOne('App\Models\SunatCDR','document_id')->orderBy('created_at','desc');
        // return $this->hasMany('App\Models\SunatCDR','document_id')->orderBy('created_at','desc')->first();
    }
    public function business () {
        return $this->belongsTo('App\Models\Business', 'business_id');
    }
    public function refered () {
        return $this->belongsTo('App\Models\Document', 'refered_document_id');
    }

    public function credit_note () {
        return $this->hasOne('App\Models\Document', 'refered_document_id');
    }


    public function has_resume_parents () {
        $id = $this->id;
        return ResumeDetail::where('detail_id', $id)->count() != 0;
    }

    /** 
    muchos a muchos de documento. Un documento tipo RC puede tener muchos detalles de tipo Documento
    Un documento puede pertenecer a muchos documentos de tipo RC. En el caso de que el RC sea rechazado y 
    se tenga que enviar de nuevo
    **/    
    public function resume_details () {
        return $this->belongsToMany('App\Models\Document', 'resume_detail', 'resume_id', 'detail_id');
    }

    public function resume_parents () {
        return $this->belongsToMany('App\Models\Document', 'resume_detail', 'detail_id', 'resume_id');
    }

    public function office () {
        return $this->belongsTo('App\Models\Office', 'office_id');
    }
    public function user () {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function sale () {
        return $this->hasOne('App\Models\Sale', 'document_id');
    }

    public function payments () {
        return $this->hasMany('App\Models\Payment', 'document_id');
    }
    public function payment () {
        return $this->hasOne('App\Models\Payment', 'document_id');
    }

    // Este metodo sirve solo cuando el documento se creo en el facturador. Los documentos que vienen
    // de xafiro no crean un registro en sale
    public function safe_delete () {
        $sale = $this->sale;
        if ($sale) {
            $sale->safe_delete();
            $sale->status = $this->status;
            $sale->save();
        }
    }

    // public function payment () {
    //     return $this->hasMany('App\Models\Payment','payment_id');
    // }

}
