<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmisorPending extends Model{
    protected $table = 'emisor_pending';
}
