<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CPerson extends Model{
    protected $table = 'cperson';

    public function customer () {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }
    public function is_deletable () {
        return $this->customer->is_deletable();
    }
}
