<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model{
    protected $table = 'permission';

    public function user_permision() {
        return $this->belongsTo('App\Models\UserPermission', 'permission_id');
    }
}
