<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Office extends Model{
    protected $table = 'office';

    public function users () {
        return $this->hasMany('App\Models\User', 'office_id');
    }
    public function cash_turns () {
        return $this->hasMany('App\Models\CashTurn', 'office_id');
    }
    public function last_document () {
        return $this->hasOne('App\Models\Document', 'office_id');
    }
    public function products () {
        return $this->belongsToMany('App\Models\Product', 'stock', 'office_id', 'product_id');
    }
    public function items () {
        return $this->belongsToMany('App\Models\Item', 'office_item', 'office_id', 'item_id')->where('office_item.status', 1);
    }
    public function is_deletable () {
        return
            $this->users->count() == 0 &&
            $this->cash_turns->count() <= 1 &&
            $this->products->count() == 0 &&
            $this->id != office_id() &&
            empty($this->last_document);
    }
}
