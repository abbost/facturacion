<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashTurn extends Model{
    protected $table = 'cash_turn';

    public function user () {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
