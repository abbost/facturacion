<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentSum extends Model{
    protected $table = 'document_sum';
    public $timestamps = false;
}
