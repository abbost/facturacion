<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model {
    protected $table = 'payment';

    public function document () {
        return $this->belongsTo('App\Models\Document', 'document_id');
    }
}
