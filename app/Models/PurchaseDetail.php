<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model{
    protected $table = 'purchase_detail';

    public function purchase (){
        return $this->belongsTo('App\Models\Purchase', 'purchase_id');
    }
    public function product () {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
    public function input ($office_id) {
        return @$this->inputs()->where('office_id', $office_id)->first();
    }
    public function inputs () {
        return $this->hasMany('App\Models\Input', 'purchase_detail_id');
    }
    public function safe_delete () {
        $this->inputs->each(function ($input) {
            $stock = $input->stock;
            $stock->quantity -= $input->quantity;
            $stock->save();
            $input->status = 0;
            $input->save();
        });
    }
    
}
