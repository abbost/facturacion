<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model{
    protected $table = 'sale';

    public function details () {
        return $this->hasMany('App\Models\SaleDetail', 'sale_id');
    }
    public function payment () {
        return $this->hasOne('App\Models\Payment', 'sale_id');
    }
    public function customer(){
        return $this->hasOne('App\Models\Customer', 'customer_id');   
    }
    public function safe_delete () {
        $this->details->each(function ($detail) {
            $detail->outputs->each(function ($output) {
                $output->safe_delete();
            });
        });
        // $this->payments->each(function ($payment) {
        //     $payment->status = 0;
        //     $payment->save();
        // });
        $this->payment->status = 0;
        $this->payment->save();

    }

    public function contact(){
        return $this->belongsTo('App\Models\Contact', 'contact_id');   
    }

}
