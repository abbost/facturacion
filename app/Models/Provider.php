<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model{
    protected $table = 'provider';

    public function purchases () {
        return $this->hasMany('App\Models\Purchase', 'provider_id');
    }

    public function is_deletable () {
        return $this->purchases->count() == 0;
    }
}
