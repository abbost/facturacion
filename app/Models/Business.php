<?php

namespace App\Models;

use App\Models\DocumentSum;
use Illuminate\Database\Eloquent\Model;

class Business extends Model{
    protected $table = 'business';

    public function offices () {
        return $this->hasMany('App\Models\Office', 'business_id');
    }

    public function documents () {
        return $this->hasMany('App\Models\Document','business_id');
    }

    public function items () {
        return $this->hasMany('App\Models\Item','business_id');
    }

    public function mailing($cur_date){        
        return $this->hasMany('App\Models\Mailing')->whereRaw("date(created_at) = '$cur_date'")->get();
    }

    public function document_sum_now () {
        [$month, $year] = explode('/', date('m/Y'));
        $document_sum = $this->hasOne('App\Models\DocumentSum', 'business_id')->where('month', $month)->where('year', $year)->get()->first();
        if (!$document_sum) {
            $document_sum = new DocumentSum;
            $document_sum->business_id = $this->id;
            $document_sum->month = $month;
            $document_sum->year = $year;
            $document_sum->quantity = 0;
            $document_sum->save();
        }
        return $document_sum;
    }
}
