<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model{
    protected $table = 'product';

    public function stocks () {
        return $this->hasMany('App\Models\Stock', 'product_id');
    }

    public function is_deletable () {
        return $this->hasMany('App\Models\Input', 'product_id')->first() == null;
    }

    public function stock ($office_id) {
        return $this->hasMany('App\Models\Stock', 'product_id')->where('office_id', $office_id)->first();
    }
}
