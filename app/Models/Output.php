<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Output extends Model{
    protected $table = 'output';

    public function office () {
        return $this->belongsTo('App\Models\Office', 'office_id');
    }
    public function product () {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
    public function sale_detail () {
        return $this->belongsTo('App\Models\SaleDetail', 'sale_detail_id');
    }
    public function input () {
        return $this->belongsTo('App\Models\Input', 'input_id');
    }
    public function stock () {
        return $this->belongsTo('App\Models\Stock', 'stock_id');
    }
    public function is_deletable () {
        return $this->sale_detail_id == null;
    }
    public function safe_delete () {
        $input = $this->input;
        $input->balance = round($input->balance + $this->quantity);
        $input->save();
        $stock = $this->stock;
        $stock->quantity = round($stock->quantity + $this->quantity);
        $stock->save();
        $this->delete();
    }
}
