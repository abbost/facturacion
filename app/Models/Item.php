<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model{
    protected $table = 'item';

    public $fillable = [
            'name',    
            'code', 
            'unit_price',
            'origin'
    ];

    public function _origin () {
        return @$this->belongsTo(@['', 'App\Models\Service', 'App\Models\Product'][$this->origin], 'origin_id');
    }
    public function is_deletable () {
        return $this->hasOne('App\Models\SaleDetail', 'item_id')->first() == null && $this->_origin->is_deletable();
    }
    public function office_item () {
        return $this->hasOne('App\Models\OfficeItem', 'item_id')->where('office_id', office_id());
    }
}
