<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResumeDetail extends Model{
    protected $table = 'resume_detail';
}
