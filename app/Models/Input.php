<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Input extends Model{
    protected $table = 'input';

    public function office () {
        return $this->belongsTo('App\Models\Office', 'office_id');
    }
    public function stock () {
        return $this->belongsTo('App\Models\Stock', 'stock_id');
    }
    public function outputs () {
        return $this->hasMany('App\Models\Output', 'input_id');
    }
}
