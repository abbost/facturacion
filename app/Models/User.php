<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable{
    use Notifiable;
    protected $table = 'user';

    public function business () {
        return $this->belongsTo('App\Models\Business', 'business_id');
    }

    public function office () {
        return $this->belongsTo('App\Models\Office', 'office_id');
    }

    public function permissions(){
        return $this->belongsToMany('App\Models\Permission', 'user_permission', 'user_id', 'permission_id')->withPivot('id');
    }

    public function has_permission ($permission = null) {
        return !$permission ?: ($this->permissions->where('title', $permission)->count() > 0);
    }

    public function missing_permissions () {
        return \App\Models\Permission::whereNotIn('id', $this->permissions->pluck('id')->toArray())->get();
    }

    public function add_permission ($permission_id) {
        $user_permission = new \App\Models\UserPermission;
        $user_permission->user_id = $this->id;
        $user_permission->permission_id = $permission_id;
        $user_permission->save();
    }

    public function delete_permission ($user_permission_id) {
        \App\Models\UserPermission::where('user_id', $this->id)->where('id', $user_permission_id)->delete();
    }
}
