<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guide extends Model{
    
    protected $table = 'guide';

    public function details () {
        return $this->hasMany('App\Models\GuideDetail','guide_id');
    }
   
   public function document (){
   		return $this->belongsTo('App\Models\Document','document_id');
   }
}
