<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleDetail extends Model{
    protected $table = 'sale_detail';

    public function sale () {
        return $this->belongsTo('App\Models\Sale', 'sale_id');
    }
    public function item() {
        return $this->hasOne('App\Models\Item', 'id', 'item_id');
    }
    public function outputs () {
        return $this->hasMany('App\Models\Output', 'sale_detail_id');
    }
}
