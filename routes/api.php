<?php

header("Access-Control-Allow-Origin: *");

$c = 'API\DocumentController';

Route::post('factura/create',                                                   "$c@factura_create");
Route::post('boleta/create',                                                    "$c@boleta_create");
Route::post('proforma/create',                                                  "$c@proforma_create");
Route::post('nota/create',                                                      "$c@nota_create");
Route::post('comunicacion_baja/create',                                         "$c@comunicacion_baja_create");
Route::post('resumen/create',                                                   "$c@resumen_create");

Route::post('document/resend',                                                  "$c@resend");
Route::get('document/list',                                                     "$c@get");
Route::get('document/details/load',                                             "$c@loadDetails");
Route::get('document/next_correlative/{business_id}/{doc_type}/{serie}',        "$c@getNextCorrelative");
Route::get('document/ticket/get',                                               "$c@getFromTicket");

Route::get('document/search',                                                   "$c@search");
Route::get('document/resend_batch/{business_id?}/{date?}',                      "$c@resendBatch");

Route::get('customer/search',                                                   'API\CustomerController@searchCustomer');
