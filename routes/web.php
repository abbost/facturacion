<?php

$crud = function ($c, $p) {
    Route::get('',                              ['uses' => "$c@index",                           'permission' => "$p.show"]);
    Route::get('show',                          ['uses' => "$c@show",                            'permission' => "$p.show"]);
    Route::get('create',                        ['uses' => "$c@create",                          'permission' => "$p.manage"]);
    Route::post('create',                       ['uses' => "$c@store",                           'permission' => "$p.manage"]);
    Route::get('edit',                          ['uses' => "$c@edit",                            'permission' => "$p.manage"]);
    Route::post('edit',                         ['uses' => "$c@update",                          'permission' => "$p.manage"]);
    Route::get('delete',                        ['uses' => "$c@cancel",                          'permission' => "$p.manage"]);
    Route::post('delete',                       ['uses' => "$c@delete",                          'permission' => "$p.manage"]);
};

#Publico
Auth::routes();
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('captcha',                           "Auth\LoginController@generate_captcha");

#Privado
#El auth se conecta directamente a la BD por eso se crear un middleware startsession llamado session para validar logeo de usuario

Route::group(['middleware' => 'session'], function () use ($crud) {

    // RUTAS GENERALES
    
    Route::group(['prefix' => 'document'], function () {
        $c = 'DocumentController';
        
        Route::get('create', "DocumentController@create");
        Route::post('create', "DocumentController@store");
        Route::get('{i}/edit', "DocumentController@edit");
        Route::post('{i}/edit', "DocumentController@update");

        Route::get('nota', "DocumentController@nota_create");
        Route::post('nota', "DocumentController@nota_store");
        
        Route::get('customer/search',           ['uses' => "CustomerController@searchCustomer",         'permission' => 'document.manage']);
        Route::get('customer/search/sunat',     ['uses' => "CustomerController@searchCustomerSunat",    'permission' => 'document.manage']);
        Route::get('details/load',              ['uses' => "$c@loadDetails",            'permission' => 'document.manage']);
        Route::post('item/create',              ['uses' => "$c@createItem",             'permission' => 'item.manage']);
        Route::get('/',                          ['uses' => "$c@index",                  'permission' => 'document.show']);    
        /*dudas aqui abajo*/
        Route::post('resend',                   ['uses' => "$c@resend",                 'permission' => 'document.manage']);
        Route::post('cancel',                   ['uses' => "$c@cancel",                 'permission' => 'document.manage']);
    });

    Route::get('summary',                   ['uses' => "SummaryController@index",       'permission' => 'document.show']);
    Route::get('summary/create',            ['uses' => "SummaryController@create",      'permission' => 'document.show']);
    Route::post('summary/resend',           ['uses' => "SummaryController@resend",      'permission' => 'document.manage']);

      
    Route::group(['prefix' => 'customer'], function () use ($crud) {                  
          $crud($c = 'CustomerController', $p = 'customer'); 
          Route::get('{id}/documents',   "CustomerController@get_documents");
    });
    Route::group(['prefix' => 'item'], function () use ($crud) {                        
        $crud($c = 'ItemController', $p = 'item');
        Route::get('get_json',                  ['uses' => "$c@get_json"]);
        Route::post('change_search_mode',        ['uses' => "$c@change_search_mode"]);
        Route::get('inputs',                    ['uses' => "$c@show_inputs",            'permission' => 'item.show']);
        Route::get('availability/change',       ['uses' => "$c@change_availability",    'permission' => 'item.manage']);
        Route::get('upload',                    ['uses' => "$c@show_upload",            'permission' => 'item.manage']);
        Route::get('product/export',            ['uses' => "$c@product_export_excel",   'permission' => 'item.manage']);
        Route::get('service/export',            ['uses' => "$c@service_export_excel",   'permission' => 'item.manage']);
        Route::post('product/import',           ['uses' => "$c@product_import_excel",   'permission' => 'item.manage']);
        Route::post('service/import',           ['uses' => "$c@service_import_excel",   'permission' => 'item.manage']);
    });

    // RUTAS PARA XAFIRO CLUB

    /*Route::group(['prefix' => 'promotion'], function () {
        $c = 'PromotionController';
        Route::get('list',                      "$c@showList");
        Route::post('create',                   "$c@create");
        Route::get('delete',                    "$c@delete");
        Route::get('exchange',                  "$c@showExchange");
        Route::get('available/get',             "$c@getAvailable");
        Route::post('use',                      "$c@use");
        Route::get('code/validate',             "$c@validateCode");
        Route::get('code/unable',               "$c@unableCode");
    });

    Route::get('club/list',                     "ClubTransactionController@index");*/
    Route::group(['prefix' => 'purchase'], function () use ($crud) {                    
        $crud($c = 'PurchaseController', $p = 'purchase'); });
    Route::group(['prefix' => 'expense'], function () use ($crud) {                     
        $crud($c = 'ExpenseController', $p = 'expense'); });
    Route::group(['prefix' => 'cash_turn'], function () use ($crud) {                   
        $crud($c = 'CashTurnController', $p = 'cash_turn'); });
    Route::group(['prefix' => 'output'], function () use ($crud) {                      
        $crud($c = 'OutputController', $p = 'output'); 
        Route::get('print/{id}', 'OutputController@print_');
    });


    Route::group(['prefix' => 'provider'], function () use ($crud) {                    
        $crud($c = 'ProviderController', $p = 'provider'); });
    Route::group(['prefix' => 'business'], function () use ($crud) {                    
        $crud($c = 'BusinessController', $p = 'business'); });
    Route::group(['prefix' => 'office'], function () use ($crud) {                      
        $crud($c = 'OfficeController', $p = 'office'); });
    Route::group(['prefix' => 'user'], function () use ($crud) {                        
        $crud($c = 'UserController', $p = 'user');
        Route::get('permission',                ['uses' => "$c@showPermission",         'permission' => 'permission.manage'] );
        Route::post('permission/add',           ['uses' => "$c@add_permissions",        'permission' => 'permission.manage']);
        Route::post('permission/delete',        ['uses' => "$c@delete_permission",      'permission' => 'permission.manage']);
        Route::get('notPermission',             ['uses' => "$c@notPermission",          ]);
    });

    Route::get('indicator',                     ['uses' => "IndicatorController@index", 'permission' => 'indicator.show']);
    Route::get('question',                     'QuestionController@index');

    /** servicios **/
    Route::get('ws_search_person/{type}/{doc}', function ($type, $doc) {
        return search_person($type,$doc);
    });

    Route::get('product/search', 'ItemController@search');
 

    /** Para logistica **/

    Route::get('contact/create',               'ContactController@create');
    Route::post('contact/create',               'ContactController@store');
    Route::get('contact/{i}/edit',               'ContactController@edit');
    Route::post('contact/{i}/edit',               'ContactController@update');
    Route::get('contact/search',               'ContactController@search');

    Route::get('guide/',            'GuideController@index');
    Route::get('guide/create/{document_id}',      'GuideController@create');
    Route::post('guide/create',     'GuideController@store');
    Route::get('guide/copy/{id}',     'GuideController@copy');
    Route::get('guide/test/{id}',     'GuideController@test');

    Route::get('guide/car',            'CarController@index');
    Route::get('guide/car/create',            'CarController@create');
    Route::post('guide/car/create',            'CarController@store');
    Route::get('guide/car/edit/{id}',            'CarController@edit');
    Route::post('guide/car/update',            'CarController@update');
    Route::post('guide/car/delete/{id}',            'CarController@delete');

    Route::get('guide/driver',                      'DriverController@index');
    Route::get('guide/driver/create',               'DriverController@create');
    Route::post('guide/driver/create',               'DriverController@store');
    Route::get('guide/driver/edit/{id}',            'DriverController@edit');
    Route::post('guide/driver/update',              'DriverController@update');
    Route::post('guide/driver/delete/{id}',         'DriverController@delete');
    
});


