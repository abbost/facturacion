
/** AGREGADO EL 24-01-2020 **/
create table contact(
  id int primary key auto_increment,
  customer_id int,
  name varchar(255),
  address varchar(255),
  created_at datetime,
  updated_at datetime
);

alter table document add column contact_name varchar(255);
alter table document add column contact_address varchar(255);
alter table document add column guide varchar(30);
alter table business add column has_contact tinyint(1) default 0;

alter table sale_detail drop foreign key `sale_detail_ibfk_2`;
alter table service add column status tinyint(1) default 1;
alter table product add column status tinyint(1) default 1;
alter table item add column status tinyint(1) default 1;

alter table business add column sector varchar(5) default 'com';

alter table customer add column status tinyint(1) default 1;
alter table cperson add column status tinyint(1) default 1;
alter table cbusiness add column status tinyint(1) default 1;

alter table sale add column contact_id int after env;


create table guide(
	id int PRIMARY KEY auto_increment,
	document_id int null,
	date date,
	place_from VARCHAR(50),
	place_to VARCHAR(50),
	quantity int,
	description VARCHAR(255),
	cost FLOAT(10,2),
	weight FLOAT(10,2),
	custom_name VARCHAR(100),
	custom_doc VARCHAR(100),
	car_brand VARCHAR(100),
	car_plate VARCHAR(10),
	car_certificate VARCHAR(100),
	driver_license VARCHAR(100),
	created_at datetime,
	updated_at datetime
);

create table guide_detail(
	id int PRIMARY KEY auto_increment,
	guide_id int,
	quantity int,
	description VARCHAR(255),
	cost FLOAT(10,2),
	weight FLOAT(10,2),	
	created_at datetime,
	updated_at datetime
);

create table car(
	id int PRIMARY KEY auto_increment,
	brand varchar(20),
	plate varchar(20),
	status tinyint(1) default 1,
	created_at DATETIME,
	updated_at DATETIME
)

create table driver(
	id int PRIMARY KEY auto_increment,
	name varchar(50),
	license varchar(20),
	status tinyint(1) default 1,
	created_at DATETIME,
	updated_at DATETIME
)

alter table output add column comments text after cost;
alter table business add column has_guide tinyint(1) default 0;


-- 05-10-20

alter table document add column contact_id int after contact_address;

update document 
join sale on document.sale_id = sale.id
set document.contact_id = sale.contact_id
where sale.contact_id is not null;


alter table output add column document_detail_id int after sale_detail_id;

update `output` _output
join sale_detail on _output.sale_detail_id = sale_detail.id
set _output.document_detail_id = sale_detail.document_detail_id;


 alter table item add column code varchar(20) after name;



update document doc1
join document doc2 on doc1.refered_document_id = doc2.id
set doc1.customer_id = doc2.customer_id
where doc1.doc_type = '07'


# 12 -11 2020

alter table business add column ticket_size varchar(5) default 'b7';

# 26 -11 -2020

CREATE TABLE mailing(
	id int auto_increment primary key,
	document_id int null,
	business_id int,
	email varchar(100),
	created_at datetime
);