('document.show',       'ver documentos'),                  ('document.manage',     'gestionar documentos'),
('invoice.show',        'emitir documentos'), 
('customer.show',       'ver clientes'),                    ('customer.manage',     'gestionar clientes'),
('item.show',           'ver items'),                       ('item.manage',         'gestionar items'),
('purchase.show',       'ver compras'),                     ('purchase.manage',     'gestionar compras'),
('expense.show',        'ver gastos'),                      ('expense.manage',      'gestionar gastos'),
('cash_turn.show',      'ver cierres de caja'),             ('cash_turn.manage',    'gestionar cierres de caja'),
('output.show',         'ver consumos'),                    ('output.manage',       'gestionar consumos'),
('provider.show',       'ver proveedores'),                 ('provider.manage',     'gestionar proveedores'),
('business.show',       'ver datos de emisor'),             ('business.manage',     'gestionar datos de emisor'),
('office.show',       'ver sucursales'),                  ('office.manage',     'gestionar sucursales'),
('user.show',           'ver usuarios'),                    ('user.manage',         'gestionar usuarios'),
('permission.manage',   'gestionar permisos de usuario'),
('indicator.show',      'ver indicadores'),
('office.multiwork',    'trabajar con multiples sucursales en el sistema');

insert into user_permission (user_id, permission_id) select u.id, p.id from user u join permission p order by u.id, p.id;